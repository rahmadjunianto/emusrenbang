@extends('musrenbang.layout')

@section('content')

@if(is_null($usulan->USULAN_ID) and is_null($usulan->USULAN_URGENSI) and is_null($rt) and is_null($usulan->USULAN_LAT) and is_null($usulan->USULAN_LONG) and is_null($usulan->ALAMAT))

@else 


<div id="content" class="app-content" role="main">
  <div class="hbox hbox-auto-xs hbox-auto-sm ng-scope">
    <div class="col">
      <div class="app-content-body ">

          <div class="bg-light lter">
              <ul class="breadcrumb bg-white m-b-none">
                <li><a href="#" class="btn no-shadow" ui-toggle-class="app-aside-folded" target=".app">
                 <i class="icon-bdg_expand1 text"></i>
                  <i class="icon-bdg_expand2 text-active"></i>
                </a>   </li>
                <li><a href= "{{ url('/') }}/musrenbang/2017">Dashboard</a></li>
                <li><i class="fa fa-angle-right"></i>Usulan</li>
                <li class="active"><i class="fa fa-angle-right"></i>Ubah Usulan</li>
              </ul>
          </div>

          <div class="wrapper-lg bg-dark-grey">
            <div class="row">
              <div class="col-md-12">

                <div class="panel bg-white">

                  <div class="panel-heading wrapper-lg">
                    <h5 class="inline font-semibold text-orange m-n ">Ubah Usulan</h5>
                  </div>

                  <div class="tab-content tab-content-alt-1 bg-white">
                        <div class="bg-white wrapper-lg input-jurnal">
                            <form action="{{url('/')}}/musrenbang/{{ $tahun }}/usulan/edit/submit" method="post" class="form-horizontal" enctype="multipart/form-data">

                              <input type="hidden" name="usulan_id" value="{{$usulan->USULAN_ID}}">
                              <input type="hidden" name="usulan_tujuan" value="{{$usulan->USULAN_TUJUAN}}">

                              <div class="input-wrapper"> 
                                  <div class="form-group">
                                    <label class="col-sm-2">Isu Usulan</label>
                                    <div class="col-sm-10">
                                      <input type="text" class="form-control" name="isu" readonly="" value="{{ $usulan->kamus->isu->ISU_NAMA }}">
                                      <input type="hidden" class="form-control" id="isu-f" name="isu" readonly="" value="{{ $usulan->kamus->ISU_ID }}">
                                    </div>
                                   
                                  </div>

                                  <!-- kamus usulan -->
                                  <div class="form-group">
                                    <label class="col-sm-2">Kamus Usulan</label>
                                    <div class="col-sm-10">
                                      <input type="text" class="form-control" name="kamus" readonly="" value="{{ $usulan->kamus->KAMUS_NAMA }}">
                                    </div>
                                  </div>

                                  <!-- kriteria -->
                                  <div class="form-group" >
                                    <label class="col-sm-2">Kriteria </label>
                                    <div class="col-sm-10">
                                        <textarea width="20" name="kriteria" id="kriteria" class="form-control" readonly="">{{ $usulan->kamus->KAMUS_KRITERIA }}</textarea>
                                    </div>
                                  </div>

                                   <div class="form-group">
                                    <label class="col-sm-2">Volume</label>
                                    <div class="col-sm-6">
                                      <input type="text" id="nominal1" class="form-control harga-free-input" onkeyup="SetNumber('nominal1')" onmouseout="SetNumber('nominal1')" onkeypress="return event.charCode >= 48 && event.charCode <= 57" placeholder="Masukan Nilai Volume pertama dalam Angka (Panjang)" name="vol" required="" value="{{ $usulan->USULAN_VOLUME }}">
                                    </div>
                                    <div class="col-sm-4">
                                      <input type="text" class="form-control" name="satuan" readonly="" placeholder="Satuan" id="satuan" value="{{ $usulan->kamus->KAMUS_SATUAN }}">
                                    </div>
                                  </div>

                                  <div class="form-group">
                                    <label class="col-sm-2"></label>
                                    <div class="col-sm-6">
                                      <input type="text" id="nominal1" class="form-control harga-free-input" onkeyup="SetNumber('nominal1')" onmouseout="SetNumber('nominal1')" onkeypress="return event.charCode >= 48 && event.charCode <= 57" placeholder="Masukan Nilai Volume kedua dalam Angka (lebar)" name="vol1" required="" value="{{ $usulan->USULAN_VOLUME1 }}">
                                    </div>
                                    <div class="col-sm-4">
                                      <input type="text" class="form-control" name="satuan" readonly="" placeholder="Satuan" id="satuan" value="{{ $usulan->kamus->KAMUS_SATUAN }}">
                                    </div>
                                  </div>

                                  <div class="form-group">
                                    <label class="col-sm-2"></label>
                                    <div class="col-sm-6">
                                      <input type="text" id="nominal1" class="form-control harga-free-input" onkeyup="SetNumber('nominal1')" onmouseout="SetNumber('nominal1')" onkeypress="return event.charCode >= 48 && event.charCode <= 57" placeholder="Masukan Nilai Volume ketiga dalam Angka (tinggi)" name="vol2" required="" value="{{ $usulan->USULAN_VOLUME2 }}">
                                    </div>
                                    <div class="col-sm-4">
                                      <input type="text" class="form-control" name="satuan" readonly="" placeholder="Satuan" id="satuan" value="{{ $usulan->kamus->KAMUS_SATUAN }}">
                                    </div>
                                  </div>

                                  <div class="form-group" id="div-upload-gambar">
                                    <label class="col-sm-2">Upload Gambar</label>
                                    <div class="col-sm-8">
                                      <input ui-jq="filestyle" name="image1" type="file" data-icon="true" data-classbutton="btn btn-default" data-classinput="form-control inline v-middle input-s" id="filestyle-0" tabindex="-1" style="position: absolute; clip: rect(10px 10px 10px 10px);">
                                    </div>
                                    <div class="col-sm-1">
                                      <button class="btn btn-info" id="upload" type="button"><i class="fa fa-plus"></i></button>
                                    </div>
                                  </div>
                                 

                                  <div class="form-group">
                                    <label class="col-sm-2">Masukan @if(Auth::user()->level == 5) RW @elseif(Auth::user()->level == 1) RT @endif Penerima Manfaat</label>
                                    <div class="col-sm-8" style="margin-top:10px">
                                      @foreach ($rt as $rt)
                                        <input type="checkbox" class="" value="{{$rt->RT_ID}}" style="margin-left:10px;" name="rt[]"> {{$rt->RT_NAMA}}
                                      @endforeach
                                      </div>
                                  </div>

                                  <div class="form-group">
                                    <label class="col-sm-2"> </label>
                                    <div class="col-sm-10">
                                      <input type="hidden" class="form-control" name="kec" readonly="" value="{{ $rt->rw->kelurahan->kecamatan->KEC_NAMA }}">
                                      <input type="hidden" class="form-control" name="kel" readonly="" placeholder="Kelurahan" value="{{ $rt->rw->kelurahan->KEL_NAMA }}">
                                      <input type="hidden" class="form-control" name="rw" readonly="" placeholder="RW" value="RW {{ $rt->rw->RW_NAMA }}">
                                    </div>
                                  </div>


                                  <div class="form-group">
                                    <label class="col-sm-2">Urgensi Kegiatan</label>
                                    <div class="col-sm-10">
                                        <input ui-jq="filestyle" name="urgensi" type="text" data-icon="true" data-classbutton="btn btn-default" data-classinput="form-control inline v-middle input-s" id="filestyle-1" tabindex="-1" style="position: absolute; clip: rect(10px 10px 10px 10px);">
                                      <input type="text" class="form-control" name="urgensi" placeholder="Isi alasan atau urgensi kegiatan" required="" value="{{$usulan->USULAN_URGENSI}}">
                                    </div>
                                  </div>

                                   <div class="form-group">
                                    <label class="col-sm-2">Alamat Lokasi </label>
                                    <div class="col-sm-10">
                                       <textarea width="20" name="alamat" class="form-control" placeholder="Masukan Detail Alamat (untuk memudahkan survei lokasi kegiatan)">{{$usulan->ALAMAT}}</textarea>
                                    </div>
                                  </div>
                                  

<hr><br>
                                  <div class="form-group">
                                    <div class="col-md-12">
                                      <div class="col-sm-12" style="margin-top: -25px;">
                                        <a href="{{ url('/') }}/musrenbang/{{ $tahun }}/usulan-{{$usulan->USULAN_TUJUAN}}" class="btn input-xl btn-info"><i class="fa fa-reply m-r-xs"></i>Batal</a>
                                          <button type="submit" class="btn btn-success pull-right" data-dismiss="modal" value="edit" name="val">Simpan</button>  
                                       
                                      </div>
                                    </div>
                                  </div>
                              </div>
                              <input type="hidden" name="log_id" id="log_id" value="{{ $log_id }}">
                              <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                            </form>
                          </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
      </div>
    </div>
    </div>
</div>
<div id="form-tolak" class="modal fade" role="dialog">
<form method="post" action="{{ url('/') }}/musrenbang/{{ $tahun }}/usulan/tolak">
{{ csrf_field() }}
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Tolak Usulan</h4>
      </div>
      <div class="modal-body">
      <div class="form-group">
          <label>Alasan : </label>
          <input type="hidden" name="usulan_id" value="{{$usulan->USULAN_ID}}">
          <input type="hidden" name="log_id" id="log_id" value="{{ $log_id }}">
          <textarea class="form-control" rows="6" placeholder="Isi Alasan" name="alasan" required></textarea>
      </div>
      </div>
      <div class="modal-footer">
        
        @if(Auth::user()->app == 1 &&  Auth::user()->level == 5)
          <button type="submit" class="btn btn-danger">Tolak</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        @endif
      </div>
    </div>
  </div>
</form>
</div>
@endsection


@section('plugin')

<script>
    $(document).ready(function(){

        $("#upload").click(function(){
            $("#div1").fadeIn("fast");
            $("#upload").addClass("addGambar");
        });

        $("#isu").change(function(e, params){
            var id  = $('#isu').val();
           $('#kamus').find('option').remove().end().append('<option>Kamus Usulan</option>');
            $.ajax({
              type  : "get",
              url   : "{{ url('/') }}/musrenbang/{{ $tahun }}/usulan/data/kamus/"+id,
              success : function (data) {
                $('#kamus').append(data['opt']).trigger('chosen:updated');
                if(data['tipe'] == 1 || data['tipe'] == 2) $('#div-upload-gambar').fadeIn('fast').removeClass('hide');
                else $('#div-upload-gambar').fadeOut('fast').addClass('hide');
              }
            });
        });

        $("#kamus").change(function(e, params){
          // alert('kamus');
            var id  = $('#kamus').val();
            $('#satuan').val();
            $.ajax({
              type  : "get",
              url   : "{{ url('/') }}/musrenbang/{{ $tahun }}/usulan/data/satuan/"+id,
              success : function (data) {
                $('#satuan').val(data);
              }
            });
        });
        $("#isu").change();
    });
</script> 


    <!--Start of Zendesk Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="https://v2.zopim.com/?4XsJAVMVHAuWk8KVNd6RcwgTq6up8MOA";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zendesk Chat Script-->


@endif


@endsection

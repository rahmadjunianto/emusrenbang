@extends('musrenbang.layout')

@section('content')
<div id="content" class="app-content" role="main">
  <div class="hbox hbox-auto-xs hbox-auto-sm ng-scope">
    <div class="col">
      <div class="app-content-body ">

          <div class="bg-light lter">    
              <ul class="breadcrumb bg-white m-b-none">
                <li><a href="#" class="btn no-shadow" ui-toggle-class="app-aside-folded" target=".app">
                  <i class="icon-bdg_expand1 text"></i>
                  <i class="icon-bdg_expand2 text-active"></i>
                </a>   </li>
                <li><a href= "{{ url('/') }}/main">Dashboard</a></li>
                <li><i class="fa fa-angle-right"></i>Detail Renja SKPD </li>
                <li class="active"><i class="fa fa-angle-right"></i>{{$skpd->SKPD_NAMA}}</li>                                
              </ul>
          </div>
          <div class="wrapper-lg">
            <div class="row">
              <div class="col-md-12">
                <div class="panel bg-white">
                  <div class="wrapper-lg">
                    <div class="dropdown col-sm-2 pull-right m-t-n-sm">
                      <button class="btn btn-danger dropdown-toggle" type="button" data-toggle="dropdown">Export
                      <span class="caret"></span></button>
                      <ul class="dropdown-menu">
                        <li class="dropdown-header">Export Per SKPD</li>
                        <li><a href="{{ url('/') }}/musrenbang/{{$tahun}}/public/usulan/skpd/export/{{$id}}">Semua</a></li>
                        <li><a href="{{ url('/') }}/musrenbang/{{$tahun}}/public/usulan/skpd/export/{{$id}}/2">Ditolak</a></li>
                        <li><a href="{{ url('/') }}/musrenbang/{{$tahun}}/public/usulan/skpd/export/{{$id}}/1">Dalam Proses Dan Diterima</a></li>
                        <li class="dropdown-header">Export Per Kecamatan</li>
                        <li><a href="{{ url('/') }}/musrenbang/{{$tahun}}/public/usulan/skpdperkecamatan/export/{{$id}}">Semua</a></li>
                        <li><a href="{{ url('/') }}/musrenbang/{{$tahun}}/public/usulan/skpdperkecamatan/export/{{$id}}/2">Tolak</a></li>
                        <li><a href="{{ url('/') }}/musrenbang/{{$tahun}}/public/usulan/skpdperkecamatan/export/{{$id}}/1">Dalam Proses Dan Diterima</a></li>
                      </ul>
                    </div>
                    <!-- <div class="col-sm-2 pull-right m-t-n-sm">
                      <a href="{{ url('/') }}/musrenbang/{{$tahun}}/public/usulan/skpd/export/{{$id}}"><button class="btn btn-danger">Exports</button></a>
                    </div>
                     <div class="col-sm-2 pull-right m-t-n-sm">
                      <a href="{{ url('/') }}/musrenbang/{{$tahun}}/public/usulan/skpdperkecamatan/export/{{$id}}"><button class="btn btn-danger">Export / Kecamatan</button></a>
                    </div> -->

                    <div class="col-sm-10">
                       <div class="nav-tabs-alt tabs-alt-1 b-t three-row" id="tab-jurnal" >
                          <ul class="nav nav-tabs" role="tablist">
                             <li class="active">
                              <a data-target="#tab-1" role="tab" data-toggle="tab" data-status="1" class="buttonstatus">[{{$diproses}}] Di Proses <i class="fa fa-refresh text-info"></i></a>
                             </li>
                             <li>
                              <a data-target="#tab-2" role="tab" data-toggle="tab" data-status="2" class="buttonstatus">[{{$diterima}}] Di Terima <i class="fa fa-check text-success"></i></a>
                             </li>
                             <li>
                              <a data-target="#tab-3" role="tab" data-toggle="tab" data-status="0" class="buttonstatus">[{{$ditolak}}] DI Tolak <i class="fa fa-close text-danger"></i></a>
                             </li>
                          </ul>
                        </div> 
                      </div> 
                    <div class="col-sm-2 pull-right m-t-n-sm">
                      <select class="form-control dtSelect" id="dtSelect">
                            <option value="10">10</option>
                            <option value="25">25</option>
                            <option value="50">50</option>
                            <option value="100">100</option>
                        </select>
                    </div>
                    <h5 class="font-semibold text-orange m-n ">Detail SKPD</h5>
                  </div>           
                  <div class="tab-content tab-content-alt-1 bg-white">
                        <div role="tabpanel" class="active tab-pane" id="tab-1">  
                            <div class="table-responsive dataTables_wrapper">
                             <table ui-jq="dataTable" ui-options="{
                                      sAjaxSource: '{{ url('/') }}/musrenbang/{{$tahun}}/grafikRenja/detail/getRenjaSkpd/{{$id}}/1',
                                      aoColumns: [
                                       { mData: 'NO' },
                                       { mData: 'PENGUSUL' },
                                       { mData: 'ISUKAMUS' },
                                       { mData: 'URGENSI' },
                                       { mData: 'LOKASI' },
                                       { mData: 'ALAMAT' },
                                       { mData: 'VOLUME' },
                                       { mData: 'HARGA' },
                                       { mData: 'NOMINAL' },
                                       { mData: 'STATUS' },
                                      ],
                                    }" class="table table-striped b-t b-b" id="table-usulan">
                                    <thead>
                                      <tr>
                                        <th>No</th>
                                        <th>Pengusul</th>
                                        <th>Isu/Kamus</th>
                                        <th>Urgensi</th>
                                        <th>Lokasi</th>
                                        <th>Alamat / Keterangan</th>
                                        <th>Volume</th>
                                        <th>Harga / Satuan</th>
                                        <th>Nominal Usulan</th>
                                        <th>Status</th>
                                      </tr>
                                      <tr>
                                        <th colspan="10" class="th_search">
                                            <i class="icon-bdg_search"></i>
                                            <input type="search" class="table-search form-control b-none w-full" placeholder="Cari Usulan" aria-controls="DataTables_Table_0">
                                        </th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                  </table>
                              </div>
                          </div>
                  </div>
                </div>                
              </div>
            </div>
          </div>
      </div>
      <!-- App-content-body -->  
    </div>
    <!-- .col -->
</div>

@endsection

@section('plugin')

<script type="text/javascript">
$(document).ready(function() {
    $(".buttonstatus").on("click",function(event) {

      status = $(this).data('status');
      
      resetTable();
    });
    var table = $('#example').DataTable();

    $("#example tfoot th").each( function ( i ) {
        var select = $('<select><option value=""></option></select>')
            .appendTo( $(this).empty() )
            .on( 'change', function () {
                table.column( i )
                    .search( $(this).val() )
                    .draw();
            } );

        table.column( i ).data().unique().sort().each( function ( d, j ) {
            select.append( '<option value="'+d+'">'+d+'</option>' )
        } );
    } );
} );
</script>

<script type="text/javascript">
  function resetTable(){
   // var kec  = $('#kecamatan').val();
   // var kel  = $('#kelurahan').val();
   
    $('#table-usulan').DataTable().destroy();
   
    $('#table-usulan').DataTable({
     sAjaxSource: '{{ url('/') }}/musrenbang/{{$tahun}}/grafikRenja/detail/getRenjaSkpd/{{$id}}/'+status,
     aoColumns: [
     { mData: 'NO' },
     { mData: 'PENGUSUL' },
     { mData: 'ISUKAMUS' },
     { mData: 'URGENSI' },
     { mData: 'LOKASI' },
     { mData: 'ALAMAT' },
     { mData: 'VOLUME' },
     { mData: 'HARGA' },
     { mData: 'NOMINAL' },
     { mData: 'STATUS' },

   ]});
  }
  function hapus(id){
    var token        = $('#token').val();
    $.confirm({
        title: 'Hapus Data!',
        content: 'Yakin hapus data?',
        buttons: {
            Ya: {
                btnClass: 'btn-danger',
                action: function(){
                  $.ajax({
                      url: "{{ url('/') }}/musrenbang/{{ $tahun }}/usulan/delete",
                      type: "POST",
                      data: {'_token'         : token,
                            'id'              : id},
                      success: function(msg){
                          $.alert(msg);
                          $('.table').DataTable().ajax.reload();
                        }
                  });
                }
            },
            Tidak: function () {
            }
        }
    });
  }
</script>

<script type="text/javascript">
$(document).ready(function(){
  
  
});
</script>
@endsection



@extends('musrenbang.layout')

@section('content')
<style media="screen">
#pac-input {
background-color: #fff;
margin-top: 10px;
font-family: Roboto;
font-size: 15px;
font-weight: 300;
margin-left: 12px;
padding: 0 11px 0 13px;
text-overflow: ellipsis;
width: 300px;
}
</style>
<div id="content" class="app-content" role="main">
  <div class="hbox hbox-auto-xs hbox-auto-sm ng-scope">
    <div class="col">
      <div class="app-content-body ">

          <div class="bg-light lter">
              <ul class="breadcrumb bg-white m-b-none">
                <li><a href="#" class="btn no-shadow" ui-toggle-class="app-aside-folded" target=".app">
                  <i class="icon-bdg_expand1 text"></i> 
                  <i class="icon-bdg_expand2 text-active"></i>
                </a>   </li>
                <li><a href= "{{ url('/') }}/musrenbang/2017">Dashboard</a></li>
                <li><i class="fa fa-angle-right"></i>Usulan</li>
                <li class="active"><i class="fa fa-angle-right"></i>Tambah Kamus Usulan</li>
              </ul>
          </div>

          <div class="wrapper-lg bg-dark-grey">
            <div class="row">
              <div class="col-md-12">
                <div class="nav-tabs-alt tabs-alt-1 b-t three-row" id="tab-jurnal" >
                          <ul class="nav nav-tabs" role="tablist">
                             <li class="active">
                              <a data-target="#tab-1" role="tab" data-toggle="tab" data-status="1" class="buttonstatus"> Input Isu Baru </a>
                             </li>
                             <li>
                              <a data-target="#tab-2" role="tab" data-toggle="tab" data-status="2" class="buttonstatus">Input Kamus Baru </a>
                             </li>
                             <li>
                             <a data-target="#tab-3" role="tab" data-toggle="tab" data-status="3" class="buttonstatus">_</a>
                             </li>
                          </ul>
                
                  <div class="tab-content tab-content-alt-1 bg-white">
                        <div class="bg-white wrapper-lg input-jurnal">
                          @if (count($errors) > 0)
                              <div class="alert alert-danger">
                                  <ul>
                                      @foreach ($errors->all() as $error)
                                          <li>{{ $error }}</li>
                                      @endforeach
                                  </ul>
                              </div>
                          @endif
                            <form action="{{url('/')}}/musrenbang/{{$tahun}}/usulan/tambah/submit" method="post" class="form-horizontal" enctype="multipart/form-data">
                              <div class="input-wrapper">

                                  <div class="form-group">
                                    <label class="col-sm-2">Isu Usulan</label>
                                    <div class="col-sm-10">
                                      <select ui-jq="chosen" class="w-full isu form-control" name="isu" id="isu" required>
                                            <option value="" style="color:black">Pilih Isu</option>
                                        @foreach ($isu as $isu)
                                            <option value="{{$isu->ISU_ID}}" style="color:black">{{$isu->ISU_NAMA}}</option>
                                        @endforeach
                                      </select>
                                    </div>
                                  </div>

                                  <div class="form-group">
                                    <label class="col-sm-2">Kamus Usulan </label>
                                    <div class="col-sm-10">
                                      <select ui-jq="chosen" class="w-full form-control" name="kamus" id="kamus" required>
                                            <option value="" style="color:black">Pilih Kamus</option>
                                      </select>
                                    </div>
                                  </div>


                                  <div class="form-group" style="display: none" id="kriteria">
                                    <label class="col-sm-2">Kriteria Usulan</label>
                                    <div class="col-sm-10">
                                       <textarea width="20" name="kriteria" class="form-control" placeholder=""></textarea>
                                    </div>
                                  </div>

                                  

                                  <div class="form-group">
                                    <label class="col-sm-2">Volume</label>
                                    <div class="col-sm-6">
                                      <input type="number" class="form-control" name="vol" placeholder="Masukan Nilai Volume dalam Angka" required="">
                                    </div>
                                    <div class="col-sm-4">
                                      <input type="text" class="form-control" name="satuan" readonly="" placeholder="Satuan" id="satuan">
                                    </div>
                                  </div>

                                 
                                   <div class="form-group">
                                    <label class="col-sm-2">Urgensi Kegiatan / Keterangan</label>
                                    <div class="col-sm-10">
                                       <textarea width="20" name="urgensi" class="form-control" placeholder="Masukan Alasan kenapa kegiatan ini diperlukan dan keterangan lainya."></textarea>
                                    </div>
                                  </div>

                                  <div class="form-group">
                                    <label class="col-sm-2">Alamat Lokasi </label>
                                    <div class="col-sm-10">
                                       <textarea width="20" name="alamat" class="form-control" placeholder="Masukan Detail Alamat (untuk memudahkan survei lokasi kegiatan)"></textarea>
                                    </div>
                                  </div>


                                

                                  <hr class="m-t-xl">
                                  <div class="form-group">
                                      
                                      <div class="col-md-4"></div>
                                      
                                      <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">

                                    <div class="col-md-8">
                                      <a href="{{ url('/') }}/musrenbang/{{ $tahun }}" class="btn input-xl btn-danger pull-right"><i class="fa fa-close m-r-xs"></i>Batal</a>
                                      <button class="btn input-xl btn-success pull-right m-r-xs" type="submit"><i class="fa fa-check m-r-xs"></i>Simpan</button>
                                    </div>
                                  </div>
                              </div>
                            </form>
                          </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
      </div>
    </div>
    </div>
</div>
@endsection





@section('plugin')

<script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBzixLw-QwjOpEiL8qmveEmafmgM27mgnk&libraries=places&callback=initMap">
</script>

<script>
    $(document).ready(function(){

        $("#upload").click(function(){
            $("#div1").fadeIn("fast");
            $("#upload").addClass("addGambar");
        });

        $("#isu").change(function(e, params){
            var id  = $('#isu').val();
           $('#kamus').find('option').remove().end().append('<option>Kamus Usulan</option>');
            $.ajax({
              type  : "get",
              url   : "{{ url('/') }}/musrenbang/{{ $tahun }}/usulan/data/kamus/"+id,
              success : function (data) {
                $('#kamus').append(data['opt']).trigger('chosen:updated');
                if(data['tipe'] == 1 || data['tipe'] == 2) $('#div-upload-gambar').fadeIn('fast').removeClass('hide');
                else $('#div-upload-gambar').fadeOut('fast').addClass('hide');
              }
            });
        });

        $("#kamus").change(function(e, params){
          // alert('kamus');
            var id  = $('#kamus').val();
            $('#satuan').val();
            $.ajax({
              type  : "get",
              url   : "{{ url('/') }}/musrenbang/{{ $tahun }}/usulan/data/satuan/"+id,
              success : function (data) {
                $('#satuan').val(data);
              }
            });
        });
        $("#isu").change();
    });
</script>


<!--Start of Zendesk Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="https://v2.zopim.com/?4XsJAVMVHAuWk8KVNd6RcwgTq6up8MOA";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zendesk Chat Script-->

@endsection

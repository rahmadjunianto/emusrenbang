@extends('musrenbang.layout')

@section('content')
<div id="content" class="app-content" role="main">
  <div class="hbox hbox-auto-xs hbox-auto-sm ng-scope">
    <div class="col">
      <div class="app-content-body ">

          <div class="bg-light lter">    
              <ul class="breadcrumb bg-white m-b-none">
                <li><a href="#" class="btn no-shadow" ui-toggle-class="app-aside-folded" target=".app">
                  <i class="icon-bdg_expand1 text"></i>
                  <i class="icon-bdg_expand2 text-active"></i>
                </a>   </li>
                <li><a href= "{{ url('/') }}/musrenbang/2017">Dashboard</a></li>
                <li><i class="fa fa-angle-right"></i>Usulan</li>   
                <li class="active"><i class="fa fa-angle-right"></i>Perpengusul</li>  
                <li class="active"><i class="fa fa-angle-right"></i>RW</li>                                
              </ul>
          </div>
          <div class="wrapper-lg">
            <div class="row">
              <div class="col-sm-12">
                <div class="panel bg-white wrapper-lg" style="height: 165px;">
                  <div class="form-group col-sm-12">
                      <label class="col-sm-2">RW</label>
                      <div class="col-sm-4">
                        <select ui-jq="chosen" class="filter w-full isu form-control" name="rw" id="rw">                                      
                          <option value="x" style="color:black">RW</option>
                          @foreach($rw as $r)
                           <option value="{{$r->RW_ID}}" style="color:black">{{$r->RW_NAMA}}</option>
                          @endforeach
                        </select>
                      </div>
                      <label class="col-sm-2">Tipe</label>
                      <div class="col-sm-4">
                        <select ui-jq="chosen" class="filter w-full isu form-control" name="tipe" id="tipe">                                      
                          <option value="x" style="color:black">Tipe</option>
                          <option value="1" style="color:black">Renja</option>
                          <option value="2" style="color:black">PIPPK</option>
                        </select>
                      </div>
                  </div>                  
                  <div class="form-group col-sm-12">
                      <label class="col-sm-2">Isu</label>
                      <div class="col-sm-4">
                        <select ui-jq="chosen" class="filter w-full isu form-control" name="isu" id="isu">                                      
                          <option value="x" style="color:black">Isu</option>
                          @foreach($isu as $i)
                            <option value="{{$i->ISU_ID}}" style="color:black">{{$i->ISU_NAMA}}</option>
                          @endforeach
                        </select>
                      </div>
                      <label class="col-sm-2">Kamus</label>
                      <div class="col-sm-4">
                        <select ui-jq="chosen" class="filter w-full isu form-control" name="kamus" id="kamus">                        
                          <option value="x" style="color:black">Kamus</option>
                          @foreach($kamus as $k)
                            <option value="{{$k->KAMUS_ID}}" style="color:black">{{$k->KAMUS_NAMA}}</option>
                          @endforeach
                        </select>
                      </div>
                  </div>                 
                </div>  


              </div>
              <div class="col-md-12">
                <div class="panel bg-white">
                  <div class="wrapper-lg">
                    <div class="row">
                      <div class="col-sm-10">
                       <div class="nav-tabs-alt tabs-alt-1 b-t three-row" id="tab-jurnal" >
                          <ul class="nav nav-tabs" role="tablist">
                             <li class="active">
                              <a data-target="#tab-1" role="tab" data-toggle="tab" data-status="1" class="buttonstatus">Di Proses <i class="fa fa-refresh text-info"></i></a>
                             </li>
                             <li>
                              <a data-target="#tab-2" role="tab" data-toggle="tab" data-status="2" class="buttonstatus">Di Terima <i class="fa fa-check text-success"></i></a>
                             </li>
                             <li>
                              <a data-target="#tab-3" role="tab" data-toggle="tab" data-status="0" class="buttonstatus">DI Tolak <i class="fa fa-close text-danger"></i></a>
                             </li>
                          </ul>
                        </div> 
                      </div>    
                      <div class="col-sm-2 pull-right m-t-n-sm">

                        <a href="{{URL::to('/musrenbang/2017/usulan/getExport')}}" id="export-to-excel" class="btn btn-success pull-right" style="margin-top:10px;"><i class="fa fa-back"></i> Export excel</a>
                        <select class="form-control dtSelect" id="dtSelect" style="margin-top:57px">
                              <option value="10">10</option>
                              <option value="25">25</option>
                              <option value="50">50</option>
                              <option value="100">100</option>
                          </select>

                      </div>
                   </div> 

                  </div>  


                  <div class="tab-content tab-content-alt-0 bg-dark-grey" id="tab_usulan">
                        <div role="tabpanel" class="active tab-pane" id="tab-1">  
                            <div class="table-responsive dataTables_wrapper">
                              <table id="table-usulan" ui-jq="dataTable" ui-options="{
                                    sAjaxSource: '{{ url('/') }}/musrenbang/{{$tahun}}/usulan/getRw/1',
                                    aoColumns: [
                                    { mData: 'USULAN_ID',class:'hide' },
                                    { mData: 'NO',class:'text-center' },
                                    { mData: 'LOKASI' },
                                    { mData: 'ISU' },
                                    { mData: 'VOLUME' },
                                    { mData: 'HARGA' },
                                    { mData: 'ANGGARAN' },
                                    { mData: 'STATUS' },
                                    { mData: 'AKSI' },
                                    ]}" class="table table-usulan table-striped b-t b-b">
                              
                                    <thead>
                                      <tr>
                                        <th class="hide">#</th>
                                        <th>#</th>
                                        <th>Pengusul</th>
                                        <th>Isu / Kamus</th>
                                        <th>Volume</th>
                                        <th>Harga</th>
                                        <th>Total</th>
                                        <th>Status/Lokasi</th>
                                        <th>Opsi</th>
                                      </tr>
                                      <tr>
                                        <th></th>
                                        @if(Auth::user()->level >= 5)

                                        <th colspan="9" class="th_search">

                                        @else
                                        <th colspan="9" class="th_search">
                                        @endif
                                            <i class="icon-bdg_search"></i>
                                            <input type="search" class="table-search form-control b-none w-full" placeholder="Cari" aria-controls="DataTables_Table_0">
                                        </th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                  </table>
                              </div>
                          </div>
                        </div>  

                  </div>
                </div>                
              </div>
            </div>
          </div>
      </div>
      <!-- App-content-body -->  
    </div>
    <!-- .col -->
</div>

@endsection

@section('plugin')
<script type="text/javascript">
var status;
$(document).ready(function(){
  /*$("#kecamatan").change(function(e, params){
    var kec  = $('#kecamatan').val();
    $('#kelurahan').find('option').remove().end().append('<option value="x">Kelurahan</option>');    
    $('#rw').find('option').remove().end().append('<option value="x">RW</option>');    
    resetTable();
    if(kec != 'x'){
      $.ajax({
        type  : "get",
        url   : "{{ url('/') }}/musrenbang/{{ $tahun }}/usulan/s/kelurahan/"+kec,
        success : function (data) {
          $('#kelurahan').append(data).trigger('chosen:updated');
        }
      });
    }
  });
  $("#kelurahan").change(function(e, params){
    var kel  = $('#kelurahan').val();
    $('#rw').find('option').remove().end().append('<option value="x">RW</option>');    
    resetTable();
    if(kel!='x'){
      $.ajax({
        type  : "get",
        url   : "{{ url('/') }}/musrenbang/{{ $tahun }}/usulan/s/rw/"+kel,
        success : function (data) {
          $('#rw').append(data).trigger('chosen:updated');
        }
      });
    }
  });*/
  /*$("#rw").change(function(e, params){
    resetTable();
  });
  $("#tipe").change(function(e, params){
    var tipe  = $('#tipe').val();
    $('#isu').find('option').remove().end().append('<option value="x">Isu</option>');    
    $('#kamus').find('option').remove().end().append('<option value="x">Kamus</option>');    
    resetTable();
    if(tipe!='x'){
      $.ajax({
        type  : "get",
        url   : "{{ url('/') }}/musrenbang/{{ $tahun }}/usulan/s/isu/"+tipe,
        success : function (data) {
          $('#isu').append(data).trigger('chosen:updated');
        }
      });
    }
  });
  $("#isu").change(function(e, params){
    var isu  = $('#isu').val();
    $('#kamus').find('option').remove().end().append('<option value="x">Kamus</option>');
    resetTable();
    if(isu!='x'){
      $.ajax({
        type  : "get",
        url   : "{{ url('/') }}/musrenbang/{{ $tahun }}/usulan/s/kamus/"+isu,
        success : function (data) {
          $('#kamus').append(data).trigger('chosen:updated');
        }
      });
    }
  });
  $("#kamus").change(function(e, params){
    resetTable();
  });*/

  $(".buttonstatus").on("click",function(event) {

    status = $(this).data('status');
    
    resetTable();
  });

  $(".filter").on('change',function(e){
    resetTable();
  });

  function resetTable(){
   // var kec  = $('#kecamatan').val();
   // var kel  = $('#kelurahan').val();
    var rw   = $('#rw').val();
    var tipe = $('#tipe').val();
    var isu  = $('#isu').val();
    var kamus= $('#kamus').val();
    $('#table-usulan').DataTable().destroy();

    $('#table-usulan').DataTable({
          sAjaxSource: "{{ url('/') }}/musrenbang/{{$tahun}}/usulan/filterUsulanRw/"+status+"/"+rw+"/"+isu+"/"+tipe+"/"+kamus,
          aoColumns: [
          { mData: 'USULAN_ID',class:'hide' },
          { mData: 'NO',class:'text-center' },
          { mData: 'LOKASI' },
          { mData: 'ISU' },
          { mData: 'VOLUME' },
          { mData: 'HARGA' },
          { mData: 'ANGGARAN' },
          { mData: 'STATUS' },
          { mData: 'AKSI' },
          ]
    });
  }
});


$(document).ready(function() {
    $('#example').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    } );
} );
</script>
@endsection



@extends('musrenbang.layout')

@section('content')
<style media="screen">
  #pac-input {
    background-color: #fff;
    margin-top: 10px;
    font-family: Roboto;
    font-size: 15px;
    font-weight: 300;
    margin-left: 12px;
    padding: 0 11px 0 13px;
    text-overflow: ellipsis;
    width: 300px;
  }
</style>
<div id="content" class="app-content" role="main">
  <div class="hbox hbox-auto-xs hbox-auto-sm ng-scope">
    <div class="col">
      <div class="app-content-body ">

        <div class="bg-light lter">
          <ul class="breadcrumb bg-white m-b-none">
            <li><a href="#" class="btn no-shadow" ui-toggle-class="app-aside-folded" target=".app">

              <i class="icon-bdg_expand1 text"></i>
              <i class="icon-bdg_expand2 text-active"></i>
            </a>   </li>
            <li><a href= "{{ url('/') }}/musrenbang/2017">Dashboard</a></li>
            <li class="active"><i class="fa fa-angle-right"></i>Profile</li>
          </ul>
        </div>

          <div class="row">
            <div class="col-md-12">
              <div class="panel bg-white">

                <div class="tab-content tab-content-alt-0 bg-dark-grey" id="tab_usulan">

                  <div role="tabpanel" class="active tab-pane" id="tab-1">
                    <form action="{{url('/')}}/musrenbang/users/ubahpassword" method="POST">
                      {{csrf_field()}}
                      @if(Auth::user()->validasi == '0' and Auth::user()->level == '1')
                    <div class="wrapper-sm col-sm-12">
                    <div class="col-md-12"><a href="{{url('/')}}/musrenbang/2017/validasi" class="btn btn-success w-full">KLIK DISINI UNTUK VALIDASI</button></a><br><br><br></div>
                    </div>
                    @endif
                      <div class="wrapper-sm col-sm-12">
                        <div class="panel bg-white">
                          <div class="panel-heading wrapper-lg">
                            <h5 class="inline font-semibold text-orange m-n ">Pengaturan Password</h5>
                          </div>

                          <div class="panel-body wrapper-lg">
                            @if (count($errors) > 0)
                            <div class="alert alert-danger">
                              <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                              </ul>
                            </div>
                            @endif
                            <br><br>
                            <div class="form-group">
                              <label class="col-sm-3">Nama Account</label>
                              <div class="col-sm-5">
                                <input type="text" class="form-control" value="{{Auth::User()->name}}" placeholder="" readonly>
                              </div>
                            </div>
                            <br><br>
                            <div class="form-group">
                              <label class="col-sm-3">Username</label>
                              <div class="col-sm-5">
                                <input type="text" class="form-control" value="{{Auth::User()->email}}" placeholder="" readonly name="email">
                              </div>
                            </div> 
                            <br><br>  
                            <div class="form-group">
                              <label class="col-sm-3">Password Baru</label>
                              <div class="col-sm-5">
                                <input type="password" value="" class="form-control" placeholder="password baru" name="password">
                              </div>
                            </div>
                            <br><br>
                            <div class="form-group">
                              <label class="col-sm-3">Konfirmasi Password</label>
                              <div class="col-sm-5">
                                <input type="password" value="" class="form-control" placeholder="konfirmasi password" name="password_confirmation">
                              </div>
                            </div>
                            <br><br>
                            <div class="form-group">
                              <label class="col-sm-3"></label>
                              <div class="col-sm-5">
                                <button type="submit" class="btn btn-success pull-right">Simpan</button>
                              </div>
                            </div>
                          </div>

                        </div>

                      </div>
                    </form>

                    <!-- profil RW -->
                    @if(Auth::user()->level==1)
                    <div class="wrapper-sm col-sm-12 m-t-n-xl">
                      <div class="panel bg-white">

                        <div class="panel-heading wrapper-lg">
                          <h5 class="inline font-semibold text-orange m-n ">Edit Profil</h5>
                        </div>

                        <div class="panel-body wrapper-lg">
                          @if (count($errors) > 0)
                          <div class="alert alert-danger">
                            <ul>
                              @foreach ($errors->all() as $error)
                              <li>{{ $error }}</li>
                              @endforeach
                            </ul>
                          </div>
                          @endif
                          <form action="{{url('/')}}/musrenbang/rw/ubah" method="POST">
                            <input type="hidden" name="rwid" value="{{Auth::user()->RW_ID}}">
                            {{csrf_field()}}
                            <br><br>
                            <div class="form-group">
                              <label class="col-sm-3">RW</label>
                              <div class="col-sm-5">
                                <input type="text" class="form-control" value="{{Auth::User()->RW->RW_NAMA}}" placeholder="RW" readonly>
                              </div>
                            </div>
                            <br><br>  
                            <div class="form-group">
                              <label class="col-sm-3">Nama Ketua RW</label>
                              <div class="col-sm-5">
                                <input type="text" value="{{Auth::User()->RW->RW_KETUA}}" class="form-control" placeholder="Nama RW" name="ketuarw">
                              </div>
                            </div>
                            <br><br>
                            <div class="form-group">
                              <label class="col-sm-3">N.I.K</label>
                              <div class="col-sm-5">
                                <input type="text" value="{{Auth::User()->RW->RW_NIK}}" class="form-control" placeholder="N.I.K" name="nik">
                              </div>
                            </div>
                            <br><br>
                            <div class="form-group">
                              <label class="col-sm-3">No TLp/Hp</label>
                              <div class="col-sm-5">
                                <input type="text" value="{{Auth::User()->RW->RW_TELP}}" class="form-control" placeholder="No Tlp/Hp" name="notelepon">
                              </div>
                            </div>
                            <br><br>
                            <div class="form-group">
                              <label class="col-sm-3"></label>
                              <div class="col-sm-5">
                                <button type="submit" class="btn btn-success pull-right">Simpan</button>
                              </div>
                            </div>
                          </form>
                        </div>
                        <!-- end profil rw -->


                        <!-- Kelola profil RT -->
                        <div class="wrapper-sm col-sm-12">
                          <div class="panel bg-white">
                            <div class="panel-heading wrapper-lg">
                              <h5 class="inline font-semibold text-orange m-n ">Kelola RT</h5>
                            </div>

                            <div class="panel-body wrapper-lg">
                              <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modaltambahrt">
                                Tambah RT
                              </button> 
                            </div>

                            <div class="panel-body wrapper-lg">
                              @foreach(Auth::user()->RW->rt as $rt)
                              <div>
                                <input type="hidden" value={{$rt->RT_ID}} id="RTID">
                                <p id="RTNAMA"> Nama RT : <span>{{$rt->RT_NAMA}}</span></p>
                                <p id="RTKETUA"> Ketua RT : <span>{{$rt->RT_KETUA}}</span></p>
                                <p id="RTNIK"> NIK : <span>{{$rt->RT_NIK}}</span></p>
                                <p id="RTTELP"> No.Hp : <span>{{$rt->RT_TELP}}</span></p>
                                <button class="buttonubah btn btn-default" data-toggle="modal" data-target="#modaleditrt">Ubah</button>
                                <hr>
                              </div>
                              @endforeach
                            </div>

                          </div>
                        </div>
                        
                        <!-- end kelola profil RT -->
                      </div>
                    </div>
                    @endif
                    <!-- end profil RW -->

                    <!-- profil lurah -->
                    @if(Auth::user()->level==5)
                    <div class="wrapper-sm col-sm-12 m-t-n-xl">
                      <div class="panel bg-white">

                        <div class="panel-heading wrapper-lg">
                          <h5 class="inline font-semibold text-orange m-n ">Edit Profil Akun Lurah</h5>
                        </div>
                        <div class="panel-body wrapper-lg">
                          @if (count($errors) > 0)
                          <div class="alert alert-danger">
                            <ul>
                              @foreach ($errors->all() as $error)
                              <li>{{ $error }}</li>
                              @endforeach
                            </ul>
                          </div>
                          @endif
                          <form action="{{url('/')}}/musrenbang/lurah/ubah" method="POST">
                            <input type="hidden" name="lurahid" value="{{Auth::user()->KEL_ID}}">
                            {{csrf_field()}}
                            <br><br>
                            <div class="form-group">
                              <label class="col-sm-3">Kelurahan</label>
                              <div class="col-sm-5">
                                <input type="text" class="form-control" value="{{Auth::User()->kelurahan->KEL_NAMA}}" placeholder="Kelurahan" readonly>
                              </div>
                            </div>
                            <br><br>  
                            <div class="form-group">
                              <label class="col-sm-3">Nama Lurah </label>
                              <div class="col-sm-5">
                                <input type="text" value="{{Auth::User()->kelurahan->KEL_LURAH}}" class="form-control" placeholder="Nama Lurah" name="lurah">
                              </div>
                            </div>
                            <br><br>
                            <div class="form-group">
                              <label class="col-sm-3">N.I.P</label>
                              <div class="col-sm-5">
                                <input type="text" value="{{Auth::User()->kelurahan->KEL_NIP}}" class="form-control" placeholder="N.I.P" name="nip">
                              </div>
                            </div>
                            <br><br>
                            <div class="form-group">
                              <label class="col-sm-3">No TLp/Hp</label>
                              <div class="col-sm-5">
                                <input type="text" value="{{Auth::User()->kelurahan->KEL_TELP}}" class="form-control" placeholder="No Tlp/Hp" name="notelepon">
                              </div>
                            </div>
                            <br><br>
                            <div class="form-group">
                              <label class="col-sm-3"></label>
                              <div class="col-sm-5">
                                <button type="submit" class="btn btn-success pull-right">Simpan</button>
                              </div>
                            </div>
                          </form>
                        </div>
                      </div>
                    </div>
                    @endif
                    <!-- end profil lurah -->

                    <!-- profil camat -->
                    @if(Auth::user()->level==6)
                    <div class="wrapper-sm col-sm-12 m-t-n-xl">
                      <div class="panel bg-white">

                        <div class="panel-heading wrapper-lg">
                          <h5 class="inline font-semibold text-orange m-n ">Edit Profil Akun Camat</h5>
                        </div>
                        <div class="panel-body wrapper-lg">
                          @if (count($errors) > 0)
                          <div class="alert alert-danger">
                            <ul>
                              @foreach ($errors->all() as $error)
                              <li>{{ $error }}</li>
                              @endforeach
                            </ul>
                          </div>
                          @endif
                          <form action="{{url('/')}}/musrenbang/camat/ubah" method="POST">
                            <input type="hidden" name="camatid" value="{{Auth::user()->KEC_ID}}">
                            {{csrf_field()}}
                            <br><br>
                            <div class="form-group">
                              <label class="col-sm-3">Kecamatan</label>
                              <div class="col-sm-5">
                                <input type="text" class="form-control" value="{{Auth::User()->kecamatan->KEC_NAMA}}" placeholder="Kelurahan" readonly>
                              </div>
                            </div>
                            <br><br>  
                            <div class="form-group">
                              <label class="col-sm-3">Nama Camat </label>
                              <div class="col-sm-5">
                                <input type="text" value="{{Auth::User()->kecamatan->KEC_CAMAT}}" class="form-control" placeholder="Nama Camat" name="camat">
                              </div>
                            </div>
                            <br><br>
                            <div class="form-group">
                              <label class="col-sm-3">N.I.P</label>
                              <div class="col-sm-5">
                                <input type="text" value="{{Auth::User()->kecamatan->KEC_NIP}}" class="form-control" placeholder="N.I.P" name="nip">
                              </div>
                            </div>
                            <br><br>
                            <div class="form-group">
                              <label class="col-sm-3">No TLp/Hp</label>
                              <div class="col-sm-5">
                                <input type="text" value="{{Auth::User()->kecamatan->KEC_TELP}}" class="form-control" placeholder="No Tlp/Hp" name="notelepon">
                              </div>
                            </div>
                            <br><br>
                            <div class="form-group">
                              <label class="col-sm-3"></label>
                              <div class="col-sm-5">
                                <button type="submit" class="btn btn-success pull-right">Simpan</button>
                              </div>
                            </div>
                          </form>
                        </div>
                      </div>
                    </div>
                    @endif
                    <!-- end profil camat -->

                    

                    @if(Auth::user()->validasi == '0' and Auth::user()->level == '1')
                    <div class="wrapper-sm col-sm-12 m-t-n-xl">
                    <div class="col-md-12"><a href="{{url('/')}}/musrenbang/2017/validasi" class="btn btn-success w-full">KLIK DISINI UNTUK VALIDASI</button></a><br><br><br></div>
                    </div>
                    @endif
                  </div>  
                </div>  
              </div>
            </div>
          </div>
      </div>
    </div>
  </div>
</div>
</div>

<div id="modaltambahrt" class="modal fade" role="dialog">
  <form action="{{url('/')}}/musrenbang/rw/tambahrt" method="POST">
    {{csrf_field()}}
    <input type="hidden" value="{{Auth::user()->RW_ID}}" name="rwid">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Tambah RT</h4>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <label for="namart">Nomor RT</label>
            <input type="text" name="namart" id="namart" class="form-control">
          </div>
          <div class="form-group">
            <label for="ketuart">Nama Ketua RT</label>
            <input type="text" name="ketuart" id="ketuart" class="form-control">
          </div>
          <div class="form-group">
            <label for="nik">NIK</label>
            <input type="text" name="nik" id="nik" class="form-control">
          </div>
          <div class="form-group">
            <label for="notelepon">No Telepon</label>
            <input type="text" name="notelepon" id="notelepon" class="form-control">
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-default">Simpan</button>
        </div>
      </div>
    </div>
  </form>
</div>

<div id="modaleditrt" class="modal fade" role="dialog">
  <form action="{{url('/')}}/musrenbang/rt/editrt" method="POST" id="formubahrt">
    {{csrf_field()}}
    <input type="hidden" value="{{Auth::user()->RW_ID}}" name="rwid">
    <input type="hidden" value="" name="rtid">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Ubah RT</h4>
        </div>
        <div class="modal-body">
          <div class="form-group">
            <label for="namart">Nomor RT</label>
            <input type="text" name="namart" id="namart" class="form-control">
          </div>
          <div class="form-group">
            <label for="ketuart">Nama Ketua RT</label>
            <input type="text" name="ketuart" id="ketuart" class="form-control">
          </div>
          <div class="form-group">
            <label for="nik">NIK</label>
            <input type="text" name="nik" id="nik" class="form-control">
          </div>
          <div class="form-group">
            <label for="notelepon">No Telepon</label>
            <input type="text" name="notelepon" id="notelepon" class="form-control">
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-default">Simpan</button>
        </div>
      </div>
    </div>
  </form>
</div>
@endsection


@section('plugin')
<script type="text/javascript">
  var oldMarker;
  function initMap() {
    var bandung = {lat: -6.914744, lng: 107.609810};
    var map = new google.maps.Map(document.getElementById('maps'), {
      zoom: 15,
      center: bandung
    });

    google.maps.event.addListener(map, 'click', function( event ){
      $('#latitude').val(event.latLng.lat());
      $('#longitude').val(event.latLng.lng());


      placeMarker(event.latLng,map);
    });

        // Create the search box and link it to the UI element.
        var input = document.getElementById('pac-input');
        var searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function() {
          searchBox.setBounds(map.getBounds());
        });

        var markers = [];
        // [START region_getplaces]
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function() {
          var places = searchBox.getPlaces();

          if (places.length == 0) {
            return;
          }

        // Clear out the old markers.
        markers.forEach(function(marker) {
          marker.setMap(null);
        });
        markers = [];

        // For each place, get the icon, name and location.
        var bounds = new google.maps.LatLngBounds();
        places.forEach(function(place) {
          var icon = {
            url: place.icon,
            size: new google.maps.Size(71, 71),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(17, 34),
            scaledSize: new google.maps.Size(25, 25)
          };

          // Create a marker for each place.
          markers.push(new google.maps.Marker({
            map: map,
            icon: icon,
            title: place.name,
            position: place.geometry.location
          }));

          if (place.geometry.viewport) {
            // Only geocodes have viewport.
            bounds.union(place.geometry.viewport);
          } else {
            bounds.extend(place.geometry.location);
          }
        });
        map.fitBounds(bounds);
      });
        // [END region_getplaces]
      }

      function placeMarker(location,map) {
        marker = new google.maps.Marker({
          position: location,
          map: map

        });

        if (oldMarker != undefined){
          oldMarker.setMap(null);
        }
        oldMarker = marker;
      }
    </script>
    <script>
// This example adds a search box to a map, using the Google Place Autocomplete
// feature. People can enter geographical searches. The search box will return a
// pick list containing a mix of places and predicted search terms.

function initAutocomplete() {
  var map = new google.maps.Map(document.getElementById('maps'), {
    center: {lat: -33.8688, lng: 151.2195},
    zoom: 13,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  });

// Create the search box and link it to the UI element.
var input = document.getElementById('pac-input');
var searchBox = new google.maps.places.SearchBox(input);
map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

// Bias the SearchBox results towards current map's viewport.
map.addListener('bounds_changed', function() {
  searchBox.setBounds(map.getBounds());
});

var markers = [];
// [START region_getplaces]
// Listen for the event fired when the user selects a prediction and retrieve
// more details for that place.
searchBox.addListener('places_changed', function() {
  var places = searchBox.getPlaces();

  if (places.length == 0) {
    return;
  }

// Clear out the old markers.
markers.forEach(function(marker) {
  marker.setMap(null);
});
markers = [];

// For each place, get the icon, name and location.
var bounds = new google.maps.LatLngBounds();
places.forEach(function(place) {
  var icon = {
    url: place.icon,
    size: new google.maps.Size(71, 71),
    origin: new google.maps.Point(0, 0),
    anchor: new google.maps.Point(17, 34),
    scaledSize: new google.maps.Size(25, 25)
  };

  // Create a marker for each place.
  markers.push(new google.maps.Marker({
    map: map,
    icon: icon,
    title: place.name,
    position: place.geometry.location
  }));

  if (place.geometry.viewport) {
    // Only geocodes have viewport.
    bounds.union(place.geometry.viewport);
  } else {
    bounds.extend(place.geometry.location);
  }
});
map.fitBounds(bounds);
});
// [END region_getplaces]
}


</script>
<script async defer
src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBzixLw-QwjOpEiL8qmveEmafmgM27mgnk&libraries=places&callback=initMap">
</script>

<script>
  $(document).ready(function(){
    $(".buttonubah").on('click',function(event) {
      event.preventDefault();

      
      RTNAMA = $(this).siblings("#RTNAMA").children('span:first-child').html();
      RTKETUA = $(this).siblings("#RTKETUA").children('span:first-child').html();
      RTNIK = $(this).siblings("#RTNIK").children('span:first-child').html();
      RTTELP = $(this).siblings("#RTTELP").children('span:first-child').html();
      RTID = $(this).siblings("#RTID").val();

      $("#formubahrt input[name='namart']").val(RTNAMA);
      $("#formubahrt input[name='ketuart']").val(RTKETUA);
      $("#formubahrt input[name='nik']").val(RTNIK);
      $("#formubahrt input[name='notelepon']").val(RTTELP);
      $("#formubahrt input[name='rtid']").val(RTID);

    });

    $("#upload").click(function(){
      $("#div1").fadeIn("fast");
      $("#upload").addClass("addGambar");
    });

    $("#isu").change(function(e, params){
      var id  = $('#isu').val();
      $('#kamus').find('option').remove().end().append('<option>Kamus Usulan</option>');
      $.ajax({
        type  : "get",
        url   : "{{ url('/') }}/musrenbang/{{ $tahun }}/usulan/data/kamus/"+id,
        success : function (data) {
          $('#kamus').append(data['opt']).trigger('chosen:updated');
          if(data['tipe'] == 1) $('#div-upload-gambar').fadeIn('fast').removeClass('hide');
          else $('#div-upload-gambar').fadeOut('fast').addClass('hide');
        }
      });
    });

    $("#kamus").change(function(e, params){
      var id  = $('#kamus').val();
      $('#satuan').val();
      $.ajax({
        type  : "get",
        url   : "{{ url('/') }}/musrenbang/{{ $tahun }}/usulan/data/satuan/"+id,
        success : function (data) {
          $('#satuan').val(data);
        }
      });
    });

  });
</script>


<!--Start of Zendesk Chat Script-->
<script type="text/javascript">
  window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
    d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
      _.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
      $.src="https://v2.zopim.com/?4XsJAVMVHAuWk8KVNd6RcwgTq6up8MOA";z.t=+new Date;$.
      type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
    </script>
    <!--End of Zendesk Chat Script-->

    @endsection

@extends('musrenbang.layout')

@section('content')
<style media="screen">
#pac-input {
background-color: #fff;
margin-top: 10px;
font-family: Roboto;
font-size: 15px;
font-weight: 300;
margin-left: 12px;
padding: 0 11px 0 13px;
text-overflow: ellipsis;
width: 300px;
}
</style>
<div id="content" class="app-content" role="main">
  <div class="hbox hbox-auto-xs hbox-auto-sm ng-scope">
    <div class="col">
      <div class="app-content-body ">

          <div class="bg-light lter">
              <ul class="breadcrumb bg-white m-b-none">
                <li><a href="#" class="btn no-shadow" ui-toggle-class="app-aside-folded" target=".app">
                   <i class="icon-bdg_expand1 text"></i> 
                  <i class="icon-bdg_expand2 text-active"></i>
                </a>   </li>
                <li><a href= "{{ url('/') }}/musrenbang/2017">Dashboard</a></li>
                <li><i class="fa fa-angle-right"></i>Usulan</li>
                <li class="active"><i class="fa fa-angle-right"></i>Tambah Usulan @if($usulan_tujuan ==1) Renja  @elseif($usulan_tujuan ==2) PIPPK @endif</li>
              </ul>
          </div>

          <div class="wrapper-lg bg-dark-grey">
            <div class="row">
              <div class="col-md-12">
                <div class="panel bg-white">

                  <div class="panel-heading wrapper-lg">
                    <h5 class="inline font-semibold text-orange m-n ">Tambah Usulan @if($usulan_tujuan ==1) 
                    Renja   @elseif($usulan_tujuan ==2) PIPPK @endif 
                    @if(Auth::user()->level==5)Tahap Kelurahan
                    @elseif(Auth::user()->level==6)Tahap Kecamatan 
                    @endif</h5>
                  </div>

                  <div class="tab-content tab-content-alt-1 bg-white">
                        <div class="bg-white wrapper-lg input-jurnal">
                          @if (count($errors) > 0)
                              <div class="alert alert-danger">
                                  <ul>
                                      @foreach ($errors->all() as $error)
                                          <li>{{ $error }}</li>
                                      @endforeach
                                  </ul>
                              </div>
                          @endif
                            <form action="{{url('/')}}/musrenbang/{{$tahun}}/usulan/tambah/submit" method="post" class="form-horizontal" enctype="multipart/form-data">
                              <div class="input-wrapper">

                                  <div class="form-group">
                                    <label class="col-sm-2">Isu Usulan</label>
                                    <div class="col-sm-10">
                                      <select ui-jq="chosen" class="w-full isu form-control" name="isu" id="isu" required>
                                            <option value="" style="color:black">Pilih Isu</option>
                                        @foreach ($isu as $isu)
                                            <option value="{{$isu->ISU_ID}}" style="color:black">{{$isu->ISU_NAMA}}</option>
                                        @endforeach
                                      </select>
                                    </div>
                                  </div>

                                  <div class="form-group">
                                    <label class="col-sm-2">Kamus Usulan </label>
                                    <div class="col-sm-10">
                                      <select ui-jq="chosen" class="w-full form-control" name="kamus" id="kamus">
                                            <option value="" style="color:black">Pilih Kamus</option>
                                      </select>
                                    </div>
                                  </div>

                                  <div class="form-group" style="display:none" id="kriteria1">
                                    <label class="col-sm-2">Kriteria Kamus </label>
                                    <div class="col-sm-10">
                                        <textarea width="20" name="kriteria" id="kriteria" class="form-control" readonly=""></textarea>
                                    </div>
                                  </div>

                                  

                                  <div class="form-group">
                                    <label class="col-sm-2">Volume</label>
                                    <div class="col-sm-6">
                                      <input type="text" onkeypress="return event.charCode >= 48 && event.charCode <= 57" placeholder="Masukan Nilai Volume pertama dalam Angka (Panjang)" name="vol" required="" class="form-control">
                                    </div>
                                    <div class="col-sm-4">
                                      <input type="text" class="form-control" name="satuan" readonly="" placeholder="Satuan" id="satuan">
                                    </div>
                                  </div>

                                  <div class="form-group">
                                    <label class="col-sm-2"></label>
                                    <div class="col-sm-6">
                                      <input type="text" onkeypress="return event.charCode >= 48 && event.charCode <= 57" placeholder="Masukan Nilai Volume ke dua dalam Angka (Panjang)" name="vol1" class="form-control">
                                    </div>
                                    <div class="col-sm-4">
                                      <select ui-jq="chosen" class="w-full form-control" name="sat" id="sat">
                                        <option value="" style="color:black">Pilih Satuan</option>
                                        @foreach ($satuan as $sat)
                                            <option value="{{$sat->SATUAN_ID}}" style="color:black">{{$sat->SATUAN_NAMA}}</option>
                                        @endforeach
                                      </select>
                                    </div>
                                  </div>

                                  <div class="form-group">
                                    <label class="col-sm-2"></label>
                                    <div class="col-sm-6">
                                      <input type="text" onkeypress="return event.charCode >= 48 && event.charCode <= 57" placeholder="Masukan Nilai Volume ke tiga dalam Angka (tinggi)" name="vol2" class="form-control">
                                    </div>
                                    <div class="col-sm-4">
                                      <select ui-jq="chosen" class="w-full form-control" name="sat1" id="sat1">
                                        <option value="" style="color:black">Pilih Satuan</option>
                                        @foreach ($satuan as $sat1)
                                            <option value="{{$sat1->SATUAN_ID}}" style="color:black">{{$sat1->SATUAN_NAMA}}</option>
                                        @endforeach
                                      </select>
                                    </div>
                                  </div>

                                  <div class="form-group" id="div-upload-gambar" style="display: none">
                                    <label class="col-sm-2">Upload Gambar</label>
                                    <div class="col-sm-7">
                                      <p style="color:red">
                                      
                                      </p>
                                      <input ui-jq="filestyle" name="image[]" type="file" data-icon="true" data-classbutton="btn btn-default" data-classinput="form-control inline v-middle input-s" id="filestyle-0" tabindex="-1" style="position: absolute; clip: rect(10px 10px 10px 10px);" accept=".jpg,.png,.jpeg">
                                    </div>
                                    <div class="col-sm-1">
                                      <button class="btn btn-info" id="upload" type="button"><i class="fa fa-plus"> Tambah Gambar</i></button>
                                    </div>
                                  </div>

                                  <div class="form-group" style="display:none" id="div1">
                                    <label class="col-sm-2"></label>
                                    <div class="col-sm-9">
                                      <input ui-jq="filestyle" name="image[]" type="file" data-icon="true" data-classbutton="btn btn-default" data-classinput="form-control inline v-middle input-s" id="filestyle-1" tabindex="-1" style="position: absolute; clip: rect(10px 10px 10px 10px);" accept=".jpg,.png,.jpeg">
                                    </div>
                                    <div class="col-sm-1">
                                    </div>
                                  </div>

                                   

                                   <div class="form-group">
                                    <label class="col-sm-2">Masukan @if(Auth::user()->level == 5) RW @elseif(Auth::user()->level == 6) Kelurahan @elseif(Auth::user()->level == 1) RT @endif Penerima Manfaat</label>
                                    <div class="col-sm-8" style="margin-top:10px">
                                      @if(Auth::user()->level == 1) 
                                        @foreach ($rt as $rt)
                                        <input type="checkbox" class="" value="{{$rt->RT_ID}}" style="margin-left:10px;" name="rt[]"> {{$rt->RT_NAMA}}
                                        @endforeach
                                      @elseif(Auth::user()->level == 5) 
                                        @foreach ($rt as $rt)
                                        <input type="checkbox" class="" value="{{$rt->RW_ID}}" style="margin-left:10px;" name="rt[]"> {{$rt->RW_NAMA}}
                                        @endforeach
                                      @elseif(Auth::user()->level == 6) 
                                        @foreach ($rt as $rt)
                                        <input type="checkbox" class="" value="{{$rt->KEL_ID}}" style="margin-left:10px;" name="rt[]"> {{$rt->KEL_NAMA}}
                                        @endforeach
                                      @endif
                                      </div>
                                  </div>

                                  <input type="hidden" class="form-control" name="kec" readonly="" value="{{ Auth::user()->kelurahan->kecamatan->KEC_NAMA }}">
                                      <input type="hidden" class="form-control" name="kel" readonly="" placeholder="Kelurahan" value="{{ Auth::user()->kelurahan->KEL_NAMA }}">
                                      <input type="hidden" class="form-control" name="rw" readonly="" placeholder="RW" value="RW {{ Auth::user()->rw->RW_NAMA }}">
                                 

                                   <div class="form-group">
                                    <label class="col-sm-2">Urgensi Kegiatan / Keterangan</label>
                                    <div class="col-sm-10">
                                       <textarea width="20" name="urgensi" class="form-control" placeholder="Masukan Alasan kenapa kegiatan ini diperlukan dan keterangan lainya."></textarea>
                                    </div>
                                  </div>

                                  <div class="form-group">
                                    <label class="col-sm-2">Alamat Lokasi </label>
                                    <div class="col-sm-10">
                                       <textarea width="20" name="alamat" class="form-control" placeholder="Masukan Detail Alamat (untuk memudahkan survei lokasi kegiatan)"></textarea>
                                    </div>
                                  </div>


                                  <div class="form-group">
                                    <label class="col-sm-2">Tentukan Posisi Di Peta</label>
                                    
                                    <div class="col-sm-10">
                                     <a class="text text-orange pull-right m-t-md">*Tekan peta untuk memilih lokasi</a>
                                    </div>
                                    <input type="hidden" class="form-control" name="latitude" placeholder="Latitude" id="latitude" required="">
                                      <input type="hidden" class="form-control" name="longitude" placeholder="Longitude" id="longitude" required="">
                                    
                                  </div>


                                  <div class="form-group">
                                    <input id="pac-input" class="controls" type="text" placeholder="Search Box">
                                    <br>
                                    <div class="col-sm-12" id="maps" style="height: 500px;">
                                    </div>
                                  </div>


                                 

                                  <input type="hidden" value="{{$usulan_tujuan}}" name="usulan_tujuan">

                                  <hr class="m-t-xl">
                                  <div class="form-group">
                                      
                                      <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">

                                    <div class="col-md-12">
                                      <a href="{{ url('/') }}/musrenbang/{{ $tahun }}/usulan-{{$usulan_tujuan}}" class="btn input-xl btn-info"><i class="fa fa-reply m-r-xs"></i>Batal</a>
                                      <button class="btn input-xl btn-success pull-right m-r-xs" type="submit"><i class="fa fa-check m-r-xs"></i>Simpan</button>
                                    </div>
                                  </div>
                              </div>
                            </form>
                          </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
      </div>
    </div>
    </div>
</div>
@endsection





@section('plugin')
<script type="text/javascript">
      var oldMarker;
      function initMap() {
        var bandung = {lat: -6.914744, lng: 107.609810};
        var map = new google.maps.Map(document.getElementById('maps'), {
          zoom: 15,
          style: 'mapbox://styles/mapbox/satellite-v9',
          center: bandung
        });

        google.maps.event.addListener(map, 'click', function( event ){
          $('#latitude').val(event.latLng.lat());
          $('#longitude').val(event.latLng.lng());


          placeMarker(event.latLng,map);
        });

        // Create the search box and link it to the UI element.
        var input = document.getElementById('pac-input');
        var searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function() {
        searchBox.setBounds(map.getBounds());
        });

        var markers = [];
        // [START region_getplaces]
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function() {
        var places = searchBox.getPlaces();

        if (places.length == 0) {
          return;
        }

        // Clear out the old markers.
        markers.forEach(function(marker) {
          marker.setMap(null);
        });
        markers = [];

        // For each place, get the icon, name and location.
        var bounds = new google.maps.LatLngBounds();
        places.forEach(function(place) {
          var icon = {
            url: place.icon,
            size: new google.maps.Size(71, 71),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(17, 34),
            scaledSize: new google.maps.Size(25, 25)
          };

          // Create a marker for each place.
          markers.push(new google.maps.Marker({
            map: map,
            icon: icon,
            title: place.name,
            position: place.geometry.location
          }));

          if (place.geometry.viewport) {
            // Only geocodes have viewport.
            bounds.union(place.geometry.viewport);
          } else {
            bounds.extend(place.geometry.location);
          }
        });
        map.fitBounds(bounds);
        });
        // [END region_getplaces]
      }

      function placeMarker(location,map) {
            marker = new google.maps.Marker({
                position: location,
                map: map

            });

            if (oldMarker != undefined){
                oldMarker.setMap(null);
            }
            oldMarker = marker;
        }
</script>


<script>
// This example adds a search box to a map, using the Google Place Autocomplete
// feature. People can enter geographical searches. The search box will return a
// pick list containing a mix of places and predicted search terms.

function initAutocomplete() {
var map = new google.maps.Map(document.getElementById('maps'), {
center: {lat: -33.8688, lng: 151.2195},
zoom: 13,
mapTypeId: google.maps.MapTypeId.ROADMAP
});

// Create the search box and link it to the UI element.
var input = document.getElementById('pac-input');
var searchBox = new google.maps.places.SearchBox(input);
map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

// Bias the SearchBox results towards current map's viewport.
map.addListener('bounds_changed', function() {
searchBox.setBounds(map.getBounds());
});

var markers = [];
// [START region_getplaces]
// Listen for the event fired when the user selects a prediction and retrieve
// more details for that place.
searchBox.addListener('places_changed', function() {
var places = searchBox.getPlaces();

if (places.length == 0) {
  return;
}

// Clear out the old markers.
markers.forEach(function(marker) {
  marker.setMap(null);
});
markers = [];

// For each place, get the icon, name and location.
var bounds = new google.maps.LatLngBounds();
places.forEach(function(place) {
  var icon = {
    url: place.icon,
    size: new google.maps.Size(71, 71),
    origin: new google.maps.Point(0, 0),
    anchor: new google.maps.Point(17, 34),
    scaledSize: new google.maps.Size(25, 25)
  };

  // Create a marker for each place.
  markers.push(new google.maps.Marker({
    map: map,
    icon: icon,
    title: place.name,
    position: place.geometry.location
  }));

  if (place.geometry.viewport) {
    // Only geocodes have viewport.
    bounds.union(place.geometry.viewport);
  } else {
    bounds.extend(place.geometry.location);
  }
});
map.fitBounds(bounds);
});
// [END region_getplaces]
}


</script>
<script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBzixLw-QwjOpEiL8qmveEmafmgM27mgnk&libraries=places&callback=initMap">
</script>

<script>
    $(document).ready(function(){

        $("#upload").click(function(){
            $("#div1").fadeIn("fast");
            $("#upload").addClass("addGambar");
        });

        $("#isu").change(function(e, params){
            var id  = $('#isu').val();
            console.log(id);
           $('#kamus').find('option').remove().end().append('<option>Kamus Usulan</option>');
            $.ajax({
              type  : "get",
              url   : "{{ url('/') }}/musrenbang/{{ $tahun }}/usulan/data/kamus/filter/"+id,
              success : function (data) {
                console.log(data);
                $('#kamus').append(data['opt']).trigger('chosen:updated');
                if(data['tipe'] == 1 || data['tipe'] == 2) $('#div-upload-gambar').fadeIn('fast').removeClass('hide');
                else $('#div-upload-gambar').fadeOut('fast').addClass('hide');
              }
            });
        });

        $("#kamus").change(function(e, params){
          // alert('kamus');
            var id  = $('#kamus').val();
            $('#satuan').val();
            $("#kriteria1").fadeIn("fast");
            $.ajax({
              type  : "get",
              url   : "{{ url('/') }}/musrenbang/{{ $tahun }}/usulan/data/satuan/"+id,
              success : function (data) {
                $('#satuan').val(data['satuan']);
                $('#kriteria').val(data['kriteria']);
              }
            });
        });
        $("#isu").change();
    });
</script>


<!--Start of Zendesk Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="https://v2.zopim.com/?4XsJAVMVHAuWk8KVNd6RcwgTq6up8MOA";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zendesk Chat Script-->

@endsection

@extends('musrenbang.layout')

@section('content')
<div id="content" class="app-content" role="main">
  <div class="hbox hbox-auto-xs hbox-auto-sm ng-scope">
    <div class="col">
      <div class="app-content-body ">

          <div class="bg-light lter">    
              <ul class="breadcrumb bg-white m-b-none">
                <li><a href="#" class="btn no-shadow" ui-toggle-class="app-aside-folded" target=".app">
                  <i class="icon-bdg_expand1 text"></i>
                  <i class="icon-bdg_expand2 text-active"></i>
                </a>   </li>
                <li><a href= "{{ url('/') }}/main">Dashboard</a></li>
                <li class="active"><i class="fa fa-angle-right"></i>Berita Acara</li>                               
              </ul>
          </div>
          <div class="wrapper-lg">
            <div class="row">
             
              <div class="col-md-12">
                <div class="panel bg-white">
                  <div class="wrapper-lg">
                    @if(Auth::user()->level==5)
                       <a href="https://drive.google.com/open?id=0BxIx84aCMiVnTmhHOWN0VHFCcGM" target="_blank" class="btn btn-default"><i class="fa fa-back"></i> Download</a> 
                    @elseif(Auth::user()->level==6) 
                        <a href="https://drive.google.com/open?id=0BxIx84aCMiVnTFlEM2w1Rkc2VEU" target="_blank" class="btn btn-default"><i class="fa fa-back"></i> Download</a>
                    @endif
                      <a href="#" data-toggle="modal" data-target="#uploadBerita" class="btn btn-default"><i class="fa fa-back"></i> Foto Berita Acara</a>
                      <a href="#" data-toggle="modal" data-target="#uploadFoto" class="btn btn-default"><i class="fa fa-back"></i> Foto Rembuk Warga</a>
                      <div class="col-sm-2 pull-right m-t-n-sm">
                        <select class="form-control dtSelect" id="dtSelect">
                              <option value="10">10</option>
                              <option value="25">25</option>
                              <option value="50">50</option>
                              <option value="100">100</option>
                          </select>
                      </div>
                  
                  </div>  


                  <div class="tab-content tab-content-alt-0 bg-dark-grey" id="tab_usulan">
                        <div role="tabpanel" class="active tab-pane" id="tab-1">  
                            <div class="table-responsive dataTables_wrapper">
                             <table ui-jq="dataTable" ui-options="" class="table table-striped b-t b-b" id="table-usulan">
                                    <thead>
                                      <tr>
                                         <th class="hide"></th>
                                        <th>No</th>
                                        <th>RW</th>
                                        <th>Nama Rw</th>
                                        <th>Tgl upload</th>
                                        <th>Foto Berita Acara</th>
                                        <th>Foto Rembuk Warga</th>
                                      </tr>
                                      <tr>
                                       
                                        <th colspan="6" class="th_search">
                                            <i class="icon-bdg_search"></i>
                                            <input type="search" class="table-search form-control b-none w-full" placeholder="Cari Usulan" aria-controls="DataTables_Table_0">
                                        </th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      @php
                                        $i =1;
                                      @endphp
                                       @foreach($user as $u)
                                           
                                        <tr>
                                            <td class="hide">{{$u->id}}</td>
                                            <td> {{$i++}} </td>
                                            <td> {{$u->rw->RW_NAMA}} </td>
                                            <td> {{$u->rw->RW_KETUA}} </td>
                                            @if($u->BeritaAcara[0] != null)
                                            <td> {{$u->BeritaAcara[0]->TGL}} </td>
                                            <td> 
                                              @foreach($u->BeritaAcara as $ba)
                                                @if($ba->lampiran != null)
                                                   @foreach($ba->lampiran as $lampiran)
                                                   <a href="{{ url('/') }}/uploads/{{ $lampiran }}" target="_blanḳ">
                                                    {{$lampiran}}
                                                   </a>
                                                   @endforeach
                                                @endif 
                                              @endforeach
                                            </td>

                                            <td> 
                                             @foreach($u->BeritaAcaraFoto as $foto)
                                                @if($foto->image != null)
                                                 @foreach($foto->image as $fo)
                                                  <a href="{{ url('/') }}/uploads/{{ $fo }}" target="_blanḳ">
                                                    {{$fo}}
                                                  </a>
                                                 @endforeach 
                                                @endif
                                              @endforeach
                                            </td>
                                            @endif
                                            
                                        </tr>    
                                      @endforeach 
                                      
                                    </tbody>
                                  </table>

                          </div>
                        </div>  


                       
                  </div>
                </div>                
              </div>
            </div>
          </div>
      </div>
      <!-- App-content-body -->  
    </div>
    <!-- .col -->
</div>


<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">


<div id="uploadBerita" class="modal fade" role="dialog">
  <form action="{{url('/')}}/musrenbang/berita/tambah" method="POST" enctype="multipart/form-data">
    {{csrf_field()}}
    <input type="hidden" value="{{Auth::user()->RW_ID}}" name="rwid">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Upload Foto Berita Acara (.png/.jpg)</h4>
        </div>
       <div class="modal-body">
          <div class="form-group" id="div-upload-gambar">
            <label>Upload Foto Berita Acara</label>
            <div>
              <p style="color:red">
                *Maximum ukuran file 1mb
              </p>
              <input ui-jq="filestyle" name="image[]" type="file" data-icon="true" data-classbutton="btn btn-default" data-classinput="form-control inline v-middle input-s" id="filestyle-2" tabindex="-1" style="position: absolute; clip: rect(10px 10px 10px 10px);">
            </div>
          </div>
          <div class="form-group" up id="div1" style="display: none">
            <input ui-jq="filestyle" name="image[]" type="file" data-icon="true" data-classbutton="btn btn-default" data-classinput="form-control inline v-middle input-s" id="filestyle-1" tabindex="-1" style="position: absolute; clip: rect(10px 10px 10px 10px);">
          </div>
          <br>
          <div>
            <button class="btn btn-info" id="upload" type="button"><i class="fa fa-plus"> Tambah Gambar</i></button>
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-default">Simpan</button>
        </div>
      </div>
    </div>
  </form>
</div>



<div id="uploadFoto" class="modal fade" role="dialog">
  <form action="{{url('/')}}/musrenbang/berita/tambahGambar" method="POST" enctype="multipart/form-data">
    {{csrf_field()}}
    <input type="hidden" value="{{Auth::user()->RW_ID}}" name="rwid">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Upload Foto Berita Acara</h4>
        </div>
        <div class="modal-body">
          <div class="form-group" id="div-upload-gambar">
            <label>Upload Foto Rembuk Warga</label>
            <div>
              <p style="color:red">
                *Maximum ukuran file 1mb
              </p>
              <input ui-jq="filestyle" name="image1[]" type="file" data-icon="true" data-classbutton="btn btn-default" data-classinput="form-control inline v-middle input-s" id="filestyle-1-1" tabindex="-1" style="position: absolute; clip: rect(10px 10px 10px 10px);">
            </div>
          </div>
          <div class="form-group" up id="div11" style="display: none">
            <input ui-jq="filestyle" name="image1[]" type="file" data-icon="true" data-classbutton="btn btn-default" data-classinput="form-control inline v-middle input-s" id="filestyle-1-2" tabindex="-1" style="position: absolute; clip: rect(10px 10px 10px 10px);">
          </div>
          <br>
          <div>
            <button class="btn btn-info" id="upload1" type="button"><i class="fa fa-plus"> Tambah Gambar</i></button>
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-default">Simpan</button>
        </div>
      </div>
    </div>
  </form>
</div>

@endsection

@section('plugin')
<script>
    $(document).ready(function(){
        $("#upload").click(function(){
            $("#div1").fadeIn("fast");
            $("#upload").addClass("addGambar");
        });

        $("#upload1").click(function(){
            $("#div11").fadeIn("fast");
            $("#upload1").addClass("addGambar");
        });
    });
</script>
<script type="text/javascript">
$(document).ready(function(){
  $("#kecamatan").change(function(e, params){
    var kec  = $('#kecamatan').val();
    $('#kelurahan').find('option').remove().end().append('<option value="x">Kelurahan</option>');    
    $('#rw').find('option').remove().end().append('<option value="x">RW</option>');    
    resetTable();
    if(kec != 'x'){
      $.ajax({
        type  : "get",
        url   : "{{ url('/') }}/musrenbang/{{ $tahun }}/usulan/s/kelurahan/"+kec,
        success : function (data) {
          $('#kelurahan').append(data).trigger('chosen:updated');
        }
      });
    }
  });
  $("#kelurahan").change(function(e, params){
    var kel  = $('#kelurahan').val();
    $('#rw').find('option').remove().end().append('<option value="x">RW</option>');    
    resetTable();
    if(kel!='x'){
      $.ajax({
        type  : "get",
        url   : "{{ url('/') }}/musrenbang/{{ $tahun }}/usulan/s/rw/"+kel,
        success : function (data) {
          $('#rw').append(data).trigger('chosen:updated');
        }
      });
    }
  });
  $("#rw").change(function(e, params){
    resetTable();
  });
  $("#tipe").change(function(e, params){
    var tipe  = $('#tipe').val();
    $('#isu').find('option').remove().end().append('<option value="x">Isu</option>');    
    $('#kamus').find('option').remove().end().append('<option value="x">Kamus</option>');    
    resetTable();
    if(tipe!='x'){
      $.ajax({
        type  : "get",
        url   : "{{ url('/') }}/musrenbang/{{ $tahun }}/usulan/s/isu/"+tipe,
        success : function (data) {
          $('#isu').append(data).trigger('chosen:updated');
        }
      });
    }
  });
  $("#isu").change(function(e, params){
    var isu  = $('#isu').val();
    $('#kamus').find('option').remove().end().append('<option value="x">Kamus</option>');
    resetTable();
    if(isu!='x'){
      $.ajax({
        type  : "get",
        url   : "{{ url('/') }}/musrenbang/{{ $tahun }}/usulan/s/kamus/"+isu,
        success : function (data) {
          $('#kamus').append(data).trigger('chosen:updated');
        }
      });
    }
  });
  $("#kamus").change(function(e, params){
    resetTable();
  });

  function resetTable(){
    var kec  = $('#kecamatan').val();
    var kel  = $('#kelurahan').val();
    var rw   = $('#rw').val();
    var tipe = $('#tipe').val();
    var isu  = $('#isu').val();
    var kamus= $('#kamus').val();
    $('#table-usulan').DataTable().destroy();
    $('#table-usulan').DataTable({
          sAjaxSource: "/musrenbang/{{$tahun}}/usulan/getUsulan/"+tipe+"/"+kec+"/"+kel+"/"+rw+"/"+isu+"/"+kamus,
          aoColumns: [
            { mData: 'USULAN_ID',class:'hide' },
            { mData: 'NO',class:'text-center' },
            { mData: 'PENGUSUL' },
            { mData: 'TIPE' },
            { mData: 'URGENSI' },
            { mData: 'LOKASI' },
            { mData: 'ISU' },
            { mData: 'VOLUME' },
            { mData: 'TOTAL' },
            { mData: 'STATUS' }
          ]
        });
  }
});
</script>
@endsection



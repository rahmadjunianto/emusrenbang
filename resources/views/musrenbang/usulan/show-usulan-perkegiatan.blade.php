@extends('musrenbang.layout')

@section('content')
<div id="content" class="app-content" role="main">
  <div class="hbox hbox-auto-xs hbox-auto-sm ng-scope">
    <div class="col">
      <div class="app-content-body ">

          <div class="bg-light lter">    
              <ul class="breadcrumb bg-white m-b-none">
                <li><a href="#" class="btn no-shadow" ui-toggle-class="app-aside-folded" target=".app">
                  <i class="icon-bdg_expand1 text"></i>
                  <i class="icon-bdg_expand2 text-active"></i>
                </a>   </li>
                <li><a href= "{{ url('/') }}/main">Dashboard</a></li>
                <li><i class="fa fa-angle-right"></i>Rekap Usulan</li>   
                <li class="active"><i class="fa fa-angle-right"></i>@if($id==1) Renja @else PIPPK @endif</li>
                <li class="active"><i class="fa fa-angle-right"></i>{{$tahun}}</li>                                
              </ul>
          </div>
          <div class="wrapper-lg">
            <div class="row">
              <div class="col-sm-12">
                <div class="panel bg-white wrapper-lg" style="height: 165px;">
                  <div class="form-group col-sm-12">
                      <label class="col-sm-2">
                        @if(Auth::user()->level==8) 
                              SKPD
                            @else   
                              Pengusul
                            @endif </label>
                      <div class="col-sm-4">
                        <select ui-jq="chosen" class="filter w-full isu form-control" name="pengusul" id="pengusul">                                      
                          <option value="x" style="color:black">
                            @if(Auth::user()->level==8) 
                              SKPD
                            @else   
                              Pengusul
                            @endif  
                          </option>
                          @foreach($users as $u)
                           <option value="{{$u->id}}" style="color:black">{{$u->name}}</option>
                          @endforeach
                        </select>
                      </div>
                      <label class="col-sm-2">Kamus</label>
                      <div class="col-sm-4">
                        <select ui-jq="chosen" class="filter w-full isu form-control" name="kamus" id="kamus">                        
                          <option value="x" style="color:black">Kamus</option>
                          @foreach($kamus as $k)
                            <option value="{{$k->KAMUS_ID}}" style="color:black">{{$k->KAMUS_NAMA}}</option>
                          @endforeach
                        </select>
                      </div>
                  </div>                  
                  <div class="form-group col-sm-12">
                      <label class="col-sm-2">Isu</label>
                      <div class="col-sm-4">
                        <select ui-jq="chosen" class="filter w-full isu form-control" name="isu" id="isu">                                      
                          <option value="x" style="color:black">Isu</option>
                          @foreach($isu as $i)
                            <option value="{{$i->ISU_ID}}" style="color:black">{{$i->ISU_NAMA}}</option>
                          @endforeach
                        </select>
                      </div>
                      @if(Auth::user()->level == 7)
                      <label class="col-sm-2">Kecamatan</label>
                      <div class="col-sm-4">
                        <select ui-jq="chosen" class="w-full isu form-control" name="isu" id="kecamatan">                                      
                          <option value="x" style="color:black">Kecamatan</option>
                          @foreach($kecamatan as $k)
                            <option @if($k->KEC_ID == $kecid) selected @endif value="{{$k->KEC_ID}}" style="color:black">{{$k->KEC_NAMA}}</option>
                          @endforeach
                        </select>
                      </div>
                      @endif
                  </div>                 
                </div>  


              </div>
              <div class="col-md-12">
                <div class="panel bg-white">
                  <div class="wrapper-lg">
                    <h4>Rekap Usulan @if($id==1) Renja @else PIPPK @endif Tahun {{$tahun}}</h4>
                    <div class="row">
                      <div class="col-sm-10">
                       <div class="nav-tabs-alt tabs-alt-1 b-t three-row" id="tab-jurnal" >
                          <ul class="nav nav-tabs" role="tablist">
                             <li class="active">
                              <a data-target="#tab-1" role="tab" data-toggle="tab" data-status="1" class="buttonstatus">[{{$diproses}}] Di Proses <i class="fa fa-refresh text-info"></i></a>
                             </li>
                             <li>
                              <a data-target="#tab-2" role="tab" data-toggle="tab" data-status="2" class="buttonstatus">[{{$diterima}}] Di Terima <i class="fa fa-check text-success"></i></a>
                             </li>
                             <li>
                              <a data-target="#tab-3" role="tab" data-toggle="tab" data-status="0" class="buttonstatus">[{{$ditolak}}] DI Tolak <i class="fa fa-close text-danger"></i></a>
                             </li>
                          </ul>
                        </div> 
                      </div>    
                      <div class="col-sm-2 pull-right m-t-n-sm">

                        <a href="{{URL::to('/musrenbang/2017/export/usulan/'.$id)}}" id="export-to-excel" class="btn btn-success pull-right" style="margin-top:10px;"><i class="fa fa-back"></i> Export excel</a>
                        <select class="form-control dtSelect" id="dtSelect" style="margin-top:57px">
                              <option value="10">10</option>
                              <option value="25">25</option>
                              <option value="50">50</option>
                              <option value="100">100</option>
                          </select>

                      </div>
                   </div> 

                  </div>  

                  <div class="tab-content tab-content-alt-0 bg-dark-grey" id="tab_usulan">
                        <div role="tabpanel" class="active tab-pane" id="tab-1">  
                            <div class="table-responsive dataTables_wrapper">
                              <table id="table-usulan" ui-jq="dataTable" ui-options="
                                  {
                                    
                                    sAjaxSource: '{{ url('/') }}/musrenbang/{{$tahun}}/data/rekap/kelurahan',
                                    aoColumns: [
                                    { mData: 'kegiatanid',class:'hide' },
                                    { mData: 'no',class:'text-center' },
                                    { mData: 'aksi' },
                                    { mData: 'skpd' },
                                    { mData: 'progkeg' },
                                    { mData: 'isukamus' },
                                    { mData: 'status' },
                                    ],
                                    order: [[6, 'asc']]
                                  }" class="table table-usulan table-striped b-t b-b">
                              
                                    <thead>
                                      <tr>
                                        <th class="hide">#</th>
                                        <th>No</th>
                                        <th>Action</th>
                                        <th>SKPD</th>
                                        <th>Program / <font style='color:orange;'>Kegiatan</font></th>
                                        <th>Isu / <font style='color:orange'>Usulan</font></th>
                                        <th>Status</th>
                                      </tr>
                                      <tr>
                                        <th></th>
                                        @if(Auth::user()->level >= 5)

                                        <th colspan="6" class="th_search">

                                        @else
                                        <th colspan="6" class="th_search">
                                        @endif
                                            <i class="icon-bdg_search"></i>
                                            <input type="search" class="table-search form-control b-none w-full" placeholder="Cari" aria-controls="DataTables_Table_0">
                                        </th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                          <td colspan="2"> </td>
                                          <td colspan="3" style="text-align:right;"><b>Total Nominal : Rp. <text id="totalnominal"></text> </b></td>
                                        </tr>  
                                      </tfoot>
                                  </table>
                              </div>
                          </div>
                        </div>  

                  </div>
                </div>                
              </div>
            </div>
          </div>
      </div>
      <!-- App-content-body -->  
    </div>
    <!-- .col -->


<div class="plih-komponen modal fade" id="detail-usulan" tabindex="-1" role="dialog">
  <div class="modal-dialog bg-white modal-lg">
    <div class="panel panel-default">
      <div class="wrapper-lg">
        <h5 class="inline font-semibold text-orange m-n text16 "> Lokasi Usulan</h5>
      </div>
      <div class="table-responsive">
        <table class="table table-popup table-striped b-t b-b table-komponen" id="table-komponen">
          <thead>
            <tr>
              <th>No</th>
              <th>Pengusul</th>
              <th>Usulan</th>                          
              <th>Volume</th>                          
              <th>Harga</th>                          
              <th>Total</th>                          
              <th>Opsi</th>                          
            </tr>
            <tr>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>  
            <tr>
              <th colspan="7" class="th_search">
                <i class="icon-bdg_search"></i>
                <input type="search" id="cari-komponen" class="cari-komponen form-control b-none w-full" placeholder="Cari" aria-controls="DataTables_Table_0">
              </th>
            </tr>
          </thead>
          <tbody>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>


<!-- log -->
<div class="info modal fade" id="info" tabindex="-1" role="dialog">
  <div class="modal-dialog" style="width: 80%;">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h5 class="inline font-semibold text-orange m-n text16 ">Info Kegiatan</h5>
      </div>
      <div class="modal-body">
       <div class="row">
        <div class="col-sm-2">Dibuat Oleh</div>
        <div class="col-sm-4">: <span id="t1"></span></div>
        <div class="col-sm-2">Staff 1</div>
        <div class="col-sm-4">: <span id="t2"></span></div>
        <div class="col-sm-2">Waktu di buat</div>
        <div class="col-sm-4">: <span id="t3"></span></div>
        <div class="col-sm-2">Staff 2</div>
        <div class="col-sm-4">: <span id="t4"></span></div>
        <div class="col-sm-2">Update terkahir</div>
        <div class="col-sm-4">: <span id="updated"></span></div>
      </div>
      <div class="wrapper-lg">
       <div class="streamline b-l b-grey m-l-lg m-b padder-v" id="timeline-log">
      </div>                
    </div>
  </div>
</div>
</div>
</div>

</div>

@endsection

@section('plugin')
<script type="text/javascript">
var status = 1;
  
  function detail(id){
    $.ajax({
      type  : "get",
      url   : "{{ url('/') }}/musrenbang/{{ $tahun }}/data/get/usulan/"+id,
      success : function (data) {
          $('#t1').text(data['header']['USULAN_ID']);
          $('#t2').text(data['header']['KAMUS']);
          $('#t3').text(data['header']['VOLUME']);
          $('#t4').text(data['header']['HARGA']);
          $('#t5').text(data['header']['TOTAL']);
          $('#timeline-log').html(data['header']['log']);
          $('#info').modal('show');
          
      }
    });
  }

  /*$("#detail-usulan").click(function(e, params){
    alert('areief');
    var id  = $('#detail-usulan').val();
    //$('#pilih-komponen').attr('disabled',false);
    $('.table-komponen').DataTable().destroy();
    $('.table-komponen').DataTable({
      sAjaxSource: "{{ url('/') }}/musrenbang/{{ $tahun }}/data/get/usulan/"+id,
      aoColumns: [
      { mData: 'USULAN_ID',class:'hide' },
      { mData: 'PENGUSUL',class:'hide' },
      { mData: 'USULAN',class:'hide' },
      { mData: 'VOLUME',class:'hide' },
      { mData: 'HARGA',class:'hide' },
      { mData: 'TOTAL',class:'text-center' }
      ]
    });
  });*/


  $('.table-komponen').on('click','tbody > tr', function(){
    id        = $(this).children('td').eq(0).html();
    pengusul  = $(this).children('td').eq(1).html();
    usulan    = $(this).children('td').eq(2).html();
    volume    = $(this).children('td').eq(3).html();
    harga     = $(this).children('td').eq(4).html();
    total     = $(this).children('td').eq(5).html();
    $('#usulanid').val(id);
    $('#pengusul').val(pengusul);
    $('#usulan').val(usulan);
    $('#volume').val(volume);
    $('#harga').val(harga);
    $('#total').val(total);
  //  $('#satuan-1').find('option').remove().end().append('<option value="'+sat+'">'+sat+'</option>').trigger('chosen:updated');      
    $('#detail-usulan').modal('hide');
  });

  function changePriority(usulanid, kel_id){
    
    var priority = prompt("Silahkan masukkan nilai prioritas", "1");
    console.log(usulanid+'-'+kel_id+'-'+priority);
    $.ajax({
      'url' : '<?php echo URL::to('/'); ?>/musrenbang/usulan/set_prioritas/'+usulanid+'/'+kel_id+'/'+priority,
      'type' : 'GET',
      'success' : function(data) {
        console.log(data);
        $("#usulan-"+usulanid).html(priority);
      }
    });
    
  }

$(document).ready(function(){
  $(".buttonstatus").on("click",function(event) {

    status = $(this).data('status');
    
    resetTable();
  });

  $(".filter").on('change',function(e){
    resetTable();
  });

  $("#kecamatan").on('change',function(e){
    //resetTable();
    var kecamatan= $('#kecamatan').val();
    window.location = "{{ url('/') }}/musrenbang/2017/usulan/show/1/"+kecamatan;
  });

  function resetTable(){
    var pengusul   = $('#pengusul').val();
    var isu  = $('#isu').val();
    var kamus= $('#kamus').val();
    var kecamatan= $('#kecamatan').val();
    $('#table-usulan').DataTable().destroy();
    if(kecamatan == undefined || kecamatan == null)
    {
      kecamatan = "x"
    }
    $('#table-usulan').DataTable({
          sAjaxSource: "{{ url('/') }}/musrenbang/{{$tahun}}/data/usulan/{{$id}}/"+status+"/"+pengusul+"/"+isu+"/"+kamus+"/"+kecamatan,
           aoColumns: [
            { mData: 'usulanid',class:'hide' },
            { mData: 'no',class:'text-center' },
            { mData: 'aksi' },
            { mData: 'skpd' },
            { mData: 'kegiatan' },
            { mData: 'isukamus' },
            { mData: 'namapengusul' },
            { mData: 'status' },
          ],
          "order": [[7, "asc"]],
          initComplete:function(setting,json){
            $("#totalnominal").html(json.jumlah);

        }
    });
  }
});

function hapus(id){
    var token        = $('#token').val();
    $.confirm({
        title: 'Hapus Data!',
        content: 'Yakin hapus data?',
        buttons: {
            Ya: {
                btnClass: 'btn-danger',
                action: function(){
                  $.ajax({
                      url: "{{ url('/') }}/musrenbang/{{ $tahun }}/usulan/delete",
                      type: "POST",
                      data: {'_token'         : token,
                            'id'              : id},
                      success: function(msg){
                          $.alert(msg);
                          $('.table').DataTable().ajax.reload();
                        }
                  });
                }
            },
            Tidak: function () {
            }
        }
    });
  }


  function prioritas(id){
    var token        = $('#token').val();
    $.confirm({
        title: 'Hapus Data!',
        content: 'Yakin hapus data?',
        buttons: {
            Ya: {
                btnClass: 'btn-danger',
                action: function(){
                  $.ajax({
                      url: "{{ url('/') }}/musrenbang/{{ $tahun }}/usulan/delete",
                      type: "POST",
                      data: {'_token'         : token,
                            'id'              : id},
                      success: function(msg){
                          $.alert(msg);
                          $('.table').DataTable().ajax.reload();
                        }
                  });
                }
            },
            Tidak: function () {
            }
        }
    });
  }

  function cekbox(){
    var closestTr = $(':checkbox:checked').closest('tr').attr('id');
    alert(closestTr);
}
</script>
@endsection



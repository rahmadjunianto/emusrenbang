@extends('musrenbang.layout')

@section('content')
<div id="content" class="app-content" role="main">

  <div class="hbox hbox-auto-xs hbox-auto-sm ng-scope">
    <div class="col">
      <div class="app-content-body ">

        <div class="bg-light lter">    
          <ul class="breadcrumb bg-white m-b-none">
            <li>
              <a href="#" class="btn no-shadow" ui-toggle-class="app-aside-folded" target=".app">
                <i class="icon-bdg_expand2 text"></i> 
                <i class="icon-bdg_expand1 text-active"></i>
              </a>   
            </li>
            <li class="active"><i class="fa fa-angle-right"></i>Dashboard</li>                
          </ul>
        </div>

 

        <div class="wrapper-xl padder-bottom-none" style="height:600px; ">
        @if((Auth::user()->app == 1 && (Auth::user()->level == 1 )) && Auth::user()->active == 1 )
        <!-- tampilan baris ke satu  -->
          <div class="row">
            <div class="col-md-12 hvr-grow" >
              <div class="panel panel-default">
                  <div class="panel-body wrapper-sm">
                    <h3 class="m-t-xs text-orange font-semibold m-b-sm">Informasi Koordinasi</h3>
                    <div class="tree">
                        <ul>
                        <li>
                          <a href="#">TAPD</a>
                          <ul>
                            <li>
                              <a href="#" style="background-color: #FFEFD5; color:black">Kec.{{ Auth::user()->rw->kelurahan->kecamatan->KEC_NAMA }}</a>
                              <ul>
                                @foreach($keluargaKel as $kelkel)
                                  @if($kelkel->KEL_ID == Auth::user()->rw->kelurahan->KEL_ID )
                                  <li>
                                    <a href="#" style="background-color: #FFDAB9; color:black">Kel.{{$kelkel->KEL_NAMA}}</a>
                                    <ul>
                                      @foreach($keluargaRw as $kelrw)
                                        @if($kelrw->RW_NAMA == Auth::user()->rw->RW_NAMA )
                                         <li><a href="#" style="background-color: #CD853F; color:black">Rw.{{ $kelrw->RW_NAMA }}</a></li>
                                        @else
                                          <li><a href="#">Rw.{{ $kelrw->RW_NAMA }}</a></li>
                                        @endif
                                      @endforeach
                                    </ul>  
                                  </li>
                                  @endif
                                @endforeach
                                
                              </ul>
                            </li>
                          </ul>
                        </li>
                      </ul>
                    </div>
                  </div>
              </div>
            </div>
          </div> 

        <!-- tampilan baris ke dua  -->
          <div class="row">

                  <div class="col-md-6 hvr-grow" >
                    <div class="panel panel-default">
                      
                        <div class="panel-body wrapper-sm">
                          <h4 class="m-t-xs text-orange font-semibold m-b-sm">Proses tahapan pengusulan</h4>
                          <div class='list-group gallery' id="gallery-image">
                                <a class="thumbnail fancybox" rel="ligthbox" href="{{ url('/') }}/tahapan.png">
                                    <img class="img-responsive" alt="" src="{{ url('/') }}/tahapan.png" />
                                    <div class='text-right'>
                                        <small class='text-muted'>Info Tahapan</small>
                                    </div> <!-- text-right / end -->
                                </a>
                           </div> <!-- list-group / end -->
                        </div>
                    </div>
                  </div>

               
                    <div class="col-md-6">
                      <div class="panel panel-default" style="height: 290px;overflow: scroll;">
                        <div class="panel-body no-padder">
                          <div class="wrapper-lg m-t-n-sm">
                            <h5 class="inline font-semibold text-orange m-n ">Info Tahapan</h5>
                          </div>
                          <div class="tab-content tab-content-alt bg-white m-t-n-md">
                          <table class="table">
                            <tr>
                              <th>Tahapan</th>
                              <th>Tanggal</th>
                              <th>Status</th>
                            </tr>
                            <tr>
                              <td>Rembuk Rw</td>
                              @if(empty($tahapan1))
                              <td>-</td>
                              @else
                              <td>{{ date('d M',strtotime($tahapan1->TGL_AWAL)) }} - {{ date('d M',strtotime($tahapan1->TGL_AKHIR)) }}</td>
                              @endif
                              @if($t_rembuk == 0) <td class="text-info"><i class="fa fa-refresh"></i></td>
                              @elseif($t_rembuk == 1) <td class="text-success"><i class="fa fa-check"></i></td>
                              @else <td class="text-danger">-</td>
                              @endif
                            </tr>
                             <tr>
                              <td>Rembuk LKK</td>
                              @if(empty($tahapan5))
                              <td>-</td>
                              @else
                              <td>{{ date('d M',strtotime($tahapan5->TGL_AWAL)) }} - {{ date('d M',strtotime($tahapan5->TGL_AKHIR)) }}</td>
                              @endif
                              @if($t_pippk == 0) <td class="text-info"><i class="fa fa-refresh"></i></td>
                              @elseif($t_pippk == 1) <td class="text-success"><i class="fa fa-check"></i></td>
                              @else <td class="text-danger">-</td>
                              @endif
                            </tr>
                             <tr>
                              <td>Musrenbang Kel</td>
                              @if(empty($tahapan2))
                              <td>-</td>
                              @else
                              <td>{{ date('d M',strtotime($tahapan2->TGL_AWAL)) }} - {{ date('d M',strtotime($tahapan2->TGL_AKHIR)) }}</td>
                              @endif
                              @if($t_kel == 0) <td class="text-info"><i class="fa fa-refresh"></i></td>
                              @elseif($t_kel == 1) <td class="text-success"><i class="fa fa-check"></i></td>
                              @else <td class="text-danger">-</td>
                              @endif
                            </tr>
                             <tr>
                              <td>Musrenbang Kec</td>
                              @if(empty($tahapan3))
                              <td>-</td>
                              @else
                              <td>{{ date('d M',strtotime($tahapan3->TGL_AWAL)) }} - {{ date('d M',strtotime($tahapan3->TGL_AKHIR)) }}</td>
                              @endif
                              @if($t_kec == 0) <td class="text-info"><i class="fa fa-refresh"></i></td>
                              @elseif($t_kec == 1) <td class="text-success"><i class="fa fa-check"></i></td>
                              @else <td class="text-danger">-</td>
                              @endif
                            </tr>
                             <tr>
                              <td>Forum Gabungan</td>
                              @if(empty($tahapan4))
                              <td>-</td>
                              @else
                              <td>{{ date('d M',strtotime($tahapan4->TGL_AWAL)) }} - {{ date('d M',strtotime($tahapan4->TGL_AKHIR)) }}</td>
                              @endif
                              @if($t_skpd == 0) <td class="text-info"><i class="fa fa-refresh"></i></td>
                              @elseif($t_skpd == 1) <td class="text-success"><i class="fa fa-check"></i></td>
                              @else <td class="text-danger">-</td>
                              @endif
                            </tr>
                          </table>
                          </div>                  
                        </div>
                      </div>
                    </div>        
          </div>

        <!-- tampilan baris ke tiga -->   
        <!-- PIPPK -->
            <div class="col-md-12">
              <div class="panel panel-default">
                <div class="panel-body wrapper-sm">
                  <div class="table-responsive">
                    <table class="table table-bordered" style="background-color: white">
                      <tr>
                        <td colspan="10" align="center">
                          <b> Total usulan PIPPK Rw.{{ Auth::user()->rw->RW_NAMA }} Kel.{{ Auth::user()->rw->kelurahan->KEL_NAMA }} Kec.{{ Auth::user()->rw->kelurahan->kecamatan->KEC_NAMA }} melalui E-Musrenbang </b>
                        </td>
                      </tr>
                      <tr>
                          <td><b>Tahun</b></td>
                          <td><b>Total Semua Usulan</b></td>
                          <td><b>Usulan Tidak Lolos Validasi (tidak diterima)</b></td>
                          <td><b>Usulan Lolos Validasi </b></td>
                          <td><b>Usulan Masuk APBD</b></td>
                      </tr>  
                       <tr>
                          <td>{{$tahun}}</td>
                          <td>
                             <h5 class="m-t-xs text-orange font-semibold m-b-sm">
                              {{ number_format(
                                $jumPIPPKProses + $jumPIPPKTerima + $jumPIPPKTolak
                                ,0,',','.') }} / 
                              <span class="text-success"> 
                              Rp.{{ number_format(
                              $totPIPPKProses + $totPIPPKTerima + $totPIPPKTolak
                              ,0,',','.') }}
                              </span>
                              </h5>
                          </td>
                          <td>
                             <i class="fa fa-close text-danger"></i>
                             <h5 class="m-t-xs text-orange font-semibold m-b-sm">
                              {{ number_format( 
                                $jumPIPPKTolak
                                ,0,',','.') }} / <br>
                              <span class="text-success"> Rp.{{ number_format(
                                $totPIPPKTolak
                                ,0,',','.') }}
                              </span></h5>
                          </td>
                          <td>
                             <i class="fa fa-refresh text-info"></i>
                              <h5 class="m-t-xs text-orange font-semibold m-b-sm">
                              {{ number_format(
                                $jumPIPPKProses
                                ,0,',','.') }} / <br>
                              <span class="text-success"> Rp.{{ number_format(
                               $totPIPPKProses
                                ,0,',','.') }}
                              </span></h5>
                          </td>
                          <td>
                            <i class="fa fa-check text-success"></i>
                            <h5 class="m-t-xs text-orange font-semibold m-b-sm"> 
                            {{ number_format($jumPIPPKTerima,0,',','.') }} / 
                            <span class="text-success"> 
                            Rp.{{ number_format($totPIPPKTerima,0,',','.') }}
                            </span>
                            </h5>
                          </td>
                      </tr> 
                      <tr>
                          <td>2018</td>
                          <td>
                             <h5 class="m-t-xs text-orange font-semibold m-b-sm">
                              {{ number_format(
                                $jumPIPPKProses2018 + $jumPIPPKTerima2018 + $jumPIPPKTolak2018
                                ,0,',','.') }} / 
                              <span class="text-success"> 
                              Rp.{{ number_format(
                              $totPIPPKProses2018 + $totPIPPKTerima2018 + $totPIPPKTolak2018
                              ,0,',','.') }}
                              </span>
                              </h5>
                          </td>
                          <td>
                             <i class="fa fa-close text-danger"></i>
                             <h5 class="m-t-xs text-orange font-semibold m-b-sm">
                              {{ number_format( 
                                $jumPIPPKTolak2018
                                ,0,',','.') }} / <br>
                              <span class="text-success"> Rp.{{ number_format(
                                $totPIPPKTolak2018
                                ,0,',','.') }}
                              </span></h5>
                          </td>
                          <td>
                             <i class="fa fa-refresh text-info"></i>
                              <h5 class="m-t-xs text-orange font-semibold m-b-sm">
                              {{ number_format(
                                $jumPIPPKProses2018
                                ,0,',','.') }} / <br>
                              <span class="text-success"> Rp.{{ number_format(
                               $totPIPPKProses2018
                                ,0,',','.') }}
                              </span></h5>
                          </td>
                          <td>
                            <i class="fa fa-check text-success"></i>
                            <h5 class="m-t-xs text-orange font-semibold m-b-sm"> 
                            {{ number_format($jumPIPPKTerima2018,0,',','.') }} / 
                            <span class="text-success"> 
                            Rp.{{ number_format($totPIPPKTerima2018,0,',','.') }}
                            </span>
                            </h5>
                          </td>
                      </tr>      
                    </table>
                  </div>
                </div>
              </div>
            </div>

        <!-- tampilan baris ke empat -->
          <!-- RENJA -->
            <div class="col-md-12">
              <div class="panel panel-default">
                <div class="panel-body wrapper-sm">
                  <div class="table-responsive">
                    <table class="table table-bordered" style="background-color: white">
                      <tr>
                        <td colspan="10" align="center">
                        <b> Total usulan RENJA Rw.{{ Auth::user()->rw->RW_NAMA }} Kel.{{ Auth::user()->rw->kelurahan->KEL_NAMA }} Kec.{{ Auth::user()->rw->kelurahan->kecamatan->KEC_NAMA }} melalui E-Musrenbang </b>                        </td>
                      </tr>
                      <tr>
                          <td><b>Tahun</b></td>
                          <td><b>Total Semua Usulan</b></td>
                          <td><b>Usulan Tidak Lolos Validasi (tidak diterima)</b></td>
                          <td><b>Usulan Lolos Validasi </b></td>
                          <td><b>Usulan Masuk APBD</b></td>
                      </tr>  
                       <tr>
                          <td>{{$tahun}}</td>
                          <td>
                             <h5 class="m-t-xs text-orange font-semibold m-b-sm">
                              {{ number_format(
                                $jumRenProses + $jumRenTerima + $jumRenTolak
                                ,0,',','.') }} / 
                              <span class="text-success"> 
                              Rp.{{ number_format(
                              $totRenProses + $totRenTerima + $totRenTolak
                              ,0,',','.') }}
                              </span>
                              </h5>
                          </td>
                          <td>
                             <i class="fa fa-close text-danger"></i>
                             <h5 class="m-t-xs text-orange font-semibold m-b-sm">
                              {{ number_format( 
                                $jumRenTolak
                                ,0,',','.') }} / <br>
                              <span class="text-success"> Rp.{{ number_format(
                                $totRenTolak
                                ,0,',','.') }}
                              </span></h5>
                          </td>
                          <td>
                             <i class="fa fa-refresh text-info"></i>
                              <h5 class="m-t-xs text-orange font-semibold m-b-sm">
                              {{ number_format(
                                $jumRenProses
                                ,0,',','.') }} / <br>
                              <span class="text-success"> Rp.{{ number_format(
                               $totRenProses
                                ,0,',','.') }}
                              </span></h5>
                          </td>
                          <td>
                            <i class="fa fa-check text-success"></i>
                            <h5 class="m-t-xs text-orange font-semibold m-b-sm"> 
                            {{ number_format($jumRenTerima,0,',','.') }} / 
                            <span class="text-success"> 
                            Rp.{{ number_format($totRenTerima,0,',','.') }}
                            </span>
                            </h5>
                          </td>
                      </tr> 
                      <tr>
                          <td>2018</td>
                          <td>
                             <h5 class="m-t-xs text-orange font-semibold m-b-sm">
                              {{ number_format(
                                $jumRenProses2018 + $jumRenTerima2018 + $jumRenTolak2018
                                ,0,',','.') }} / 
                              <span class="text-success"> 
                              Rp.{{ number_format(
                                $totRenProses2018 + $totRenTerima2018 + $totRenTolak2018
                              ,0,',','.') }}
                              </span>
                              </h5>
                          </td>
                          <td>
                             <i class="fa fa-close text-danger"></i>
                             <h5 class="m-t-xs text-orange font-semibold m-b-sm">
                              {{ number_format( 
                                $jumRenTolak2018
                                ,0,',','.') }} / <br>
                              <span class="text-success"> Rp.{{ number_format(
                                $totRenTolak2018
                                ,0,',','.') }}
                              </span></h5>
                          </td>
                          <td>
                             <i class="fa fa-refresh text-info"></i>
                              <h5 class="m-t-xs text-orange font-semibold m-b-sm">
                              {{ number_format(
                                $jumRenProses2018
                                ,0,',','.') }} / <br>
                              <span class="text-success"> Rp.{{ number_format(
                               $totRenProses2018
                                ,0,',','.') }}
                              </span></h5>
                          </td>
                          <td>
                            <i class="fa fa-check text-success"></i>
                            <h5 class="m-t-xs text-orange font-semibold m-b-sm"> 
                            {{ number_format($jumRenTerima2018,0,',','.') }} / 
                            <span class="text-success"> 
                            Rp.{{ number_format($totRenTerima2018,0,',','.') }}
                            </span>
                            </h5>
                          </td>
                      </tr>  
                    </table>
                  </div>
                </div>
              </div>
            </div> 

        @endif   

       
           

      </div>
      </div>
    </div>
  </div>
</div>


<style type="text/css">
  .gallery
{
    display: inline-block;
    margin-top: 20px;
}
</style>


<style type="text/css">
  /*Now the CSS Created by R.S*/
* {margin: 0; padding: 0;}

.tree ul {
    padding-top: 20px; position: relative;
  
  transition: all 0.5s;
  -webkit-transition: all 0.5s;
  -moz-transition: all 0.5s;
}

.tree li {
  float: left; text-align: center;
  list-style-type: none;
  position: relative;
  padding: 20px 5px 0 5px;
  
  transition: all 0.5s;
  -webkit-transition: all 0.5s;
  -moz-transition: all 0.5s;
}

/*We will use ::before and ::after to draw the connectors*/

.tree li::before, .tree li::after{
  content: '';
  position: absolute; top: 0; right: 50%;
  border-top: 1px solid #ccc;
  width: 50%; height: 20px;
}
.tree li::after{
  right: auto; left: 50%;
  border-left: 1px solid #ccc;
}

/*We need to remove left-right connectors from elements without 
any siblings*/
.tree li:only-child::after, .tree li:only-child::before {
  display: none;
}

/*Remove space from the top of single children*/
.tree li:only-child{ padding-top: 0;}

/*Remove left connector from first child and 
right connector from last child*/
.tree li:first-child::before, .tree li:last-child::after{
  border: 0 none;
}
/*Adding back the vertical connector to the last nodes*/
.tree li:last-child::before{
  border-right: 1px solid #ccc;
  border-radius: 0 5px 0 0;
  -webkit-border-radius: 0 5px 0 0;
  -moz-border-radius: 0 5px 0 0;
}
.tree li:first-child::after{
  border-radius: 5px 0 0 0;
  -webkit-border-radius: 5px 0 0 0;
  -moz-border-radius: 5px 0 0 0;
}

/*Time to add downward connectors from parents*/
.tree ul ul::before{
  content: '';
  position: absolute; top: 0; left: 50%;
  border-left: 1px solid #ccc;
  width: 0; height: 20px;
}

.tree li a{
  border: 1px solid #ccc;
  padding: 5px 10px;
  text-decoration: none;
  color: #666;
  font-family: arial, verdana, tahoma;
  font-size: 11px;
  display: inline-block;
  
  border-radius: 5px;
  -webkit-border-radius: 5px;
  -moz-border-radius: 5px;
  
  transition: all 0.5s;
  -webkit-transition: all 0.5s;
  -moz-transition: all 0.5s;
}

/*Time for some hover effects*/
/*We will apply the hover effect the the lineage of the element also*/
.tree li a:hover, .tree li a:hover+ul li a {
  background: #c8e4f8; color: #000; border: 1px solid #94a0b4;
}
/*Connector styles on hover*/
.tree li a:hover+ul li::after, 
.tree li a:hover+ul li::before, 
.tree li a:hover+ul::before, 
.tree li a:hover+ul ul::before{
  border-color:  #94a0b4;
}

/*Thats all. I hope you enjoyed it.
Thanks :)*/
</style>

<style type="text/css">
#chartdiv {
  width: 100%;
  height: 500px;
}
#chartdiv1 {
  width: 100%;
  height: 500px;
}
</style>


@endsection

@section('plugin')


<!--Start of Zendesk Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="https://v2.zopim.com/?4XsJAVMVHAuWk8KVNd6RcwgTq6up8MOA";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zendesk Chat Script-->

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-91000808-1', 'auto');
  ga('send', 'pageview');

</script> 



<script type="text/javascript">
  $(document).ready(function(){
    //FANCYBOX
    //https://github.com/fancyapps/fancyBox
    $(".fancybox").fancybox({
        openEffect: "none",
        closeEffect: "none"
    });
});
</script>

<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css" media="screen">
<script src="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.js"></script>

@endsection
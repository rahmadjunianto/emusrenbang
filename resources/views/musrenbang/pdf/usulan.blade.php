<head>
	<style type="text/css">
		body
		{
			font-size: 10;
		}
		.table-bordered, .table-bordered > thead > tr > th
		,.table-bordered > tbody > tr > td,
		{
			border: 1px solid black;
			border-collapse: collapse;
		}

		.table-bordered > thead
		{
			text-align: center;
		}

		.table-bordered
		{
			table-layout: fixed;
		}

		.table-bordered, .table-bordered > thead > tr > th
		,.table-bordered > tbody > tr > td
		{
			word-wrap: break-word;
		}	
	</style>
</head>
<body>
	<center><b>LAMPIRAN II</b></center>
	<center><b>Daftar Urutan Kegiatan Prioritas Kecamatan</b></center>
	<center>Kecamatan .. , PD Penanggung Jawab : ..</center>
	<table align="center" class="table-bordered" width="100%">
		<thead>
			<tr>
				<th style="width: 5%">No</th>
				<th style="width: 17%">Lokasi/Nama Pengusul (Kecamatan/Kelurahan/RW)</th>
				<th style="width: 17%">Kegiatan/Isu</th>
				<th style="width: 15%">Urgensi</th>
				<th style="width: 6%">No Prioritas</th>
				<th style="width: 8%">Volume</th>
				<th style="width: 10%">Satuan</th>
				<th style="width: 10%">Harga Satuan (Rp)</th>
				<th style="width: 10%">Anggaran (Rp)</th>
				<th style="width: 10%">Keterangan</th>
			</tr>
			<tr>
				<th>1</th>
				<th>2</th>
				<th>3</th>
				<th>4</th>
				<th>5</th>
				<th>6</th>
				<th>7</th>
				<th>8</th>
				<th>9</th>
				<th>10</th>
			</tr>
		</thead>
	</table>
</body>
@extends('musrenbang.public.layout')

@section('content')
<div id="content" class="app-content" role="main">
  <div class="hbox hbox-auto-xs hbox-auto-sm ng-scope">
    <div class="col">
      <div class="app-content-body ">

          <div class="bg-light lter">    
              <ul class="breadcrumb bg-white m-b-none">
                <li><a href="#" class="btn no-shadow" ui-toggle-class="app-aside-folded" target=".app">
                  <i class="icon-bdg_expand1 text"></i>
                  <i class="icon-bdg_expand2 text-active"></i>
                </a>   </li>
                <li><a href= "{{ url('/') }}/main">Dashboard</a></li>
                <li class="active"><i class="fa fa-angle-right"></i>RW</li>                                
              </ul>
          </div>
          <div class="wrapper-lg">
            <div class="row">
              <div class="col-md-12">
                <div class="panel bg-white">
                  <div class="wrapper-lg">
                    <div class="col-sm-2 pull-right m-t-n-sm">
                      <select class="form-control dtSelect" id="dtSelect">
                            <option value="10">10</option>
                            <option value="25">25</option>
                            <option value="50">50</option>
                            <option value="100">100</option>
                            <option value="500">500</option>
                            <option value="1000">1000</option>
                        </select>
                    </div>
                    <h5 class="font-semibold text-orange m-n ">Daftar RW</h5>
                  </div>           
                  <div class="tab-content tab-content-alt-1 bg-white">
                        <div role="tabpanel" class="active tab-pane" id="tab-1">  
                            <div class="table-responsive dataTables_wrapper">
                             <table id="table-rw" ui-jq="dataTable" class="table table-striped b-t b-b" id="table-usulan">
                                    <thead>
                                      <tr>
                                        <th rowspan="2" class="hide"></th>
                                        <th rowspan="2">No</th>
                                        <th rowspan="2">Kecamatan</th>
                                        <th rowspan="2">Kelurahan</th>
                                        <th rowspan="2">No RW</th>
                                        <th rowspan="2">Username RW</th>
                                        <th rowspan="2">NIK Ketua RW</th>
                                        <th rowspan="2">Nama Ketua RW</th>
                                        <th rowspan="2">No Telp Ketua RW</th>
                                        <th rowspan="2">Jumlah RT</th>
                                        <th rowspan="2">Status Valid</th>
                                        <th colspan="2">PIPPK</th>
                                        <th colspan="2">RENJA</th>
                                        <th colspan="2">TOTAL</th>
                                        <th rowspan="2">Berita Acara Rembuk Warga</th>
                                      </tr>
                                      <tr>
                                        <th>Jumlah USULAN</th>
                                        <th>Nominal</th>
                                        <th>Jumlah USULAN</th>
                                        <th>Nominal</th>
                                        <th>Jumlah USULAN</th>
                                        <th>Nominal</th>
                                      </tr>
                                      <tr>
                                        <th></th>
                                        <th colspan="18" class="th_search">
                                            <i class="icon-bdg_search"></i>
                                            <input type="search" class="table-search form-control b-none w-full" placeholder="Cari Usulan" aria-controls="DataTables_Table_0">
                                        </th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                  </table>
                              </div>
                          </div>
                  </div>
                </div>                
              </div>
            </div>
          </div>
      </div>
      <!-- App-content-body -->  
    </div>
    <!-- .col -->
</div>

@endsection

@section('plugin')
<script type="text/javascript">
$(document).ready(function(){

});

$(window).on('load', function (e) {
  $('#table-rw').DataTable().destroy();
  $('#table-rw').DataTable(
    {
      'processing': true,
      'serverSide': true,
      'ajax'      : {
        'url' : '{{ url('/') }}/musrenbang/{{$tahun}}/public/kelurahan/getrwajax',
        'data': function (d){
          d.id = 'All';
        }
      },
      aoColumns: [
       { mData: 'RWID',class:'hide' },
       { mData: 'NO',class:'text-center' },
       { mData: 'KECAMATAN' },
       { mData: 'KELURAHAN' },
       { mData: 'RW' },
       { mData: 'USERNAME' },
       { mData: 'NIKKETUA' },
       { mData: 'NAMAKETUA' },
       { mData: 'TELP' },
       { mData: 'JUMLAHRT' },
       { mData: 'AKTIF' },
       { mData: 'PIPPK' },
       { mData: 'TOTALNOMINALPIPPK' },
       { mData: 'RENJA' },
       { mData: 'TOTALNOMINALRENJA' },
       { mData: 'totalusulan' },
       { mData: 'TOTALNOMINALUSULAN' },
       { mData: 'BERITA' },
      ],
    });
  $('#table-rw')
    .on('processing.dt', function ( e, settings, processing ) {
        if (processing) {
          if (! $('div.loading').length)
            $('<div class="loading">Loading&#8230;</div>').appendTo('body');
        }
        else $('div.loading').remove();
    });
});
</script>
@endsection



<table id="datatable{{$rwid}}" class="display" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th>No</th>
            <th>Kamus Usulan</th>
            <th>Isu</th>
            <th>Tipe</th>
            <th>Lokasi</th>
            <th>Keterangan</th>
            <th>Harga Satuan</th>
            <th>Volume</th>
            <th>Total Nominal</th>
            <th>Status</th>
            <th>Feedback</th>
        </tr>
    </thead>
    <tbody>
    <?php
    $no =1;
    foreach ($usulans as $row){
        $fullURL = "http://".$_SERVER['HTTP_HOST'].'/musrenbang/2017/public/usulan/detail/'.$row->USULAN_ID;
        $jumlahKomentar = "";
        $jumlahShare = "";

        if ($response){
            $jumlahKomentar = $response[$fullURL]["share"]["comment_count"];
            $jumlahShare    = $response[$fullURL]["share"]["share_count"];
        }

        $harga_satuan = $row->kamus()->first()->KAMUS_HARGA;
        $komentar_url = "/musrenbang/2017/public/usulan/detail/".$row->USULAN_ID."#Komentar";
    ?>
        <tr>
            <td>{{$no}}</td>
            <td>{{$row->kamus()->first()->KAMUS_NAMA}}</td>
            <td><?php echo $row->getHTMLIsuKamus(); ?></td>
            <td>{{$row->getIsuTipe()}}</td>
            <td>{{$row->ALAMAT}}</td>
            <td>{{$row->getStringKeterangan()}}</td>
            <td>{{$harga_satuan}}</td>
            <td>{{$row->USULAN_VOLUME}} {{$row->kamus()->first()->KAMUS_SATUAN}}</td>
            <td>{{$row->USULAN_VOLUME * $harga_satuan}}</td> 
            <td><?php echo App\Model\Usulan::toHtmlIconStatus($row->USULAN_STATUS,$row->USULAN_POSISI) ?></td>
            <td>
                <a href="#" onclick="sendLike({{$row->USULAN_ID}},'like'); return false;"><i class="fa fa-thumbs-up" aria-hidden="true"></i>{{$row->getLikes()->where('is_like',true)->count()}}</a> 
                <a href="#" onclick="sendLike({{$row->USULAN_ID}},'dislike'); return false;"><i class="fa fa-thumbs-down" aria-hidden="true"></i>{{$row->getLikes()->where('is_like',false)->count()}}</a>
                <a href="{{$komentar_url}}" target="_blank"><i class="fa fa-comments-o" aria-hidden="true"></i>{{$jumlahKomentar}}</a>
                <a href="#"><i class="fa fa-share-alt" aria-hidden="true"></i>{{$jumlahShare}}</a><br>
                <?php
                if (Session::get('facebook_user')){
                    $l = $row->getLikes()->where('oauth_uid',Session::get('facebook_user')['id'])->first();
                    if ($l) {
                        if ($l->is_like) echo "<font style='color:green'>Anda menyukai usulan ini</font>";
                        else echo "<font style='color:red'> Anda menolak usulan ini. <a href='".$komentar_url."' target='_blank'> Mengapa? </a></font>";
                    }
                }
                ?>
            </td>
        </tr>
    <?php
    $no++;
    }
    ?>
    </tbody>
</table>

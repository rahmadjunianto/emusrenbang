@extends('musrenbang.public.layout_accordion')

@section('content')
<div id="content" class="app-" role="main">

  <div class="hbox hbox-auto-xs hbox-auto-sm ng-scope">
    <div class="col">
      <div class="app-content-body ">

          <div class="bg-light lter">
              <ul class="breadcrumb bg-white m-b-none">
                <li><a href="#" class="btn no-shadow" ui-toggle-class="app-aside-folded" target=".app">
                  <!-- <i class="icon-bdg_expand1 text"></i> -->
                  <i class="icon-bdg_expand2 text-active"></i>
                </a>   </li>
                <li><a href= "{{ url('/') }}/musrenbang/2017">Dashboard</a></li>
                <li><i class="fa fa-angle-right"></i>Usulan</li>
                <li class="active"><i class="fa fa-angle-right"></i>Detail Usulan</li>
              </ul>
          </div>

          <div class="wrapper-lg bg-dark-grey">
            <div class="row">
              <div class="col-md-12">

                <div class="panel bg-white">

                  <div class="wrapper-lg">
                    <h5 class="inline font-semibold text-orange m-n ">
                    <?php
                    if ($rts != null){
                      ?>
                      Kelurahan {{ $rts->rw->kelurahan->KEL_NAMA }}, RW {{ $rts->rw->RW_NAMA }}
                    <?php
                    }
                    ?>$
                    </h5>
                   
                  </div>

                   <!-- Main tab -->
                       <div class="nav-tabs-alt tabs-alt-1 b-t four-row" id="tab-jurnal" >
                        <ul class="nav nav-tabs" role="tablist">
                           <li class="active">
                            <a data-target="#tab-1" role="tab" data-toggle="tab">RW</a>
                           </li>
                           <!-- <li>
                            <a data-target="#tab-2" role="tab" data-toggle="tab">KELURAHAN</a>
                           </li>
                           <li>
                            <a data-target="#tab-3" role="tab" data-toggle="tab">KECAMATAN</a>
                           </li>
                           <li>
                            <a data-target="#tab-4" role="tab" data-toggle="tab">KOTA</a>
                           </li>
                            <li>
                            <a data-target="#tab-5" role="tab" data-toggle="tab">APBD</a>
                           </li> -->

                        </ul>

                      </div>
                  <!-- / main tab -->

                  <div class="tab-content tab-content-alt-0 bg-dark-grey" id="tab_usulan">
                        <div role="tabpanel" class="active tab-pane" id="tab-1">
                          <div class="wrapper-sm col-sm-8">
                            <div class="panel bg-white">
                              <div class="panel-heading wrapper-lg">
                                <h5 class="inline font-semibold text-orange m-n ">Usulan</h5>
                              </div>
                              <div class="panel-body wrapper-lg">
                                <div class="form-group">
                                  <label class="col-sm-12">Isu : <span style="margin-left:20px;">{{ $rw->kamus->isu->ISU_NAMA }}</span></label>
                                </div>
                                <div class="form-group">
                                  <label class="col-sm-12">Kamus Usulan : <span style="margin-left:20px;">{{ $rw->kamus->KAMUS_NAMA }}</span></label>
                                </div>
                                
                                <div class="form-group">
                                  <label class="col-sm-12">Volume :  <span style="margin-left:20px;">{{ $rw->USULAN_VOLUME }} {{ $rw->kamus->KAMUS_SATUAN }}</span></label>
                                  
                                </div>
                                
                                <?php
                                if ($daftar_rt != null){
                                ?>
                                <div class="form-group">
                                  <label class="col-sm-12">RT Penerima Manfaat RT : <span style="margin-left:20px;">{{ $daftar_rt->implode('RT_NAMA', ', ') }}</span></label>
                                </div>
                                <?php
                                }
                                ?>

                                <div class="form-group">
                                  <label class="col-sm-12">Urgensi / Keterangan : <span style="margin-left:20px;">{{ $rw->USULAN_URGENSI }}</span></label>
                                </div>
                                 <div class="form-group">
                                  <label class="col-sm-12">Lokasi : <span style="margin-left:20px;">{{ $rw->ALAMAT }}</span></label>
                                </div>
                             
                              </div>
                            </div>
                          </div>
                          <div class="wrapper-sm col-sm-4">
                            <div class="panel bg-white">
                              <div class="panel-heading wrapper-lg">
                                <h5 class="inline font-semibold text-orange m-n ">Status</h5>
                              </div>
                              <div class="wrapper-lg">
                                <div class="streamline b-l m-b b-success">
                                  <div class="sl-item b-success b-l">
                                    <div class="m-l">
                                      <div class="text-muted">{{ $timeline->TIME_UPDATED }}</div>
                                      @if($timeline->USULAN_POSISI == 1 && $timeline->USULAN_STATUS == 2)
                                      <p class="text-grey"><i class="fa fa-check text-success"></i> Kelurahan </p>
                                      @elseif($timeline->USULAN_POSISI == 1 && $timeline->USULAN_STATUS == 0)
                                      <p class="text-grey"><i class="fa fa-close text-danger"></i> Kelurahan </p>
                                      <p>Alasan : {{ $rw->USULAN_ALASAN }}</p>
                                      @elseif($timeline->USULAN_POSISI == 1)
                                      <p class="text-grey"><i class="fa fa-refresh text-info"></i> Kelurahan </p>
                                      @elseif($timeline->USULAN_POSISI < 1)
                                      @else
                                      <p class="text-grey"><i class="fa fa-check text-success"></i> Kelurahan </p>
                                      @endif
                                    </div>
                                  </div>
                                  @if($timeline->USULAN_POSISI < 2)
                                  @else
                                  <div class="sl-item b-success b-l">
                                    <div class="m-l">
                                      @if($timeline->USULAN_POSISI == 2 && $timeline->USULAN_STATUS == 2)
                                      <p class="text-grey"><i class="fa fa-check text-success"></i> Kecamatan </p>
                                      @elseif($timeline->USULAN_POSISI <= 2 && $timeline->USULAN_STATUS == 0)
                                      <p class="text-grey"><i class="fa fa-close text-danger"></i> Kecamatan </p>
                                      <p>Alasan : {{ $rw->USULAN_ALASAN }}</p>
                                      @elseif($timeline->USULAN_POSISI == 2)
                                      <p class="text-grey"><i class="fa fa-refresh text-info"></i> Kecamatan </p>
                                      @else
                                      <p class="text-grey"><i class="fa fa-check text-success"></i> Kecamatan </p>
                                      @endif
                                    </div>
                                  </div>
                                  @endif
                                  @if($timeline->USULAN_POSISI < 3)
                                  @else
                                  <div class="sl-item b-success b-l">
                                    <div class="m-l">
                                      @if($timeline->USULAN_POSISI == 3 && $timeline->USULAN_STATUS == 2)
                                      <p class="text-grey"><i class="fa fa-check text-success"></i> Kota </p>
                                      @elseif($timeline->USULAN_POSISI <= 3 && $timeline->USULAN_STATUS == 0)
                                      <p class="text-grey"><i class="fa fa-close text-danger"></i> Kota </p>
                                      <p>Alasan : {{ $rw->USULAN_ALASAN }}</p>
                                      @elseif($timeline->USULAN_POSISI == 3)
                                      <p class="text-grey"><i class="fa fa-refresh text-info"></i> Kota </p>
                                      @else
                                      <p class="text-grey"><i class="fa fa-check text-success"></i> Kota </p>
                                      @endif
                                    </div>
                                  </div>
                                  @endif
                                  @if($timeline->USULAN_POSISI < 4)
                                  @else
                                  <div class="sl-item b-success b-l">
                                    <div class="m-l">
                                      @if($timeline->USULAN_POSISI == 4 && $timeline->USULAN_STATUS == 2)
                                      <p class="text-grey"><i class="fa fa-check text-success"></i> APBD </p>
                                      @elseif($timeline->USULAN_POSISI <= 4 && $timeline->USULAN_STATUS == 0)
                                      <p class="text-grey"><i class="fa fa-close text-danger"></i> APBD </p>
                                      <p>Alasan : {{ $rw->USULAN_ALASAN }}</p>
                                      @elseif($timeline->USULAN_POSISI == 4)
                                      <p class="text-grey"><i class="fa fa-refresh text-info"></i> APBD </p>
                                      @else
                                      <p class="text-grey"><i class="fa fa-check text-success"></i> APBD </p>
                                      @endif
                                    </div>
                                  </div>
                                  @endif
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="wrapper-sm col-sm-12" style="margin-top: -30px;">
                            <div class="panel bg-white">
                              <div class="panel-heading wrapper-lg">
                                <h5 class="inline font-semibold text-orange m-n ">Lokasi : {{ $rw->ALAMAT }}</h5>
                              </div>
                              <div class="wrapper-lg" id="map" style="height: 300px;">
                              </div>
                            </div>
                          </div>

                          <div class="wrapper-sm col-sm-12" style="margin-top: -30px;">
                            <div class="panel bg-white">
                              <div class="panel-heading wrapper-lg">
                                <h5 class="inline font-semibold text-orange m-n">Foto</h5>
                              </div>
                              <div class="wrapper-lg" style="height: : 300px;">
                                @foreach($rw->USULAN_GAMBAR as $img)
                                  <a href="{{ url('/') }}/uploads/{{ $img }}" target="_blank">
                                  <img src="{{ url('/') }}/uploads/{{ $img }}" style="max-height: 240px;">
                                  </a>
                                @endforeach
                              </div>
                            </div>
                          </div>
                          
                          <div class="wrapper-sm col-sm-12" style="margin-top: -30px;">
                            <div class="panel bg-white">
                              <div class="panel-heading wrapper-lg">
                                <h5 class="inline font-semibold text-orange m-n" id="Komentar">Komentar</h5>
                              </div>
                              <div class="wrapper-lg fb-comments" data-href="{{$pageURL}}" data-width="1000" data-numposts="5"></div>
                            </div>
                          </div>

                        </div>
                        <div role="tabpanel" class="tab-pane" id="tab-2">
                        @if(!empty($kel))
                          <div class="wrapper-sm col-sm-8">
                            <div class="panel bg-white">
                              <div class="panel-heading wrapper-lg">
                                <h5 class="inline font-semibold text-orange m-n ">Usulan</h5>
                              </div>
                              <div class="panel-body wrapper-lg">
                                <div class="form-group">
                                  <label class="col-sm-2">Isu</label>
                                  <label class="col-sm-10">: {{ $kel->kamus->isu->ISU_NAMA }}</label>
                                </div>
                                <div class="form-group">
                                  <label class="col-sm-2">Kamus Usulan</label>
                                  <label class="col-sm-10">: {{ $kel->kamus->KAMUS_NAMA }}</label>
                                </div>
                                <div class="form-group">
                                  <label class="col-sm-2">Urgensi</label>
                                  <label class="col-sm-10">: {{ $kel->USULAN_URGENSI }}</label>
                                </div>
                                <div class="form-group">
                                  <label class="col-sm-2">Volume</label>
                                  <label class="col-sm-10">: {{ $kel->USULAN_VOLUME }} {{ $kel->kamus->KAMUS_SATUAN }}</label>
                                </div>
                                
                              </div>
                            </div>
                          </div>
                          <div class="wrapper-sm col-sm-4">
                            <div class="panel bg-white">
                              <div class="panel-heading wrapper-lg">
                                <h5 class="inline font-semibold text-orange m-n ">Status</h5>
                              </div>
                              <div class="wrapper-lg">
                                <div class="streamline b-l m-b b-success">
                                  <div class="sl-item b-success b-l">
                                    <div class="m-l">
                                      <div class="text-muted">{{ $timeline->TIME_UPDATED }}</div>
                                      @if($timeline->USULAN_POSISI == 1 && $timeline->USULAN_STATUS == 2)
                                      <p class="text-grey"><i class="fa fa-check text-success"></i> Kelurahan </p>
                                      @elseif($timeline->USULAN_POSISI == 1 && $timeline->USULAN_STATUS == 0)
                                      <p class="text-grey"><i class="fa fa-close text-danger"></i> Kelurahan </p>
                                      <p>Alasan : {{ $rw->USULAN_ALASAN }}</p>
                                      @elseif($timeline->USULAN_POSISI == 1)
                                      <p class="text-grey"><i class="fa fa-refresh text-info"></i> Kelurahan </p>
                                      @elseif($timeline->USULAN_POSISI < 1)
                                      @else
                                      <p class="text-grey"><i class="fa fa-check text-success"></i> Kelurahan </p>
                                      @endif
                                    </div>
                                  </div>
                                  @if($timeline->USULAN_POSISI < 2)
                                  @else
                                  <div class="sl-item b-success b-l">
                                    <div class="m-l">
                                      @if($timeline->USULAN_POSISI == 2 && $timeline->USULAN_STATUS == 2)
                                      <p class="text-grey"><i class="fa fa-check text-success"></i> Kecamatan </p>
                                      @elseif($timeline->USULAN_POSISI <= 2 && $timeline->USULAN_STATUS == 0)
                                      <p class="text-grey"><i class="fa fa-close text-danger"></i> Kecamatan </p>
                                      <p>Alasan : {{ $rw->USULAN_ALASAN }}</p>
                                      @elseif($timeline->USULAN_POSISI == 2)
                                      <p class="text-grey"><i class="fa fa-refresh text-info"></i> Kecamatan </p>
                                      @else
                                      <p class="text-grey"><i class="fa fa-check text-success"></i> Kecamatan </p>
                                      @endif
                                    </div>
                                  </div>
                                  @endif
                                  @if($timeline->USULAN_POSISI < 3)
                                  @else
                                  <div class="sl-item b-success b-l">
                                    <div class="m-l">
                                      @if($timeline->USULAN_POSISI == 3 && $timeline->USULAN_STATUS == 2)
                                      <p class="text-grey"><i class="fa fa-check text-success"></i> Kota </p>
                                      @elseif($timeline->USULAN_POSISI <= 3 && $timeline->USULAN_STATUS == 0)
                                      <p class="text-grey"><i class="fa fa-close text-danger"></i> Kota </p>
                                      <p>Alasan : {{ $rw->USULAN_ALASAN }}</p>
                                      @elseif($timeline->USULAN_POSISI == 3)
                                      <p class="text-grey"><i class="fa fa-refresh text-info"></i> Kota </p>
                                      @else
                                      <p class="text-grey"><i class="fa fa-check text-success"></i> Kota </p>
                                      @endif
                                    </div>
                                  </div>
                                  @endif
                                  @if($timeline->USULAN_POSISI < 4)
                                  @else
                                  <div class="sl-item b-success b-l">
                                    <div class="m-l">
                                      @if($timeline->USULAN_POSISI == 4 && $timeline->USULAN_STATUS == 2)
                                      <p class="text-grey"><i class="fa fa-check text-success"></i> APBD </p>
                                      @elseif($timeline->USULAN_POSISI <= 4 && $timeline->USULAN_STATUS == 0)
                                      <p class="text-grey"><i class="fa fa-close text-danger"></i> APBD </p>
                                      <p>Alasan : {{ $rw->USULAN_ALASAN }}</p>
                                      @elseif($timeline->USULAN_POSISI == 4)
                                      <p class="text-grey"><i class="fa fa-refresh text-info"></i> APBD </p>
                                      @else
                                      <p class="text-grey"><i class="fa fa-check text-success"></i> APBD </p>
                                      @endif
                                    </div>
                                  </div>
                                  @endif
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="wrapper-sm col-sm-12" style="margin-top: -30px;">
                            <div class="panel bg-white">
                              <div class="panel-heading wrapper-lg">
                                <h5 class="inline font-semibold text-orange m-n ">Lokasi : </h5>
                              </div>
                              <div class="wrapper-lg" id="map-kel" style="height: 300px;">
                              </div>
                            </div>
                          </div>

                          <div class="wrapper-sm col-sm-12" style="margin-top: -30px;">
                            <div class="panel bg-white">
                              <div class="panel-heading wrapper-lg">
                                <h5 class="inline font-semibold text-orange m-n">Foto</h5>
                              </div>
                              <div class="wrapper-lg" style="height: : 300px;">
                              <?php 
                              if ($kel->USULAN_GAMBAR) {
                              ?>
                                @if(!empty(explode('#',$rw->USULAN_GAMBAR)[0]))
                                <img src="{{ url('/') }}/uploads/{{ explode('#',$kel->USULAN_GAMBAR)[0] }}" style="max-height: 240px;">
                                @endif
                                
                                @if(!empty(explode('#',$rw->USULAN_GAMBAR)[1]))
                                <img src="{{ url('/') }}/uploads/{{ explode('#',$kel->USULAN_GAMBAR)[1] }}" style="max-height: 240px;">
                                @endif
                              <?php
                              }
                              ?>
                              </div>
                            </div>
                          </div>

                          <div class="wrapper-sm col-sm-12" style="margin-top: -30px;">
                            <div class="panel bg-white">
                              <div class="panel-heading wrapper-lg">
                                <h5 class="inline font-semibold text-orange m-n" id="Komentar">Komentar</h5>
                              </div>
                              <div class="wrapper-lg fb-comments" data-href="{{$pageURL}}" data-width="1000" data-numposts="5"></div>
                            </div>
                          </div>

                        @else
                        @endif
                        
                        </div>
                        <div role="tabpanel" class="tab-pane" id="tab-3">
                        @if(!empty($kec))
                          <div class="wrapper-sm col-sm-8">
                            <div class="panel bg-white">
                              <div class="panel-heading wrapper-lg">
                                <h5 class="inline font-semibold text-orange m-n ">Usulan</h5>
                              </div>
                              <div class="panel-body wrapper-lg">
                                <div class="form-group">
                                  <label class="col-sm-2">Isu</label>
                                  <label class="col-sm-10">: {{ $kec->kamus->isu->ISU_NAMA }}</label>
                                </div>
                                <div class="form-group">
                                  <label class="col-sm-2">Urgensi</label>
                                  <label class="col-sm-10">: {{ $kec->USULAN_URGENSI }}</label>
                                </div>
                                <div class="form-group">
                                  <label class="col-sm-2">Volume</label>
                                  <label class="col-sm-10">: {{ $kec->USULAN_VOLUME }}</label>
                                </div>
                                
                              </div>
                            </div>
                          </div>
                          <div class="wrapper-sm col-sm-4">
                            <div class="panel bg-white">
                              <div class="panel-heading wrapper-lg">
                                <h5 class="inline font-semibold text-orange m-n ">Status</h5>
                              </div>
                              <div class="wrapper-lg">
                                <div class="streamline b-l m-b b-success">
                                  <div class="sl-item b-success b-l">
                                    <div class="m-l">
                                      <div class="text-muted">{{ $timeline->TIME_UPDATED }}</div>
                                      @if($timeline->USULAN_POSISI == 1 && $timeline->USULAN_STATUS == 2)
                                      <p class="text-grey"><i class="fa fa-check text-success"></i> Kelurahan </p>
                                      @elseif($timeline->USULAN_POSISI == 1 && $timeline->USULAN_STATUS == 0)
                                      <p class="text-grey"><i class="fa fa-close text-danger"></i> Kelurahan </p>
                                      <p>Alasan : {{ $rw->USULAN_ALASAN }}</p>
                                      @elseif($timeline->USULAN_POSISI == 1)
                                      <p class="text-grey"><i class="fa fa-refresh text-info"></i> Kelurahan </p>
                                      @elseif($timeline->USULAN_POSISI < 1)
                                      @else
                                      <p class="text-grey"><i class="fa fa-check text-success"></i> Kelurahan </p>
                                      @endif
                                    </div>
                                  </div>
                                  @if($timeline->USULAN_POSISI < 2)
                                  @else
                                  <div class="sl-item b-success b-l">
                                    <div class="m-l">
                                      @if($timeline->USULAN_POSISI == 2 && $timeline->USULAN_STATUS == 2)
                                      <p class="text-grey"><i class="fa fa-check text-success"></i> Kecamatan </p>
                                      @elseif($timeline->USULAN_POSISI <= 2 && $timeline->USULAN_STATUS == 0)
                                      <p class="text-grey"><i class="fa fa-close text-danger"></i> Kecamatan </p>
                                      <p>Alasan : {{ $rw->USULAN_ALASAN }}</p>
                                      @elseif($timeline->USULAN_POSISI == 2)
                                      <p class="text-grey"><i class="fa fa-refresh text-info"></i> Kecamatan </p>
                                      @else
                                      <p class="text-grey"><i class="fa fa-check text-success"></i> Kecamatan </p>
                                      @endif
                                    </div>
                                  </div>
                                  @endif
                                  @if($timeline->USULAN_POSISI < 3)
                                  @else
                                  <div class="sl-item b-success b-l">
                                    <div class="m-l">
                                      @if($timeline->USULAN_POSISI == 3 && $timeline->USULAN_STATUS == 2)
                                      <p class="text-grey"><i class="fa fa-check text-success"></i> Kota </p>
                                      @elseif($timeline->USULAN_POSISI <= 3 && $timeline->USULAN_STATUS == 0)
                                      <p class="text-grey"><i class="fa fa-close text-danger"></i> Kota </p>
                                      <p>Alasan : {{ $rw->USULAN_ALASAN }}</p>
                                      @elseif($timeline->USULAN_POSISI == 3)
                                      <p class="text-grey"><i class="fa fa-refresh text-info"></i> Kota </p>
                                      @else
                                      <p class="text-grey"><i class="fa fa-check text-success"></i> Kota </p>
                                      @endif
                                    </div>
                                  </div>
                                  @endif
                                  @if($timeline->USULAN_POSISI < 4)
                                  @else
                                  <div class="sl-item b-success b-l">
                                    <div class="m-l">
                                      @if($timeline->USULAN_POSISI == 4 && $timeline->USULAN_STATUS == 2)
                                      <p class="text-grey"><i class="fa fa-check text-success"></i> APBD </p>
                                      @elseif($timeline->USULAN_POSISI <= 4 && $timeline->USULAN_STATUS == 0)
                                      <p class="text-grey"><i class="fa fa-close text-danger"></i> APBD </p>
                                      <p>Alasan : {{ $rw->USULAN_ALASAN }}</p>
                                      @elseif($timeline->USULAN_POSISI == 4)
                                      <p class="text-grey"><i class="fa fa-refresh text-info"></i> APBD </p>
                                      @else
                                      <p class="text-grey"><i class="fa fa-check text-success"></i> APBD </p>
                                      @endif
                                    </div>
                                  </div>
                                  @endif
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="wrapper-sm col-sm-12" style="margin-top: -30px;">
                            <div class="panel bg-white">
                              <div class="panel-heading wrapper-lg">
                                <h5 class="inline font-semibold text-orange m-n ">Lokasi : Kec A Kel B RW 01 RT 01</h5>
                              </div>
                              <div class="wrapper-lg" id="map-kec" style="height: 300px;">
                              </div>
                            </div>
                          </div>

                          <div class="wrapper-sm col-sm-12" style="margin-top: -30px;">
                            <div class="panel bg-white">
                              <div class="panel-heading wrapper-lg">
                                <h5 class="inline font-semibold text-orange m-n">Foto</h5>
                              </div>
                              <div class="wrapper-lg" style="height: : 300px;">
                                <img src="{{ url('/') }}/uploads/{{ explode('#',$kec->USULAN_GAMBAR)[0] }}" style="max-height: 240px;">
                                @if(!empty(explode('#',$kec->USULAN_GAMBAR)[1]))
                                <img src="{{ url('/') }}/uploads/{{ explode('#',$kec->USULAN_GAMBAR)[1] }}" style="max-height: 240px;">
                                @endif
                              </div>
                            </div>
                          </div>

                          <div class="wrapper-sm col-sm-12" style="margin-top: -30px;">
                            <div class="panel bg-white">
                              <div class="panel-heading wrapper-lg">
                                <h5 class="inline font-semibold text-orange m-n" id="Komentar">Komentar</h5>
                              </div>
                              <div class="wrapper-lg fb-comments" data-href="{{$pageURL}}" data-width="1000" data-numposts="5"></div>
                            </div>
                          </div>

                        @else
                        @endif
                        
                        </div>
                        <div role="tabpanel" class="tab-pane" id="tab-4">
                        @if(!empty($kota))
                          <div class="wrapper-sm col-sm-8">
                            <div class="panel bg-white">
                              <div class="panel-heading wrapper-lg">
                                <h5 class="inline font-semibold text-orange m-n ">Usulan</h5>
                              </div>
                              <div class="panel-body wrapper-lg">
                                <div class="form-group">
                                  <label class="col-sm-2">Isu</label>
                                  <label class="col-sm-10">: {{ $kota->kamus->isu->ISU_NAMA }}</label>
                                </div>
                                <div class="form-group">
                                  <label class="col-sm-2">Urgensi</label>
                                  <label class="col-sm-10">: {{ $kota->USULAN_URGENSI }}</label>
                                </div>
                                <div class="form-group">
                                  <label class="col-sm-2">Volume</label>
                                  <label class="col-sm-10">: {{ $kota->USULAN_VOLUME }}</label>
                                </div>
                              
                              </div>
                            </div>
                          </div>
                          <div class="wrapper-sm col-sm-4">
                            <div class="panel bg-white">
                              <div class="panel-heading wrapper-lg">
                                <h5 class="inline font-semibold text-orange m-n ">Status</h5>
                              </div>
                              <div class="wrapper-lg">
                                <div class="streamline b-l m-b b-success">
                                  <div class="sl-item b-success b-l">
                                    <div class="m-l">
                                      <div class="text-muted">{{ $timeline->TIME_UPDATED }}</div>
                                      @if($timeline->USULAN_POSISI == 1 && $timeline->USULAN_STATUS == 2)
                                      <p class="text-grey"><i class="fa fa-check text-success"></i> Kelurahan </p>
                                      @elseif($timeline->USULAN_POSISI == 1 && $timeline->USULAN_STATUS == 0)
                                      <p class="text-grey"><i class="fa fa-close text-danger"></i> Kelurahan </p>
                                      <p>Alasan : {{ $rw->USULAN_ALASAN }}</p>
                                      @elseif($timeline->USULAN_POSISI == 1)
                                      <p class="text-grey"><i class="fa fa-refresh text-info"></i> Kelurahan </p>
                                      @elseif($timeline->USULAN_POSISI < 1)
                                      @else
                                      <p class="text-grey"><i class="fa fa-check text-success"></i> Kelurahan </p>
                                      @endif
                                    </div>
                                  </div>
                                  @if($timeline->USULAN_POSISI < 2)
                                  @else
                                  <div class="sl-item b-success b-l">
                                    <div class="m-l">
                                      @if($timeline->USULAN_POSISI == 2 && $timeline->USULAN_STATUS == 2)
                                      <p class="text-grey"><i class="fa fa-check text-success"></i> Kecamatan </p>
                                      @elseif($timeline->USULAN_POSISI <= 2 && $timeline->USULAN_STATUS == 0)
                                      <p class="text-grey"><i class="fa fa-close text-danger"></i> Kecamatan </p>
                                      <p>Alasan : {{ $rw->USULAN_ALASAN }}</p>
                                      @elseif($timeline->USULAN_POSISI == 2)
                                      <p class="text-grey"><i class="fa fa-refresh text-info"></i> Kecamatan </p>
                                      @else
                                      <p class="text-grey"><i class="fa fa-check text-success"></i> Kecamatan </p>
                                      @endif
                                    </div>
                                  </div>
                                  @endif
                                  @if($timeline->USULAN_POSISI < 3)
                                  @else
                                  <div class="sl-item b-success b-l">
                                    <div class="m-l">
                                      @if($timeline->USULAN_POSISI == 3 && $timeline->USULAN_STATUS == 2)
                                      <p class="text-grey"><i class="fa fa-check text-success"></i> Kota </p>
                                      @elseif($timeline->USULAN_POSISI <= 3 && $timeline->USULAN_STATUS == 0)
                                      <p class="text-grey"><i class="fa fa-close text-danger"></i> Kota </p>
                                      <p>Alasan : {{ $rw->USULAN_ALASAN }}</p>
                                      @elseif($timeline->USULAN_POSISI == 3)
                                      <p class="text-grey"><i class="fa fa-refresh text-info"></i> Kota </p>
                                      @else
                                      <p class="text-grey"><i class="fa fa-check text-success"></i> Kota </p>
                                      @endif
                                    </div>
                                  </div>
                                  @endif
                                  @if($timeline->USULAN_POSISI < 4)
                                  @else
                                  <div class="sl-item b-success b-l">
                                    <div class="m-l">
                                      @if($timeline->USULAN_POSISI == 4 && $timeline->USULAN_STATUS == 2)
                                      <p class="text-grey"><i class="fa fa-check text-success"></i> APBD </p>
                                      @elseif($timeline->USULAN_POSISI <= 4 && $timeline->USULAN_STATUS == 0)
                                      <p class="text-grey"><i class="fa fa-close text-danger"></i> APBD </p>
                                      <p>Alasan : {{ $rw->USULAN_ALASAN }}</p>
                                      @elseif($timeline->USULAN_POSISI == 4)
                                      <p class="text-grey"><i class="fa fa-refresh text-info"></i> APBD </p>
                                      @else
                                      <p class="text-grey"><i class="fa fa-check text-success"></i> APBD </p>
                                      @endif
                                    </div>
                                  </div>
                                  @endif
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="wrapper-sm col-sm-12" style="margin-top: -30px;">
                            <div class="panel bg-white">
                              <div class="panel-heading wrapper-lg">
                                <h5 class="inline font-semibold text-orange m-n ">Lokasi : Kec A Kel B RW 01 RT 01</h5>
                              </div>
                              <div class="wrapper-lg" id="map-kota" style="height: 300px;">
                              </div>
                            </div>
                          </div>

                          <div class="wrapper-sm col-sm-12" style="margin-top: -30px;">
                            <div class="panel bg-white">
                              <div class="panel-heading wrapper-lg">
                                <h5 class="inline font-semibold text-orange m-n">Foto</h5>
                              </div>
                              <div class="wrapper-lg" style="height: : 300px;">
                                <img src="{{ url('/') }}/uploads/{{ explode('#',$kota->USULAN_GAMBAR)[0] }}" style="max-height: 240px;">
                                @if(!empty(explode('#',$rw->USULAN_GAMBAR)[1]))
                                <img src="{{ url('/') }}/uploads/{{ explode('#',$kota->USULAN_GAMBAR)[1] }}" style="max-height: 240px;">
                                @endif
                              </div>
                            </div>
                          </div>

                          <div class="wrapper-sm col-sm-12" style="margin-top: -30px;">
                            <div class="panel bg-white">
                              <div class="panel-heading wrapper-lg">
                                <h5 class="inline font-semibold text-orange m-n" id="Komentar">Komentar</h5>
                              </div>
                              <div class="wrapper-lg fb-comments" data-href="{{$pageURL}}" data-width="1000" data-numposts="5"></div>
                            </div>
                          </div>

                        @else
                        @endif
                        </div>
                        <div role="tabpanel" class="tab-pane" id="tab-5">
                        @if(!empty($apbd))
                          <div class="wrapper-sm col-sm-8">
                            <div class="panel bg-white">
                              <div class="panel-heading wrapper-lg">
                                <h5 class="inline font-semibold text-orange m-n ">Usulan</h5>
                              </div>
                              <div class="panel-body wrapper-lg">
                                <div class="form-group">
                                  <label class="col-sm-2">Isu</label>
                                  <label class="col-sm-10">: {{ $apbd->kamus->isu->ISU_NAMA }}</label>
                                </div>
                                <div class="form-group">
                                  <label class="col-sm-2">Urgensi</label>
                                  <label class="col-sm-10">: {{ $apbd->USULAN_URGENSI }}</label>
                                </div>
                                <div class="form-group">
                                  <label class="col-sm-2">Volume</label>
                                  <label class="col-sm-10">: {{ $apbd->USULAN_VOLUME }}</label>
                                </div>
                             
                              </div>
                            </div>
                          </div>
                          <div class="wrapper-sm col-sm-4">
                            <div class="panel bg-white">
                              <div class="panel-heading wrapper-lg">
                                <h5 class="inline font-semibold text-orange m-n ">Status</h5>
                              </div>
                              <div class="wrapper-lg">
                                <div class="streamline b-l m-b b-success">
                                  <div class="sl-item b-success b-l">
                                    <div class="m-l">
                                      <div class="text-muted">{{ $timeline->TIME_UPDATED }}</div>
                                      @if($timeline->USULAN_POSISI == 1 && $timeline->USULAN_STATUS == 2)
                                      <p class="text-grey"><i class="fa fa-check text-success"></i> Kelurahan </p>
                                      @elseif($timeline->USULAN_POSISI == 1 && $timeline->USULAN_STATUS == 0)
                                      <p class="text-grey"><i class="fa fa-close text-danger"></i> Kelurahan </p>
                                      <p>Alasan : {{ $rw->USULAN_ALASAN }}</p>
                                      @elseif($timeline->USULAN_POSISI == 1)
                                      <p class="text-grey"><i class="fa fa-refresh text-info"></i> Kelurahan </p>
                                      @elseif($timeline->USULAN_POSISI < 1)
                                      @else
                                      <p class="text-grey"><i class="fa fa-check text-success"></i> Kelurahan </p>
                                      @endif
                                    </div>
                                  </div>
                                  @if($timeline->USULAN_POSISI < 2)
                                  @else
                                  <div class="sl-item b-success b-l">
                                    <div class="m-l">
                                      @if($timeline->USULAN_POSISI == 2 && $timeline->USULAN_STATUS == 2)
                                      <p class="text-grey"><i class="fa fa-check text-success"></i> Kecamatan </p>
                                      @elseif($timeline->USULAN_POSISI <= 2 && $timeline->USULAN_STATUS == 0)
                                      <p class="text-grey"><i class="fa fa-close text-danger"></i> Kecamatan </p>
                                      <p>Alasan : {{ $rw->USULAN_ALASAN }}</p>
                                      @elseif($timeline->USULAN_POSISI == 2)
                                      <p class="text-grey"><i class="fa fa-refresh text-info"></i> Kecamatan </p>
                                      @else
                                      <p class="text-grey"><i class="fa fa-check text-success"></i> Kecamatan </p>
                                      @endif
                                    </div>
                                  </div>
                                  @endif
                                  @if($timeline->USULAN_POSISI < 3)
                                  @else
                                  <div class="sl-item b-success b-l">
                                    <div class="m-l">
                                      @if($timeline->USULAN_POSISI == 3 && $timeline->USULAN_STATUS == 2)
                                      <p class="text-grey"><i class="fa fa-check text-success"></i> Kota </p>
                                      @elseif($timeline->USULAN_POSISI <= 3 && $timeline->USULAN_STATUS == 0)
                                      <p class="text-grey"><i class="fa fa-close text-danger"></i> Kota </p>
                                      <p>Alasan : {{ $rw->USULAN_ALASAN }}</p>
                                      @elseif($timeline->USULAN_POSISI == 3)
                                      <p class="text-grey"><i class="fa fa-refresh text-info"></i> Kota </p>
                                      @else
                                      <p class="text-grey"><i class="fa fa-check text-success"></i> Kota </p>
                                      @endif
                                    </div>
                                  </div>
                                  @endif
                                  @if($timeline->USULAN_POSISI < 4)
                                  @else
                                  <div class="sl-item b-success b-l">
                                    <div class="m-l">
                                      @if($timeline->USULAN_POSISI == 4 && $timeline->USULAN_STATUS == 2)
                                      <p class="text-grey"><i class="fa fa-check text-success"></i> APBD </p>
                                      @elseif($timeline->USULAN_POSISI <= 4 && $timeline->USULAN_STATUS == 0)
                                      <p class="text-grey"><i class="fa fa-close text-danger"></i> APBD </p>
                                      <p>Alasan : {{ $rw->USULAN_ALASAN }}</p>
                                      @elseif($timeline->USULAN_POSISI == 4)
                                      <p class="text-grey"><i class="fa fa-refresh text-info"></i> APBD </p>
                                      @else
                                      <p class="text-grey"><i class="fa fa-check text-success"></i> APBD </p>
                                      @endif
                                    </div>
                                  </div>
                                  @endif
                                </div>
                              </div>
                            </div>
                          </div>

                          <div class="wrapper-sm col-sm-12" style="margin-top: -30px;">
                            <div class="panel bg-white">
                              <div class="panel-heading wrapper-lg">
                                <h5 class="inline font-semibold text-orange m-n ">Lokasi : Kec A Kel B RW 01 RT 01</h5>
                              </div>
                              <div class="wrapper-lg" id="map-kota" style="height: 300px;">
                              </div>
                            </div>
                          </div>

                          <div class="wrapper-sm col-sm-12" style="margin-top: -30px;">
                            <div class="panel bg-white">
                              <div class="panel-heading wrapper-lg">
                                <h5 class="inline font-semibold text-orange m-n">Foto</h5>
                              </div>
                              <div class="wrapper-lg" style="height: : 300px;">
                                <img src="{{ url('/') }}/uploads/{{ explode('#',$apbd->USULAN_GAMBAR)[0] }}" style="max-height: 240px;">
                                @if(!empty(explode('#',$rw->USULAN_GAMBAR)[1]))
                                <img src="{{ url('/') }}/uploads/{{ explode('#',$apbd->USULAN_GAMBAR)[1] }}" style="max-height: 240px;">                                
                                @endif
                              </div>
                            </div>
                          </div>

                          <div class="wrapper-sm col-sm-12" style="margin-top: -30px;">
                            <div class="panel bg-white">
                              <div class="panel-heading wrapper-lg">
                                <h5 class="inline font-semibold text-orange m-n" id="Komentar">Komentar</h5>
                              </div>
                             
                                <div class="wrapper-lg fb-comments" data-href="{{$pageURL}}" data-width="1000" data-numposts="5"></div>
                             
                            </div>
                          </div>

                        @else
                        @endif
                        </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
      </div>
      <!-- App-content-body -->
    </div>
    <!-- .col -->
    </div>
</div>
<div id="form-tolak" class="modal fade" role="dialog">
<form method="post" action="{{ url('/') }}/musrenbang/{{ $tahun }}/usulan/tolak">
{{ csrf_field() }}
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Tolak Usulan</h4>
      </div>
      <div class="modal-body">
      <div class="form-group">
          <label>Alasan : </label>
          <input type="hidden" name="usulan_id" value="{{ $rw->USULAN_ID }}">
          <textarea class="form-control" rows="6" placeholder="Isi Alasan" name="alasan" required></textarea>
      </div>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-danger">Tolak</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
      </div>
    </div>
  </div>
</form>
</div>
@endsection


@section('plugin')
    <script>
      function initMap() {
        @if(!empty($kel))
        var uluru2 = {lat: {{ $kel->USULAN_LAT }}, lng: {{ $kel->USULAN_LONG }}};
        var map2 = new google.maps.Map(document.getElementById('map-kel'), {
          zoom: 15,
          center: uluru2
        });
        var marker2 = new google.maps.Marker({
          position: uluru2,
          map: map2
        });
        @endif

        @if(!empty($rw))
        var uluru = {lat: {{ $rw->USULAN_LAT }}, lng: {{ $rw->USULAN_LONG }}};
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 15,
          center: uluru
        });
        var marker = new google.maps.Marker({
          position: uluru,
          map: map
        });
        @endif
        @if(!empty($kec))
        var uluru3 = {lat: {{ $kec->USULAN_LAT }}, lng: {{ $kec->USULAN_LONG }}};
        var map3 = new google.maps.Map(document.getElementById('map-kec'), {
          zoom: 15,
          center: uluru3
        });
        var marker3 = new google.maps.Marker({
          position: uluru3,
          map: map3
        });
        @endif
      }

    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBgP7Og3t7CZhdJdxMQjv3d5HKHuqWj0fc&callback=initMap">
    </script>


<!--Start of Zendesk Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="https://v2.zopim.com/?4XsJAVMVHAuWk8KVNd6RcwgTq6up8MOA";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zendesk Chat Script-->

@endsection

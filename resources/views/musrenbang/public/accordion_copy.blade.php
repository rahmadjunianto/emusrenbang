<!DOCTYPE html>
<html lang="en">
<head>
<title>Daftar Usulan</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jq-2.2.4/dt-1.10.13/datatables.min.css"/>
<link rel="stylesheet" href="https://use.fontawesome.com/004068cd22.css">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> 
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/jq-2.2.4/dt-1.10.13/datatables.min.js"></script>
</head>
<body>
<BR>
<h2 align="center">Daftar Usulan Renja & PIPPK pada Musrenbang {{$tahun}}</h2>
<h4 align="center">
<?php
if (!Session::get('fb_user_access_token')) {
?>

  Silahkan <a href="{{$login_url}}">masuk</a> untuk menggunakan fitur komentar, share dan like.

<?php
} else {
?>
  Selamat datang, {{Session::get('facebook_user')['name']}}!
<?php
}
?>
</h4>

<br>
  <div class="container-fluid col-md-12">
    <div class="panel-group" id="accordionKecamatan">
    <?php
    foreach ($arr as $kec_name => $kel_row){
    ?>

      <div class="panel panel-default">
        <div class="panel-heading">
          <h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordionKecamatan" href="#collapse{{str_replace(' ', '', $kec_name)}}"> {{$idkey[md5($kec_name)]}}-Kecamatan {{$kec_name}} (Usulan PIPPK: {{$usulancount['pippk'][md5($kec_name)]}} Renja: {{$usulancount['renja'][md5($kec_name)]}}) </a></h4>
        </div>

        <div id="collapse{{str_replace(' ', '', $kec_name)}}" class="panel-collapse collapse">
          <div class="panel-body"> 
            <!-- Here we insert another nested accordion -->
            <div class="panel-group" id="kelurahan{{str_replace(' ', '', $kec_name)}}">

              <?php
              foreach ($kel_row as $kel_name => $rw_row){
              ?>
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title"><a data-toggle="collapse" data-parent="#kelurahan{{str_replace(' ', '', $kec_name)}}" href="#collapse{{str_replace(' ', '', $kel_name)}}"> {{$idkey[md5($kec_name.$kel_name)]}}-Kelurahan {{$kel_name}} (Usulan PIPPK: {{$usulancount['pippk'][md5($kec_name.$kel_name)]}} Renja: {{$usulancount['renja'][md5($kec_name.$kel_name)]}})</a></h4>
                </div>

                <div id="collapse{{str_replace(' ', '', $kel_name)}}" class="panel-collapse collapse">
                  <div class="panel-body"> 
                    <div class="panel-group" id="rw{{str_replace(' ', '', $kel_name)}}">

                    <?php
                    foreach ($rw_row as $rw_name => $rw_id){
                    ?>
                      <div class="panel panel-default">
                        <div class="panel-heading">
                          <h4 class="panel-title"><a rwid="{{$rw_id}}" data-toggle="collapse" data-parent="#rw{{str_replace(' ', '', $kel_name)}}" href="#rw{{str_replace(' ','',$rw_id)}}"> {{$idkey[md5($kec_name.$kel_name.$rw_name)]}}-RW {{$rw_name}} (Usulan PIPPK: {{$usulancount['pippk'][md5($kec_name.$kel_name.$rw_name)]}} Renja: {{$usulancount['renja'][md5($kec_name.$kel_name.$rw_name)]}}) </a></h4>
                        </div>

                        <div id="rw{{str_replace(' ','',$rw_id)}}" class="panel-collapse collapse">
                          <div class="panel-body" id="rwtabel{{$rw_id}}"> 
                            
                          </div>
                        </div>
                      </div>
                      <?php
                      }
                      ?>
                  </div>
                </div>
              </div>
            </div>
            <?php
            }
            ?>
            <!-- Inner accordion ends here --> 
          </div>
        </div>
      </div>
    </div>
    <?php
    }
    ?>  
  </div>
  
</div>
<script>
var fetched_rw = [];
var current_table = undefined;
$('a').on('click', function (e) {
  rw_id = $($(e.currentTarget.outerHTML)[0]).attr('rwid');

  if ((typeof rw_id != "undefined") && (fetched_rw.indexOf(rw_id) == -1)) {
    var count = 0;
    var refreshint = setInterval(function(){
      count++;
      document.getElementById('rwtabel'+rw_id).innerHTML = "Loading Data." + new Array(count % 10).join('.');
    }, 100);
    console.log("Fetch table: "+rw_id);

    fetched_rw.push(rw_id);
    $.get("/musrenbang/{{$tahun}}/public/api/rw/usulantabel/"+rw_id, function (data){
      clearInterval(refreshint);
      $("#rwtabel"+rw_id).html(data); //load ke data
      current_table = $("#datatable"+rw_id).DataTable();
    });
  }
});

function sendLike(usulan_id, is_like){
  $.get("/musrenbang/public/api/usulan/"+usulan_id+"/"+is_like, function(data){
    data = JSON.parse(data);
    alert(data.message);
  });
}
</script>
</body>
</html>
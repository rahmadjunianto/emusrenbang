<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Accordion</title>
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $(function() {
    $("#accordionkec, #accordionkel, #accordionrw").accordion({
      collapsible: true,
      activate: function(event, ui) {
        //console.log(ui.newHeader.text());
      },
    });
  });
  </script>
</head>
<body>
 
<div id="accordionkec">
<?php
foreach ($arr as $key => $row){
?>
  <h3>Kecamatan: {{$key}}</h3>
    <div id="accordionkel">
      <?php
      foreach ($row as $key2 => $row2){
      ?>
          <h3>Kelurahan: {{$key2}}</h3>
          <div id="accordionrw">
          <?php
          foreach ($row2 as $key3 => $row3){
          ?>
            <h3>RW: {{$key3}} </h3>
            <div id="content">
              <p>{{$row3}}</p>
            </div>
          <?php
          }
          ?>
          </div>
      <?php
      }
      ?>
    </div>
<?php 
}
?>
</div>
 
 
</body>
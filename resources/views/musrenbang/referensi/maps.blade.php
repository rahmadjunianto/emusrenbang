@extends('musrenbang.layout')

@section('content')
<style media="screen">
#pac-input {
background-color: #fff;
margin-top: 10px;
font-family: Roboto;
font-size: 15px;
font-weight: 300;
margin-left: 12px;
padding: 0 11px 0 13px;
text-overflow: ellipsis;
width: 300px;
}
</style>
<div id="content" class="app-content" role="main">
  <div class="hbox hbox-auto-xs hbox-auto-sm ng-scope">
    <div class="col">
      <div class="app-content-body ">

          <div class="bg-light lter">
              <ul class="breadcrumb bg-white m-b-none">
                <li><a href="#" class="btn no-shadow" ui-toggle-class="app-aside-folded" target=".app">
                  <i class="icon-bdg_expand1 text"></i> 
                  <i class="icon-bdg_expand2 text-active"></i>
                </a>   </li>
                <li><a href= "{{ url('/') }}/musrenbang/2017">Dashboard</a></li>
                <li><i class="fa fa-angle-right"></i>Usulan</li>
                <li class="active"><i class="fa fa-angle-right"></i>Tambah Usulan </li>
              </ul>
          </div>

          <div class="wrapper-lg bg-dark-grey">
            <div class="row">
              <div class="col-md-12">
                <div class="panel bg-white">

                  <div class="panel-heading wrapper-lg">
                    <h5 class="inline font-semibold text-orange m-n ">- </h5>
                  </div>

                  <div class="tab-content tab-content-alt-1 bg-white">
                        <div class="bg-white wrapper-lg input-jurnal">
                          @if (count($errors) > 0)
                              <div class="alert alert-danger">
                                  <ul>
                                      @foreach ($errors->all() as $error)
                                          <li>{{ $error }}</li>
                                      @endforeach
                                  </ul>
                              </div>
                          @endif
                            
                                   <div class="form-group">
                                    <input id="pac-input" class="controls" type="text" placeholder="Search Box">
                                    <br>
                                    <div class="col-sm-12" id="maps" style="height: 500px;">
                                    </div>
                                  </div>

                                 
                          </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
      </div>
    </div>
    </div>
</div>
@endsection





@section('plugin')
<script type="text/javascript">
      var oldMarker;
      function initMap() {
        //output variabel data
        var bandung = {lat: {{ $usulan->USULAN_LAT }}, lng: {{ $usulan->USULAN_LONG }}};
        //layout map
        var map = new google.maps.Map(document.getElementById('maps'), {
          zoom: 15,
          style: 'mapbox://styles/mapbox/satellite-v9',
          //styles: [{"featureType":"all","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#d3fcbd"},{"saturation":"14"},{"lightness":"8"},{"gamma":"1.20"},{"weight":"1"}]},{"featureType":"landscape","elementType":"geometry.fill","stylers":[{"hue":"#46ff00"}]},{"featureType":"landscape","elementType":"geometry.stroke","stylers":[{"hue":"#ff0000"}]},{"featureType":"landscape","elementType":"labels","stylers":[{"hue":"#3dff00"},{"visibility":"off"}]},{"featureType":"landscape.man_made","elementType":"geometry","stylers":[{"lightness":"100"},{"saturation":"12"},{"visibility":"simplified"},{"color":"#eef5d4"}]},{"featureType":"landscape.man_made","elementType":"geometry.fill","stylers":[{"color":"#f6ecc6"},{"lightness":"44"},{"saturation":"-14"}]},{"featureType":"landscape.natural","elementType":"geometry","stylers":[{"hue":"#89ff00"},{"saturation":"-47"},{"lightness":"-12"}]},{"featureType":"poi","elementType":"geometry","stylers":[{"visibility":"simplified"}]},{"featureType":"poi","elementType":"geometry.fill","stylers":[{"color":"#d5f9ca"}]},{"featureType":"poi.attraction","elementType":"geometry.fill","stylers":[{"color":"#a257f9"},{"visibility":"on"},{"lightness":"63"},{"saturation":"-67"}]},{"featureType":"poi.medical","elementType":"geometry.fill","stylers":[{"color":"#f6d7d7"}]},{"featureType":"poi.park","elementType":"labels","stylers":[{"visibility":"on"}]},{"featureType":"poi.school","elementType":"geometry.fill","stylers":[{"color":"#dfdbea"},{"saturation":"-59"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#f2fc2f"},{"lightness":"25"},{"saturation":"2"},{"visibility":"on"}]},{"featureType":"transit.station.airport","elementType":"geometry","stylers":[{"color":"#e1b9f0"},{"saturation":"-12"}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#b0dff8"}]}]
          center: bandung
        });
        //marker map
         var marker = new google.maps.Marker({
          position: bandung,
          icon: "{{ url('/') }}/marker-green.png",
          map: map
        });
         //info map
         var contentString = '{{ $usulan->kamus->KAMUS_NAMA }}<br>Lokasi: {{$usulan->ALAMAT}}';
         
        var infowindow = new google.maps.InfoWindow({
          content: contentString
        });

        marker.addListener('click', function() {
          infowindow.open(map, marker);
        });

        google.maps.event.addListener(map, 'click', function( event ){
          $('#latitude').val(event.latLng.lat());
          $('#longitude').val(event.latLng.lng());


          placeMarker(event.latLng,map);
        });

        // Create the search box and link it to the UI element.
        var input = document.getElementById('pac-input');
        var searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function() {
        searchBox.setBounds(map.getBounds());
        });

        var markers = [];
        // [START region_getplaces]
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function() {
        var places = searchBox.getPlaces();

        if (places.length == 0) {
          return;
        }

        // Clear out the old markers.
        markers.forEach(function(marker) {
          marker.setMap(null);
        });
        markers = [];

        // For each place, get the icon, name and location.
        var bounds = new google.maps.LatLngBounds();
        places.forEach(function(place) {
          var icon = {
            url: place.icon,
            size: new google.maps.Size(71, 71),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(17, 34),
            scaledSize: new google.maps.Size(25, 25)
          };

          // Create a marker for each place.
          markers.push(new google.maps.Marker({
            map: map,
            icon: icon,
            title: place.name,
            position: place.geometry.location
          }));

          if (place.geometry.viewport) {
            // Only geocodes have viewport.
            bounds.union(place.geometry.viewport);
          } else {
            bounds.extend(place.geometry.location);
          }
        });
        map.fitBounds(bounds);
        });
        // [END region_getplaces]
      }

      function placeMarker(location,map) {
            marker = new google.maps.Marker({
                position: location,
                map: map

            });

            if (oldMarker != undefined){
                oldMarker.setMap(null);
            }
            oldMarker = marker;
        }
</script>


<script>
// This example adds a search box to a map, using the Google Place Autocomplete
// feature. People can enter geographical searches. The search box will return a
// pick list containing a mix of places and predicted search terms.

function initAutocomplete() {

  var map = new google.maps.Map(document.getElementById('maps'), {
  center: {lat: -33.8688, lng: 151.2195},
  zoom: 13,
  // styles: [{"featureType":"all","elementType":"all","stylers":[{"visibility":"on"}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"visibility":"simplified"},{"color":"#d3fcbd"},{"saturation":"14"},{"lightness":"8"},{"gamma":"1.20"},{"weight":"1"}]},{"featureType":"landscape","elementType":"geometry.fill","stylers":[{"hue":"#46ff00"}]},{"featureType":"landscape","elementType":"geometry.stroke","stylers":[{"hue":"#ff0000"}]},{"featureType":"landscape","elementType":"labels","stylers":[{"hue":"#3dff00"},{"visibility":"off"}]},{"featureType":"landscape.man_made","elementType":"geometry","stylers":[{"lightness":"100"},{"saturation":"12"},{"visibility":"simplified"},{"color":"#eef5d4"}]},{"featureType":"landscape.man_made","elementType":"geometry.fill","stylers":[{"color":"#f6ecc6"},{"lightness":"44"},{"saturation":"-14"}]},{"featureType":"landscape.natural","elementType":"geometry","stylers":[{"hue":"#89ff00"},{"saturation":"-47"},{"lightness":"-12"}]},{"featureType":"poi","elementType":"geometry","stylers":[{"visibility":"simplified"}]},{"featureType":"poi","elementType":"geometry.fill","stylers":[{"color":"#d5f9ca"}]},{"featureType":"poi.attraction","elementType":"geometry.fill","stylers":[{"color":"#a257f9"},{"visibility":"on"},{"lightness":"63"},{"saturation":"-67"}]},{"featureType":"poi.medical","elementType":"geometry.fill","stylers":[{"color":"#f6d7d7"}]},{"featureType":"poi.park","elementType":"labels","stylers":[{"visibility":"on"}]},{"featureType":"poi.school","elementType":"geometry.fill","stylers":[{"color":"#dfdbea"},{"saturation":"-59"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#f2fc2f"},{"lightness":"25"},{"saturation":"2"},{"visibility":"on"}]},{"featureType":"transit.station.airport","elementType":"geometry","stylers":[{"color":"#e1b9f0"},{"saturation":"-12"}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#b0dff8"}]}]
  mapTypeId: google.maps.MapTypeId.ROADMAP
  });

  // Create the search box and link it to the UI element.
  var input = document.getElementById('pac-input');
  var searchBox = new google.maps.places.SearchBox(input);
  map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

  // Bias the SearchBox results towards current map's viewport.
  map.addListener('bounds_changed', function() {
  searchBox.setBounds(map.getBounds());
  });

  var markers = [];
  // [START region_getplaces]
  // Listen for the event fired when the user selects a prediction and retrieve
  // more details for that place.
  searchBox.addListener('places_changed', function() {
  var places = searchBox.getPlaces();

  if (places.length == 0) {
    return;
  }

// Clear out the old markers.
markers.forEach(function(marker) {
  marker.setMap(null);
});
markers = [];

// For each place, get the icon, name and location.
var bounds = new google.maps.LatLngBounds();
places.forEach(function(place) {
  var icon = {
    url: place.icon,
    size: new google.maps.Size(71, 71),
    origin: new google.maps.Point(0, 0),
    anchor: new google.maps.Point(17, 34),
    scaledSize: new google.maps.Size(25, 25)
  };

  // Create a marker for each place.
  markers.push(new google.maps.Marker({
    map: map,
    icon: icon,
    title: place.name,
    position: place.geometry.location
  }));

  if (place.geometry.viewport) {
    // Only geocodes have viewport.
    bounds.union(place.geometry.viewport);
  } else {
    bounds.extend(place.geometry.location);
  }
});
map.fitBounds(bounds);
});
// [END region_getplaces]
}

</script>





<script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBzixLw-QwjOpEiL8qmveEmafmgM27mgnk&libraries=places&callback=initMap">
</script>

<script>
    $(document).ready(function(){

        $("#upload").click(function(){
            $("#div1").fadeIn("fast");
            $("#upload").addClass("addGambar");
        });

        $("#isu").change(function(e, params){
            var id  = $('#isu').val();
           $('#kamus').find('option').remove().end().append('<option>Kamus Usulan</option>');
            $.ajax({
              type  : "get",
              url   : "{{ url('/') }}/musrenbang/{{ $tahun }}/usulan/data/kamus/"+id,
              success : function (data) {
                $('#kamus').append(data['opt']).trigger('chosen:updated');
                if(data['tipe'] == 1 || data['tipe'] == 2) $('#div-upload-gambar').fadeIn('fast').removeClass('hide');
                else $('#div-upload-gambar').fadeOut('fast').addClass('hide');
              }
            });
        });

        $("#kamus").change(function(e, params){
          // alert('kamus');
            var id  = $('#kamus').val();
            $('#satuan').val();
            $.ajax({
              type  : "get",
              url   : "{{ url('/') }}/musrenbang/{{ $tahun }}/usulan/data/satuan/"+id,
              success : function (data) {
                $('#satuan').val(data);
              }
            });
        });
        $("#isu").change();
    });
</script>


<!--Start of Zendesk Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="https://v2.zopim.com/?4XsJAVMVHAuWk8KVNd6RcwgTq6up8MOA";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zendesk Chat Script-->

@endsection

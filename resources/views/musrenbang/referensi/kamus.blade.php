@extends('musrenbang.layout')

@section('content')
<div id="content" class="app-content" role="main">
  <div class="hbox hbox-auto-xs hbox-auto-sm ng-scope">
    <div class="col">
      <div class="app-content-body ">

          <div class="bg-light lter">
              <ul class="breadcrumb bg-white m-b-none">
                <li><a href="#" class="btn no-shadow" ui-toggle-class="app-aside-folded" target=".app">
                  <i class="icon-bdg_expand1 text"></i>
                  <i class="icon-bdg_expand2 text-active"></i>
                </a>   </li>
                <li><a href= "{{ url('/') }}/main">Dashboard</a></li>
                <li class="active"><i class="fa fa-angle-right"></i>Daftar Usulan</li>
              </ul>
          </div>

          <div class="wrapper-lg bg-dark-grey">
            <div class="row">
              <div class="col-md-12">

                <div class="panel bg-white">

                  <div class="wrapper-lg">
                    <h5 class="inline font-semibold text-orange m-n ">Daftar Usulan</h5>
                    @if(Auth::user()->level == 1 && Auth::user()->app == 1 )
                    <a class="pull-right btn m-t-n-sm btn-success input-xl" href="{{ url('/') }}/musrenbang/2017/usulan/tambah"><i class="m-r-xs fa fa-plus"></i> Tambah Usulan</a>
                    @endif
                  </div>

                  <div class="tab-content tab-content-alt-1 bg-white">
                        <div role="tabpanel" class="active tab-pane" id="tab-1">
                            <div class="table-responsive dataTables_wrapper">
                             <table ui-jq="dataTable" ui-options="{
                                    sAjaxSource: '{{ url('/') }}/musrenbang/{{$tahun}}/usulan/data',
                                    aoColumns: [
                                    { mData: 'USULAN_ID',class:'hide' },
                                    { mData: 'NO',class:'text-center' },
                                    { mData: 'LOKASI' },
                                    { mData: 'ISU' },
                                    { mData: 'VOLUME' },
                                    { mData: 'STATUS' },
                                    { mData: 'AKSI' },
                                    ]}" class="table table-usulan table-striped b-t b-b">
                                    <thead>
                                      <tr>
                                        <th class="hide">#</th>
                                        <th>#</th>
                                        <th>Lokasi</th>
                                        <th>Isu / Kamus</th>
                                        <th>Volume</th>
                                        <th>Status/Lokasi</th>
                                        <th>Opsi</th>
                                      </tr>
                                      <tr>
                                        <th></th>
                                        <th colspan="6" class="th_search">
                                            <i class="icon-bdg_search"></i>
                                            <input typppe="search" class="table-search form-control b-none w-full" placeholder="Cari" aria-controls="DataTables_Table_0">
                                        </th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                  </table>
                          		</div>
                        	</div>
                  </div>
                </div>


              </div>
            </div>

          </div>





      </div>
      <!-- App-content-body -->

    </div>
    <!-- .col -->


    </div>
</div>
<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
@endsection

@section('plugin')
<script type="text/javascript">
  function hapus(id){
    var token        = $('#token').val();
    $.confirm({
        title: 'Hapus Data!',
        content: 'Yakin hapus data?',
        buttons: {
            Ya: {
                btnClass: 'btn-danger',
                action: function(){
                  $.ajax({
                      url: "{{ url('/') }}/musrenbang/{{ $tahun }}/usulan/delete",
                      type: "POST",
                      data: {'_token'         : token,
                            'id'              : id},
                      success: function(msg){
                          $.alert(msg);
                          $('.table').DataTable().ajax.reload();
                        }
                  });
                }
            },
            Tidak: function () {
            }
        }
    });
  }
</script>
@endsection

@extends('musrenbang.layout')

@section('content')
<div id="content" class="app-content" role="main">
  <div class="hbox hbox-auto-xs hbox-auto-sm ng-scope">
    <div class="col">
      <div class="app-content-body ">

      	<div class="bg-light lter">
              <ul class="breadcrumb bg-white m-b-none">
                <li><a href="#" class="btn no-shadow" ui-toggle-class="app-aside-folded" target=".app">
                  <i class="icon-bdg_expand1 text"></i>
                  <i class="icon-bdg_expand2 text-active"></i>
                </a>   </li>
                <li><a href= "{{ url('/') }}/main">Dashboard</a></li>
                <li class="active"><i class="fa fa-angle-right"></i>Kelola Kewilayahan</li>
              </ul>
          </div>

           <div class="wrapper-lg">
            <div class="row">
              <div class="col-md-12">
                <div class="panel bg-white">
                  <div class="wrapper-lg">
                    <h5 class="inline font-semibold text-orange m-n ">Kelola Kewilayahan Kecamatan</h5>
                    {{-- <div class="col-sm-2 pull-right m-t-n-sm">
                       <a href="{{ url('/') }}/musrenbang/{{ $tahun }}/pengaturan/kecamatan/add"  class="btn btn-success pull-right" style="margin-top:10px;"><i class="fa fa-back"></i> Tambah</a>
                     </div> --}}
                  </div>
                  <div class="tab-content tab-content-alt-1 bg-white">
                        <div role="tabpanel" class="active tab-pane" id="tab-1">
                            <div class="table-responsive dataTables_wrapper">
                             <table ui-jq="dataTable" ui-options="{
                                    sAjaxSource: '{{ url('/') }}/musrenbang/{{$tahun}}/pengaturan/kecamatan/getKec',
                                    aoColumns: [
                                    { mData: 'NO' },
                                    { mData: 'KEC' },
                                    { mData: 'NAMA' },
                                    { mData: 'TELP' },
                                    { mData: 'STAT' },
                                    { mData: 'AKSI' }
                                    ]}" class="table table-striped b-t b-b" id="table-tahapan">
                                    <thead>
                                      <tr>
                                      	<th>NO</th>
                                        <th>KECAMATAN</th>
                                        <th>CAMAT</th>
                                        <th>NO TLP</th>
                                        <th>STATUS</th>
                                        <th>OPSI</th>
                                      </tr>
                                      <tr>
                                        <th colspan="6" class="th_search">
                                            <i class="icon-bdg_search"></i>
                                            <input type="search" class="table-search form-control b-none w-full" placeholder="Cari Kelurahan" aria-controls="DataTables_Table_0">
                                        </th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                  </table>
                              </div>
                          </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>

     </div>
      <!-- App-content-body -->
    </div>
    <!-- .col -->
</div>


<div id="form-tinjau" class="modal fade" role="dialog">
<form method="post" action="{{ url('/') }}/musrenbang/{{ $tahun }}/usulan/simpan/profile/kec" id="formtinjau">
{{ csrf_field() }}
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Profile Kecamatan</h4>
      </div>
      <div class="modal-body">
         <div class="form-group">
            <label class="col-sm-4">Kecamatan</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" name="KEC_NAMA" required="" value="">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-4">Nama Camat</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" name="KEC_CAMAT" required="" value="">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-4">NIP Camat</label>
            <div class="col-sm-8">
              <input type="number" class="form-control" name="KEC_NIP" required="" value="">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-4">No Tlp/Hp</label>
            <div class="col-sm-8">
              <input type="number" class="form-control" name="KEC_TELP" required="" value="" length="5">
            </div>
          </div>
          <input type="hidden" class="form-control" name="KEC_ID" required="" value="" >
      </div>
      <div class="modal-footer">


          <button type="submit" class="btn btn-info">Simpan</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>

      </div>
    </div>
  </div>
</form>
</div>

@endsection

@section('plugin')
<script type="text/javascript">
  function reset(id){
    $.confirm({
        title: 'Reset Password!',
        content: 'Yakin reset password?',
        buttons: {
            Ya: {
                btnClass: 'btn-danger',
                action: function(){
                  $.ajax({
                    url: "{{ url('/') }}/musrenbang/{{ $tahun }}/pengaturan/kecamatan/reset/"+id,
                    type: "GET",
                    success: function(msg){
                      $.alert(msg);
                    }
                  });
                }
            },
            Tidak: function () {
            }
        }
    });
  }

  function getProfil(id){

      $.ajax({
          method: 'GET', // Type of response and matches what we said in the route
          url: "{{ url('/') }}/musrenbang/{{ $tahun }}/pengaturan/kecamatan/getProfile/"+id,
           // a JSON object to send back
          success: function(response){ // What to do if we succeed
              console.log(response);
              $("#formtinjau input[name='KEC_NAMA']").val(response.KEC_NAMA);
              $("#formtinjau input[name='KEC_CAMAT']").val(response.KEC_CAMAT);
              $("#formtinjau input[name='KEC_NIP']").val(response.KEC_NIP);
              $("#formtinjau input[name='KEC_TELP']").val(response.KEC_TELP);
              $("#formtinjau input[name='KEC_ID']").val(response.KEC_ID);

          },
          error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
              console.log(JSON.stringify(jqXHR));
              console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
          }
    });

  }

  function active(id){
    $.confirm({
        title: 'Aktivasi account!',
        content: 'Yakin Account di Aktifkan?',
        buttons: {
            Ya: {
                btnClass: 'btn-danger',
                action: function(){
                  $.ajax({
                    url: "{{ url('/') }}/musrenbang/{{ $tahun }}/pengaturan/aktivasi/"+id,
                    type: "GET",
                    success: function(msg){
                      $.alert(msg);
                    }
                  });
                }
            },
            Tidak: function () {
            }
        }
    });
  }

  function nonActive(id){
    $.confirm({
        title: 'Matikan account!',
        content: 'Yakin Account di Matikan?',
        buttons: {
            Ya: {
                btnClass: 'btn-danger',
                action: function(){
                  $.ajax({
                    url: "{{ url('/') }}/musrenbang/{{ $tahun }}/pengaturan/nonAktivasi/"+id,
                    type: "GET",
                    success: function(msg){
                      $.alert(msg);
                    }
                  });
                }
            },
            Tidak: function () {
            }
        }
    });
  }
</script>
@endsection
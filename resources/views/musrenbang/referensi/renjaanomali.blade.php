@extends('musrenbang.layout')

@section('content')
<div id="content" class="app-content" role="main">
  <div class="hbox hbox-auto-xs hbox-auto-sm ng-scope">
    <div class="col">
      <div class="app-content-body ">

        <div class="bg-light lter">
          <ul class="breadcrumb bg-white m-b-none">
            <li><a href="#" class="btn no-shadow" ui-toggle-class="app-aside-folded" target=".app">
              <i class="icon-bdg_expand1 text"></i>
              <i class="icon-bdg_expand2 text-active"></i>
            </a>   </li>
            <li><a href= "{{ url('/') }}/main">Dashboard</a></li>
            <li class="active"><i class="fa fa-angle-right"></i>Daftar Renja Anomali (Renja RW KELURAHAN > 500 Juta)</li>
          </ul>
        </div>

        <div class="wrapper-lg bg-dark-grey">
          <div class="row">
            <div class="col-md-12">

              <div class="panel bg-white">

                <div class="wrapper-lg">
                  <h5 class="inline font-semibold text-orange m-n ">Daftar Anomali Renja RW > 500 Juta</h5>
                  <div class="col-sm-1 pull-right m-t-n-sm">
                      <select class="form-control dtSelect" id="dtSelect">
                            <option value="10">10</option>
                            <option value="25">25</option>
                            <option value="50">50</option>
                            <option value="100">100</option>
                        </select>
                    </div>   
                </div>

                <div class="tab-content tab-content-alt-1 bg-white table-kecamatan" id="table-kecamatan">
                  <div role="tabpanel" class="active tab-pane table-kecamatan" id="tab-1">
                    <div class="table-responsive dataTables_wrapper table-kecamatan">
                      <table id="table" ui-jq="dataTable" ui-options="{
                      sAjaxSource: '{{ url('/') }}/musrenbang/{{$tahun}}/public/anomali/renja/500000000/api',
                      aoColumns: [
                      { mData: 'no',class:'text-center' },
                      { mData: 'nama' },
                      { mData: 'nominal' }
                      ]}" class="table table-kecamatan table-striped b-t b-b">
                      <thead>
                        <tr>
                          <th>#</th>
                          <th>Nama</th>
                          <th>Nominal</th>
                        </tr>
                        <tr>
                          <th colspan="4" class="th_search">
                            <i class="icon-bdg_search"></i>
                            <input type="search" class="table-search form-control b-none w-full" placeholder="Cari" aria-controls="DataTables_Table_0">
                          </th>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>


@endsection
@section('plugin')

@endsection

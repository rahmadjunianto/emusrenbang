@extends('musrenbang.layout')

@section('content')
<div id="content" class="app-content" role="main">
  <div class="hbox hbox-auto-xs hbox-auto-sm ng-scope">
    <div class="col">
      <div class="app-content-body ">

          <div class="bg-light lter">    
              <ul class="breadcrumb bg-white m-b-none">
                <li><a href="#" class="btn no-shadow" ui-toggle-class="app-aside-folded" target=".app">
                  <i class="icon-bdg_expand1 text"></i>
                  <i class="icon-bdg_expand2 text-active"></i>
                </a>   </li>
                <li><a href= "{{ url('/') }}/main">Dashboard</a></li>
                <li class="active"><i class="fa fa-angle-right"></i>Kelola RT </li>                                
              </ul>
          </div>
          <div class="wrapper-lg">
            <div class="row">
              <div class="col-md-12">
                <div class="panel bg-white">
                  <div class="wrapper-lg">

                    <h5 class="inline font-semibold text-orange m-n ">Kelola  RT </h5>

                  </div>           
                  <div class="tab-content tab-content-alt-1 bg-white">
                        <div role="tabpanel" class="active tab-pane" id="tab-1">  
                            <div class="table-responsive dataTables_wrapper">
                             <table ui-jq="dataTable" ui-options="{
                                    sAjaxSource: '{{ url('/') }}/musrenbang/{{$tahun}}/pengaturan/rt/getrt/{{$id}}',
                                    aoColumns: [
                                    { mData: 'NO' },
                                    { mData: 'RT_NAMA' },
                                    { mData: 'RT_KETUA' },
                                    { mData: 'NIK' },
                                    { mData: 'TELP' },
                                    { mData: 'AKSI' }
                                    ]}" class="table table-striped b-t b-b" id="table-tahapan">
                                    <thead>
                                      <tr>
                                        <th>No</th>
                                        <th>Nomor RT</th>
                                        <th>Ketua RT</th>
                                        <th>N.I.K</th>
                                        <th>No Telepon</th>
                                        <th>Aksi</th>
                                      </tr>
                                       <tr>
                                        <th colspan="6" class="th_search">
                                            <i class="icon-bdg_search"></i>
                                            <input type="search" class="table-search form-control b-none w-full" placeholder="Cari Tahapan" aria-controls="DataTables_Table_0">
                                        </th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                  </table>
                              </div>
                          </div>
                  </div>
                </div>                
              </div>
            </div>
          </div>
      </div>
      <!-- App-content-body -->  
    </div>
    <!-- .col -->
</div>

<div id="form-tinjau" class="modal fade" role="dialog">
<form method="post" action="{{ url('/') }}/musrenbang/{{ $tahun }}/usulan/simpan/profile/rt" id="formtinjau">
{{ csrf_field() }}
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Profile RT</h4>
      </div>
      <div class="modal-body">
         <div class="form-group">
            <label class="col-sm-4">Nomor RT</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" name="RT_NAMA" required="" value="">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-4">Nama Ketua RT</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" name="RT_KETUA" required="" value="">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-4">NIK Ketua RT</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" name="RT_NIK" required="" value="">
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-4">No Tlp/Hp</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" name="RT_TELP" required="" value="" length="5">
            </div>
          </div>
          <input type="hidden" class="form-control" name="RT_ID" required="" value="" >
      </div>
      <div class="modal-footer">
        
       
          <button type="submit" class="btn btn-info">Simpan</button>
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        
      </div>
    </div>
  </div>
</form>
</div>
@endsection

@section('plugin')
<script type="text/javascript">
  

  function getProfil(id){
        
      $.ajax({
          method: 'GET', // Type of response and matches what we said in the route
          url: "{{ url('/') }}/musrenbang/{{ $tahun }}/pengaturan/rt/getProfile/"+id,
           // a JSON object to send back
          success: function(response){ // What to do if we succeed
              console.log(response);8
              $("#formtinjau input[name='RT_NAMA']").val(response.RT_NAMA);
              $("#formtinjau input[name='RT_KETUA']").val(response.RT_KETUA);
              $("#formtinjau input[name='RT_NIK']").val(response.RT_NIK);
              $("#formtinjau input[name='RT_TELP']").val(response.RT_TELP);
              $("#formtinjau input[name='RT_ID']").val(response.RT_ID);

          },
          error: function(jqXHR, textStatus, errorThrown) { // What to do if we fail
              console.log(JSON.stringify(jqXHR));
              console.log("AJAX error: " + textStatus + ' : ' + errorThrown);
          }
    }); 
    
  }
</script>
@endsection



<!DOCTYPE html>
<html lang="en" class="">
<head>
  <meta charset="utf-8" />
  <title>.bdg Musrenbang</title>
  <meta name="description" content="Bandung Web Kit" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
  <link rel="stylesheet" href="{{ url('/') }}/libs_dashboard/assets/animate.css/animate.css" type="text/css" />
  <link rel="stylesheet" href="{{ url('/') }}/libs_dashboard/assets/font-awesome/css/font-awesome.min.css" type="text/css" />
  <link rel="stylesheet" href="{{ url('/') }}/libs_dashboard/assets/simple-line-icons/css/simple-line-icons.css" type="text/css" />
  <link rel="stylesheet" href="{{ url('/') }}/libs_dashboard/jquery/bootstrap/dist/css/bootstrap.css" type="text/css" />
  <link rel="stylesheet" href="{{ url('/') }}/libs_dashboard/jquery/jquery.confirm/css/jquery-confirm.css" type="text/css" />
  <link rel="stylesheet" href="{{ url('/') }}/assets/css/font.css" type="text/css" />
  <link rel="stylesheet" href="{{ url('/') }}/assets/css/style.css" type="text/css" />
  <link rel="stylesheet" href="{{ asset('toastr/toastr.min.css') }}" type="text/css" />
  <link href="{{ url('/') }}/assets/css/hover-min.css" rel="stylesheet">
  <script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
<script src="https://www.amcharts.com/lib/3/pie.js"></script>
<script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
<link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
<script src="https://www.amcharts.com/lib/3/themes/light.js"></script>
  @yield('css')

</head>
<body>
<div class="app app-header-fixed ">
    <!-- header -->
  <header id="header" class="app-header navbar" role="menu">
      <!-- navbar header -->
      <div class="navbar-header bg-info">
        <button class="pull-right visible-xs dk" ui-toggle-class="show" target=".navbar-collapse">
          <i class="glyphicon glyphicon-cog"></i>
        </button>
        <button class="pull-left visible-xs" ui-toggle-class="off-screen" target=".app-aside" ui-scroll="app">
          <i class="glyphicon glyphicon-align-justify"></i>
        </button>
        <!-- brand -->
        <a href="{{ url('/') }}/musrenbang/2017" class="navbar-brand text-lt hvr-float-shadow">
          <img src="{{ url('/') }}/assets/img/logo-small.png" srcset="{{ url('/') }}/assets/img/logo-small@2x.png 2x" alt="." class="small-logo hide">
          <img src="{{ url('/') }}/assets/img/logo.png" srcset="{{ url('/') }}/assets/img/logo@2x.png 2x" alt="." class="large-logo">
        </a>
        <!-- / brand -->
      </div>
      <!-- / navbar header -->

      <!-- navbar collapse -->
      <div class="collapse pos-rlt navbar-collapse bg-info">
        <!-- buttons -->
        <div class="nav navbar-nav hidden-xs">   </div>
        <!-- / buttons -->

        <!-- nabar right -->
        <ul class="nav navbar-nav navbar-right">

           <li class="dropdown padder-md">
              @if($tahun == '2017')
              <p class="font-semibold m-t-lg pull-left" style="color:black;">Tahun Usulan : 2017 ||
                <a href="{{ url('/') }}/musrenbang/2018/usulan/dashboard">2018</a>
              </p>
              @else
               <p class="font-semibold m-t-lg pull-left" style="color:black;">Tahun Usulan : 2018 ||
                <a href="{{ url('/') }}/musrenbang/2017/usulan/dashboard">2017</a>
              </p>
              @endif
           </li>

          <li>
            <a href="{{ url('/') }}/musrenbang/2017/profile" class="hvr-curl-top-right hvr-wobble-vertical">
              <i class="icon-bdg_people"></i>
              @if(Auth::user()->level != 9)
              <span>
                @if(Auth::user()->level == 1)
                  RW {{ Auth::user()->rw->RW_NAMA }},
                  Kel {{ Auth::user()->rw->kelurahan->KEL_NAMA }},
                  Kec {{ Auth::user()->rw->kelurahan->kecamatan->KEC_NAMA }}

                @elseif(Auth::user()->level == 2 || Auth::user()->level == 3 || Auth::user()->level == 4)
                    {{ Auth::user()->email }}, {{ Auth::user()->rw->kelurahan->KEL_NAMA }}

                @elseif(Auth::user()->level == 5 )
                    Kel {{ Auth::user()->rw->kelurahan->KEL_NAMA }},
                    Kec {{ Auth::user()->rw->kelurahan->kecamatan->KEC_NAMA }}

                @elseif(Auth::user()->level == 6 )
                    Kec {{ Auth::user()->kecamatan->KEC_NAMA }}

                @elseif(Auth::user()->level >= 7 )
                    {{ Auth::user()->name}}

                @endif

              </span>
              @else
              <span>Admin Bappelitbang</span>
              @endif
            </a>
          </li>

          <li>

            <a href="{{ url('/') }}/keluar" class="hvr-pulse">
              <!-- <i class="icon-bdg_keys"></i> -->
              <span>Logout</span>
            </a>
          </li>

        </ul>
        <!-- / navbar right -->
      </div>
      <!-- / navbar collapse -->
  </header>
  <!-- / header -->


    <!-- aside -->
  <aside id="aside" class="app-aside hidden-xs bg-white">
      <div class="aside-wrap">
        <div class="navi-wrap">
         <!-- nav -->
          <nav ui-nav class="navi white-navi clearfix">
            <ul class="nav">


              <li>
                <a href="{{ url('/musrenbang/2017') }}" class="auto padding-l-r-lg hvr-buzz-out">
                  <i class="mi-paper"></i>
                  <span class="font-semibold"> Dashboard </span>
                </a>
              </li>


          @if(Auth::user()->level != 9)


              <li>
                  @if(Auth::user()->level == 10)
                  @else
                <a href="#" class="auto padding-l-r-lg parent hvr-buzz-out">
                  <i class="mi-paper"></i>
                  <span class="pull-right text-heading">
                    <i class="text8 icon-bdg_arrow1 text"></i>
                    <i class="text8 icon-bdg_arrow2 text-active"></i>
                  </span>
                  <span class="font-semibold"> Input Usulan </span>
                </a>
                @endif
                 <ul class="nav nav-sub dk">
                  @if(Auth::user()->level == 1 && Auth::user()->active == 1 || Auth::user()->level == 2 || Auth::user()->level == 3 || Auth::user()->level == 4)
                  @if(Auth::user()->role == 1)
                  <li>
                    <a href="{{ url('/') }}/musrenbang/{{$tahun}}/usulan/usulkankembali" class="padding-l-r-lg hvr-buzz-out">
                       <span>&nbsp; &nbsp; &nbsp; Usulan Thn 2017</span>
                    </a>
                  </li>
                  @endif
                  @if(Auth::user()->role == 0 && Auth::user()->level == 1)
                  <li>
                    <a href="{{ url('/') }}/musrenbang/2018/usulan/tambah/1" class="padding-l-r-lg hvr-buzz-out">
                       <span>&nbsp; &nbsp; &nbsp; Renja</span>
                    </a>
                  </li>
                  @endif
                  @if(Auth::user()->role == 0)
                  <li>
                    <a href="{{ url('/') }}/musrenbang/2018/usulan/tambah/2" class="padding-l-r-lg hvr-buzz-out">
                       <span>&nbsp; &nbsp; &nbsp; PIPPK</span>
                    </a>
                  </li>
                  @endif
                  @elseif(Auth::user()->level == 5  || Auth::user()->level == 6 && Auth::user()->active == 1 )
                  <li>
                    <a href="{{ url('/') }}/musrenbang/2018/usulan/tambah/1" class="padding-l-r-lg hvr-buzz-out">
                       <span>&nbsp; &nbsp; &nbsp; Renja</span>
                    </a>
                    </li>
                  @endif
                  </ul>
              </li>


              <li>
                <a href="#" class="auto padding-l-r-lg parent hvr-buzz-out">
                  <i class="mi-paper"></i>
                  <span class="pull-right text-heading">
                    <i class="text8 icon-bdg_arrow1 text"></i>
                    <i class="text8 icon-bdg_arrow2 text-active"></i>
                  </span>
                  <span class="font-semibold"> Rekap Usulan </span>
                </a>
                 <ul class="nav nav-sub dk">
                  <li>
                      <a href="#" class="auto padding-l-r-lg parent">
                        <i class=""></i>
                        <span class="pull-right text-heading">
                          <i class="text8 icon-bdg_arrow1 text"></i>
                          <i class="text8 icon-bdg_arrow2 text-active"></i>
                        </span>
                        <span class="font-semibold">&nbsp; &nbsp; &nbsp; Renja</span>
                        </a>
                      <ul class="nav nav-sub dk">
                        @if(Auth::user()->level == 1)
                        <li>
                          <a href="{{ url('/') }}/musrenbang/2017/usulan-1" class="padding-l-r-lg hvr-buzz-out">
                             <span>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 2017</span>
                          </a>
                        </li>
                        <li>
                          <a href="{{ url('/') }}/musrenbang/2018/usulan-1" class="padding-l-r-lg hvr-buzz-out">
                             <span>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 2018</span>
                          </a>
                        </li>
                        @elseif(Auth::user()->level == 5 || Auth::user()->level == 6 || Auth::user()->level == 7 || Auth::user()->level == 10)
                        <li>
                           <a href="{{ url('/') }}/musrenbang/2017/usulan/show/1" class="padding-l-r-lg hvr-buzz-out">
                             <span>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 2017</span>
                          </a>
                        </li>
                        <li>
                           <a href="{{ url('/') }}/musrenbang/2018/usulan/show/1" class="padding-l-r-lg hvr-buzz-out">
                             <span>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 2018</span>
                          </a>
                        </li>
                        @endif

                      </ul>
                  </li>
                   <li>
                      <a href="#" class="auto padding-l-r-lg parent">
                        <i class=""></i>
                        <span class="pull-right text-heading">
                          <i class="text8 icon-bdg_arrow1 text"></i>
                          <i class="text8 icon-bdg_arrow2 text-active"></i>
                        </span>
                        <span class="font-semibold">&nbsp; &nbsp; &nbsp; PIPPK</span>
                        </a>
                      <ul class="nav nav-sub dk">
                        @if(Auth::user()->level == 1 || Auth::user()->level == 2 || Auth::user()->level == 3 || Auth::user()->level == 4)
                        <li>
                          <a href="{{ url('/') }}/musrenbang/2017/usulan-2" class="padding-l-r-lg hvr-buzz-out">
                             <span>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 2017</span>
                          </a>
                        </li>
                        <li>
                          <a href="{{ url('/') }}/musrenbang/2018/usulan-2" class="padding-l-r-lg hvr-buzz-out">
                             <span>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 2018</span>
                          </a>
                        </li>
                        @elseif(Auth::user()->level == 5 || Auth::user()->level == 6 )
                        <li>
                          <a href="{{ url('/') }}/musrenbang/2017/usulan/show/2" class="padding-l-r-lg hvr-buzz-out">
                             <span>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 2017</span>
                          </a>
                        </li>
                        <li>
                          <a href="{{ url('/') }}/musrenbang/2018/usulan/show/2" class="padding-l-r-lg hvr-buzz-out">
                             <span>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 2018</span>
                          </a>
                        </li>
                        @endif
                      </ul>
                  </li>
                  </ul>
              </li>


               @if(Auth::user()->level == 0)
              <li>
                <a href="{{ url('/musrenbang/2017/usulan/add-kel/1') }}" class="auto padding-l-r-lg hvr-buzz-out">
                  <i class="mi-paper"></i>
                  <span class="font-semibold"> Input Usulan </span>
                </a>
              </li>
              @endif



              <!-- <li>
                <a href="#" class="auto padding-l-r-lg parent hvr-buzz-out">
                  <i class="mi-paper"></i>
                  <span class="pull-right text-heading">
                    <i class="text8 icon-bdg_arrow1 text"></i>
                    <i class="text8 icon-bdg_arrow2 text-active"></i>
                  </span>
                  <span class="font-semibold"> Prioritas Usulan </span>
                </a>
                 <ul class="nav nav-sub dk">
                  <li>
                    <a href="{{ url('/') }}/musrenbang/2018/usulan/prioritas/1" class="padding-l-r-lg hvr-buzz-out">
                       <span>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Renja</span>
                    </a>
                  </li>
                  <li>
                    <a href="{{ url('/') }}/musrenbang/2018/usulan/prioritas/2" class="padding-l-r-lg hvr-buzz-out">
                       <span>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; PIPPK</span>
                    </a>
                  </li>
                 </ul>
              </li>     -->


                @if((Auth::user()->level==1 || Auth::user()->level== 2 || Auth::user()->level==3 || Auth::user()->level==4)&& Auth::user()->active == 1)
                <li>
                  <a href="{{ url('/musrenbang/2018/berita') }}" class="auto padding-l-r-lg hvr-buzz-out">
                    <i class="mi-paper"></i>
                    <span class="font-semibold">Berita Acara</span>
                  </a>
                </li>
                @endif

                @if(Auth::user()->level==5 )
                <li>
                  <a href="{{ url('/musrenbang/2017/usulan/daftarBerita') }}" class="auto padding-l-r-lg hvr-buzz-out">
                    <i class="mi-paper"></i>
                    <span class="font-semibold">Berita Acara</span>
                  </a>
                </li>
                @endif
                @if(Auth::user()->level == 6)
                 <li>
                  <a href="{{ url('/musrenbang/2017/usulan/daftarBerita') }}" class="auto padding-l-r-lg hvr-buzz-out">
                    <i class="mi-paper"></i>
                    <span class="font-semibold">Berita Acara</span>
                  </a>
                </li>
                @endif

                @if(Auth::user()->level == 7)
                <li>
                  <a href="{{ url('/musrenbang/2017/berita') }}" class="auto padding-l-r-lg hvr-buzz-out">
                    <i class="mi-paper"></i>
                    <span class="font-semibold">Berita Acara</span>
                  </a>
                </li>
                @endif
                @if(Auth::user()->level != 10)
                <li>
                  <a href="{{ url('/musrenbang/2017/kamus') }}" class="auto padding-l-r-lg hvr-buzz-out">
                    <i class="mi-paper"></i>
                    <span class="font-semibold">Kamus Usulan</span>
                  </a>
                </li>
                @endif
                <li>
                  <a href="{{ url('/musrenbang/2017/profile') }}" class="auto padding-l-r-lg hvr-buzz-out">
                    <i class="mi-paper"></i>
                    <span class="font-semibold">Profil</span>
                  </a>
                </li>

           @endif

              @if(Auth::user()->level == 9)
              <li>
                <a href="#" class="auto padding-l-r-lg hvr-buzz-out">
                  <i class="mi-paper"></i>
                  <span class="pull-right text-heading">
                    <i class="text8 icon-bdg_arrow1 text"></i>
                    <i class="text8 icon-bdg_arrow2 text-active"></i>
                  </span>
                  <span class="font-semibold">Monitoring Tahapan</span>
                </a>
                <ul class="nav nav-sub dk">
                  <li>
                    <a href="{{ url('/') }}/musrenbang/{{$tahun}}/pivot/rw/1" class="padding-l-r-lg">
                       <span> &nbsp; &nbsp; &nbsp; Rembuk RW</span>
                    </a>
                  </li>
                  <li>
                    <a onclick="$.alert('Ditutup!')" class="padding-l-r-lg ">
                       <span> &nbsp; &nbsp; &nbsp; Musrenbang Kelurahan</span>
                    </a>
                  </li>
                   <li>
                    <a onclick="$.alert('Ditutup!')" class="padding-l-r-lg ">
                       <span> &nbsp; &nbsp; &nbsp; Musrenbang Kecamatan</span>
                    </a>
                  </li>
                   <li>
                    <a onclick="$.alert('Ditutup!')" class="padding-l-r-lg ">
                       <span> &nbsp; &nbsp; &nbsp; Forum SKPD</span>
                    </a>
                  </li>
                  <li>
                    <a onclick="$.alert('Ditutup!')" class="padding-l-r-lg ">
                       <span> &nbsp; &nbsp; &nbsp; Musrenbang Kota</span>
                    </a>
                  </li>
                  <li>
                    <a onclick="$.alert('Ditutup!')" class="padding-l-r-lg ">
                       <span> &nbsp; &nbsp; &nbsp; Integrasi SIRA</span>
                    </a>
                  </li>
                  <li>
                    <a onclick="$.alert('Ditutup!')" class="padding-l-r-lg ">
                       <span> &nbsp; &nbsp; &nbsp; Penggangaran APBD</span>
                    </a>
                  </li>
                </ul>
              </li>

              <li>
                <a href="#" class="auto padding-l-r-lg hvr-buzz-out">
                  <i class="mi-paper"></i>
                  <span class="pull-right text-heading">
                    <i class="text8 icon-bdg_arrow1 text"></i>
                    <i class="text8 icon-bdg_arrow2 text-active"></i>
                  </span>
                  <span class="font-semibold">Rekap Usulan</span>
                </a>
                <ul class="nav nav-sub dk">
                  <li>
                    <a href="{{ url('/') }}/musrenbang/{{$tahun}}/admin/renjaSkpd" class="padding-l-r-lg ">
                       <span> &nbsp; &nbsp; &nbsp; RENJA Per SKPD</span>
                    </a>
                  </li>
                  <li>
                    <a href="{{ url('/') }}/musrenbang/{{$tahun}}/admin/pippk" class="padding-l-r-lg ">
                       <span> &nbsp; &nbsp; &nbsp; PIPPK Per Kec</span>
                    </a>
                  </li>
                   <li>
                    <a href="{{ url('/') }}/musrenbang/{{$tahun}}/admin/kecamatan" class="padding-l-r-lg ">
                       <span> &nbsp; &nbsp; &nbsp; Per Kecamatan</span>
                    </a>
                  </li>
                   <li>
                    <a href="{{ url('/') }}/musrenbang/{{$tahun}}/admin/kelurahan" class="padding-l-r-lg ">
                       <span> &nbsp; &nbsp; &nbsp; Per Kelurahan</span>
                    </a>
                  </li>
                </ul>
              </li>


               <li>
                <a href="#" class="auto padding-l-r-lg hvr-buzz-out">
                  <i class="mi-paper"></i>
                  <span class="pull-right text-heading">
                    <i class="text8 icon-bdg_arrow1 text"></i>
                    <i class="text8 icon-bdg_arrow2 text-active"></i>
                  </span>
                  <span class="font-semibold">Data Master</span>
                </a>
                <ul class="nav nav-sub dk">
                   <li>
                    <a href="{{ url('/') }}/musrenbang/{{$tahun}}/public/rw" class="padding-l-r-lg ">
                       <span> &nbsp; &nbsp; &nbsp; RW</span>
                    </a>
                  </li>
                  {{-- <li>
                    <a onclick="$.alert('Ditutup!')" class="padding-l-r-lg ">
                       <span> &nbsp; &nbsp; &nbsp; RW</span>
                    </a>
                  </li> --}}
                  <li>
                    <a href="{{ url('/') }}/musrenbang/{{$tahun}}/public/kelurahan" class="padding-l-r-lg ">
                       <span> &nbsp; &nbsp; &nbsp; Kelurahan</span>
                    </a>
                  </li>
                   <li>
                    <a href="{{ url('/') }}/musrenbang/{{$tahun}}/public/kecamatan" class="padding-l-r-lg ">
                       <span> &nbsp; &nbsp; &nbsp; Kecamatan</span>
                    </a>
                  </li>
                </ul>
              </li>



              <li>
                <a href="#" class="auto padding-l-r-lg hvr-buzz-out">
                  <i class="mi-paper"></i>
                  <span class="pull-right text-heading">
                    <i class="text8 icon-bdg_arrow1 text"></i>
                    <i class="text8 icon-bdg_arrow2 text-active"></i>
                  </span>
                  <span class="font-semibold">Anomali Usulan</span>
                </a>
                <ul class="nav nav-sub dk">
                  <li>
                    <a href="{{ url('/') }}/musrenbang/{{$tahun}}/admin/anomali/renjaanomaliview" class="padding-l-r-lg ">
                       <span> &nbsp; &nbsp; &nbsp; RENJA Nominal Usulan</span>
                    </a>
                  </li>
                  <li>
                    <a href="{{ url('/') }}/musrenbang/{{$tahun}}/admin/anomali/renjajumlahanomaliview" class="padding-l-r-lg ">
                       <span> &nbsp; &nbsp; &nbsp; RENJA > 4</span>
                    </a>
                  </li>
                  <li>
                    <a href="{{ url('/') }}/musrenbang/{{$tahun}}/admin/anomali/pippklkkanomaliview" class="padding-l-r-lg ">
                       <span> &nbsp; &nbsp; &nbsp; PIPPK > 100 jt</span>
                    </a>
                  </li>
                </ul>
              </li>

              <li>
                <a href="{{ url('/') }}/musrenbang/{{$tahun}}/kamusUsulan" class="auto padding-l-r-lg hvr-buzz-out">
                  <i class="mi-paper"></i>
                  <span class="font-semibold">Kamus Usulan</span>
                </a>
              </li>

              <li >
                <a href="#" class="auto padding-l-r-lg parent">
                  <i class="mi-paper"></i>
                  <span class="pull-right text-heading">
                    <i class="text8 icon-bdg_arrow1 text"></i>
                    <i class="text8 icon-bdg_arrow2 text-active"></i>
                  </span>
                  <span class="font-semibold">Berita Acara</span>
                </a>
                 <ul class="nav nav-sub dk">
                  <li>
                    <a onclick="$.alert('Ditutup!')" class="padding-l-r-lg ">
                       <span> &nbsp; &nbsp; &nbsp; Rembuk Warga</span>
                    </a>
                  </li>
                   <li>
                    <a onclick="$.alert('Ditutup!')" class="padding-l-r-lg ">
                       <span> &nbsp; &nbsp; &nbsp; Musrenbang Kelurahan</span>
                    </a>
                  </li>
                  <li>
                    <a onclick="$.alert('Ditutup!')" class="padding-l-r-lg ">
                       <span> &nbsp; &nbsp; &nbsp; Musrenbang Kecamatan</span>
                    </a>
                  </li>
                  <li>
                    <a onclick="$.alert('Ditutup!')" class="padding-l-r-lg ">
                       <span> &nbsp; &nbsp; &nbsp; Forum Gabungan</span>
                    </a>
                  </li>
                  <li>
                    <a  onclick="$.alert('Ditutup!')" class="padding-l-r-lg ">
                       <span> &nbsp; &nbsp; &nbsp; Musrenbang Kota</span>
                    </a>
                  </li>
                  </ul>
              </li>

               <li >
                <a href="#" class="auto padding-l-r-lg parent">
                  <i class="mi-paper"></i>
                  <span class="pull-right text-heading">
                    <i class="text8 icon-bdg_arrow1 text"></i>
                    <i class="text8 icon-bdg_arrow2 text-active"></i>
                  </span>
                  <span class="font-semibold">Pengaturan Akun</span>
                </a>
                 <ul class="nav nav-sub dk">
                  <li>
                    <a href="{{ url('/') }}/musrenbang/{{ $tahun }}/pengaturan/kecamatan" class="padding-l-r-lg ">
                       <span> &nbsp; &nbsp; &nbsp; Kewilayahan</span>
                    </a>
                  </li>
                   <li>
                    <a href="{{ url('/') }}/musrenbang/{{ $tahun }}/pengaturan/skpd" class="padding-l-r-lg ">
                       <span> &nbsp; &nbsp; &nbsp; SKPD</span>
                    </a>
                  </li>
                   <li>
                    <a href="{{ url('/') }}/musrenbang/{{ $tahun }}/pengaturan/admin" class="padding-l-r-lg ">
                       <span> &nbsp; &nbsp; &nbsp; Bappelitbang</span>
                    </a>
                  </li>
                  </ul>
              </li>

              <li>
                <a href="{{ url('/musrenbang/2017/tahapan') }}" class="auto padding-l-r-lg">
                  <i class="mi-paper"></i>
                  <span class="font-semibold">Tahapan Usulan</span>
                </a>
              </li>
              @endif

              <!-- <li>
                <a href="{{ url('/musrenbang/notifikasi/index') }}" class="auto padding-l-r-lg hvr-buzz-out">
                  <i class="mi-paper"></i>
                  <span class="font-semibold">Notifikasi</span>
                </a>
              </li> -->


            </ul>
          </nav>
          <!-- nav -->
        </div>


         @if(Auth::user()->level == 9) <br><br><br><br><br> <br><br> @endif
        <div class="navi-wrap navi-footer navi-footer-white">
          <nav ui-nav class="navi clearfix ">
            <ul class="nav">

              <li class="hidden-folded text-heading text-xs">
                <a href="https://drive.google.com/open?id=0BxIx84aCMiVnZC1mRWRsM2tCRFU" class="padding-l-r-lg hvr-underline-reveal">
                  <span>Download Manual</span>
                </a>
              </li>
              <li class="hidden-folded text-heading text-xs">

                <span>Made with <i class="fa fa-heart text-xs text-danger"></i> in Bandung<br> <p>0.9.3a version</p> </span>

              </li>


            </ul>
          </nav>
        </div>
      </div>
  </aside>
  <!-- / aside -->

<!-- content -->
@yield('content')



</div>

<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Peringatan!</h4>
      </div>
      <div class="modal-body">
        <p>Harap validasi akun anda dengan merubah password dan validasi profile RW dan validasi RT!</p>
      </div>
      <div class="modal-footer">
        <a href="{{ url('/') }}/musrenbang/{{$tahun}}/profile" class="btn btn-warning">Validasi</a>
      </div>
    </div>

  </div>
</div>


<script src="{{ url('/') }}/libs_dashboard/jquery/jquery/dist/jquery.js"></script>
<script src="{{ url('/') }}/libs_dashboard/jquery/bootstrap/dist/js/bootstrap.js"></script>
<script src="{{ url('/') }}/libs_dashboard/jquery/jquery.confirm/js/jquery-confirm.js"></script>
@yield('pluginat')

<script src="{{ url('/') }}/assets/js/ui-load.js"></script>
<script src="{{ url('/') }}/assets/js/ui-jp.config.js"></script>
<script src="{{ url('/') }}/assets/js/ui-jp.js"></script>
<script src="{{ url('/') }}/assets/js/ui-nav.js"></script>
<script src="{{ url('/') }}/assets/js/ui-toggle.js"></script>
<script src="{{ url('/') }}/assets/js/ui-client.js"></script>
<script src="{{ url('/') }}/assets/js/numeral.js"></script>
<script src="{{ url('/') }}/assets/js/custom.js"></script>

 <!-- Popup Belanja Komponen -->
 <script src="{{ asset('toastr/toastr.min.js') }}"></script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-91338267-1', 'auto');
  ga('send', 'pageview');

</script>
<script>
function notify(message,description,alert) {

    toastr.options = {
        closeButton: true,
        progressBar: true,
        showMethod: "slideDown",
        timeOut: 5000,
    };
    if (alert == "error") {
        toastr.error(description, message);
    } else {
        toastr.success(description, message);
    }
}
@if (Session::has('message'))
    notify("{{ session('message_title','Info') }}","{{ session('message') }}","{{ session('message_type','ok') }}");
@endif
</script>

<!--Start of Zendesk Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="https://v2.zopim.com/?4YrzaHQNp7E1JR7FBWrebPIrUtWzMzwH";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zendesk Chat Script-->
<script type="text/javascript">
    $(document).ready(function() {
      @if(Auth::user()->validasi == '0' and Auth::user()->level == '1')
        $('#myModal').modal('show');
      @endif
    });
</script>

<!-- Start of Woopra Code -->
<script>
(function(){
        var t,i,e,n=window,o=document,a=arguments,s="script",r=["config","track","identify","visit","push","call","trackForm","trackClick"],c=function(){var t,i=this;for(i._e=[],t=0;r.length>t;t++)(function(t){i[t]=function(){return i._e.push([t].concat(Array.prototype.slice.call(arguments,0))),i}})(r[t])};for(n._w=n._w||{},t=0;a.length>t;t++)n._w[a[t]]=n[a[t]]=n[a[t]]||new c;i=o.createElement(s),i.async=1,i.src="//static.woopra.com/js/w.js",e=o.getElementsByTagName(s)[0],e.parentNode.insertBefore(i,e)
})("woopra");

woopra.config({
    domain: 'bappeda.bandung.go.id'
});
woopra.track();
</script>
<!-- End of Woopra Code -->

@yield('plugin')
</body>
</html>

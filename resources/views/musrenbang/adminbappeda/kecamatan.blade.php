@extends('musrenbang.adminbappeda.layout')

@section('content')
<div id="content" class="app-content" role="main">
  <div class="hbox hbox-auto-xs hbox-auto-sm ng-scope">
    <div class="col">
      <div class="app-content-body ">

        <div class="bg-light lter">
          <ul class="breadcrumb bg-white m-b-none">
            <li><a href="#" class="btn no-shadow" ui-toggle-class="app-aside-folded" target=".app">
              <i class="icon-bdg_expand1 text"></i>
              <i class="icon-bdg_expand2 text-active"></i>
            </a>   </li>
            <li><a href= "{{ url('/') }}/main">Dashboard</a></li>
            <li class="active"><i class="fa fa-angle-right"></i>Daftar Usulan Kecamatan</li>
          </ul>
        </div>

        <div class="wrapper-lg bg-dark-grey">
          <div class="row">
            <div class="col-md-12">

              <div class="panel bg-white">

                <div class="wrapper-lg">
                  <h5 class="inline font-semibold text-orange m-n ">Daftar Usulan Per Kecamatan</h5>
                  <div class="col-sm-1 pull-right m-t-n-sm">
                      <select class="form-control dtSelect" id="dtSelect">
                            <option value="10">10</option>
                            <option value="25">25</option>
                            <option value="50">50</option>
                            <option value="100">100</option>
                        </select>
                    </div>   
                </div>

                <div class="tab-content tab-content-alt-1 bg-white table-kecamatan" id="table-kecamatan">
                  <div role="tabpanel" class="active tab-pane table-kecamatan" id="tab-1">
                    <div class="table-responsive dataTables_wrapper table-kecamatan">
                      <table id="table" ui-jq="dataTable" ui-options="{
                      sAjaxSource: '{{ url('/') }}/musrenbang/{{$tahun}}/public/kecamatan/getKecamatan',
                      aoColumns: [
                      { mData: 'KECAMATAN_ID',class:'hide' },
                      { mData: 'NO',class:'text-center' },
                      { mData: 'KECAMATAN_USERNAME'},
                      { mData: 'KECAMATAN_NAMA' },
                      { mData: 'KEL' },
                      { mData: 'RW' },
                      { mData: 'RWA' },
                      { mData: 'rwsudahinput' },
                      { mData: 'PIPPK' },
                      { mData: 'RENJA' },
                      { mData: 'totalusulan' },
                      { mData: 'TOTALNOMINALUSULAN' },
                      ]}" class="table table-kecamatan table-striped b-t b-b">
                      <thead>
                        <tr>
                          <th class="hide" rowspan="2">#</th>
                          <th rowspan="2">#</th>
                          <th rowspan="2">ID Kecamatan</th>
                          <th rowspan="2">Kecamatan</th>
                          <th rowspan="2">Total Kelurahan</th>
                          <th rowspan="2">Total RW</th>
                          <th rowspan="2">Total Aktifasi RW</th>
                          <th rowspan="2">Total RW SUDAH INPUT</th>
                          <th colspan="3">Total Usulan</th>
                          <th rowspan="2" >Nominal Usulan</th>
                        </tr>
                        <tr>
                          <th>PIPPK</th>
                          <th>RENJA</th>
                          <th>TOTAL</th>
                        </tr>
                        <tr>
                          <th></th>
                          <th colspan="11" class="th_search">
                            <i class="icon-bdg_search"></i>
                            <input type="search" class="table-search form-control b-none w-full" placeholder="Cari" aria-controls="DataTables_Table_0">
                          </th>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>

<div id="table-kelurahan" class="table-kelurahan hide bg-white">
  <table class="table table-kelurahan table-kelurahan-isi table-striped b-t b-b">
    <thead>
      <tr>
        <th class="hide">ID</th>                    
        <th>Kelurahan</th>                          
        <th>Total RW</th>                       
        <th>Total RW Sudah Input</th>                       
        <th>Total PIPPK</th>                                       
        <th>Total RENJA</th>                                       
        <th>Total Usulan</th>                                       
        <th>Nominal Usulan</th>                                       
      </tr>                                  
    </thead>
    <tbody>
    </tbody>
  </table>
</div>

<div id="table-rw" class="table-rw hide bg-white">
  <table class="table table-rw table-rw-isi table-striped b-t b-b">
    <thead>
      <tr>
        <th class="hide">ID</ th>                          
        <th>RW</th>                          
        <th>Status Aktif</th>                                       
        <th>Total PIPPK</th>                                       
        <th>Total RENJA</th>                                       
      </tr>                                  
    </thead>
    <tbody>
    </tbody>
  </table>
</div>
@endsection
@section('plugin')
<script type="text/javascript">
  $('.table-kecamatan').on('click', '.table-kecamatan > tbody > tr ', function () {
    
    if($("tr").hasClass('program_rincian') == false){
      kode_kecamatan = $(this).children("td").eq(0).html();
    } 
    if(!$(this).hasClass('program_rincian')){
      if($(this).hasClass('shown')){      
        $('.program_rincian').slideUp('fast').remove(); 
        $(this).removeClass('shown'); 
        $('.icon-bdg_arrow6',this).addClass('icon-bdg_arrow5').css({'color':'#b5bbc2'}).removeClass('icon-bdg_arrow6');
      }else{
        $('.program_rincian').slideUp('fast').remove(); 
        $(this).addClass('shown');
        data_detail = '<tr class="program_rincian"><td style="padding:0!important;" colspan="11">' + $('#table-kelurahan').html() + '</td></tr>';
        $(data_detail).insertAfter('.table-kecamatan .table tbody tr.shown');
        $('.icon-bdg_arrow5',this).addClass('icon-bdg_arrow6').css({'color':'#00b0ef'}).removeClass('icon-bdg_arrow5');
        $('.table-kelurahan-isi').DataTable({
          sAjaxSource: "/musrenbang/2017/public/kecamatan/getKelurahan/"+kode_kecamatan,
          aoColumns: [
          { mData: 'ID',class:'hide' },
          { mData: 'KELURAHAN' },
          { mData: 'RW' },
          { mData: 'rwsudahinput'},
          { mData: 'PIPPK' },
          { mData: 'RENJA' },
          { mData: 'totalusulan'},
          { mData: 'TOTALNOMINALUSULAN'},
          ]
        });
        // $('.table-kelurahan-isi').on('click','tbody > tr', function(){
        //   console.log($(this));
        //   if($("tr").hasClass('program_rincian_') == false){
        //     kode_kelurahan = $(this).children("td").eq(0).html();
        //   } 
        //   if(!$(this).hasClass('program_rincian_')){
        //     if($(this).hasClass('shown')){      
        //       $('.program_rincian_').slideUp('fast').remove(); 
        //       $(this).removeClass('shown'); 
        //       $('.icon-bdg_arrow6',this).addClass('icon-bdg_arrow5').css({'color':'#b5bbc2'}).removeClass('icon-bdg_arrow6');
        //     }else{
        //       $('.program_rincian_').slideUp('fast').remove(); 
        //       $(this).addClass('shown');
        //       data_detail = '<tr class="program_rincian_"><td style="padding:0!important;" colspan="6">' + $('#table-rw').html() + '</td></tr>';
        //       $(data_detail).insertAfter('.table-kelurahan-isi .table-kelurahan-isi .table tbody tr.shown');
        //       $('.icon-bdg_arrow5',this).addClass('icon-bdg_arrow6').css({'color':'#00b0ef'}).removeClass('icon-bdg_arrow5');
        //       $('.table-rw-isi').DataTable({
        //         sAjaxSource: "/musrenbang/2017/public/kecamatan/getRw/"+kode_kelurahan,
        //         aoColumns: [
        //         { mData: 'ID',class:'hide' },
        //         { mData: 'RW' },
        //         { mData: 'STATUS' },
        //         { mData: 'PIPPK' },
        //         { mData: 'RENJA' }
        //         ]
        //       });
        //     }
        //   }          
        // })
      }
    }
    });
</script>
@endsection

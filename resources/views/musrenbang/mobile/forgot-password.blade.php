@extends('musrenbang.mobile.layout.layout1')


@section('content')
<div class="top-header bg-blue">
        <div class="container">
          <a href="#" class="pull-left"><i class="fa fa-arrow-left"></i></a>
          <p class="pull-left">RESET PASSWORD</p>
          <a href="#" class="pull-right"><i class="fa fa-ellipsis-v"></i></a>
        </div>
      </div>

      <div class="container wrapper-mobile">
        <div class="login-form">
          <form action="" class="form-horizontal">
            <p>Masukan email anda untuk melakukan setting ulang Password</p>
                  <div class="input-group m-b-md">
                    <span class="input-group-addon bg-white "><i class="fa fa-envelope-o"></i></span>
                    <input type="text" class="input-xxl no-borders form-control" placeholder="Email">
                  </div>

            
            <button type="submit" class="w-full m-b-md btn btn-orange input-xxl">RESET PASSWORD</button>

            
          </form>
        </div>
      </div>

@endsection


@section('plugin')

@endsection      
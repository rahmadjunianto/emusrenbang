@extends('musrenbang.mobile.layout.layout3')


@section('content')

			<div class="top-header bg-blue">
				<div class="container">
					<a href="#" class="menu-mobile pull-left active"><i class="fa fa-bars"></i></a>
					<p class="pull-left">{{$title}} {{$tahun}}</p>
					<a href="#" class="pull-right">
						<!-- <span class="badge badge-sm up bg-danger pull-right-xs">2</span> -->
						<i class="fa  fa-bell-o"></i>
					</a>
				</div>
			</div>

			<div class="tabs-alt-mobile-blue">
                <ul class="nav nav-tabs" role="tablist" id="tab-mobile">
                	<li  class="active">
                          <a data-target="#tab-1" role="tab" data-toggle="tab">
                            Usulan Renja
                          </a>
                    </li>
                    <li>
                          <a data-target="#tab-2" role="tab" data-toggle="tab">
                            Usulan PIPPK
                          </a>
                    </li>                    
                </ul>
            </div>
			

			<div class="content container bg-grey">
				<div class="tab-content tab-content-alt-mobile">
					<div role="tabpanel" class="tab-pane active" id="tab-1">
							<form action="" class="form-horizontal">
								<!-- Search -->
								<div class="input-group m-b-md">			              
					              <input type="text" class="input-xxl no-borders form-control" placeholder="Cari Program Usulan Renja">
					              <span class="input-group-addon bg-white "><i class="fa fa-search"></i></span>
					            </div>
					        </form>

					        <div class="list-card box-shadow">	            		
					            	<div class="list-content">
					            		@foreach($kamus_renja as $kr)
					            		<p>{{$kr->KAMUS_NAMA}} <br />
					            			<span class="grey">Satuan </span>
					            			<span>{{$kr->KAMUS_SATUAN}}</span><br>
					            			<span class="grey">Jenis </span>
					            			<span>{{$kr->KAMUS_JENIS}}</span>
					            			@if(Auth::user()->level == 5 || Auth::user()->level == 6 )
					            			<span class="m-l-md grey">Anggaran:</span><span>{{$kr->KAMUS_HARGA}}</span>
					            			@endif
					            		</p>
					            		@endforeach
					            	</div>
				            </div>
				           
				    </div>

				    <div role="tabpanel" class="tab-pane" id="tab-2">
							<form action="" class="form-horizontal">
								<!-- Search -->
								<div class="input-group m-b-md">			              
					              <input type="text" class="input-xxl no-borders form-control" placeholder="Cari Program Usulan PIPPK">
					              <span class="input-group-addon bg-white "><i class="fa fa-search"></i></span>
					            </div>
					        </form>

				            <div class="list-card box-shadow">	            		
					            	<div class="list-content">
					            		@foreach($kamus_pippk as $kp)
					            		<p>{{$kp->KAMUS_NAMA}} <br />
					            			<span class="grey">Satuan </span>
					            			<span>{{$kp->KAMUS_SATUAN}}</span><br>
					            			<span class="grey">Jenis </span>
					            			<span>{{$kp->KAMUS_JENIS}}</span>
					            			@if(Auth::user()->level == 5 || Auth::user()->level == 6 )
					            			<span class="m-l-md grey">Anggaran:</span><span>{{$kp->KAMUS_HARGA}}</span>
					            			@endif
					            		</p>
					            		@endforeach
					            	</div>
				            </div>
				    </div>

				   
				</div>
	            
				

	             
			    
			</div>
@endsection


@section('plugin')

@endsection      
@extends('musrenbang.mobile.layout.layout2')


@section('content')
      

      <div class="content container bg-grey">
        
              <div class="list-card box-shadow">                  
                  <div class="list-content notifikasi">
                    <p><i class="fa fa-circle red"></i>Anda Memiliki <span>203</span> usulan untuk divalidasi <span class="time">2 hours ago</span></p>
                    <p><i class="fa fa-circle yellow"></i>RT 02, RW.08, <span>Kecamatan Andir</span> Menambah-kan <span>4</span> Usulan Baru <span class="time">2 hours ago</span></p>
                    <p><i class="fa fa-circle green"></i>Usulan Anda Disetujui Tingkat <span>Kec. Andir</span> <span class="time">2 hours ago</span></p>
                    
                  </div>
              </div>
              
      </div>

@endsection


@section('plugin')

@endsection      
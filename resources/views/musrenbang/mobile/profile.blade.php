@extends('musrenbang.mobile.layout.layout2')


@section('content')
			

			<div class="content container bg-grey">
				
				<form action="" class="form-horizontal">
					<!-- Search -->
					<div class="input-group m-b-md">			              
		              <input type="text" class="input-xxl no-borders form-control" placeholder="Cari Daftar Kecamatan">
		              <span class="input-group-addon bg-white "><i class="fa fa-search"></i></span>
		            </div>
		        </form>

	            <div class="list-card box-shadow">
	            	<div class="heading full">	            		
	            		<p>Daftar Kecamatan <span>Usulan</span></p>

	            	</div>
	            	<div class="list-content">
	            		<div class="row">
	            			<div class="col-xs-8 padder-right-none"><p>Kecamatan Andir</p></div>
	            			<div class="col-xs-4 padder-left-none"><p class="text-right text-green">100</p></div>

	            			<ul class="child-kecamatan">
	            				<li><i class="fa fa-circle"></i>Kelurahan Antapani Kidul <span>20</span></li>
	            				<li><i class="fa fa-circle"></i>Kelurahan Antapani Kulon <span>30</span></li>
	            				<li><i class="fa fa-circle"></i>Kelurahan Antapani Tengah <span>30</span></li>
	            				<li><i class="fa fa-circle"></i>Kelurahan Antapani Wetan <span>20</span></li>
	            			</ul>
	            		</div>

	            		<div class="row">
	            			<div class="col-xs-8 padder-right-none"><p>Kecamatan Antapani / Cicadas</p></div>
	            			<div class="col-xs-4 padder-left-none"><p class="text-right text-green">201</p></div>
	            		</div>

	            		<div class="row">
	            			<div class="col-xs-8 padder-right-none"><p>Kecamatan Arcamanik</p></div>
	            			<div class="col-xs-4 padder-left-none"><p class="text-right text-green">78</p></div>
	            		</div>

	            		<div class="row">
	            			<div class="col-xs-8 padder-right-none"><p>Kecamatan Astana Anyar</p></div>
	            			<div class="col-xs-4 padder-left-none"><p class="text-right text-green">45</p></div>
	            		</div>

	            		<div class="row">
	            			<div class="col-xs-8 padder-right-none"><p>Kecamatan Babakan Ciparay</p></div>
	            			<div class="col-xs-4 padder-left-none"><p class="text-right text-green">123</p></div>
	            		</div>

	            		<div class="row">
	            			<div class="col-xs-8 padder-right-none"><p>Kecamatan Gunung Kidul</p></div>
	            			<div class="col-xs-4 padder-left-none"><p class="text-right text-green">15</p></div>
	            		</div>

	            		<div class="row">
	            			<div class="col-xs-8 padder-right-none"><p>Kecamatan Bandung Kulon</p></div>
	            			<div class="col-xs-4 padder-left-none"><p class="text-right text-green">22</p></div>
	            		</div>

	            		<div class="row">
	            			<div class="col-xs-8 padder-right-none"><p>Kecamatan Bandung Wetan</p></div>
	            			<div class="col-xs-4 padder-left-none"><p class="text-right text-green">19</p></div>
	            		</div>	            	

	            		
	            	</div>
	            </div>

	             
			    
			</div>
@endsection


@section('plugin')
<script>
      var oldMarker;
      function initMap() {
        var bandung = {lat: -6.914744, lng: 107.609810};
        var map = new google.maps.Map(document.getElementById('maps'), {
          zoom: 17,
          center: bandung,
          navigationControl: false,
	      mapTypeControl: false,
	      scaleControl: false,
	      draggable: false,
	      scrollwheel: false,
        });
                
      }
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBzixLw-QwjOpEiL8qmveEmafmgM27mgnk&callback=initMap">
    </script>
@endsection      
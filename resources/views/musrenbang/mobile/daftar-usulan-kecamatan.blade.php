@extends('musrenbang.mobile.layout.layout2')


@section('content')
<div class="content container bg-grey">

	            <div class="list-card box-shadow">
	            	<div class="heading">
	            		<i class="fa fa-user-circle"></i>
	            		<p>Kelurahan Kebon Jeruk</p>
	            	</div>
	            	<div class="list-content">
	            		<p>Banyaknya RW : <span>25</span></p>
	            		<p>Banyaknya usulan : <span>210  dari 8 RW</span></p>
	            		<p>Besaran Dana : <span>Rp. 10.000.000,00</span></p>
	            		<button type="submit" class="w-full m-b-md btn btn-soft-green input-xxl">Lihat Usulan per RW</button>
	            	</div>
	            </div>

	             <div class="list-card box-shadow">
	            	<div class="heading">
	            		<i class="fa fa-user-circle"></i>
	            		<p>Kelurahan Ciroyom</p>
	            	</div>
	            	<div class="list-content">
	            		<p>Banyaknya RW : <span>12</span></p>
	            		<p>Banyaknya usulan : <span>210  dari 8 RW</span></p>
	            		<p>Besaran Dana : <span>Rp. 10.000.000,00</span></p>
	            		<button type="submit" class="w-full m-b-md btn btn-soft-green input-xxl">Lihat Usulan per RW</button>
	            	</div>
	            </div>

	             <div class="list-card box-shadow">
	            	<div class="heading">
	            		<i class="fa fa-user-circle"></i>
	            		<p>Kelurahan Dungus Cariang</p>
	            	</div>
	            	<div class="list-content">
	            		<p>Banyaknya RW : <span>12</span></p>
	            		<p>Banyaknya usulan : <span>210  dari 8 RW</span></p>
	            		<p>Besaran Dana : <span>Rp. 10.000.000,00</span></p>
	            		<button type="submit" class="w-full m-b-md btn btn-soft-green input-xxl">Lihat Usulan per RW</button>
	            	</div>
	            </div>
			    
			</div>
@endsection


@section('plugin')

@endsection      
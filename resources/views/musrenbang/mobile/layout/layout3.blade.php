 <!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>.bdg Musrenbang</title>
  <meta name="description" content="Bandung Web Kit" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="stylesheet" href="{{ url('/') }}/libs_dashboard/assets/animate.css/animate.css" type="text/css" />
    <link rel="stylesheet" href="{{ url('/') }}/libs_dashboard/assets/font-awesome/css/font-awesome.min.css" type="text/css" />
    <link rel="stylesheet" href="{{ url('/') }}/libs_dashboard/assets/simple-line-icons/css/simple-line-icons.css" type="text/css" />
    <link rel="stylesheet" href="{{ url('/') }}/libs_dashboard/jquery/bootstrap/dist/css/bootstrap.css" type="text/css" />

  
  <link rel="stylesheet" href="{{ url('/') }}/mobile_assets/css/font.css" type="text/css" />
  <link rel="stylesheet" href="{{ url('/') }}/mobile_assets/fonts/simdaicon/simdaicon.css" type="text/css" />
  <link rel="stylesheet" href="{{ url('/') }}/mobile_assets/fonts/simdaicon/mobileicon.css" type="text/css" />
  <link rel="stylesheet" href="{{ url('/') }}/mobile_assets/css/style.css" type="text/css" />
  <link rel="stylesheet" href="{{ url('/') }}/mobile_assets/css/mobile.css" type="text/css" />


  @yield('css')

</head>

<body class="pages">
  <div class="bg-screen bg-white">


    <!-- content -->
    @yield('content')

   <aside class="app-aside hidden-xs active">
      <div class="profile">
        <span class="thumb-md avatar pull-left m-t-n-sm m-b-n-sm m-r-sm">
                  <img src="{{ url('/') }}/RW.gif" alt="...">                
              </span>
              <p class="pull-left">{{$user->name}}</p>
              <p class="pull-left"><span>KEL.{{$user->kelurahan->KEL_NAMA}}, KEC.{{$user->kecamatan->KEC_NAMA}}</span></p>
      </div>

      <nav>
        <ul>
          <li class="parent">
            <a href="#"><i class="fa fa-pencil m-r-sm"></i> Input Usulan <i class="pull-right fa fa-chevron-down m-t-xxs"></i></a>
            <ul class="submenu">
              <li class="active"><a href="{{ url('/') }}/musrenbang/2017/mobile/input/1">Renja</a></li>
              <li><a href="{{ url('/') }}/musrenbang/2017/mobile/input/2">PIPPK</a></li>
            </ul>
          </li>
          <li class="parent">
            <a href="#"><i class="fa fa-pencil m-r-sm"></i> Rekap Usulan <i class="pull-right fa fa-chevron-down m-t-xxs"></i></a>
            <ul class="submenu">
              <li><a href="{{ url('/') }}/musrenbang/2017/mobile/daftarUsulan/1">Renja</a></li>
              <li><a href="{{ url('/') }}/musrenbang/2017/mobile/daftarUsulan/2">PIPPK</a></li>
            </ul>
          </li>
          <li><a href="{{ url('/musrenbang/2017/mobile/kamus') }}"><i class="fa fa-bookmark-o m-r-sm"></i> Kamus Usulan</a></li>
          <li><a href="{{ url('/musrenbang/2017/mobile/berita') }}"><i class="fa fa-list-alt m-r-sm"></i> Berita acara</a></li>
          <li><a href="{{ url('/') }}/keluar"><i class="fa fa-sign-out m-r-sm"></i> Logout</a></li>
        </ul>
      </nav>
  </aside>
    <!-- .bg-screen -->  
   




</div>

  <script src="{{ url('/') }}/libs_dashboard/jquery/jquery/dist/jquery.js"></script>
  <script src="{{ url('/') }}/libs_dashboard/jquery/bootstrap/dist/js/bootstrap.js"></script>
  <script src="{{ url('/') }}/mobile_assets/js/ui-load.js"></script>
  <script src="{{ url('/') }}/mobile_assets/js/ui-jp.config.js"></script>
  <script src="{{ url('/') }}/mobile_assets/js/ui-jp.js"></script>
  <script src="{{ url('/') }}/mobile_assets/js/ui-nav.js"></script>
  <script src="{{ url('/') }}/mobile_assets/js/ui-toggle.js"></script>
  <script src="{{ url('/') }}/mobile_assets/js/ui-client.js"></script>
  <script src="{{ url('/') }}/mobile_assets/js/custom.js"></script>

  @yield('pluginat')


@yield('plugin')


</body>

</html>


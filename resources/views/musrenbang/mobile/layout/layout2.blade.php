 <!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>.bdg Musrenbang</title>
  <meta name="description" content="Bandung Web Kit" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="stylesheet" href="{{ url('/') }}/libs_dashboard/assets/animate.css/animate.css" type="text/css" />
    <link rel="stylesheet" href="{{ url('/') }}/libs_dashboard/assets/font-awesome/css/font-awesome.min.css" type="text/css" />
    <link rel="stylesheet" href="{{ url('/') }}/libs_dashboard/assets/simple-line-icons/css/simple-line-icons.css" type="text/css" />
    <link rel="stylesheet" href="{{ url('/') }}/libs_dashboard/jquery/bootstrap/dist/css/bootstrap.css" type="text/css" />

  
  <link rel="stylesheet" href="{{ url('/') }}/mobile_assets/css/font.css" type="text/css" />
  <link rel="stylesheet" href="{{ url('/') }}/mobile_assets/fonts/simdaicon/simdaicon.css" type="text/css" />
  <link rel="stylesheet" href="{{ url('/') }}/mobile_assets/fonts/simdaicon/mobileicon.css" type="text/css" />
  <link rel="stylesheet" href="{{ url('/') }}/mobile_assets/css/style.css" type="text/css" />
  <link rel="stylesheet" href="{{ url('/') }}/mobile_assets/css/mobile.css" type="text/css" />


  @yield('css')

</head>

<body class="pages">
  <div class="bg-screen bg-white">

    <div class="top-header bg-blue">
        <div class="container">
          <a href="#" class="menu-mobile pull-left active"><i class="fa fa-bars"></i></a>
          <p class="pull-left">{{$title}}</p>
          <a href="#" class="pull-right">
           <!--  <span class="badge badge-sm up bg-danger pull-right-xs">2</span> -->
            <i class="fa  fa-bell-o"></i>
          </a>
        </div>
      </div>

      <div class="top-profile">
        <div class="profile">
          <span class="thumb-md avatar m-t-n-sm m-b-n-sm m-r-sm">
                    <img src="{{ url('/') }}/RW.gif" alt="...">                
                </span>
                <p>{{$user->name}}</p>
                <p><span>KEL.{{$user->kelurahan->KEL_NAMA}}, KEC.{{$user->kecamatan->KEC_NAMA}}</span></p>
        </div>
      </div>

    <!-- content -->
    @yield('content')

    <aside class="app-aside hidden-xs active">
      <div class="profile">
        <span class="thumb-md avatar pull-left m-t-n-sm m-b-n-sm m-r-sm">
                  <img src="{{ url('/') }}/RW.gif" alt="...">                
              </span>
              <p class="pull-left">{{$user->name}}</p>
              <p class="pull-left"><span>KEL.{{$user->kelurahan->KEL_NAMA}}, KEC.{{$user->kecamatan->KEC_NAMA}}</span></p>
      </div>

      <nav>
        <ul>
          <li class="parent">
            <a href="#"><i class="fa fa-pencil m-r-sm"></i> Input Usulan <i class="pull-right fa fa-chevron-down m-t-xxs"></i></a>
            <ul class="submenu">
              <li><a href="{{ url('/') }}/musrenbang/2017/usulan/tambah/1">Renja</a></li>
              <li><a href="{{ url('/') }}/musrenbang/2017/usulan/tambah/2">PIPPK</a></li>
            </ul>
          </li>
          <li class="parent">
            <a href="#"><i class="fa fa-book m-r-sm"></i> Rekap Usulan <i class="pull-right fa fa-chevron-down m-t-xxs"></i></a>
            <ul class="submenu">
              <li><a href="{{ url('/') }}/musrenbang/2017/mobile/daftarUsulan/1">Renja</a></li>
              <li><a href="{{ url('/') }}/musrenbang/2017/mobile/daftarUsulan/2">PIPPK</a></li>
            </ul>
          </li>
          <li><a href="{{ url('/musrenbang/2017/mobile/kamus') }}"><i class="fa fa-bookmark-o m-r-sm"></i> Kamus Usulan</a></li>
          <li><a href="{{ url('/musrenbang/2017/berita') }}"><i class="fa fa-list-alt m-r-sm"></i> Berita acara</a></li>
          <li><a href="{{ url('/') }}/keluar"><i class="fa fa-sign-out m-r-sm"></i> Logout</a></li>
        </ul>
      </nav>
  </aside>
    <!-- .bg-screen -->  
   

@yield('pluginat')


@yield('plugin')


</div>

  <script src="{{ url('/') }}/libs_dashboard/jquery/jquery/dist/jquery.js"></script>
  <script src="{{ url('/') }}/libs_dashboard/jquery/bootstrap/dist/js/bootstrap.js"></script>
  <script src="{{ url('/') }}/mobile_assets/js/ui-load.js"></script>
  <script src="{{ url('/') }}/mobile_assets/js/ui-jp.config.js"></script>
  <script src="{{ url('/') }}/mobile_assets/js/ui-jp.js"></script>
  <script src="{{ url('/') }}/mobile_assets/js/ui-nav.js"></script>
  <script src="{{ url('/') }}/mobile_assets/js/ui-toggle.js"></script>
  <script src="{{ url('/') }}/mobile_assets/js/ui-client.js"></script>
  <script src="{{ url('/') }}/mobile_assets/js/custom.js"></script>

 <script src="{{ asset('toastr/toastr.min.js') }}"></script>


</body>

</html>


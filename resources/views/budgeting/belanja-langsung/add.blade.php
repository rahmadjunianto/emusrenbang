@extends('budgeting.layout')

@section('content')
<div id="content" class="app-content" role="main">
  <div class="hbox hbox-auto-xs hbox-auto-sm ng-scope">
    <div class="col">
      <div class="app-content-body ">

          <div class="bg-light lter">    
              <ul class="breadcrumb bg-white m-b-none">
                <li><a href="#" class="btn no-shadow" ui-toggle-class="app-aside-folded" target=".app">
                  <i class="icon-bdg_expand1 text"></i>
                  <i class="icon-bdg_expand2 text-active"></i>
                </a>   </li>
                <li><a href= "{{ url('/') }}/main">Dashboard</a></li>
                <li><i class="fa fa-angle-right"></i>Belanja</li>                               
                <li><i class="fa fa-angle-right"></i>Belanja Langsung</li>                                
                <li class="active"><i class="fa fa-angle-right"></i>Tambah Belanja Langsung</li>                                
              </ul>
          </div>

          <div class="wrapper-lg bg-dark-grey">
            <div class="row">
              <div class="col-md-12">
                
                <div class="panel bg-white">

                  <div class="panel-heading wrapper-lg">
                    <h5 class="inline font-semibold text-orange m-n ">Tambah Belanja Langsung</h5>
                  </div>

                  <div class="tab-content tab-content-alt-1 bg-white">
                        <div class="bg-white wrapper-lg input-jurnal">
                            <form action="#" class="form-horizontal">
                              <div class="input-wrapper">
                                  <div class="form-group">
                                    <label class="col-sm-1">Program</label>
                                    <div class="col-sm-5">
                                      <select ui-jq="chosen" class="w-full">
                                          <option value="">Pilih Program</option>
                                          <option value="kegiatanA">Program A</option>
                                          <option value="kegiatanA">Program B</option>
                                          <option value="kegiatanA">Program C</option>
                                          <option value="kegiatanA">Program D</option>
                                      </select>
                                    </div>
                                    <label class="col-sm-1">Kegiatan</label>
                                    <div class="col-sm-5">
                                      <select ui-jq="chosen" class="w-full">
                                          <option value="">Pilih Kegiatan</option>
                                          <option value="kegiatanA">Kegiatan A</option>
                                          <option value="kegiatanA">Kegiatan A</option>
                                          <option value="kegiatanA">Kegiatan A</option>
                                          <option value="kegiatanA">Kegiatan A</option>
                                      </select>
                                    </div>
                                  </div>

                                  <hr class="m-t-xl">
                                  <div class="form-group">
                                  <h5 class="text-orange">Detail Kegiatan</h5>
                                    <label class="col-sm-1">Jenis Kegiatan</label>
                                    <div class="col-sm-5">
                                      <select ui-jq="chosen" class="w-full">
                                          <option value="">Pilih Program</option>
                                          <option value="kegiatanA">Program A</option>
                                          <option value="kegiatanA">Program B</option>
                                          <option value="kegiatanA">Program C</option>
                                          <option value="kegiatanA">Program D</option>
                                      </select>
                                    </div>
                                    <label class="col-sm-1">Sumber Dana</label>
                                    <div class="col-sm-5">
                                      <select ui-jq="chosen" class="w-full">
                                          <option value="">Pilih Kegiatan</option>
                                          <option value="kegiatanA">Kegiatan A</option>
                                          <option value="kegiatanA">Kegiatan A</option>
                                          <option value="kegiatanA">Kegiatan A</option>
                                          <option value="kegiatanA">Kegiatan A</option>
                                      </select>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-sm-1">Kategori Pagu</label>
                                    <div class="col-sm-5">
                                      <select ui-jq="chosen" class="w-full">
                                          <option value="">Pilih Program</option>
                                          <option value="kegiatanA">Program A</option>
                                          <option value="kegiatanA">Program B</option>
                                          <option value="kegiatanA">Program C</option>
                                          <option value="kegiatanA">Program D</option>
                                      </select>
                                    </div>
                                    <label class="col-sm-1">Waktu Kegiatan</label>
                                    <div class="col-sm-2">
                                      <select ui-jq="chosen" class="w-full">
                                          <option value="">Dari Bulan</option>
                                          <option value="kegiatanA">Kegiatan A</option>
                                          <option value="kegiatanA">Kegiatan A</option>
                                          <option value="kegiatanA">Kegiatan A</option>
                                          <option value="kegiatanA">Kegiatan A</option>
                                      </select>
                                    </div>
                                    <div class="col-sm-2">
                                      <select ui-jq="chosen" class="w-full">
                                          <option value="">Sampai Bulan</option>
                                          <option value="kegiatanA">Kegiatan A</option>
                                          <option value="kegiatanA">Kegiatan A</option>
                                          <option value="kegiatanA">Kegiatan A</option>
                                          <option value="kegiatanA">Kegiatan A</option>
                                      </select>
                                    </div>
                                  </div>

                                  <div class="form-group">
                                    <label class="col-sm-1">Sasaran Kegiatan</label>
                                    <div class="col-sm-5">
                                      <select ui-jq="chosen" class="w-full">
                                          <option value="">Pilih Sasaran</option>
                                          <option value="kegiatanA">Program A</option>
                                          <option value="kegiatanA">Program B</option>
                                          <option value="kegiatanA">Program C</option>
                                          <option value="kegiatanA">Program D</option>
                                      </select>
                                    </div>
                                    <label class="col-sm-1">Tagging Kegiatan</label>
                                    <div class="col-sm-2">
                                      <select ui-jq="chosen" class="w-full">
                                          <option value="">Tag 1</option>
                                          <option value="kegiatanA">Program A</option>
                                          <option value="kegiatanA">Program B</option>
                                          <option value="kegiatanA">Program C</option>
                                          <option value="kegiatanA">Program D</option>
                                      </select>
                                    </div>
                                    <div class="col-sm-2">
                                      <select ui-jq="chosen" class="w-full">
                                          <option value="">Tag 2</option>
                                          <option value="kegiatanA">Program A</option>
                                          <option value="kegiatanA">Program B</option>
                                          <option value="kegiatanA">Program C</option>
                                          <option value="kegiatanA">Program D</option>
                                      </select>
                                    </div>
                                  </div>                                  

                                  <div class="form-group">
                                    <label class="col-sm-1">Lokasi Kegiatan</label>
                                    <div class="col-sm-3">
                                      <select ui-jq="chosen" class="w-full">
                                          <option value="">Pilih Lokasi</option>
                                          <option value="kegiatanA">Program A</option>
                                          <option value="kegiatanA">Program B</option>
                                          <option value="kegiatanA">Program C</option>
                                          <option value="kegiatanA">Program D</option>
                                      </select>
                                    </div>
                                    <label class="col-sm-1">Lat</label>
                                    <div class="col-sm-3">
                                      <input type="text" class="form-control" name="lat">
                                    </div>
                                    <label class="col-sm-1">Long</label>
                                    <div class="col-sm-3">
                                      <input type="text" class="form-control" name="lat">
                                    </div>
                                  </div>                                  
                                  
                                  <hr class="m-t-xl">

                                  <div class="form-group">
                                    <h5 class="text-orange">Indikator Kegiatan</h5>
                                    <label class="col-sm-1">Indikator</label>
                                    <label class="col-sm-6">Tolak Ukur</label>
                                    <label class="col-sm-2">Target</label>
                                    <label class="col-sm-2">Satuan</label>
                                    <label class="col-sm-1">#</label>
                                  </div>

                                  <div class="form-group">
                                    <label class="col-sm-1">Capaian Program</label>
                                    <div class="col-sm-6">
                                      <input type="text" class="form-control" name="capaian">
                                    </div>
                                    <div class="col-sm-2">
                                      <input type="text" class="form-control" name="target">
                                    </div>
                                    <div class="col-sm-2">
                                      <select ui-jq="chosen" class="w-full">
                                          <option value="">Satuan</option>
                                          <option value="kegiatanA">Program A</option>
                                          <option value="kegiatanA">Program B</option>
                                          <option value="kegiatanA">Program C</option>
                                          <option value="kegiatanA">Program D</option>
                                      </select>
                                    </div>
                                    <div class="col-sm-1">
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-sm-1">Input Kegiatan</label>
                                    <div class="col-sm-6">
                                      <input type="text" class="form-control" name="capaian">
                                    </div>
                                    <div class="col-sm-2">
                                      <input type="text" class="form-control" name="target">
                                    </div>
                                    <div class="col-sm-2">
                                      <select ui-jq="chosen" class="w-full">
                                          <option value="">Satuan</option>
                                          <option value="kegiatanA">Program A</option>
                                          <option value="kegiatanA">Program B</option>
                                          <option value="kegiatanA">Program C</option>
                                          <option value="kegiatanA">Program D</option>
                                      </select>
                                    </div>
                                    <div class="col-sm-1">
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-sm-1">Output Kegiatan</label>
                                    <div class="col-sm-6">
                                      <input type="text" class="form-control" name="capaian">
                                    </div>
                                    <div class="col-sm-2">
                                      <input type="text" class="form-control" name="target">
                                    </div>
                                    <div class="col-sm-2">
                                      <select ui-jq="chosen" class="w-full">
                                          <option value="">Satuan</option>
                                          <option value="kegiatanA">Program A</option>
                                          <option value="kegiatanA">Program B</option>
                                          <option value="kegiatanA">Program C</option>
                                          <option value="kegiatanA">Program D</option>
                                      </select>
                                    </div>
                                    <div class="col-sm-1">
                                      <button class="btn btn-success"><i class="fa fa-plus"></i></button>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="col-sm-1">Hasil</label>
                                    <div class="col-sm-6">
                                      <input type="text" class="form-control" name="capaian">
                                    </div>
                                    <div class="col-sm-2">
                                      <input type="text" class="form-control" name="target">
                                    </div>
                                    <div class="col-sm-2">
                                      <select ui-jq="chosen" class="w-full">
                                          <option value="">Satuan</option>
                                          <option value="kegiatanA">Program A</option>
                                          <option value="kegiatanA">Program B</option>
                                          <option value="kegiatanA">Program C</option>
                                          <option value="kegiatanA">Program D</option>
                                      </select>
                                    </div>
                                    <div class="col-sm-1">
                                    </div>
                                  </div>

                                  <hr class="m-t-xl">
                                  <div class="form-group">
                                    <div class="col-md-12">
                                      
                                 <a class="btn input-xl m-t-md btn-danger pull-right"><i class="fa fa-close m-r-xs"></i>Batal</a>
                                 <button class="btn input-xl m-t-md btn-success pull-right" type="submit" style="margin-right: 10px;"><i class="fa fa-check m-r-xs"></i>Simpan</button>
                                    </div>

                                  </div>
                              </div>
                              

                              
                              <!--/input-wrapper -->
                            </form>


                          </div>
                  </div>
                </div>

                
              </div>
            </div>

          </div>
          
      
        
        

      </div>
      <!-- App-content-body -->  

    </div>
    <!-- .col -->


    </div>
    <!-- end hbox hbox-auto-xs -->

    <!-- Input SPP -->
  

<!-- / Input SPP -->


</div>
@endsection

@section('plugin')
@endsection
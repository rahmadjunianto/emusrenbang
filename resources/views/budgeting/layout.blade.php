<!DOCTYPE html>
<html lang="en" class="">
<head>
  <meta charset="utf-8" />
  <title>.bdg Planning & Budgeting</title>
  <meta name="description" content="Bandung Web Kit" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
  <link rel="stylesheet" href="{{ url('/') }}/libs_dashboard/assets/animate.css/animate.css" type="text/css" />
  <link rel="stylesheet" href="{{ url('/') }}/libs_dashboard/assets/font-awesome/css/font-awesome.min.css" type="text/css" />
  <link rel="stylesheet" href="{{ url('/') }}/libs_dashboard/assets/simple-line-icons/css/simple-line-icons.css" type="text/css" />
  <link rel="stylesheet" href="{{ url('/') }}/libs_dashboard/jquery/bootstrap/dist/css/bootstrap.css" type="text/css" /> 
  <link rel="stylesheet" href="{{ url('/') }}/libs_dashboard/jquery/jquery.confirm/css/jquery-confirm.css" type="text/css" />
  <link rel="stylesheet" href="{{ url('/') }}/assets/css/font.css" type="text/css" />
  <link rel="stylesheet" href="{{ url('/') }}/assets/css/style.css" type="text/css" />
</head>
<body>
<div class="app app-header-fixed ">
    <!-- header -->
  <header id="header" class="app-header navbar" role="menu">
      <!-- navbar header -->
      <div class="navbar-header bg-info">
        <button class="pull-right visible-xs dk" ui-toggle-class="show" target=".navbar-collapse">
          <i class="glyphicon glyphicon-cog"></i>
        </button>
        <button class="pull-right visible-xs" ui-toggle-class="off-screen" target=".app-aside" ui-scroll="app">
          <i class="glyphicon glyphicon-align-justify"></i>
        </button>
        <!-- brand -->
        <a href="#" class="navbar-brand text-lt">          
          <img src="{{ url('/') }}/assets/img/logo-small.png" srcset="{{ url('/') }}/assets/img/logo-small@2x.png 2x" alt="." class="small-logo hide">
          <img src="{{ url('/') }}/assets/img/logo.png" srcset="{{ url('/') }}/assets/img/logo@2x.png 2x" alt="." class="large-logo">
        </a>
        <!-- / brand -->
      </div>
      <!-- / navbar header -->

      <!-- navbar collapse -->
      <div class="collapse pos-rlt navbar-collapse bg-info">
        <!-- buttons -->
        <div class="nav navbar-nav hidden-xs">
                  
        </div>
        <!-- / buttons -->

        

        <!-- nabar right -->
        <ul class="nav navbar-nav navbar-right">
         <li class="dropdown">
            <!-- <a href="#" data-toggle="dropdown" class="dropdown-toggle">
              <i class="icon-bdg_alert text14"></i>
              <span class="visible-xs-inline">Notifikasi</span>
              <span class="badge badge-sm up bg-danger pull-right-xs">2</span>
            </a> -->
            <!-- dropdown -->            
            <div class="dropdown-menu w-xl animated fadeIn">
              <div class="panel bg-white">                
                <div class="list-group">
                  <a href="" class="list-group-item">
                    <span class="pull-left m-r-sm">
                      <i class="fa fa-circle text-warning"></i>
                    </span>
                    <span class="clear block m-l-md m-b-none">
                      Data SPP telah diinput, silahkan lakukan verifikasi
                    </span>
                  </a>
                  <a href="" class="list-group-item">
                    <span class="clear block m-b-none m-l-md">
                      Data SPP telah diinput, silahkan lakukan verifikasi
                    </span>
                  </a>
                  <a href="" class="list-group-item text-center">
                    <span class="clear block m-b-none text-primary" >
                      Lihat Semua
                    </span>
                  </a>
                </div>
                
              </div>
            </div>
            <!-- / dropdown -->
          </li>
        
         
          <li class="dropdown">
            <div class="separator"></div>
          </li>

           <li class="dropdown padder-md">
              <p class="font-semibold m-t-lg text-white pull-left">Tahun Anggaran : </p>
             <div class="m-t-md pull-right">
                  <select name="year" id="" class="form-control transparent-select">
                    <option value="2016">2016</option>
                    <option value="2017">2017</option>
                    <option value="2018">2018</option>
                    <option value="2019">2019</option>
                  </select>
              </div>
           </li>

           <li class="dropdown">
            <div class="separator"></div>
          </li>

          <li class="dropdown">
            <a href="#" data-toggle="dropdown" class="bg-none profile-header dropdown-toggle clear" data-toggle="dropdown">
             
              <span class="hidden-sm hidden-md m-r-xl">Ridwan Kamil</span> <i class="text14 icon-bdg_setting3 pull-right"></i>
            </a>
            <!-- dropdown -->
            <ul class="dropdown-menu animated fadeIn w-ml">             
              <li>
                <a href>
                  <i class="icon-bdg_chat"></i>
                  <span>Help</span>
                </a>
              </li>
              <li>
                <a href>
                  <i class="icon-bdg_people"></i>
                  <span>Profile</span>
                </a>
              </li>
              <li>
                <a href>
                  <i class="icon-bdg_setting3"></i>
                  <span>Account Settings</span>
                </a>
              </li>
              
             
              <li class="divider"></li>
              <li>
                <a href="index.html">
                  <i class="icon-bdg_arrow4"></i>
                  <span>Logout</span>
                </a>
              </li>
            </ul>
            <!-- / dropdown -->
          </li>
        </ul>
        <!-- / navbar right -->
      </div>
      <!-- / navbar collapse -->
  </header>
  <!-- / header -->


    <!-- aside -->
  <aside id="aside" class="app-aside hidden-xs bg-white">
      <div class="aside-wrap">
        <div class="navi-wrap">         
         <!-- nav -->
          <nav ui-nav class="navi white-navi clearfix">
            <ul class="nav">
              <li>
                <a href="jurnal.html" class="auto padding-l-r-lg">                  
                  <i class="mi-paper"></i>
                  <span class="font-semibold">Dashboard</span>
                </a>                
              </li>
              <li >
                <a href="#" class="auto padding-l-r-lg parent">                  
                  <i class="mi-paper"></i>
                  <span class="pull-right text-heading">
                    <i class="text8 icon-bdg_arrow1 text"></i>
                    <i class="text8 icon-bdg_arrow2 text-active"></i>
                  </span>
                  <span class="font-semibold">Belanja</span>
                </a>                

                 <ul class="nav nav-sub dk">                  
                  <li>
                    <a href="{{ url('/') }}/main/2017/murni/belanja-langsung" class="padding-l-r-lg ">                      
                       <span >Belanja Langsung</span>
                    </a>
                  </li>

                  <li>
                    <a href="{{ url('/') }}/main/2017/murni/belanja-tidak-langsung" class="padding-l-r-lg ">                       
                       <span >Belanja Tidak Langsung</span>
                    </a>
                  </li>                 
                  </ul>
              </li>
               <li>
                <a href="{{ url('/') }}/main/2017/murni/pendapatan" class="auto padding-l-r-lg">                  
                  <i class="mi-paper"></i>
                  <span class="font-semibold">Pendapatan</span>
                </a>                
              </li>
               <li >
                <a href="{{ url('/') }}/main/2017/murni/pembiayaan" class="auto padding-l-r-lg">                  
                  <i class="mi-paper"></i>
                  <span class="font-semibold">Pembiayaan</span>
                </a>                
              </li>
               <li >
                <a href="jurnal-penyesuaian.html" class="auto padding-l-r-lg">                  
                  <i class="mi-paper"></i>
                  <span class="font-semibold">Musrenbang</span>
                </a>                
              </li>
              <li >
                <a href="#" class="auto padding-l-r-lg parent">                  
                  <i class="mi-paper"></i>
                  <span class="pull-right text-heading">
                    <i class="text8 icon-bdg_arrow1 text"></i>
                    <i class="text8 icon-bdg_arrow2 text-active"></i>
                  </span>
                  <span class="font-semibold">Pengaturan</span>
                </a>                

                 <ul class="nav nav-sub dk">                  
                  <li>
                    <a href="#" class="padding-l-r-lg ">                      
                       <span >Tahapan</span>
                    </a>
                  </li>
                  <li>
                    <a href="{{ url('/') }}/main/{{ $tahun }}/{{ $status }}/pengaturan/urusan" class="padding-l-r-lg ">                      
                       <span >Urusan</span>
                    </a>
                  </li>
                  <li>
                    <a href="{{ url('/') }}/main/{{ $tahun }}/{{ $status }}/pengaturan/skpd" class="padding-l-r-lg ">                       
                       <span >Perangkat Daerah</span>
                    </a>
                  </li>                 
                  <li>
                    <a href="{{ url('/') }}/main/{{ $tahun }}/{{ $status }}/pengaturan/program" class="padding-l-r-lg ">                       
                       <span >Program</span>
                    </a>
                  </li>                 
                  <li>
                    <a href="#" class="padding-l-r-lg ">                       
                       <span >Kegiatan</span>
                    </a>
                  </li>                 
                  <li>
                    <a href="#" class="padding-l-r-lg ">                       
                       <span >Akun</span>
                    </a>
                  </li>                 
                  <li>
                    <a href="#" class="padding-l-r-lg ">                       
                       <span >Staff</span>
                    </a>
                  </li>                 
                  </ul>
              </li>

              <li >
                <a href="#" class="auto padding-l-r-lg parent">                  
                  <i class="mi-paper"></i>
                  <span class="pull-right text-heading">
                    <i class="text8 icon-bdg_arrow1 text"></i>
                    <i class="text8 icon-bdg_arrow2 text-active"></i>
                  </span>
                  <span class="font-semibold">Referensi</span>
                </a>                

                 <ul class="nav nav-sub dk">                  
                  <li>
                    <a href="#" class="padding-l-r-lg ">                      
                       <span >Rekening</span>
                    </a>
                  </li>
                  <li>
                    <a href="#" class="padding-l-r-lg ">                       
                       <span >Komponen</span>
                    </a>
                  </li>                 
                  </ul>
              </li>              
              <li >
                <a href="#" class="auto padding-l-r-lg parent">                  
                  <i class="mi-paper"></i>
                  <span class="pull-right text-heading">
                    <i class="text8 icon-bdg_arrow1 text"></i>
                    <i class="text8 icon-bdg_arrow2 text-active"></i>
                  </span>
                  <span class="font-semibold">Lampiran</span>
                </a>                

                 <ul class="nav nav-sub dk">                  
                  <li>
                    <a href="#" class="padding-l-r-lg ">                      
                       <span >RKPD</span>
                    </a>
                  </li>
                  <li>
                    <a href="#" class="padding-l-r-lg ">                       
                       <span >PPAS</span>
                    </a>
                  </li>                 
                  <li>
                    <a href="#" class="padding-l-r-lg ">                       
                       <span >KOMPONEN</span>
                    </a>
                  </li>                 
                  <li>
                    <a href="#" class="padding-l-r-lg ">                       
                       <span >RAPBD</span>
                    </a>
                  </li>                 
                  <li>
                    <a href="#" class="padding-l-r-lg ">                       
                       <span >APBD</span>
                    </a>
                  </li>                 
                  </ul>
              </li>              
              
            </ul>
          </nav>
          <!-- nav -->
        </div>

        <div class="navi-wrap navi-footer navi-footer-white ">
          <nav ui-nav class="navi clearfix ">
            <ul class="nav">
              <li class="hidden-folded m-t-lg text-heading text-xs ">
                <a href="" class="padding-l-r-lg">
                  <span>Presentasi</span>
                </a>
              </li>

              <li class="hidden-folded text-heading text-xs">
                <a href="" class="padding-l-r-lg">
                  <span>Download Manual</span>
                </a>
              </li>             
              <li class="hidden-folded text-heading text-xs">
                <span>Made with <i class="fa fa-heart text-xs text-danger"></i> in Bandung</span>
              </li>


            </ul>
          </nav>
        </div>
      </div>
  </aside>
  <!-- / aside -->

<!-- content -->
@yield('content')  



</div>

 <!-- Popup Belanja Komponen -->
<script src="{{ url('/') }}/libs_dashboard/jquery/jquery/dist/jquery.js"></script>
<script src="{{ url('/') }}/libs_dashboard/jquery/bootstrap/dist/js/bootstrap.js"></script>
<script src="{{ url('/') }}/libs_dashboard/jquery/jquery.confirm/js/jquery-confirm.js"></script>
<script src="{{ url('/') }}/assets/js/ui-load.js"></script>
<script src="{{ url('/') }}/assets/js/ui-jp.config.js"></script>
<script src="{{ url('/') }}/assets/js/ui-jp.js"></script>
<script src="{{ url('/') }}/assets/js/ui-nav.js"></script>
<script src="{{ url('/') }}/assets/js/ui-toggle.js"></script>
<script src="{{ url('/') }}/assets/js/ui-client.js"></script>
<script src="{{ url('/') }}/assets/js/numeral.js"></script>
<script src="{{ url('/') }}/assets/js/custom.js"></script>
@yield('plugin')
</body>
</html>
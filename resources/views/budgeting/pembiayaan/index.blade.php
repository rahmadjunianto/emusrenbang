@extends('budgeting.layout')

@section('content')
<div id="content" class="app-content" role="main">
  <div class="hbox hbox-auto-xs hbox-auto-sm ng-scope">
    <div class="col">
      <div class="app-content-body ">

          <div class="bg-light lter">    
              <ul class="breadcrumb bg-white m-b-none">
                <li><a href="#" class="btn no-shadow" ui-toggle-class="app-aside-folded" target=".app">
                  <i class="icon-bdg_expand1 text"></i>
                  <i class="icon-bdg_expand2 text-active"></i>
                </a>   </li>
                <li><a href= "{{ url('/') }}/main">Dashboard</a></li>
                <li class="active"><i class="fa fa-angle-right"></i>Pembiayaan</li>                                
              </ul>
          </div>
          <div class="wrapper-lg bg-dark-grey">
            <div class="row">
              <div class="col-md-12">
                <div class="panel bg-white">
                  <div class="wrapper-lg">
                    <button class="pull-right btn m-t-n-sm btn-success open-form-btl"><i class="m-r-xs fa fa-plus"></i> Tambah Pembiayaan</button>                  
                    <h5 class="inline font-semibold text-orange m-n ">Pembiayaan</h5>
          					<div class="col-sm-1 pull-right m-t-n-sm">
                    	<select ui-jq="chosen" class="form-control">
                        	<option value="">Baris</option>
                            <option value="kegiatanA">10</option>
                            <option value="kegiatanA">25</option>
                            <option value="kegiatanA">50</option>
                            <option value="kegiatanA">100</option>
                        </select>
                    </div>                    
                  </div>           
                  <div class="tab-content tab-content-alt-1 bg-white">
                        <div role="tabpanel" class="active tab-pane" id="tab-1">  
                            <div class="table-responsive dataTables_wrapper">
                             <table ui-jq="dataTable" ui-options="" class="table table-btl table-striped b-t b-b">
                                    <thead>
                                      <tr>
                                        <th>Kode Perangkat Daerah</th>
                                        <th>Nama Perangkat Daerah</th>
                                        <th>Anggaran</th>
                                      </tr>
                                      <tr>
                                        <th colspan="3" class="th_search">
                                            <i class="icon-bdg_search"></i>
                                            <input type="search" class="table-search form-control b-none w-full" placeholder="Cari" aria-controls="DataTables_Table_0">
                                        </th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                    <tr>                                    
                                      <td>1.01.01</td>
                                      <td>Dinas Pendidikan</td>
                                      <td>100,000,000,000</td>
                                    </tr>
                                    <tr>                                    
                                      <td>1.02.01</td>
                                      <td>Dinas Kesehatan</td>
                                      <td>100,000,000,000</td>
                                    </tr>
                                    </tbody>
                                  </table>
                              </div>
                          </div>
                  </div>
                </div>

                
              </div>
            </div>

          </div>
          
      
        
        

      </div>
      <!-- App-content-body -->  

    </div>
    <!-- .col -->


    </div>
<div class="overlay"></div>
<div class="bg-white wrapper-lg input-sidebar input-btl">
<a href="#" class="close"><i class="icon-bdg_cross"></i></a>
    <form action="#" class="form-horizontal">
      <div class="input-wrapper">
        <h5>Tambah Pendapatan</h5>
           <div class="form-group">
            <label class="col-sm-3">Perangkat Daerah</label>
            <div class="col-sm-9">
              <select ui-jq="chosen" class="w-full">
                  <option value="">Silahkan Pilih SKPD</option>
                  <option value="kegiatanA">Dinas Pendidikan</option>
                  <option value="kegiatanB">Dinas Kesehatan</option>
              </select>
            </div>
          </div>
           <div class="form-group">
            <label class="col-sm-3">Rekening</label>
            <div class="col-sm-9">
              <select ui-jq="chosen" class="w-full">
                  <option value="">Silahkan Pilih Rekening</option>
                  <option value="kegiatanA">Rekening 1</option>
                  <option value="kegiatanA">Rekening 1</option>
                  <option value="kegiatanA">Rekening 1</option>
                  <option value="kegiatanA">Rekening 1</option>
              </select>
            </div>
          </div>

          <div class="form-group">
            <label for="no_spp" class="col-md-3">Peruntukan</label>          
            <div class="col-sm-9">
              <input type="text" class="form-control" placeholder="Masukan Keterangan" >          
            </div> 
          </div>

          <div class="form-group">
            <label for="no_spp" class="col-md-3">Anggaran</label>          
            <div class="col-sm-9">
              <input type="text" class="form-control" placeholder="Masukan Anggaran" >          
            </div> 
          </div>

          <hr class="m-t-xl">
         <button class="btn input-xl m-t-md btn-success pull-right" type="submit"><i class="fa fa-plus m-r-xs "></i>Tambah Komponen</button>
      </div>
    </form>
  </div>
 </div>

<div id="table-detail-btl" class="table-detail-btl hide bg-white">
  <table ui-jq="dataTable" ui-options="" class="table table-detail-btl table-striped b-t b-b">
    <thead>
      <tr>
        <th>No</th>                                    
        <th>Rekening</th>                          
        <th>Rincian</th>                       
        <th>Anggaran</th>                                       
        <th>#</th>                                       
      </tr>                                  
    </thead>
    <tbody>
    <tr>
      <td>1</td>
      <td>5.2.1.1.01</td>
      <td>Gaji Pokok</td>
      <td>100,000,000</td>
      <td>
        <div class="action visible pull-right">
          <a href="javascript:void(0);" class="action-edit"><i class="mi-edit"></i></a>
          <a href="javascript:void(0);" class="action-delete"><i class="mi-trash"></i>
          </a>
        </div>
      </td>
    </tr>
    </tbody>
  </table>
</div>
@endsection

@section('plugin')
@endsection
@extends('budgeting.layout')

@section('content')
<div id="content" class="app-content" role="main">
  <div class="hbox hbox-auto-xs hbox-auto-sm ng-scope">
    <div class="col">
      <div class="app-content-body ">

          <div class="bg-light lter">    
              <ul class="breadcrumb bg-white m-b-none">
                <li><a href="#" class="btn no-shadow" ui-toggle-class="app-aside-folded" target=".app">
                  <i class="icon-bdg_expand1 text"></i>
                  <i class="icon-bdg_expand2 text-active"></i>
                </a>   </li>
                <li><a href= "{{ url('/') }}/main">Dashboard</a></li>
                <li><a>Pengaturan</a></li>
                <li class="active"><i class="fa fa-angle-right"></i>Urusan</li>                                
              </ul>
          </div>
          <div class="wrapper-lg bg-dark-grey">
            <div class="row">
              <div class="col-md-12">
                <div class="panel bg-white">
                  <div class="wrapper-lg">
                    <button class="pull-right btn m-t-n-sm btn-success open-form-btl"><i class="m-r-xs fa fa-plus"></i> Tambah Program</button>
                    <h5 class="inline font-semibold text-orange m-n ">Program Tahun {{ $tahun }}</h5>
          					<div class="col-sm-1 pull-right m-t-n-sm">
                    	<select class="form-control dtSelect" id="dtSelect">
                            <option value="10">10</option>
                            <option value="25">25</option>
                            <option value="50">50</option>
                            <option value="100">100</option>
                        </select>
                    </div>                    
                  </div>           
                  <div class="tab-content tab-content-alt-1 bg-white">
                        <div role="tabpanel" class="active tab-pane" id="tab-1">  
                            <div class="table-responsive dataTables_wrapper table-program">
                             <table ui-jq="dataTable" ui-options="{
                                    sAjaxSource: '{{ url('/') }}/main/{{$tahun}}/{{$status}}/pengaturan/program/getData',
                                    aoColumns: [
                                    { mData: 'id_urusan',class:'hide' },
                                    { mData: 'id_skpd',class:'hide' },
                                    { mData: 'no',class:'text-center' },
                                    { mData: 'URUSAN' },
                                    { mData: 'SKPD' },
                                    { mData: 'TOTAL' }
                                    ]}" class="table table-program table-program-head table-striped b-t b-b">
                                    <thead>
                                      <tr>
                                        <th class="hide">No</th>
                                        <th class="hide">No</th>
                                        <th>No</th>
                                        <th>Urusan</th>
                                        <th>Perangkat Daerah</th>
                                        <th>Total Program</th>
                                      </tr>
                                      <tr>
                                        <th class="hide"></th>
                                        <th class="hide"></th>
                                        <th colspan="4" class="th_search">
                                            <i class="icon-bdg_search"></i>
                                            <input type="search" class="table-search form-control b-none w-full" placeholder="Cari Program" aria-controls="DataTables_Table_0">
                                        </th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                  </table>
                              </div>
                          </div>
                  </div>
                </div>                
              </div>
            </div>
          </div>
      </div>
      <!-- App-content-body -->  
    </div>
    <!-- .col -->
</div>

<div class="overlay"></div>
<div class="bg-white wrapper-lg input-sidebar input-btl">
<a href="#" class="tutup-form"><i class="icon-bdg_cross"></i></a>
    <form id="form-urusan" class="form-horizontal">
      <div class="input-wrapper">
        <h5 id="judul-form">Tambah Program</h5>
          <div class="form-group">
            <label for="kode_urusan" class="col-md-3">Urusan</label>          
            <div class="col-sm-9">
              <select ui-jq="chosen" class="w-full" id="urusan" name="urusan">
                  <option value="0">Silahkan Pilih Urusan</option>
                  @foreach($urusan as $u)
                  <option value="{{$u->URUSAN_ID}}">{{ $u->URUSAN_KODE }} - {{ $u->URUSAN_NAMA }}</option>
                  @endforeach
              </select>            
              <input type="hidden" class="form-control" value="{{ csrf_token() }}" name="_token" id="token">          
              <input type="hidden" class="form-control" name="id_program" id="id_program">          
            </div> 
          </div>

          <div class="form-group">
            <label for="nama_urusan" class="col-md-3">Perangkat Daerah</label>          
            <div class="col-sm-9">
              <select ui-jq="chosen" class="w-full" id="skpd" name="skpd">
                  <option value="0">Silahkan Pilih Perangkat Daerah</option>
                  @foreach($skpd as $s)
                  <option value="{{$s->SKPD_ID}}">{{ $s->SKPD_KODE }} - {{ $s->SKPD_NAMA }}</option>
                  @endforeach
              </select>
            </div> 
          </div>

          <div class="form-group">
            <label for="nama_program" class="col-md-3">Nama Program</label>          
            <div class="col-sm-9">
              <input type="text" class="form-control" placeholder="Masukan Nama Program" name="nama_program" id="nama_program" value="">          
            </div> 
          </div>

          <hr class="m-t-xl">
         <button type="button" class="btn input-xl m-t-md btn-success pull-right" onclick="return simpanProgram()"><i class="fa fa-plus m-r-xs "></i>Simpan</button>
      </div>
    </form>
  </div>
 </div>

 <div id="table-detail-program" class="table-detail-program hide bg-white">
  <table class="table table-detail-program table-detail-program-isi table-striped b-t b-b">
    <thead>
      <tr>
        <th>Kode</th>                          
        <th>Nama Program</th>                       
        <th>#</th>                                       
      </tr>                                  
    </thead>
    <tbody>
    </tbody>
  </table>
</div>
@endsection

@section('plugin')
<script type="text/javascript">
  function simpanProgram(){
    var urusan        = $('#urusan').val();
    var skpd          = $('#skpd').val();
    var nama_program  = $('#nama_program').val();
    var id_program    = $('#id_program').val();
    var token        = $('#token').val();
    if(urusan == "0" || skpd == "0" || nama_program == "" ){
      $.alert('Form harap dilengkapi!');
    }else{
      if(id_program == '') uri = "{{ url('/') }}/main/{{ $tahun }}/{{ $status }}/pengaturan/program/add/submit";
      else uri = "{{ url('/') }}/main/{{ $tahun }}/{{ $status }}/pengaturan/program/edit/submit";
      $.ajax({
        url: uri,
        type: "POST",
        data: {'_token'         : token,
              'urusan'          : urusan, 
              'skpd'            : skpd, 
              'tahun'           : '{{$tahun}}', 
              'id_program'      : id_program, 
              'nama_program'    : nama_program},
        success: function(msg){
            if(msg == 1){
              $('#urusan select').val('0').trigger("chosen:updated");
              $('#skpd select').val('0').trigger("chosen:updated");
              $('#nama_program').val('');
              $('#id_program').val('');
              $('.table-program-head').DataTable().ajax.reload();              
              $.alert({
                title:'Info',
                content: 'Data berhasil disimpan',
                autoClose: 'ok|1000',
                buttons: {
                    ok: function () {
                      $('.input-spp,.input-spp-langsung,.input-sidebar').animate({'right':'-1050px'},function(){
                        $('.overlay').fadeOut('fast');
                      });                      
                    }
                }
              });
            }else{
              $.alert('Data telah tersedia!');
            }
          }
        });
    }
  }

  function hapus(id){
    var token        = $('#token').val();    
    $.confirm({
        title: 'Hapus Data!',
        content: 'Yakin hapus data?',
        buttons: {
            Ya: {
                btnClass: 'btn-danger',
                action: function(){
                  $.ajax({
                      url: "{{ url('/') }}/main/{{ $tahun }}/{{ $status }}/pengaturan/program/delete",
                      type: "POST",
                      data: {'_token'         : token,
                            'id_program'      : id},
                      success: function(msg){
                          $.alert(msg);
                          $('.table-program-head').DataTable().ajax.reload();                          
                        }
                  });
                }
            },
            Tidak: function () {
            }
        }
    });
  }

  function ubah(id) {
    $('#judul-form').text('Ubah Program');        
    $.ajax({
      url: "{{ url('/') }}/main/{{ $tahun }}/{{ $status }}/pengaturan/program/getData/"+id,
      type: "GET",
      success: function(msg){
        $('select#urusan').val(msg[0]['URUSAN_ID']).trigger("chosen:updated");
        $('select#skpd').val(msg[0]['SKPD_ID']).trigger("chosen:updated");
        $('#id_program').val(msg[0]['PROGRAM_ID']);
        $('#nama_program').val(msg[0]['PROGRAM_NAMA']);
        $('.overlay').fadeIn('fast',function(){
          $('.input-btl').animate({'right':'0'},"linear");  
          $("html, body").animate({ scrollTop: 0 }, "slow");
        });
      }
    });    
  }

  $('a.tutup-form').click(function(){
      $('select#urusan').val('0').trigger("chosen:updated");
      $('select#skpd').val('0').trigger("chosen:updated");
      $('#nama_program').val('');
      $('#id_program').val('');
  }); 
</script>
@endsection



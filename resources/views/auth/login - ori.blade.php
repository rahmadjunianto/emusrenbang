  <!DOCTYPE html>
<html lang="en" class="">
<head>
  <!-- 0.5 version -->
  <meta charset="utf-8" />
  <title>BDG Musrenbang</title>
  <meta name="description" content="Bandung Web Kit" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
  <link rel="stylesheet" href="{{ url('/') }}/libs_dashboard/assets/animate.css/animate.css" type="text/css" />
  <link rel="stylesheet" href="{{ url('/') }}/libs_dashboard/assets/font-awesome/css/font-awesome.min.css" type="text/css" />
  <link rel="stylesheet" href="{{ url('/') }}/libs_dashboard/assets/simple-line-icons/css/simple-line-icons.css" type="text/css" />
  <link rel="stylesheet" href="{{ url('/') }}/libs_dashboard/jquery/bootstrap/dist/css/bootstrap.css" type="text/css" />
  <link rel="stylesheet" href="{{ url('/') }}/assets/css/font.css" type="text/css" />
  <link rel="stylesheet" href="{{ url('/') }}/assets/fonts/simdaicon/simdaicon.css" type="text/css" />
  <link rel="stylesheet" href="{{ url('/') }}/assets/css/style.css" type="text/css" />
  <link rel="stylesheet" href="{{ url('/') }}/assets/css/homepage.css" type="text/css" />
</head>

<body>
<div class="notification bg-success text-red" style="display: none;">
  <div class="container text-white">
    <i class="mi-warning pull-left "></i>
    <p class="pull-left">Ini Merupakan area pengumuman, user dengan bebas bisa klik link yang ada di pengumuman, atau menutup jendela pengumuman dengan klik icon x, atau bisa diseting menutup otomatis</p>
    <a href="javascript:void(0);" class="close-notification"><i class="icon-bdg_cross pull-right"></i></a>
  </div>
</div>

<div class="main-container">
  <header id="homepage-header">
    <div class="container no-padder">
      
      <div class="logo pull-left">
        <a href="/"><img src="{{ url('/') }}/assets/img/logo-homepage.png" srcset="{{ url('/') }}/assets/img/logo-homepage-hires.png" alt=""></a>
      </div>


      <nav class="pull-right">
        <ul class="nav-menu">
          <li><a href="#">Video Tutorial</a></li>
          <li><a href="https://drive.google.com/open?id=0BxIx84aCMiVnZC1mRWRsM2tCRFU">Download Manual</a></li>
         <!--  <li class="sub-menu">
            <a href="#">Download <i class="mi-caret-down"></i></a>
            <ul>
              <li><a href="#">Dasar hukum</a></li>
              <li><a href="#">Manual Guide</a></li>
              <li><a href="#">Video Tutorial</a></li>
            </ul>
          </li> -->
        </ul>
      </nav>

    </div>
  </header>
</div>

<div class="main-page">
  <div class="container">
    <div class="row">
      
      <div class="img-responsive">
      <div class="col-md-8 app-slider">
        
          <img src="{{ url('/') }}/bg-apps.png" srcset="{{ url('/') }}/bg-apps.png" alt="" width="637" height="510">
        
      </div>
    </div> 

       <div class="col-md-4 text-white no-padder m-t-xl">
        <h4>Aplikasi Musrenbang Sedang Dalam Tahap Pembaharuan</h4>

        <p class="text18">Aplikasi dapat diakses pada hari <br /><span class="font-bold text-warning">Jumat, 03 Februari 2017 pukul 14:00 WIB</span></p>        
        
        

      </div>
        <!-- End Form -->

      </div>

    </div>
  </div>

  <!-- <div class="main-feature">
  <div class="container">
    <div class="row">

      <div class="col-md-4">
        <i class="mi-chair"></i>
        <h5>Rekap </h5>
        <p>Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum id elitauctor lorem quis bibendum auctor, nisi elit consequat.</p>
      </div>
      
      <div class="col-md-4">
        <i class="mi-cabinet"></i>
        <h5>Detail Lokasi</h5>
        <p>Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum id elitauctor lorem quis bibendum auctor, nisi elit consequat.</p>
      </div>
      <div class="col-md-4">
        <i class="mi-tv"></i>
        <h5>Mudah Digunakan</h5>
        <p>Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum id elitauctor lorem quis bibendum auctor, nisi elit consequat.</p>
      </div>
    </div>
  </div>
</div>  -->


<footer>
  <div class="container">
    <p class="pull-left">Copyright &copy; 2016 BDGWebkit | Made with <i class="mi-heart text-danger"></i> in Bandung</p>
    <p class="pull-right"> 
      <img src="{{ url('/') }}/assets/img/bandung.png"> &nbsp; &nbsp; &nbsp; &nbsp;
      <img src="{{ url('/') }}/assets/img/IA.png">
    </p>
  </div>
</footer>

<!--Start of Zendesk Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="https://v2.zopim.com/?4XsJAVMVHAuWk8KVNd6RcwgTq6up8MOA";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zendesk Chat Script-->

</div>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-91000808-1', 'auto');
  ga('send', 'pageview');

</script>



<script src="{{ url('/') }}/libs_dashboard/jquery/jquery/dist/jquery.js"></script>
<script src="{{ url('/') }}/libs_dashboard/jquery/bootstrap/dist/js/bootstrap.js"></script>
<script src="{{ url('/') }}/assets/js/ui-load.js"></script>
<script src="{{ url('/') }}/assets/js/ui-jp.config.js"></script>
<script src="{{ url('/') }}/assets/js/ui-jp.js"></script>
<script src="{{ url('/') }}/assets/js/ui-nav.js"></script>
<script src="{{ url('/') }}/assets/js/ui-toggle.js"></script>
<script src="{{ url('/') }}/assets/js/ui-client.js"></script>
<script src="{{ url('/') }}/assets/js/custom.js"></script>

</body>
</html>
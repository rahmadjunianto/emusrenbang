<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Usulan_Rekap_Apbd extends Model
{
    protected $table	= 'MUSRENBANG.RKP_USULAN_APBD';
    protected $primaryKey = 'RKP_ID_APBD'; 
    public $timestamps = false;
    public $incrementing = false;

    public function kamus()
    {
        return $this->belongsTo('App\Model\Kamus', 'KAMUS_ID');
    }

    public function usulan()
    {
        return $this->belongsTo('App\Model\Usulan', 'USULAN_ID');
    }

    public function rt()
    {
        return $this->belongsTo('App\Model\RT', 'RT_ID');
    }

    public function user()
    {
        return $this->belongsTo('App\Model\User','USER_CREATED');
    }
}

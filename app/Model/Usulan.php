<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\RT;

class Usulan extends Model
{
    protected $table	= 'MUSRENBANG.DAT_USULAN';
    protected $primaryKey = 'USULAN_ID'; 
    public $timestamps = false;
    public $incrementing = false;
    protected $casts = [
        'RT_ID' => 'array',
        'USULAN_GAMBAR' => 'array',
        'USULAN_VOLUME' => 'float',
    ];
    
    public function getLikes(){
        return $this->hasMany('App\Model\Likes','USULAN_ID','USULAN_ID');
    }

    public function getStringKeteranganAcc()
    {
        if($this->USULAN_STATUS == 0)
        {
            return $this->USULAN_ALASAN;
        }

        $ket = $this->USULAN_CATATANKEL;
        if($this->USULAN_POSISI == 2)
        {
            $ket = $this->USULAN_CATATANKEC;
        }
        else if($this->USULAN_POSISI == 3)
        {
            $ket = $this->USULAN_CATATANSKPD;   
        }
        else if($this->USULAN_POSISI == 4)
        {
            $ket = $this->USULAN_CATATANBAPPEDA;   
        }

        return $ket != null ? $ket : "-";
    }
    public function getStringKeterangan()
    {
        if($this->USULAN_STATUS == 0)
        {
            return "Ditolak dengan alasan : ".$this->USULAN_ALASAN;
        }

        if($this->USULAN_STATUS == 2 && $this->USULAN_POSISI == 2)
        {
            return "Dalam Proses";
        }

        return "Diterima";
    }

    public function isOnProcess($level)
    {
        if($level == 1 )
        {
            return $this->USULAN_STATUS == 1;
        }
        
        return ($this->USULAN_STATUS == 2) && ($this->USULAN_POSISI == ($level-1));
    }
    public function isDenied($level)
    {
        return ($this->USULAN_STATUS == 0) && ($this->USULAN_POSISI == ($level));
    }
    public function isAcdepted($level)
    {
        return ($this->USULAN_STATUS == 2) && ($this->USULAN_POSISI == ($level));
    }
 
    public function scopeNotDummies($qq){
        $qq->wherehas('user',function($user){
            $user->NotDummies();
        });
    }

    public function scopeUsulanBappeda($qq)
    {
        $qq->where(function($q){
            //usulan diproses Di kecamatan
            $q->where(function($query){
                $query->where('USULAN_STATUS',2)->where('USULAN_POSISI',3);
            })
            //usulan diterima di kecamatan
            ->orWhere(function($query){
                $query->where('USULAN_STATUS',2)->where('USULAN_POSISI',">=",4);
            })
            //usulan ditolak di kecamatan
            ->orWhere(function($query){
                $query->where('USULAN_STATUS',0)->where('USULAN_POSISI',">=",4);
            });
        });
    }

    public function scopeUsulanKecamatan($qq)
    {
        $qq->where(function($q){
            //usulan diproses Di kecamatan
            $q->where(function($query){
                $query->where('USULAN_STATUS',2)->where('USULAN_POSISI',1);
            })
            //usulan diterima di kecamatan
            ->orWhere(function($query){
                $query->where('USULAN_STATUS',2)->where('USULAN_POSISI',">=",2);
            })
            //usulan ditolak di kecamatan
            ->orWhere(function($query){
                $query->where('USULAN_STATUS',0)->where('USULAN_POSISI',">=",2);
            });
        });
    }

    public function scopeUsulanSKPD($qq,$idskpd = null)
    {
        $qq->where('USULAN_TUJUAN',1);
        if($idskpd != null)
        {
            $qq->wherehas('kamus',function($kamus) use ($idskpd){
                return $kamus->where("KAMUS_SKPD",$idskpd);
            });
        }
        $qq->where(function($q){
            //usulan diproses Di kecamatan
            $q->where(function($query){
                $query->where('USULAN_STATUS',2)->where('USULAN_POSISI',2);
            })
            //usulan diterima di kecamatan
            ->orWhere(function($query){
                $query->where('USULAN_STATUS',2)->where('USULAN_POSISI',">=",3);
            })
            //usulan ditolak di kecamatan
            ->orWhere(function($query){
                $query->where('USULAN_STATUS',0)->where('USULAN_POSISI',">=",3);
            });
        });
    }

    public function scopeUsulanBidang($qq,$idbidang)
    {
        $qq->where('USULAN_TUJUAN',1);
        $qq->wherehas("kamus",function($kamus) use ($idbidang){
            $kamus->wherehas("skpd",function($skpd) use ($idbidang){
                $skpd->where("SKPD_BIDANG",$idbidang);
            });
        });
    }

    public static function getNominalUsulan($level = null,$tipe=null)
    {
        $usulans = Usulan::select("USULAN_ID","USULAN_VOLUME","KAMUS_ID","USULAN_TUJUAN","USER_CREATED","USULAN_POSISI","USULAN_STATUS")
        ->wherehas("user",function($user){
            $user->NotDummies();
        })
        ->with([
            'kamus'=>function($kamus){
                $kamus->select("KAMUS_ID","KAMUS_HARGA");
            },
            'user'=>function($user){
                $user->select("id","level");
            },
        ]);

        if($level == 7)
        {
            $usulans = $usulans->UsulanSKPD();
        }

        if($tipe == 1)
        {
            $usulans = $usulans->where('USULAN_TUJUAN',1);
        }
        else if($tipe == 2)
        {
            $usulans = $usulans->where('USULAN_TUJUAN',2);
        }
        
        $usulans = $usulans->get();

        $response = array();

        $response["all"] = 0;
        $response["renja"] = 0;
        $response["renjaproseskelurahan"] = 0;
        $response["renjatolakkelurahan"] = 0;
        $response["renjaproseskecamatan"] = 0;
        $response["renjatolakkecamatan"] = 0;
        $response["renjaprosesskpd"] = 0;
        $response["renjatolakskpd"] = 0;
        $response["renjaprosesapbd"] = 0;
        $response["renjatolakapbd"] = 0;
        $response["renjafinal"] = 0;
        $response["pippk"] = 0;
        $response["pippkproseskelurahan"] = 0;
        $response["pippktolakkelurahan"] = 0;
        $response["pippkproseskecamatan"] = 0;
        $response["pippktolakkecamatan"] = 0;
        $response["pippkprosesapbd"] = 0;
        $response["pippktolakapbd"] = 0;
        $response["pippkfinal"] = 0;
        $response["usulanfinal"] = 0;


        $response["pippkrw"] = 0;
        $response["pippkpkk"] = 0;
        $response["pippklpm"] = 0;
        $response["pippkkarta"] =0;

        $response["countAll"] = 0;
        $response["countRenja"] = 0;
        $response["countrenjaproseskelurahan"] = 0;
        $response["countrenjatolakkelurahan"] = 0;
        $response["countrenjaproseskecamatan"] = 0;
        $response["countrenjatolakkecamatan"] = 0;
        $response["countrenjaprosesskpd"] = 0;
        $response["countrenjaptolakskpd"] = 0;
        $response["countrenjaprosesapbd"] = 0;
        $response["countrenjatolakapbd"] = 0;
        $response["countrenjafinal"] = 0;

        $response["countpippk"] = 0;
        $response["countpippkproseskelurahan"] = 0;
        $response["countpippktolakkelurahan"] = 0;
        $response["countpippkproseskecamatan"] = 0;
        $response["countpippktolakkecamatan"] = 0;
        $response["countpippkprosesapbd"] = 0;
        $response["countpippktolakapbd"] = 0;
        $response["countpippkfinal"] = 0;
        $response["countusulanfinal"] = 0;




        foreach ($usulans as $key => $usulan) {
            $nominal = $usulan->USULAN_VOLUME * $usulan->kamus->KAMUS_HARGA;
            $response["all"] += $nominal;
            $response["countAll"] ++;

            if($usulan->USULAN_TUJUAN == 1)
            {
                $response["renja"] += $nominal;
                $response["countRenja"]++;

                if($usulan->USULAN_POSISI == 1 && $usulan->USULAN_STATUS == 1) //PROSES kelurahan
                {
                    $response["renjaproseskelurahan"] += $nominal;
                    $response["countrenjaproseskelurahan"]++;
                }
                else if($usulan->USULAN_POSISI == 1 && $usulan->USULAN_STATUS == 0) //tolak kelurahan
                {
                    $response["renjatolakkelurahan"] += $nominal;
                    $response["countrenjatolakkelurahan"]++;
                }
                else if($usulan->USULAN_POSISI == 1 && $usulan->USULAN_STATUS == 2) //proses kecamatan
                {
                    $response["renjaproseskecamatan"] += $nominal;
                    $response["countrenjaproseskecamatan"]++;
                }
                else if($usulan->USULAN_POSISI == 2 && $usulan->USULAN_STATUS == 0) //tolak kecamatan
                {
                    $response["renjatolakkecamatan"] += $nominal;
                    $response["countrenjatolakkecamatan"]++;
                }
                else if($usulan->USULAN_POSISI == 2 && $usulan->USULAN_STATUS == 2) //proses skpd
                {
                    $response["renjaprosesskpd"] += $nominal;
                    $response["countrenjaprosesskpd"]++;
                }
                else if($usulan->USULAN_POSISI == 3 && $usulan->USULAN_STATUS == 0) //tolak skpd
                {
                    $response["renjatolakskpd"] += $nominal;
                    $response["countrenjaptolakskpd"]++;
                }
                else if($usulan->USULAN_POSISI == 3 && $usulan->USULAN_STATUS == 2) //proses apbd
                {
                    $response["renjaprosesapbd"] += $nominal;
                    $response["countrenjaprosesapbd"]++;
                }
                else if($usulan->USULAN_POSISI == 4 && $usulan->USULAN_STATUS == 0) //tolak apbd
                {
                    $response["renjatolakapbd"] += $nominal;
                    $response["countrenjatolakapbd"]++;
                }
                else if($usulan->USULAN_POSISI == 4 && $usulan->USULAN_STATUS == 2) //final
                {
                    $response["renjafinal"] += $nominal;
                    $response["usulanfinal"] +=$nominal;
                    $response["countrenjafinal"]++;
                    $response["countusulanfinal"]++;
                }
            }

            else if($usulan->USULAN_TUJUAN == 2)
            {
                $response["pippk"] += $nominal;
                $response["countpippk"]++;

                if($usulan->USULAN_POSISI == 1 && $usulan->USULAN_STATUS == 1) //PROSES kelurahan
                {
                    $response["pippkproseskelurahan"] += $nominal;
                    $response["countpippkproseskelurahan"]++;
                }
                else if($usulan->USULAN_POSISI == 1 && $usulan->USULAN_STATUS == 0) //tolak kelurahan
                {
                    $response["pippktolakkelurahan"] += $nominal;
                    $response["countpippktolakkelurahan"]++;
                }
                else if($usulan->USULAN_POSISI == 1 && $usulan->USULAN_STATUS == 2) //proses kecamatan
                {
                    $response["pippkproseskecamatan"] += $nominal;
                    $response["countpippkproseskecamatan"]++;
                }
                else if($usulan->USULAN_POSISI == 2 && $usulan->USULAN_STATUS == 0) //tolak kecamatan
                {
                    $response["pippktolakkecamatan"] += $nominal;
                    $response["countpippktolakkecamatan"]++;
                }
                else if($usulan->USULAN_POSISI == 2 && $usulan->USULAN_STATUS == 2) //proses apbd
                {
                    $response["pippkprosesapbd"] += $nominal;
                    $response["countpippkprosesapbd"]++;
                }
                else if($usulan->USULAN_POSISI == 3 && $usulan->USULAN_STATUS == 0) //tolak apbd
                {
                    $response["pippktolakapbd"] += $nominal;
                    $response["countpippktolakapbd"]++;
                }
                else if($usulan->USULAN_POSISI == 3 && $usulan->USULAN_STATUS == 2) //final
                {
                    $response["pippkfinal"] += $nominal;
                    $response["usulanfinal"] +=$nominal;
                    $response["countpippkfinal"]++;
                    $response["countusulanfinal"]++;
                }

                if($usulan->user->level == 1)
                {
                    $response["pippkrw"] += $nominal;
                }
                if($usulan->user->level == 2)
                {
                    $response["pippklpm"] += $nominal;
                }
                if($usulan->user->level == 3)
                {
                    $response["pippkpkk"] += $nominal;
                }
                if($usulan->user->level == 4)
                {
                    $response["pippkkarta"] += $nominal;
                }
            }
        }

        /*$response = [
            "all"=>$all,
            "renja"=>$renja,
            "pippk"=>$pippk,
            "pippkrw"=>$pippkrw,
            "pippklpm"=>$pippklpm,
            "pippkpkk"=>$pippkpkk,
            "pippkkarta"=>$pippkkarta,
            "countRenja"=>$countRenja,
        ];*/
        
        return $response;
    }

    public function getHTMLIsuKamus()
    {
        return $this->kamus->isu->ISU_NAMA."<br><font style='color:orange;'>".$this->kamus->KAMUS_NAMA."</font>";
    }

    public function getIsuTipe(){
        $tipe = $this->kamus->isu->ISU_TIPE;
        if ($tipe == 2) return "PIPPK";
        else return "RENJA";
    }

    public function getStringStatus()
    {
        //return $this->USULAN_STATUS." ".$this->USULAN_POSISI;
        $status = "Proses Musrenbang Kelurahan";

        if($this->USULAN_STATUS == 0 && $this->USULAN_POSISI == 1)
        {
            $status = "Ditolak di Musrenbang Kelurahan";          
        }
        else if($this->USULAN_STATUS == 2 && $this->USULAN_POSISI == 1)
        {
            $status = "Proses Musrenbang Kecamatan";       
        }
        else if($this->USULAN_STATUS == 0 && $this->USULAN_POSISI == 2)
        {
            $status = "Ditolak di Musrenbang Kecamatan";          
        }
        else if($this->USULAN_STATUS == 2 && $this->USULAN_POSISI == 2)
        {
            $status = "Proses di Forum Perangkat Daerah";          
        }
        else if($this->USULAN_STATUS == 2 && $this->USULAN_POSISI == 2)
        {
            $status = "Proses di Forum Perangkat Daerah";          
        }
        else if($this->USULAN_STATUS == 2 && $this->USULAN_POSISI == 2)
        {
            $status = "Proses di Forum Perangkat Daerah";          
        }
        else if($this->USULAN_STATUS == 0 && $this->USULAN_POSISI == 3)
        {
            $status = "Ditolak di Forum Perangkat Daerah";          
        }
        else if($this->USULAN_STATUS == 2 && $this->USULAN_POSISI == 3)
        {
            $status = "Proses di Musrenbang Kota";          
        }
        else if($this->USULAN_STATUS == 0 && $this->USULAN_POSISI == 4)
        {
            $status = "Ditolak di Musrenbang Kota";          
        }
        else if($this->USULAN_STATUS == 2 && $this->USULAN_POSISI == 4)
        {
            $status = "Proses di RKPD";          
        }
        else if($this->USULAN_STATUS == 0 && $this->USULAN_POSISI == 5)
        {
            $status = "Ditolak di RKPD";          
        }
        else if($this->USULAN_STATUS == 2 && $this->USULAN_POSISI == 5)
        {
            $status = "Proses di KUA PPAS";          
        }
        else if($this->USULAN_STATUS == 0 && $this->USULAN_POSISI == 6)
        {
            $status = "Ditolak di  KUA PPAS";          
        }
        else if($this->USULAN_STATUS == 2 && $this->USULAN_POSISI == 6)
        {
            $status = "Proses di APBD";          
        }
        else if($this->USULAN_STATUS == 0 && $this->USULAN_POSISI == 7)
        {
            $status = "Di tolak di APBD";          
        }
        else if($this->USULAN_STATUS == 2 && $this->USULAN_POSISI == 7)
        {
            $status = "Masuk APBD";          
        }

        return $status;
    }

    public static function toHtmlIconStatus($instatus,$inposisi,$level=0)
    {
        if($inposisi > 0)
        {
            $status = '<i class="fa fa-refresh text-info"></i>';
            $posisi = 'kelurahan';

            $posisiArr = [
                "Kelurahan",
                "Kecamatan",
                "Forum Perangkat Daerah",
                "Musrenbang Kota",
                "RKPD",
                "KUA PPAS",
                "APBD",
            ];

            if($instatus == 1 && $inposisi == 1)
            {
                return $status.$posisi;
            }

            if($instatus == 0)
            {
                $status = '<i class="fa fa-close text-danger"></i>';
                $posisi = $posisiArr[$inposisi-1];
            }

            if($instatus == 2)
            {
                $status = '<i class="fa fa-refresh text-info"></i>';
                $posisi = $posisiArr[$inposisi];
                if($level > 0)
                {
                    $status = '<i class="fa fa-check text-success"></i>';
                    $posisi = $posisiArr[$inposisi-1];
                }
            }

            if($level > 0 || $instatus != 2)
            {

                $inposisi--;
            }
            return $status.$posisi."<br>".Usulan::toHtmlIconStatus(2,$inposisi,$level+1);
        }
    }

    public function getHTMLIconStatus()
    {
        return $this->toHtmlIconStatus($this->USULAN_STATUS,$this->USULAN_POSISI);
    }

    public function getStringRTPenerima()
    {
        if(count($this->RT_ID) == 0)
            return "-";
        $rts = RT::whereIn('RT_ID',$this->RT_ID)->get()->map(function($rt){
            return "RT ".$rt->RT_NAMA;
        })->all();

        return implode(",", $rts);
    }

    public function getStringPengusul()
    {
        $strpengusul = "Kecamatan ".ucfirst(strtolower($this->user->kecamatan->KEC_NAMA));
        if($this->user->level != 6)
        {
            $strpengusul = $strpengusul.", Kelurahan ".ucfirst(strtolower($this->user->kelurahan->KEL_NAMA));          
            if($this->user->level != 5)
            {
                if($this->user->level == 1)
                {
                    $strpengusul = $strpengusul.", RW ".$this->user->rw->RW_NAMA;
                   // dd($strpengusul);
                }
                else if($this->user->level == 2 )
                {
                    $strpengusul = $strpengusul.", LPM ";   
                }
                else if($this->user->level == 3 )
                {
                    $strpengusul = $strpengusul.", PKK ";   
                }
                else if($this->user->level == 4 )
                {
                    $strpengusul = $strpengusul.", KARTA "; 
                }


            }
        }

        return $strpengusul;
    }

    public function user()
    {
        return $this->belongsTo('App\Model\User','USER_CREATED');
    }

    public function kamus()
    {
        return $this->belongsTo('App\Model\Kamus', 'KAMUS_ID');
    }

    public function satuan()
    {
        return $this->belongsTo('App\Model\Satuan', 'SATUAN_ID');
    }

    public function rw()
    {
        return $this->belongsTo('App\Model\RW', 'RW_ID');
    }

    public function usulan_rekap()
    {
        return $this->hasMany('App\Model\Usulan_Rekap', 'USULAN_ID');
    }

    public function usulan_rekap_kec()
    {
        return $this->hasMany('App\Model\Usulan_Rekap_Kec', 'USULAN_ID');
    }

    public function rt()
    {
        //return $this->belongsTo('App\Model\RT', 'RT_ID');
        return RT::whereIn("RT_ID",$this->RT_ID)->first();
    }

    public  function getFirstRt()
    { 

        return $this->RT_ID != null  ? RT::whereIn('RT_ID',$this->RT_ID)->first() : null;
    }

    public function tujuan(){
        return $this->belongsTo('App\Model\Tujuan','USULAN_TUJUAN');
    }

    public function usulan_kriteria()
    {
        return $this->hasMany('App\Model\Usulan_kriteria','USULAN_ID');
    }
}

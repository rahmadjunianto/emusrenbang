<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Likes extends Model {
    protected $table 		= 'MUSRENBANG.DAT_USULAN_LIKES';
    protected $primaryKey   = 'oauth_uid';
    protected $fillable 	= array('oauth_uid', 'USULAN_ID','is_like');
    public $incrementing    = true;
}
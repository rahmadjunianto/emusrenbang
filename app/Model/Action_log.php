<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Action_log extends Model
{
    protected $table = 'action_log';
    protected $primaryKey   = 'id_log';
    protected $fillable = [
        'id_user', 'aksi', 'entity_id', 'keterangan', 'waktu'
    ];
        public $timestamps = false;
}
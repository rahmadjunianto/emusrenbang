<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Sessions extends Model
{
	protected $table		= 'DATA.sessions';
    protected $primaryKey 	= 'id';
    public $timestamps 		= false;
    public $incrementing 	= false;
}

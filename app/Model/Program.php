<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Program extends Model
{
	protected $table		= 'REFERENSI.REF_PROGRAM';
    protected $primaryKey 	= 'PROGRAM_ID';
    public $timestamps 		= false;
    public $incrementing 	= false;

    public function skpd(){
    	return $this->belongsTo('App\Model\SKPD','SKPD_ID');
    }
    public function urusan(){
    	return $this->belongsTo('App\Model\Urusan','URUSAN_ID');
    }
    public function kegiatan(){
        return $this->hasMany('App\Model\Kegiatan','PROGRAM_ID');
    }
}

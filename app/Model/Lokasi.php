<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Lokasi extends Model
{
	protected $table		= 'PROTECTED.REF_LOKASI';
    protected $primaryKey 	= 'LOKASI_ID';
    public $timestamps 		= false;
    public $incrementing 	= false;
}

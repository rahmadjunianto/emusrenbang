<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\User;
use App\Model\Sessions;
use Auth;
use Config;
class keepaliveController extends Controller
{
    public function keepalive(){
        $data = User::where('login',1)->get();
        foreach($data as $d){
        	$ses = Sessions::where('user_id',$d->id)->whereRaw('user_id is not null')->orderBy('last_activity','desc')->value('last_activity');
        	if(!empty($ses)){
				if ((time() - $ses) > (Config::get('session.lifetime')*60)){
	                User::where('id',$d->id)->update(['login'=>0]);
	                print_r('ok');
            	}
        	}
        }
    }
}

<?php
namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;

class UserController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function changePassword(Request $request){
    	$this->validate($request,[
	        'email' => 'required',
	        'password' => 'required|confirmed|min:6',
	    ]);

    	$user = User::where("email",$request->email)
    				->update(["password"=>bcrypt($request->password)]);

    	return back();
    }
}

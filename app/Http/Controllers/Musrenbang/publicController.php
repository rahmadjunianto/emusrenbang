<?php

namespace App\Http\Controllers\Musrenbang;

use Illuminate\Support\Facades\Route;
//use Illuminate\Http\Request;
use Request;
use App\Http\Controllers\Controller;
use App\Model\User;
use App\Model\Usulan;
use App\Model\Usulan_Rekap_Apbd;
use App\Model\Kecamatan;
use App\Model\Kelurahan;
use App\Model\RW;
use App\Model\Isu;
use App\Model\Kamus;
use App\Model\RT;
use App\Model\Usulan_Rekap;
use App\Model\BeritaAcara;
use App\Model\BeritaAcaraFoto;
use App\Model\SKPD;
use App\Model\Likes;
use Response;
use DB;
use View;
use App;
use Session;
use Excel;
use Illuminate\Support\Facades\Redis;
use Log;

class publicController extends Controller{

    public function dataRekapLKK($tahun) {
        $kelurahans = Kelurahan::orderBy('KEL_NAMA')
                        ->with([
                            "user" => function($user){
                                $user->wherein('level',[1,2,3,4]);
                            },
                            "rw",
                            "kecamatan",
                            "user.usulan",
                            "user.usulan.kamus"
                        ])
                        ->whereNotIn('KEL_ID',[9999,9900,9000,999]);

        $view = array();
        $cachekey = "dataRekapLKK-".md5(serialize($kelurahans->toSql()).serialize($kelurahans->getBindings()));
        $cacheexpire = env('REDIS_TIMEOUT',60);

        if (Redis::exists($cachekey)){
            $view = unserialize(Redis::get($cachekey));
            Log::debug('Redis at key '.$cachekey.' HIT!. Will expire in '.Redis::ttl($cachekey).'s');
        } else {
            $kelurahans = $kelurahans->get();
            $no=0;
            foreach($kelurahans as $key => $kelurahan) {
                $viewelement = array();

                $userRW = $kelurahan->user->first(function($user){
                    return $user->level == 1;
                });
                $userpkk = $kelurahan->user->first(function($user){
                    return $user->level == 3;
                });
                $userkarta = $kelurahan->user->first(function($user){
                    return $user->level == 4;
                });
                $userlpm = $kelurahan->user->first(function($user){
                    return $user->level == 2;
                });
               
                $viewelement["no"] = $key+1;
                $viewelement["kelurahan"] = $kelurahan->KEL_NAMA;
                $viewelement["kecamatan"] = $kelurahan->kecamatan != null ? $kelurahan->kecamatan->KEC_NAMA : 'n/a';
                $viewelement["jumlahusulanRW"] = $userRW != null ? number_format($userRW->usulan->count(),0,'.',',') : '0';
                $viewelement["jumlahusulanpkk"] = $userpkk != null ? number_format($userpkk->usulan->count(),0,'.',',') : '0';
                $viewelement["jumlahusulankarta"] = $userkarta != null ? number_format($userkarta->usulan->count(),0,'.',',') : '0';
                $viewelement["jumlahusulanlpm"] = $userlpm != null ? number_format($userlpm->usulan->count(),0,'.',',') : '0';

                $viewelement["nominalusulanRW"] = $userRW != null ? number_format($userRW->totalNominal(),0,'.',',') : '0';
                $viewelement["nominalusulanpkk"] = $userpkk != null ? number_format($userpkk->totalNominal(),0,'.',',') : '0';
                $viewelement["nominalusulankarta"] = $userkarta != null ? number_format($userkarta->totalNominal(),0,'.',',') :'0';
                $viewelement["nominalusualnlpm"] = $userlpm != null ? number_format($userlpm->totalNominal(),0,'.',',') : '0';            

                array_push($view,$viewelement);
            }

            Redis::set($cachekey, serialize($view));
            Redis::expire($cachekey, $cacheexpire);
            Log::debug('Redis at key '.$cachekey.' MISS!. Set expire in: '.$cacheexpire.'s');
        }

        $out = array("aaData"=>$view);
        return Response::JSON($out);
    }

    public function pesanerror(){
        return View('musrenbang.error');
    }

    public function getapprovalkecamatan($tahun)
    {
        $kecamatans = kecamatan::with(
            [
                'usulan',
                'user'=>function($user){
                    return $user->where('level',6);
                },
            ]
        )
        ->whereNotIn('KEC_ID',[32,31,9999]);

        $cachekey = "getapprovalkecamatan-".md5(serialize($kecamatans->toSql()).serialize($kecamatans->getBindings()));
        $cacheexpire = env('REDIS_TIMEOUT',60);
        $view = array();

        if (Redis::exists($cachekey)){
            $view = unserialize(Redis::get($cachekey));
            Log::debug('Redis at key '.$cachekey.' HIT!. Will expire in '.Redis::ttl($cachekey).'s');
        } else {

            $kecamatans = $kecamatans->get();
            foreach ($kecamatans as $key => $kecamatan) {                 
                $pippk = $kecamatan->usulan->filter(function($value){
                    return ($value->USULAN_TUJUAN == 2);
                });
                $renja = $kecamatan->usulan->filter(function($value){
                    return $value->USULAN_TUJUAN == 1;
                });

                $pippkproses = $pippk->filter(function($value){
                    return (($value->USULAN_STATUS == 2) && ($value->USULAN_POSISI==1));
                })->count();
                $pippkterima = $pippk->filter(function($value){
                    return (($value->USULAN_STATUS == 2) && ($value->USULAN_POSISI>=2));
                })->count();
                $pippktolak = $pippk->filter(function($value){
                    return (($value->USULAN_STATUS == 0) && ($value->USULAN_POSISI>=2));
                })->count();
                $jumlahpippk = $pippkproses + $pippktolak + $pippkterima;

                $renjaproses = $renja->filter(function($value){
                    return (($value->USULAN_STATUS == 2) && ($value->USULAN_POSISI==1));
                })->count();
                $renjaterima = $renja->filter(function($value){
                    return (($value->USULAN_STATUS == 2) && ($value->USULAN_POSISI>=2));
                })->count();
                $renjatolak = $renja->filter(function($value){
                    return (($value->USULAN_STATUS == 0) && ($value->USULAN_POSISI>=2));
                })->count();
                $jumlahrenja = $renjaproses + $renjatolak + $renjaterima;

                array_push($view, array( 
                 'KECID'                => $kecamatan->KEC_ID,
                 'NO'                   => $key+1,
                 'KECAMATAN'            => $kecamatan->KEC_NAMA,
                 'USERNAME'             => $kecamatan->user->count() > 0 ? $kecamatan->user[0]->email : "",
                 'JUMLAHPIPPK'          => $jumlahpippk,
                 'JUMLAHPIPPKPROSES'    => $pippkproses,
                 'JUMLAHPIPPKTERIMA'    => $pippkterima,
                 'JUMLAHPIPPKTOLAK'     => $pippktolak,
                 'JUMLAHRENJA'          => $jumlahrenja,
                 'JUMLAHRENJAPROSES'    => $renjaproses,
                 'JUMLAHRENJATERIMA'    => $renjaterima,
                 'JUMLAHRENJATOLAK'     => $renjatolak,
                 'ACTIONPIPPK'          => $jumlahpippk != 0 ? ($pippkterima+$pippktolak)/$jumlahpippk * 100 : 0,
                 'ACTIONRENJA'          => $jumlahrenja != 0 ? ($renjaterima+$renjatolak)/$jumlahrenja * 100 : 0,
                 ));
            }

            Redis::set($cachekey, serialize($view));
            Redis::expire($cachekey, $cacheexpire);
            Log::debug('Redis at key '.$cachekey.' MISS!. Set expire in: '.$cacheexpire.'s');
        }
        $view = array("aaData"=>$view);
        return Response::json($view);   
    }

    public function getapproval($tahun)
    {
        $kelurahans = Kelurahan::with(
            [
                'usulan',
                'kecamatan',
                'user'=>function($user){
                    return $user->where('level',5);
                },
            ]
        )
        ->where('KEL_ID',"<>",9999);

        $cachekey = "getApproval-".md5(serialize($kelurahans->toSql()).serialize($kelurahans->getBindings()));
        $cacheexpire = env('REDIS_TIMEOUT',60);
        $view = array();

        if (Redis::exists($cachekey)){
            $view = unserialize(Redis::get($cachekey));
            Log::debug('Redis at key '.$cachekey.' HIT!. Will expire in '.Redis::ttl($cachekey).'s');
        } else {
            $kelurahans = $kelurahans->get();
            foreach ($kelurahans as $key => $kelurahan) {                 
                $pippk = $kelurahan->usulan->filter(function($value){
                    return $value->USULAN_TUJUAN == 2;
                });
                $renja = $kelurahan->usulan->filter(function($value){
                    return $value->USULAN_TUJUAN == 1;
                });

                $jumlahpippk = $pippk->count();
                $pippkproses = $pippk->filter(function($value){
                    return $value->USULAN_STATUS == 1;
                })->count();
                $pippkterima = $pippk->filter(function($value){
                    return $value->USULAN_STATUS == 2;
                })->count();
                $pippktolak = $pippk->filter(function($value){
                    return $value->USULAN_STATUS == 0;
                })->count();

                $jumlahrenja = $renja->count();

                $renjaproses = $renja->filter(function($value){
                    return $value->USULAN_STATUS == 1;
                })->count();
                $renjaterima = $renja->filter(function($value){
                    return $value->USULAN_STATUS == 2;
                })->count();
                $renjatolak = $renja->filter(function($value){
                    return $value->USULAN_STATUS == 0;
                })->count();

                array_push($view, array( 
                 'KELID'                => $kelurahan->KEL_ID,
                 'NO'                   => $key+1,
                 'KECAMATAN'            => $kelurahan->kecamatan != null ? $kelurahan->kecamatan->KEC_NAMA : "",
                 'KELURAHAN'            => $kelurahan->KEL_NAMA,
                 'USERNAME'             => $kelurahan->user->count() > 0 ? $kelurahan->user[0]->email : "",
                 'JUMLAHPIPPK'          => $jumlahpippk,
                 //'NOMINALPIPPK'         => number_format($kelurahan->getNominalUsulan(2,null),0,'.',','),
                 'JUMLAHPIPPKPROSES'    => $pippkproses,
                 //'NOMINALPIPPKPROSES'   => number_format($kelurahan->getNominalUsulan(2,1),0,'.',','),
                 'JUMLAHPIPPKTERIMA'    => $pippkterima,
                 //'NOMINALPIPPKTERIMA'   => number_format($kelurahan->getNominalUsulan(2,2),0,'.',','),
                 'JUMLAHPIPPKTOLAK'     => $pippktolak,
                 //'NOMINALPIPPKTOLAK'    => number_format($kelurahan->getNominalUsulan(2,0),0,'.',','),
                 'JUMLAHRENJA'          => $jumlahrenja,
                 'JUMLAHRENJAPROSES'    => $renjaproses,
                 'JUMLAHRENJATERIMA'    => $renjaterima,
                 'JUMLAHRENJATOLAK'     => $renjatolak,
                 'ACTIONPIPPK'          => $jumlahpippk != 0 ? ($pippkterima+$pippktolak)/$jumlahpippk * 100 : 0,
                 'ACTIONRENJA'          => $jumlahrenja != 0 ? ($renjaterima+$renjatolak)/$jumlahrenja * 100 : 0,
                 ));
            }

            Redis::set($cachekey, serialize($view));
            Redis::expire($cachekey, $cacheexpire);
            Log::debug('Redis at key '.$cachekey.' MISS!. Set expire in: '.$cacheexpire.'s');
        }
        $view = array("aaData"=>$view);
        return Response::json($view);   
    }

    public function getapprovalkelajax(){
        //var_dump($_GET);
        $kelurahans = Kelurahan::with(
            [
                'usulan',
                'kecamatan',
                'user'=>function($user){
                    return $user->where('level',5);
                },
            ]
        );

        $start = $_GET['start'];
        $length = $_GET['length'];
        $search = $_GET['search']['value'];
        $totalsemua = $kelurahans->count();

        //filter will be here
        $totalfilter = $kelurahans->count();
        $kelurahans = $kelurahans->skip($start)->take($length); 
        $kelurahans = $kelurahans->where('KEL_ID',"<>",9999)->get();

        $response = array();
        foreach ($kelurahans as $key => $kelurahan) {
                     
            $pippk = $kelurahan->usulan->filter(function($value){
                return $value->USULAN_TUJUAN == 2;
            });
            $renja = $kelurahan->usulan->filter(function($value){
                return $value->USULAN_TUJUAN == 1;
            });

            $jumlahpippk = $pippk->count();
            $pippkproses = $pippk->filter(function($value){
                return $value->USULAN_STATUS == 1;
            })->count();
            $pippkterima = $pippk->filter(function($value){
                return $value->USULAN_STATUS == 2;
            })->count();
            $pippktolak = $pippk->filter(function($value){
                return $value->USULAN_STATUS == 0;
            })->count();

            $jumlahrenja = $renja->count();

            $renjaproses = $renja->filter(function($value){
                return $value->USULAN_STATUS == 1;
            })->count();
            $renjaterima = $renja->filter(function($value){
                return $value->USULAN_STATUS == 2;
            })->count();
            $renjatolak = $renja->filter(function($value){
                return $value->USULAN_STATUS == 0;
            })->count();



            array_push($response, array( 
             'KELID'                => $kelurahan->KEL_ID,
             'NO'                   => $key+1,
             'KECAMATAN'            => $kelurahan->kecamatan != null ? $kelurahan->kecamatan->KEC_NAMA : "",
             'KELURAHAN'            => $kelurahan->KEL_NAMA,
             'USERNAME'             => $kelurahan->user->count() > 0 ? $kelurahan->user[0]->email : "",
             'JUMLAHPIPPK'          => $jumlahpippk,
             //'NOMINALPIPPK'         => number_format($kelurahan->getNominalUsulan(2,null),0,'.',','),
             'JUMLAHPIPPKPROSES'    => $pippkproses,
             //'NOMINALPIPPKPROSES'   => number_format($kelurahan->getNominalUsulan(2,1),0,'.',','),
             'JUMLAHPIPPKTERIMA'    => $pippkterima,
             //'NOMINALPIPPKTERIMA'   => number_format($kelurahan->getNominalUsulan(2,2),0,'.',','),
             'JUMLAHPIPPKTOLAK'     => $pippktolak,
             //'NOMINALPIPPKTOLAK'    => number_format($kelurahan->getNominalUsulan(2,0),0,'.',','),
             'JUMLAHRENJA'          => $jumlahrenja,
             'JUMLAHRENJAPROSES'    => $renjaproses,
             'JUMLAHRENJATERIMA'    => $renjaterima,
             'JUMLAHRENJATOLAK'     => $renjatolak,
             'ACTIONPIPPK'          => $jumlahpippk != 0 ? ($pippkterima+$pippktolak)/$jumlahpippk * 100 : 0,
             'ACTIONRENJA'          => $jumlahrenja != 0 ? ($renjaterima+$renjatolak)/$jumlahrenja * 100 : 0,
             ));
        }

        $response = array(  "recordsTotal"      => $totalsemua,
                            "recordsFiltered"   => $totalfilter,
                            "data"=>$response
                        );
        return Response::json($response);  
    }

    public function approval($tahun){
        return View('musrenbang.public.approval',compact('tahun'));
    }

    public function approvalkelajax($tahun){
        return View('musrenbang.public.approvalkelajax',compact('tahun'));
    }

    public function approvalkec($tahun){
        return View('musrenbang.public.approvalkec',compact('tahun'));
    }

    public function approvalskpd($tahun){
        return View('musrenbang.public.approvalskpd',compact('tahun'));
    }

    public function getapprovalskpd($tahun)
    {
        $skpds = skpd::with(
            [
                'usulan'=>function($usulan){
                    return $usulan->NotDummies()->UsulanSKPD();
                },
                'user'=>function($user){
                    return $user->where('level',7);
                },
            ]
        );

        $cachekey = "getapprovalskpd-".md5(serialize($skpds->toSql()).serialize($skpds->getBindings()));
        $cacheexpire = env('REDIS_TIMEOUT',60);
        $view = array();

        if (Redis::exists($cachekey)){
            $view = unserialize(Redis::get($cachekey));
            Log::debug('Redis at key '.$cachekey.' HIT!. Will expire in '.Redis::ttl($cachekey).'s');
        } else {

            $skpds = $skpds->get();
            foreach ($skpds as $key => $skpd) {                 
                $pippk = $skpd->usulan->filter(function($value){
                    return $value->USULAN_TUJUAN == 2;
                });
                $renja = $skpd->usulan->filter(function($value){
                    return $value->USULAN_TUJUAN == 1;
                });

                $jumlahpippk = $pippk->count();
                $pippkproses = $pippk->filter(function($value){
                    return (($value->USULAN_STATUS == 2) && ($value->USULAN_POSISI==2));
                })->count();
                $pippkterima = $pippk->filter(function($value){
                    return (($value->USULAN_STATUS == 2) && ($value->USULAN_POSISI>=3));
                })->count();
                $pippktolak = $pippk->filter(function($value){
                    return (($value->USULAN_STATUS == 0) && ($value->USULAN_POSISI>=3));
                })->count();

                $jumlahrenja = $renja->count();

                $renjaproses = $renja->filter(function($value){
                    return (($value->USULAN_STATUS == 2) && ($value->USULAN_POSISI==2));
                })->count();
                $renjaterima = $renja->filter(function($value){
                    return (($value->USULAN_STATUS == 2) && ($value->USULAN_POSISI>=3));
                })->count();
                $renjatolak = $renja->filter(function($value){
                    return (($value->USULAN_STATUS == 0) && ($value->USULAN_POSISI>=3));
                })->count();



                array_push($view, array( 
                 'SKPDID'                => $skpd->SKPD_ID,
                 'NO'                   => $key+1,
                 'SKPD'                 => $skpd->SKPD_NAMA,
                 'USERNAME'             => $skpd->user->count() > 0 ? $skpd->user[0]->email : "",
                 'JUMLAHRENJA'          => $jumlahrenja,
                 'JUMLAHRENJAPROSES'    => $renjaproses,
                 'JUMLAHRENJATERIMA'    => $renjaterima,
                 'JUMLAHRENJATOLAK'     => $renjatolak,
                 'ACTIONRENJA'          => $jumlahrenja != 0 ? ($renjaterima+$renjatolak)/$jumlahrenja * 100 : 0,
                 ));
            }

            Redis::set($cachekey, serialize($view));
            Redis::expire($cachekey, $cacheexpire);
            Log::debug('Redis at key '.$cachekey.' MISS!. Set expire in: '.$cacheexpire.'s');
        }
        $view = array("aaData"=>$view);
        return Response::json($view);   
    }

    public function getskpd($tahun)
    {
        $skpds = SKPD::with(
            [
                'usulan'=>function($usulan){
                    $usulan->UsulanSKPD()->NotDummies();
                },
                'usulan.kamus',
            ]
        )->get();
        
        $response = array();
        $no = 1;
        foreach($skpds as $skpd)
        {
            $usulan = $skpd->usulan;

            $renja = $usulan->where('USULAN_TUJUAN',1);

            $jumlahrenja = $renja->count();

            $jumlahpengusul = $renja->unique('USER_CREATED')->count();

            array_push($response, array( 
                                     'SKPDID'              => $skpd->SKPD_ID,
                                     'NO'                  => $no,
                                     'KODESKPD'            => $skpd->SKPD_KODE,
                                     'NAMASKPD'            => $skpd->SKPD_NAMA,
                                     'JUMLAHRENJA'         => $jumlahrenja,
                                     'JUMLAHPENGUSUL'      => $jumlahpengusul,
                                     'TOTALNOMINAL'        => number_format($skpd->totalNominal(1),0,'.',','),
                                     ));    
            $no++;
        }
        $response = array("aaData"=>$response);
        return Response::json($response);
    }

    public function getdetailskpd($tahun,$id)
    {
        $skpd = SKPD::where('SKPD_ID',$id)->with(
            [
                'usulan'=>function($usulan) use ($id){
                    $usulan->UsulanSKPD()->NotDummies();
                },
                'usulan.kamus',
                'usulan.user',
                'usulan.user.kelurahan',
                'usulan.user.rw',
                'usulan.user.kecamatan',
            ]
        )->first();
           
        $response = array();

        $no = 1;
        foreach($skpd->usulan->sortBy('USER_CREATED') as $usulan)
        {
            if($usulan->USULAN_STATUS == 1) $status = '<i class="fa fa-refresh text-info"></i>';
            elseif($usulan->USULAN_STATUS == 2) $status = '<i class="fa fa-check text-success"></i>';
            else $status = '<i class="fa fa-close text-danger"></i>';

            if($usulan->USULAN_POSISI == 1) $posisi = ' Kelurahan';
            elseif($usulan->USULAN_POSISI == 2) $posisi = ' Kecamatan';
            elseif($usulan->USULAN_POSISI == 3) $posisi = ' Kota';
            else $posisi = ' APBD';

            $pengusul = $usulan->user->email;

            $isukamus = $usulan->kamus->isu->ISU_NAMA."<br>".$usulan->kamus->KAMUS_NAMA;

            $urgensi = $usulan->USULAN_URGENSI;

            $lokasi = "Kecamatan ".$usulan->user->kecamatan->KEC_NAMA;

            if($usulan->user->level != 6)
            {
                $lokasi = $lokasi."<br> Kelurahan ".$usulan->user->kelurahan->KEL_NAMA;              
                if($usulan->user->level == 1)
                {
                    $lokasi = $lokasi."<br> RW ".$usulan->user->rw->RW_NAMA;
                }
            }

            $alamat = $usulan->ALAMAT;

            $volume = number_format($usulan->USULAN_VOLUME,0,'.',',')." ".$usulan->kamus->KAMUS_SATUAN;

            $harga = $usulan->kamus->KAMUS_HARGA;

            $nominal = $usulan->USULAN_VOLUME * $usulan->kamus->KAMUS_HARGA;

            $beritaacara = "";



            array_push($response, array( 
                                        'NO'                    =>  $no,
                                        'PENGUSUL'              =>  $pengusul,
                                        'ISUKAMUS'              =>  $isukamus,
                                        'LOKASI'                =>  $lokasi,
                                        'URGENSI'               =>  $urgensi,
                                        'LOKASI'                =>  $lokasi,
                                        'ALAMAT'                =>  $alamat,
                                        'VOLUME'                =>  $volume,
                                        'HARGA'                 =>  number_format($harga,0,'.',','),
                                        'NOMINAL'               =>  number_format($nominal,0,'.',','),
                                        'STATUS'                =>  "<p>".$status.$posisi."</p>",
                                        'BERITAACARA'           =>  $beritaacara,
                                     ));    
            $no++;
        }
        $response = array("aaData"=>$response);
        return Response::json($response);
    }

    public function skpd($tahun)
    {
        $nominal = Usulan::getNominalUsulan(7);
        
        return View('musrenbang.public.skpd',compact('tahun','nominal'));
    }

    public function renjaSkpd($tahun)
    {
        $nominal = Usulan::getNominalUsulan(7);
        
        return View('musrenbang.usulan.admin-renja',compact('tahun','nominal'));
    }

    public function lkk($tahun)
    {
        return View('musrenbang.public.lkk',compact('tahun'));
    }

     public function adminLkk($tahun)
    {
        return View('musrenbang.usulan.admin-pippk',compact('tahun'));
    }

    public function detailskpd($tahun,$id)
    {
        return View('musrenbang.public.detailskpd',compact('tahun','id'));
    }

    public function exportUsulan($tahun)
    {
        $usulan = Usulan::get();

        Excel::create('export', function($excel) use ($usulan) {

            $excel->sheet('Sheet 1', function($sheet) use ($usulan) {

                $sheet->fromArray($usulan, null, 'A1', true);

            });

        })->export('xls');
    }

    public function renja($tahun){
        $data = array();
        $data["tahun"] = $tahun;
        $data["nominal"] = Usulan::getNominalUsulan(null,1);
        return View('musrenbang.public.indexrenja',$data);
    }
    public function pippk($tahun){
        $data = array();
        $data["tahun"] = $tahun;
        $data["nominal"] = Usulan::getNominalUsulan(null,2);
        return View('musrenbang.public.indexpippk',$data);
    }
    
    public function index($tahun){
        $cachekey = 'indexPublic'.$tahun;
        $cacheexpire = env('REDIS_TIMEOUT',60);

        $data = null;

        if (false){//Redis::exists($cachekey)){
            $data = unserialize(Redis::get($cachekey));
            Log::debug('Redis at key '.$cachekey.' HIT!. Will expire in '.Redis::ttl($cachekey).'s');
        }
        else {
            $nominal = Usulan::getNominalUsulan();
            $pippk     = Usulan::wherehas('kamus',function($q){
                        $q->whereHas('isu',function($x){
                            $x->where('ISU_TIPE','2');
                        });
                    })->count();
            $renja  = Usulan::wherehas('kamus',function($q){
                        $q->whereHas('isu',function($x){
                            $x->whereIn('ISU_TIPE',['0','1']);
                        });
                    })->count();

            $o  = User::where('login',1)->count();
            $rw     = User::where('app',1)->where('level',1)->NotDummies()->count();
            $rwv    = User::where('app',1)->where('level',1)->NotDummies()->where('validasi',1)->count();
            $lpm    = User::where('app',1)->where('level',2)->NotDummies()->count();
            $lpmv   = User::where('app',1)->where('level',2)->NotDummies()->where('validasi',1)->count();
            $pkk    = User::where('app',1)->where('level',3)->NotDummies()->count();
            $pkkv   = User::where('app',1)->where('level',3)->NotDummies()->where('validasi',1)->count();
            $karta  = User::where('app',1)->where('level',4)->NotDummies()->count();
            $kartav = User::where('app',1)->where('level',4)->NotDummies()->where('validasi',1)->count();

            $rwpernahusulanpippk = User::where('app',1)->NotDummies()->where('level',1)->wherehas('usulan', function($y){
                $y->wherehas('kamus',function($q){
                        $q->whereHas('isu',function($x){
                            $x->where('ISU_TIPE','2');
                        });
                    });
            })->count();

            $rwpernahusulanrenja = User::where('app',1)->NotDummies()->where('level',1)->wherehas('usulan', function($y){
                $y->wherehas('kamus',function($q){
                        $q->whereHas('isu',function($x){
                            $x->whereIn('ISU_TIPE',['0','1']);
                        });
                    });
            })->count();

            $lpmpernahusulan = User::where('app',1)->NotDummies()->where('level',2)->wherehas('usulan')->count();
            $pkkpernahusulan = User::where('app',1)->NotDummies()->where('level',3)->wherehas('usulan')->count();
            $kartapernahusulan = User::where('app',1)->NotDummies()->where('level',4)->wherehas('usulan')->count();

            $data   = array('tahun' => $tahun,
                            'rw'=>$rw,
                            'rwv'=>$rwv,
                            'pippk'=>$pippk,
                            'renja'=>$renja,
                            'pkk'=>$pkk,
                            'pkkv'=>$pkkv,
                            'lpm'=>$lpm,
                            'lpmv'=>$lpmv,
                            'karta'=>$karta,
                            'kartav'=>$kartav,
                            'o'=>$o, 
                            'rwpernahusulanpippk'=>$rwpernahusulanpippk,
                            'rwpernahusulanrenja'=>$rwpernahusulanrenja,
                            'lpmpernahusulan'=>$lpmpernahusulan,
                            'pkkpernahusulan'=>$pkkpernahusulan,
                            'kartapernahusulan'=>$kartapernahusulan,
                            'nominal'=>$nominal);


            //Redis::set($cachekey, serialize($data));
            //Redis::expire($cachekey, $cacheexpire);
            Log::debug('Redis at key '.$cachekey.' MISS!. Set expire in: '.$cacheexpire.'s');
        }

        return View('musrenbang.public.index',$data);
    }

    public function getOnline($tahun){
    	$pippk 	= Usulan::wherehas('kamus',function($q){
		    		$q->whereHas('isu',function($x){
		    			$x->where('ISU_TIPE','2');
		    		});
		    	})->count();
    	$renja 	= Usulan::wherehas('kamus',function($q){
		    		$q->whereHas('isu',function($x){
		    			$x->whereIn('ISU_TIPE',['0','1']);
		    		});
		    	})->count();
    	$o 	= User::where('login',1)->count();
    	return array('pippk'=>$pippk,'renja'=>$renja,'o'=>$o);
    }

    public function kecamatan($tahun){
        return View('musrenbang.public.kecamatan',compact('tahun'));
    }

    public function getKecamatan($tahun){
        $data   =   Kecamatan::where('KEC_ID','!=',9999)->with(
                        [
                            "user"=>function($user){
                                $user->where('level',6);
                             },
                             "kelurahan"
                        ]
                    )->orderBy('KEC_ID');
        $view   = array();
        
        $cachekey = "getKecamatan-".md5(serialize($data->toSql()).serialize($data->getBindings()));
        $cacheexpire = env('REDIS_TIMEOUT',60);

        if (Redis::exists($cachekey)){
            $view = unserialize(Redis::get($cachekey));
            Log::debug('Redis at key '.$cachekey.' HIT!. Will expire in '.Redis::ttl($cachekey).'s');
        } else {

            $no=1;
            $data = $data->get();

            foreach($data as $d){
                $kel    = Kelurahan::where('KEC_ID',$d->KEC_ID)->select('KEL_ID')->get();
                $rw     = RW::whereIn('KEL_ID',$kel)->select('RW_ID')->get();
                $rwa     = RW::whereIn('KEL_ID',$kel)->where('RW_AKTIF',1)->select('RW_ID')->get();

                $rwsudahinput = RW::whereIn('KEL_ID',$kel)->wherehas('usulan')->count();

                $pippk  = Usulan::wherehas('kamus',function($q){
                        $q->whereHas('isu',function($x){
                            $x->where('ISU_TIPE','2');
                        });
                    })->whereIn('RW_ID',$rw)->count();
                $renja  = Usulan::wherehas('kamus',function($q){
                        $q->whereHas('isu',function($x){
                            $x->whereIn('ISU_TIPE',['0','1']);
                        });
                    })->whereIn('RW_ID',$rw)->count();
                $username="";
                if(count($d->user) > 0)
                    $username = $d->user[0]->email;
                array_push($view, array( 'KECAMATAN_ID'        => $d->KEC_ID,
                                         'NO'                  => $no,
                                         'KECAMATAN_NAMA'      => $d->KEC_NAMA,
                                         'KEL'                 => $d->kelurahan->count('KEL_ID'),
                                         'RW'                  => $rw->count(),
                                         'RWA'                 => $rwa->count(),
                                         'PIPPK'               => $pippk,
                                         'RENJA'               => $renja,
                                         'totalusulan'        => $pippk + $renja,
                                         'rwsudahinput'        => $rwsudahinput,
                                         'KECAMATAN_USERNAME'   => $username,
                                         'TOTALNOMINALUSULAN'   => number_format($d->getNominalUsulan(),0,'.',','),
                                         ));    
                $no++;        
            }

            Redis::set($cachekey, serialize($view));
            Redis::expire($cachekey, $cacheexpire);
            Log::debug('Redis at key '.$cachekey.' MISS!. Set expire in: '.$cacheexpire.'s');
        }

        $out = array("aaData"=>$view);
        return Response::JSON($out);
    }

    public function kelurahan($tahun){
        return View('musrenbang.public.kelurahan',compact('tahun'));
    }

    public function getKelurahan($tahun,$id){
        $data = NULL;
        if($id == 'all')
            $data   = Kelurahan::where('KEC_ID','!=',9999);
        else             
            $data   = Kelurahan::where('KEC_ID',$id);

        $data = $data
                ->with(
                    [
                        "user"=>function($user){
                            $user->where('level',5);
                        },
                        "rw",
                        "rw.usulan",
                        "rw.usulan.kamus"
                    ]
                )
                ->orderBy('KEL_ID');

        $view   = array();
        
        $cachekey = "getKelurahan-".md5(serialize($data->toSql()).serialize($data->getBindings()));
        $cacheexpire = env('REDIS_TIMEOUT',60);

        if (Redis::exists($cachekey)){
            $view = unserialize(Redis::get($cachekey));
            Log::debug('Redis at key '.$cachekey.' HIT!. Will expire in '.Redis::ttl($cachekey).'s');
        } else {
            $no     = 1;
            $data = $data->get();
            foreach($data as $d){
                $usulan = collect();

                $d->rw->each(function($item,$key) use ($usulan){
                    $item->usulan->each(function($usul,$key) use ($usulan){
                        $usulan->push($usul);
                    });
                });


                $totalnominalusulan = $usulan->sum(function($usul){
                    return $usul->USULAN_VOLUME * $usul->kamus->KAMUS_HARGA;
                });

                $rwsudahinput    = $d->rw->filter(function($item){
                    return $item->usulan->count() != 0;
                })->count();
                
                $rwa    = $d->rw->filter(function($item){
                    return $item->RW_AKTIF == 1;
                });
               
                
                $pippk    = $usulan->filter(function($item){
                    return $item->USULAN_TUJUAN == 2;
                })->count();

                $renja    = $usulan->filter(function($item){
                    return $item->USULAN_TUJUAN == 1;
                })->count();

                $username = "";
                if(count($d->user) > 0)
                    $username = $d->user[0]->email;
                array_push($view, array( 'KELURAHAN'            => $d->KEL_NAMA,
                                         'RW'                   => $d->rw->count(),
                                         'RWA'                  => $rwa->count(),
                                         'ID'                   => $d->KEL_ID,
                                         'PIPPK'                => $pippk,
                                         'RENJA'                => $renja,
                                         //'PIPPK'                => $pippk.' ('.number_format($d->rw->nominalUsulan(2),0,'.',',').')',
                                         //'RENJA'                => $renja.' ('.number_format($d->rw->nominalUsulan(1),0,'.',',').')',
                                         'totalusulan'          => $pippk + $renja,
                                         'rwsudahinput'         => $rwsudahinput,
                                         'KELURAHAN_USERNAME'   => $username,
                                         'TOTALNOMINALUSULAN'   => number_format($totalnominalusulan,0,'.',','),
                                         ));    
                $no++;        
            }
            
            Redis::set($cachekey, serialize($view));
            Redis::expire($cachekey, $cacheexpire);
            Log::debug('Redis at key '.$cachekey.' MISS!. Set expire in: '.$cacheexpire.'s');
        }
        $out = array("aaData"=>$view);
        return Response::JSON($out);
    }

    public function rw($tahun){
        return View('musrenbang.public.rw',compact('tahun'));
    }

    public function rwajax($tahun){
        return View('musrenbang.public.rwajax',compact('tahun'));
    }

    public function getrwAJAXnonEager($tahun){
        $start = $_GET['start'];
        $length = $_GET['length'];
        $search = $_GET['search']['value'];

        # Where condition
        $data = RW::whereNotIn('KEL_ID',[9999,9901,9900]);
        $id = $_GET['id'];
        $totalsemua = $data->count();
    }

    public function getrwAJAX($tahun){
        $start = $_GET['start'];
        $length = $_GET['length'];
        $search = $_GET['search']['value'];

        # Where condition
        $data = RW::whereNotIn('KEL_ID',[9999,9901,9900]);
        $id = $_GET['id'];
        $totalsemua = $data->count();

        if ($id != "All") {
            $data = RW::where('KEL_ID',$id);
        }
        
        $data = RW::whereHas('kelurahan',function($x) use ($search) {
            $x->where('KEL_NAMA','LIKE','%'.$search.'%');
        });

        $totalfilter = $data->count();
        # Paging
        $data = $data->skip($start)->take($length);   

        # Get Data
        $data   =   $data->with([
                        "rt",
                        "user"=>function($user){
                            return $user->where('level',1)->orderBy('RW_ID','asc');
                        },
                        "user.beritaacara",
                        "user.beritaacarafoto",
                        "kelurahan",
                        "kelurahan.kecamatan",
                        "usulan",
                        "usulan.kamus",
                    ])->get();

        $view   = array();
        $no     = 1;
        
        foreach($data as $d){
            $pippk = $d->usulan->where('USULAN_TUJUAN','2')->count();
            $renja = $d->usulan->where('USULAN_TUJUAN','1')->count();
            $iduser = $d->user->count() > 0 ? $d->user[0]->id : "";
            $berita = "";
            if($d->user->count() > 0)
            {
                if($d->user[0]->hasBeritaAcara())
                    $berita = "<a href='/musrenbang/2017/public/berita/".$iduser."'>Lihat Berita Acara</a>";
            }


            if($d->RW_AKTIF == 0) $aktif = '<span class="text-danger"><i class="fa fa-close"></i></span>';
            else $aktif = '<span class="text-success"><i class="fa fa-check"></i></span>';
            array_push($view, array( 'RW'                  => $d->RW_NAMA,
                                     'AKTIF'               => $aktif,
                                     'PIPPK'               => $pippk,
                                     'RENJA'               => $renja,
                                     'totalusulan'         => $pippk+$renja,
                                     'TOTALNOMINALUSULAN'  => number_format($d->nominalUsulan(),0,'.',','),
                                     'TOTALNOMINALPIPPK'   => number_format($d->nominalUsulan(2),0,'.',','),
                                     'TOTALNOMINALRENJA'   => number_format($d->nominalUsulan(1),0,'.',','),
                                     'USERNAME'            => $d->user->count() > 0 ? $d->user[0]->email : "",
                                     'KELURAHAN'           => $d->kelurahan->KEL_NAMA,
                                     'KECAMATAN'           => $d->kelurahan->kecamatan->KEC_NAMA,
                                     'NIKKETUA'                  => $d->RW_NIK,
                                     'NAMAKETUA'                  => $d->RW_KETUA,
                                     'TELP'                  => $d->RW_TELP,
                                     'JUMLAHRT'                  => $d->rt->count(),
                                     'NO'                   => $no, 
                                     'RWID'                 =>$d->RW_ID,
                                     'BERITA'              => $berita,
                                    ));    
            $no++;        
        }

        $out = array(
                "recordsTotal"      => $totalsemua,
                "recordsFiltered"   => $totalfilter,
                "data"              => $view,
            );
        return Response::JSON($out);
    }

    public function getrw($tahun,$id){
        $data = RW::whereNotIn('KEL_ID',[9999,9901,9900]);
        if ($id != "All") {
            $data = RW::where('KEL_ID',$id);
        }

        $data   =   $data->with([
                        "rt",
                        "user"=>function($user){
                            return $user->where('level',1);
                        },
                        "user.beritaacara",
                        "user.beritaacarafoto",
                        "kelurahan",
                        "kelurahan.kecamatan",
                        "usulan",
                        "usulan.kamus",
                    ]);
        
        $view   = array();
        $no     = 1;
        
        $cachekey = "getRW-".md5(serialize($data->toSql()).serialize($data->getBindings()));
        $cacheexpire = env('REDIS_TIMEOUT',60);
        if (Redis::exists($cachekey)){
            $view = unserialize(Redis::get($cachekey));
            Log::debug('Redis at key '.$cachekey.' HIT!. Will expire in '.Redis::ttl($cachekey).'s');
        } else {
            $data = $data->get();
            foreach($data as $d){
                $pippk = $d->usulan->where('USULAN_TUJUAN','2')->count();
               
                $renja = $d->usulan->where('USULAN_TUJUAN','1')->count();

                $iduser = $d->user->count() > 0 ? $d->user[0]->id : "";

                $berita = "";

                if($d->user->count() > 0)
                {
                    if($d->user[0]->hasBeritaAcara())
                        $berita = "<a href='/musrenbang/2017/public/berita/".$iduser."'>Lihat Berita Acara</a>";
                }


                if($d->RW_AKTIF == 0) $aktif = '<span class="text-danger"><i class="fa fa-close"></i></span>';
                else $aktif = '<span class="text-success"><i class="fa fa-check"></i></span>';
                array_push($view, array( 'RW'                  => $d->RW_NAMA,
                                         'AKTIF'               => $aktif,
                                         'PIPPK'               => $pippk,
                                         'RENJA'               => $renja,
                                         'totalusulan'         => $pippk+$renja,
                                         'TOTALNOMINALUSULAN'  => number_format($d->nominalUsulan(),0,'.',','),
                                         'TOTALNOMINALPIPPK'   => number_format($d->nominalUsulan(2),0,'.',','),
                                         'TOTALNOMINALRENJA'   => number_format($d->nominalUsulan(1),0,'.',','),
                                         'USERNAME'            => $d->user->count() > 0 ? $d->user[0]->email : "",
                                         'KELURAHAN'           => $d->kelurahan->KEL_NAMA,
                                         'KECAMATAN'           => $d->kelurahan->kecamatan->KEC_NAMA,
                                         'NIKKETUA'                  => $d->RW_NIK,
                                         'NAMAKETUA'                  => $d->RW_KETUA,
                                         'TELP'                  => $d->RW_TELP,
                                         'JUMLAHRT'                  => $d->rt->count(),
                                         'NO'                   => $no, 
                                         'RWID'                 =>$d->RW_ID,
                                         'BERITA'              => $berita,
                                        ));    
                $no++;        
            }

            Redis::set($cachekey, serialize($view));
            Redis::expire($cachekey, $cacheexpire);
            Log::debug('Redis at key '.$cachekey.' MISS!. Set expire in: '.$cacheexpire.'s');
        }

        $out = array("aaData"=>$view);
        return Response::JSON($out);
    }

    public function isu($tahun){
        return View('musrenbang.public.isu',compact('tahun'));
    }

    public function getIsu($tahun){
        $data   = Isu::all();
        $no     = 1;
        $view   = array();
        foreach ($data as $d) {
            $pippk = 0;
            $renja = 0;
            $pippk  = Usulan::wherehas('kamus',function($q) use ($d){
                    $q->whereHas('isu',function($x) use ($d){
                        $x->where('ISU_TIPE','2')->where('ISU_ID',$d->ISU_ID);
                    });
                })->count();
            $renja  = Usulan::wherehas('kamus',function($q) use ($d){
                    $q->whereHas('isu',function($x) use ($d){
                        $x->whereIn('ISU_TIPE',['0','1'])->where('ISU_ID',$d->ISU_ID);
                    });
                })->count();
            array_push($view, array( 'no'                  => $no,
                                     'ISU'                 => $d->ISU_NAMA,
                                     'PIPPK'               => $pippk,
                                     'RENJA'               => $renja,  
                                     'TOTAL'               => $pippk+$renja
                                    ));    
            $no++;   
        }
        $out = array("aaData"=>$view);
        return Response::JSON($out);        
    }

    public function kamus($tahun){
        return View('musrenbang.public.kamus',compact('tahun'));
    }

    public function kamusajax($tahun){
        return View('musrenbang.public.kamusajax',compact('tahun'));
    }

    public function getKamusAjax($tahun){
        $start = $_GET['start'];
        $length = $_GET['length'];
        $search = $_GET['search']['value'];

        $data = Kamus::where('KAMUS_NAMA','LIKE','%'.$search.'%');

        $totalsemua = $data->count();
        $totalfilter = $data->count();

        $data   = $data->skip($start)->take($length);
        $no     = 1;
        $view   = array();

        foreach ($data->get() as $d) {
            $pippk = 0;
            $renja = 0;
            $pippk  = Usulan::wherehas('kamus',function($q) use ($d){
                    $q->whereHas('isu',function($x) use ($d){
                        $x->where('ISU_TIPE','2');
                    })->where('KAMUS_ID',$d->KAMUS_ID);
                })->count();
            $renja  = Usulan::wherehas('kamus',function($q) use ($d){
                    $q->whereHas('isu',function($x) use ($d){
                        $x->whereIn('ISU_TIPE',['0','1']);
                    })->where('KAMUS_ID',$d->KAMUS_ID);
                })->count();

            array_push($view, array( 'no'                  => $no,
                                     'KAMUS'               => $d->KAMUS_NAMA,
                                     'SKPD'                => $d->skpd != null ? $d->skpd->SKPD_NAMA : "-",
                                     'PIPPK'               => $pippk,
                                     'RENJA'               => $renja,
                                     'SATUAN'              => $d->KAMUS_SATUAN,
                                     'HARGA'               => number_format($d->KAMUS_HARGA,0,'.',','),
                                     'totalusulan'         => $pippk+$renja,  
                                     'TOTALNOMINALUSULAN'  => number_format(($pippk+$renja) * $d->KAMUS_HARGA,0,'.',','),
                                     'KRITERIA'            => $d->KAMUS_KRITERIA
                                    )); 
            $no++;   
        }

        $out = array(
                "recordsTotal"      => $totalsemua,
                "recordsFiltered"   => $totalfilter,
                "data"              => $view,
            );

        return Response::JSON($out);
    }

    public function getKamus($tahun){

        $data   = Kamus::with(
            [
                
            ]
        );

        $view   = array();
        $no     = 1;
        
        $cachekey = "getKamus-".md5(serialize($data->toSql()).serialize($data->getBindings()));
        $cacheexpire = env('REDIS_TIMEOUT',60);
        if (Redis::exists($cachekey)){
            $view = unserialize(Redis::get($cachekey));
            Log::debug('Redis at key '.$cachekey.' HIT!. Will expire in '.Redis::ttl($cachekey).'s');
        } else {
            $data = $data->get();
            foreach ($data as $d) {
                $pippk = 0;
                $renja = 0;
                $pippk  = Usulan::wherehas('kamus',function($q) use ($d){
                        $q->whereHas('isu',function($x) use ($d){
                            $x->where('ISU_TIPE','2');
                        })->where('KAMUS_ID',$d->KAMUS_ID);
                    })->count();
                $renja  = Usulan::wherehas('kamus',function($q) use ($d){
                        $q->whereHas('isu',function($x) use ($d){
                            $x->whereIn('ISU_TIPE',['0','1']);
                        })->where('KAMUS_ID',$d->KAMUS_ID);
                    })->count();
                $vol = $d->getTotalVolume();
                array_push($view, array( 'no'                  => $no,
                                         'KAMUS'               => $d->KAMUS_NAMA.' ('.$d->KAMUS_ID.')',
                                         'SKPD'                => $d->skpd != null ? $d->skpd->SKPD_NAMA : "Kewilayahan (PIPPK)",
                                         'PIPPK'               => $pippk,
                                         'RENJA'               => $renja,
                                         'SATUAN'              => $d->KAMUS_SATUAN,
                                         'HARGA'               => number_format($d->KAMUS_HARGA,0,'.',','),
                                         'totalusulan'         => $pippk+$renja,  
                                         'totalvolume'         => number_format($vol,0,'.',','),
                                         'TOTALNOMINALUSULAN'  => number_format($vol * $d->KAMUS_HARGA,0,'.',','),
                                         'KRITERIA'            => $d->KAMUS_KRITERIA
                                        )); 
                $no++;   
            }

            Redis::set($cachekey, serialize($view));
            Redis::expire($cachekey, $cacheexpire);
            Log::debug('Redis at key '.$cachekey.' MISS!. Set expire in: '.$cacheexpire.'s');
            
        }

        $out = array("aaData"=>$view);
        return Response::JSON($out);        
    }

    public function showUsulan($tahun){
        $kecamatan = Kecamatan::all();
        return View::make('musrenbang.public.usulan',compact('tahun','kecamatan'));
    }

    public function showUsulanAJAX($tahun){
        $kecamatan = Kecamatan::all();
        return View::make('musrenbang.public.usulanajax',compact('tahun','kecamatan'));
    }

    private function getValueFromArr($arr, $key){
      if (array_key_exists($key, $arr)) return $arr[$key];
      else return 0;
    }

    public function showAccordion($tahun){
        $fb = App::make('SammyK\LaravelFacebookSdk\LaravelFacebookSdk');
        $login_url = $fb
                ->getRedirectLoginHelper()
                ->getLoginUrl('http://musrenbang.bandung.go.id/facebook/callback',['email']);

        $arr = [];
        $idkey = [];
        $kecamatan = Kecamatan::orderBy('KEC_ID','ASC')->get();

        $usulancount = [];
        $arrUsulanCount = $this->getCountRenjaPIPPKAllData($tahun);

        foreach ($kecamatan as $kec){
            $kecamatan_nama = $kec->KEC_NAMA;
            $arr[$kecamatan_nama] = [];

            if ($kec->KEC_ID % 10 == $kec->KEC_ID)
                $kec->KEC_ID = '0'.$kec->KEC_ID;

            $usulancount['renja'][md5($kecamatan_nama)] = $this->getValueFromArr($arrUsulanCount["RENJA"],$kec->KEC_ID);
            $usulancount['pippk'][md5($kecamatan_nama)] = $this->getValueFromArr($arrUsulanCount["PIPPK"],$kec->KEC_ID);
            $idkey[md5($kecamatan_nama)] = $kec->KEC_ID;

            foreach ($kec->kelurahan()->orderBy('KEL_ID','ASC')->get() as $kel){
                $kelurahan_nama = $kel->KEL_NAMA;
                $arr[$kecamatan_nama][$kelurahan_nama] = [];

                if ($kel->KEL_ID % 10 == $kel->KEL_ID)
                    $kel->KEL_ID = '0'.$kel->KEL_ID;

                $usulancount['renja'][md5($kecamatan_nama.$kelurahan_nama)] = $this->getValueFromArr($arrUsulanCount["RENJA"],$kec->KEC_ID.".".$kel->KEL_ID);
                $usulancount['pippk'][md5($kecamatan_nama.$kelurahan_nama)] = $this->getValueFromArr($arrUsulanCount["PIPPK"],$kec->KEC_ID.".".$kel->KEL_ID);
                $idkey[md5($kecamatan_nama.$kelurahan_nama)] = $kec->KEC_ID.'.'.$kel->KEL_ID;

                foreach ($kel->rw()->orderBy('RW_ID','ASC')->get() as $rw){
                    $rw_nama = $rw->RW_NAMA;

                    if ($rw->RW_ID % 10 == $rw->RW_ID)
                        $rw->RW_ID = '0'.$rw->RW_ID;

                    $usulancount['renja'][md5($kecamatan_nama.$kelurahan_nama.$rw_nama)] = $this->getValueFromArr($arrUsulanCount["RENJA"],$kec->KEC_ID.".".$kel->KEL_ID.".".$rw->RW_ID);
                    $usulancount['pippk'][md5($kecamatan_nama.$kelurahan_nama.$rw_nama)] = $this->getValueFromArr($arrUsulanCount["PIPPK"],$kec->KEC_ID.".".$kel->KEL_ID.".".$rw->RW_ID);
                    $arr[$kecamatan_nama][$kelurahan_nama][$rw_nama] = $rw->RW_ID;
                    $idkey[md5($kecamatan_nama.$kelurahan_nama.$rw_nama)] = $kec->KEC_ID.'.'.$kel->KEL_ID.'.'.$rw->RW_ID;
                }
            }
        }
        return View::make('musrenbang.public.accordion',compact('tahun','arr','idkey','usulancount','login_url'));
    } 

    private function getCountRenjaPIPPKAllData($tahun){
      $tablekamus = "REFERENSI.REF_KAMUS";
      $tableisu = "REFERENSI.REF_ISU";
      $tableusulan = "MUSRENBANG.DAT_USULAN";
      $tableuser = "DATA.users";
      $tablerw = "REFERENSI.REF_RW";
      $tablekecamatan = "REFERENSI.REF_KECAMATAN";
      $tablekelurahan = "REFERENSI.REF_KELURAHAN";


      $ret = [];
      $ret["PIPPK"] = [];
      $ret["RENJA"] = [];

      $usulans = Usulan::select("email","ISU_TIPE",$tableuser.".KEC_ID",$tableuser.".KEL_ID",$tableuser.".RW_ID")->join($tablekamus,$tablekamus.".KAMUS_ID",$tableusulan.".KAMUS_ID")
                    ->join($tableisu,$tableisu.".ISU_ID",$tablekamus.".ISU_ID")
                    ->join($tableuser,$tableuser.".id",$tableusulan.".USER_CREATED")
                    ->join($tablekecamatan,$tableuser.".KEC_ID",$tablekecamatan.".KEC_ID")
                    ->join($tablekelurahan,$tableuser.".KEL_ID",$tablekelurahan.".KEL_ID")
                    ->leftjoin($tablerw,$tableuser.".RW_ID",$tablerw.".RW_ID")
                    ->where("USULAN_TAHUN",$tahun)
                    ->orderBy("KEC_ID",'ASC')
                    ->orderBy("KEL_ID",'ASC')
                    ->orderBy("RW_ID","ASC")
                    ->get();

      foreach ($usulans as $usulan){        
        if ($usulan->KEC_ID % 10 == $usulan->KEC_ID)
          $usulan->KEC_ID = '0'.$usulan->KEC_ID;

        if ($usulan->KEL_ID % 10 == $usulan->KEL_ID)
          $usulan->KEL_ID = '0'.$usulan->KEL_ID;

        if ($usulan->RW_ID % 10 == $usulan->RW_ID)
          $usulan->RW_ID = '0'.$usulan->RW_ID;

        $isuTipe = "RENJA";
        if ($usulan->ISU_TIPE=="2") $isuTipe = "PIPPK";

        $key = $usulan->KEC_ID;
        if (!array_key_exists($key, $ret[$isuTipe])) $ret[$isuTipe][$key] = 1;
        else $ret[$isuTipe][$key]++;

        $key = $usulan->KEC_ID.".".$usulan->KEL_ID;
        if (!array_key_exists($key, $ret[$isuTipe])) $ret[$isuTipe][$key] = 1;
        else $ret[$isuTipe][$key]++;

        $key = $usulan->KEC_ID.".".$usulan->KEL_ID.".".$usulan->RW_ID;
        if (!array_key_exists($key, $ret[$isuTipe])) $ret[$isuTipe][$key] = 1;
        else $ret[$isuTipe][$key]++;

      }
      //dd($ret);
      return $ret;
    }

    /*
    private function getCountRenjaPIPPK($email){
      $tablekamus = "REFERENSI.REF_KAMUS";
      $tableisu = "REFERENSI.REF_ISU";
      $tableusulan = "MUSRENBANG.DAT_USULAN";
      $tableuser = "DATA.users";

      $renja = Usulan::join($tablekamus,$tablekamus.".KAMUS_ID",$tableusulan.".KAMUS_ID")
                    ->join($tableisu,$tableisu.".ISU_ID",$tablekamus.".ISU_ID")
                    ->join($tableuser,$tableuser.".id",$tableusulan.".USER_CREATED")
                    ->where($tableuser.".email","LIKE",$email."%")
                    ->where("USULAN_TAHUN",$tahun)
                    ->whereIn($tableisu.".ISU_TIPE",[0,1])
                    ->count();

      $pippk = Usulan::join($tablekamus,$tablekamus.".KAMUS_ID",$tableusulan.".KAMUS_ID")
                    ->join($tableisu,$tableisu.".ISU_ID",$tablekamus.".ISU_ID")
                    ->join($tableuser,$tableuser.".id",$tableusulan.".USER_CREATED")
                    ->where($tableuser.".email","LIKE",$email."%")
                    ->where("USULAN_TAHUN",$tahun)
                    ->where($tableisu.".ISU_TIPE",2)
                    ->count();

      return [$renja, $pippk];
    }

    public function showAccordion($tahun){
        $arr = [];
        $idkey = [];
        $kecamatan = Kecamatan::orderBy('KEC_ID','ASC')->get();

        $usulancount = [];

        foreach ($kecamatan as $kec){
            $kecamatan_nama = $kec->KEC_NAMA;
            $arr[$kecamatan_nama] = [];

            if ($kec->KEC_ID % 10 == $kec->KEC_ID)
                $kec->KEC_ID = '0'.$kec->KEC_ID;


            $usulanC = $this->getCountRenjaPIPPK($kec->KEC_ID);
            $usulancount['renja'][md5($kecamatan_nama)] = $usulanC[0];
            $usulancount['pippk'][md5($kecamatan_nama)] = $usulanC[1];
            $idkey[md5($kecamatan_nama)] = $kec->KEC_ID;

            foreach ($kec->kelurahan()->orderBy('KEL_ID','ASC')->get() as $kel){
                $kelurahan_nama = $kel->KEL_NAMA;
                $arr[$kecamatan_nama][$kelurahan_nama] = [];

                if ($kel->KEL_ID % 10 == $kel->KEL_ID)
                    $kel->KEL_ID = '0'.$kel->KEL_ID;

                $usulanC = $this->getCountRenjaPIPPK($kec->KEC_ID.".".$kel->KEL_ID);
                $usulancount['renja'][md5($kecamatan_nama.$kelurahan_nama)] = $usulanC[0];
                $usulancount['pippk'][md5($kecamatan_nama.$kelurahan_nama)] = $usulanC[1];
                $idkey[md5($kecamatan_nama.$kelurahan_nama)] = $kec->KEC_ID.'.'.$kel->KEL_ID;

                foreach ($kel->rw()->orderBy('RW_ID','ASC')->get() as $rw){
                    $rw_nama = $rw->RW_NAMA;

                    if ($rw->RW_ID % 10 == $rw->RW_ID)
                        $rw->RW_ID = '0'.$rw->RW_ID;

                    $usulanC = $this->getCountRenjaPIPPK($kec->KEC_ID.".".$kel->KEL_ID.'.'.$rw->RW_ID);
                    $usulancount['renja'][md5($kecamatan_nama.$kelurahan_nama.$rw_nama)] = $usulanC[0];
                    $usulancount['pippk'][md5($kecamatan_nama.$kelurahan_nama.$rw_nama)] = $usulanC[1];
                    $arr[$kecamatan_nama][$kelurahan_nama][$rw_nama] = $rw->RW_ID;
                    $idkey[md5($kecamatan_nama.$kelurahan_nama.$rw_nama)] = $kec->KEC_ID.'.'.$kel->KEL_ID.'.'.$rw->RW_ID;
                }
            }
        }
        return View::make('musrenbang.public.accordion',compact('tahun','arr','idkey','usulancount'));
    } */

    public function getUsulanRWTabel($tahun,$rwid){
        $usulans = Usulan::where('USULAN_TAHUN',$tahun)->where('RW_ID',$rwid)->get();

        $fb = new \Facebook\Facebook([
            'app_id'                => env('FACEBOOK_APP_ID'),
            'app_secret'            => env('FACEBOOK_APP_SECRET'),
            'default_graph_version' => 'v2.8',
            'default_access_token'  => Session::get('fb_user_access_token'),
        ]);

        //Getting batch
        $fullURLs = "";
        $count = 1;
        foreach ($usulans as $row){
            //if ($count % 49 == 0)
            $fullURL = "http://".$_SERVER['HTTP_HOST'].'/musrenbang/2017/public/usulan/detail/'.$row->USULAN_ID;
            $fullURLs = $fullURLs.$fullURL.",";
        }  
        $fullURLs = substr($fullURLs,0,-1); //truncate last one

        // Send the request to Graph
        try {
             $request = $fb->request('GET', '/',array('ids' => $fullURLs));
             $response = $fb->getClient()->sendRequest($request);
        } catch (\Facebook\Exceptions\FacebookResponseException $e) {
            Log::warning('Graph returned an error: '.$e->getMessage());
            $response = null;
        } catch (\Facebook\Exceptions\FacebookSDKException $e) {
            Log::warning('Facebook SDK returned an error: '.$e->getMessage());
            $response = null;
        }

        if ($response != null){
            $response = $response->getGraphNode();
        }

        return View::make('musrenbang.public.usulanrwtabel',compact('rwid','tahun','usulans','response'));
    }

    public function getUsulanAJAX($tahun)
    {
        $start = $_GET['start'];
        $length = $_GET['length'];
        $search = $_GET['search']['value'];

        $columnorder = $_GET['order'][0]['column'];
        $ordertype   = $_GET['order'][0]['dir'];

        $tableusulan = 'MUSRENBANG.DAT_USULAN';
        $tableuser = "users";
        $tablerw = "REFERENSI.REF_RW";
        $tablekecamatan = "REFERENSI.REF_KECAMATAN";
        $tablekelurahan = "REFERENSI.REF_KELURAHAN";
        $tablekamus = "REFERENSI.REF_KAMUS";
        $tableisu = "REFERENSI.REF_ISU";
        $tableskpd = "REFERENSI.REF_SKPD";
        $usulans = Usulan::select(
                        "USULAN_ID as id",
                        "email",
                        "USULAN_TUJUAN as tipe",
                        "USULAN_URGENSI as urgensi",
                        "level",
                        "KEC_NAMA as kecamatan",
                        "KEL_NAMA as kelurahan",
                        "RW_NAMA as rw",
                        "ISU_NAMA as isu",
                        "KAMUS_NAMA as kamus",
                        "USULAN_VOLUME as volume",
                        "KAMUS_SATUAN as satuan",
                        "KAMUS_HARGA as harga",
                        DB::raw(
                            '"KAMUS_HARGA" * "USULAN_VOLUME" as totalnominal'
                        ),
                        "SKPD_NAMA as skpd",
                        "ALAMAT as alamat",
                        "USULAN_LONG as long",
                        "USULAN_LAT as lat",
                        "USULAN_STATUS as status",
                        "USULAN_POSISI as posisi"
                    )
                    ->join($tablekamus,$tablekamus.".KAMUS_ID",$tableusulan.".KAMUS_ID")
                    ->leftJoin($tableskpd,$tableskpd.".SKPD_ID",$tablekamus.".KAMUS_SKPD")
                    ->join($tableisu,$tableisu.".ISU_ID",$tablekamus.".ISU_ID")
                    ->join($tableuser,$tableuser.".id",$tableusulan.".USER_CREATED")
                    ->join($tablekecamatan,$tableuser.".KEC_ID",$tablekecamatan.".KEC_ID")
                    ->join($tablekelurahan,$tableuser.".KEL_ID",$tablekelurahan.".KEL_ID")
                    ->leftjoin($tablerw,$tableuser.".RW_ID",$tablerw.".RW_ID");
                    

        $totalsemua = $usulans->count();

        $usulans = $usulans->where($tablekamus.".KAMUS_NAMA","ilike","%".$search."%");
        $usulans = $usulans->orWhere($tablerw.".RW_NAMA","ilike","%".$search."%");
        $usulans = $usulans->orWhere($tablekecamatan.".KEC_NAMA","ilike","%".$search."%");
        $usulans = $usulans->orWhere($tablekelurahan.".KEL_NAMA","ilike","%".$search."%");
        $usulans = $usulans->orWhere($tablerw.".RW_NAMA","ilike","%".$search."%");
        $usulans = $usulans->orWhere($tableskpd.".SKPD_NAMA","ilike","%".$search."%");
        $usulans = $usulans->orWhere($tableusulan.".ALAMAT","ilike","%".$search."%");
        $usulans = $usulans->orWhere($tableskpd.".SKPD_NAMA","ilike","%".$search."%");

        $totalfilter = $usulans->count();

        if ($columnorder == 9)
            $usulans = $usulans->orderBy('totalnominal',$ordertype);
        else if ($columnorder == 7)
            $usulans = $usulans->orderBy('USULAN_VOLUME',$ordertype);
        else if ($columnorder == 8)
            $usulans = $usulans->orderBy('harga',$ordertype);

        $usulans = $usulans->skip($start)->take($length)->get();
        $view = array();

        $no         = 1;
        $view       = array();
        foreach ($usulans as $data) {
            
            
            $lokasi     = 'Kecamatan '.$data->kecamatan.'<br>'.
                          'Kelurahan '.$data->kelurahan.'<br>'.
                          'RW '.$data->rw;

            if($data->tipe == 2) $tipe = 'PIPPK';
            else $tipe = 'RENJA';

            array_push($view, array( 'USULAN_ID'        => $data->id,
                                     'TIPE'             => $tipe,
                                     'NO'               => $no,
                                     'LOKASI'           => $lokasi,
                                     'ISU'              => $data->isu."<br><p class='text-orange'>".$data->kamus."</p>",
                                     'STATUS'           => Usulan::toHtmlIconStatus($data->status,$data->posisi),
                                     'VOLUME'           => $data->volume." ".$data->satuan,
                                     'PENGUSUL'         => $data->email,
                                     'TOTAL'            => number_format($data->totalnominal,0,'.',','),
                                     'URGENSI'          => $data->urgensi,
                                     'HARGASATUAN'      => number_format($data->harga,0,'.',','),
                                     'SKPD'             => $data->skpd != null ? $data->skpd : "Kewilayahan (PIPPK)",
                                     'ALAMAT'           => $data->alamat,
                                     'MAP'              => $data->long." ".$data->lat
                                     ));
            $no++;
        }
        
        $out = array(
                "recordsTotal"      => $totalsemua,
                "recordsFiltered"   => $totalfilter,
                "data"              => $view,
            );
        return Response::JSON($out);
    }

    public function getUsulan($tahun,$tipe,$kecamatan,$kelurahan,$rw,$isu,$kamus){
        set_time_limit(300);

        $data = Usulan::where('USULAN_TAHUN',$tahun);
        if ($tipe != 'x'){
            if($tipe == 'pippk') $type = [2];
            else $type = [0,1];
            $data   = $data->whereHas('kamus',function($q) use($type){
                $q->whereHas('isu',function($x) use($type){
                    $x->whereIn('ISU_TIPE',$type);
                });
            });
        }

        if ($kecamatan != 'x'){
            $data   = $data->whereHas('rw',function($q) use($kecamatan){
                $q->whereHas('kelurahan',function($x) use($kecamatan){
                    $x->whereHas('kecamatan',function($y) use($kecamatan){
                        $y->where('KEC_ID',$kecamatan);
                    });
                });
            });
        }

        if ($kelurahan != 'x'){
            $data   = $data->whereHas('rw',function($q) use($kelurahan){
                $q->whereHas('kelurahan',function($x) use($kelurahan){
                    $x->where('KEL_ID',$kelurahan);
                });
            });
        }

        if ($rw != 'x'){
            $data   = $data->where('RW_ID',$rw);
        }

        if ($kamus != 'x'){
            $data   = $data->where('KAMUS_ID',$kamus);
        }

        if ($isu != 'x'){
            $data   = $data->whereHas('kamus',function($q) use($isu){
                $q->whereHas('isu',function($x) use($isu){
                    $x->where('ISU_ID',$isu);
                });
            });
        }
        $data  = $data->with("rw","rw.kelurahan","kamus.skpd","rw.kelurahan.kecamatan","kamus","kamus.isu","user");
        $cachekey = "getUsulan-".md5(serialize($data->toSql()).serialize($data->getBindings())); //based on query dan bindings
        $cacheexpire = env('REDIS_TIMEOUT',60);
        $view       = array();

        if (Redis::exists($cachekey)){
            $view = unserialize(Redis::get($cachekey));
            Log::debug('Redis at key '.$cachekey.' HIT!. Will expire in '.Redis::ttl($cachekey).'s');
        } else {
            $no         = 1;
            $data = $data->get();
        
            foreach ($data as $data) {
                if($data->USULAN_STATUS == 1) $status = '<i class="fa fa-refresh text-info"></i>';
                elseif($data->USULAN_STATUS == 2) $status = '<i class="fa fa-check text-success"></i>';
                else $status = '<i class="fa fa-close text-danger"></i>';


                if($data->USULAN_POSISI == 1) $posisi = ' Kelurahan';
                elseif($data->USULAN_POSISI == 2) $posisi = ' Kecamatan';
                elseif($data->USULAN_POSISI == 3) $posisi = ' Kota';
                else $posisi = ' APBD';

                $lokasi     = 'Kecamatan '.$data->rw->kelurahan->kecamatan->KEC_NAMA.'<br>'.
                              'Kelurahan '.$data->rw->kelurahan->KEL_NAMA.'<br>'.
                              'RW '.$data->rw->RW_NAMA;

                if($data->kamus->isu->ISU_TIPE == 2) $tipe = 'PIPPK';
                else $tipe = 'RENJA';

                array_push($view, array( 'USULAN_ID'        => $data->USULAN_ID,
                                         'TIPE'             => $tipe,
                                         'NO'               => $no,
                                         'LOKASI'           => $lokasi,
                                         'ISU'              => $data->kamus->isu->ISU_NAMA."<br><p class='text-orange'>".$data->kamus->KAMUS_NAMA."</p>",
                                         'STATUS'           => "<p>".$status.$posisi."</p>",
                                         'VOLUME'           => $data->USULAN_VOLUME." ".$data->kamus->KAMUS_SATUAN,
                                         'PENGUSUL'         => $data->user->email,
                                         'TOTAL'            => number_format($data->kamus->KAMUS_HARGA * $data->USULAN_VOLUME,0,'.',','),
                                         'URGENSI'          => $data->USULAN_URGENSI,
                                         'HARGASATUAN'      => number_format($data->kamus->KAMUS_HARGA,0,'.',','),
                                         'SKPD'             => $data->kamus->skpd != null ? $data->kamus->skpd->SKPD_NAMA : 'Kewilayahan (PIPPK)',
                                         'ALAMAT'           => $data->ALAMAT,
                                         'MAP'              => $data->USULAN_LONG." ".$data->USULAN_LAT
                                         ));
                $no++;
            }

            Redis::set($cachekey, serialize($view));
            Redis::expire($cachekey, $cacheexpire);
            Log::debug('Redis at key '.$cachekey.' MISS!. Set expire in: '.$cacheexpire.'s');
        }

        $out = array("aaData"=>$view);
        return Response::JSON($out);
    }


    public function getS($tahun,$tipe,$id){
        $view   = "";
        if($tipe == 'kelurahan'){
            $data = Kelurahan::where('KEC_ID',$id)->orderBy('KEL_NAMA')->get();
            foreach ($data as $data) {
                $view         = $view."<option value='".$data->KEL_ID."' style='color:black'>".$data->KEL_NAMA."</option>";
            }            
        }
        elseif($tipe == 'rw'){
            $data = RW::where('KEL_ID',$id)->orderBy('RW_NAMA')->get();
            foreach ($data as $data) {
                $view         = $view."<option value='".$data->RW_ID."' style='color:black'>".$data->RW_NAMA."</option>";
            }            
        }
        elseif($tipe == 'isu'){
            if($id == 'pippk') $type = ['2'];
            else $type = ['0','1'];
            $data = Isu::whereIn('ISU_TIPE',$type)->orderBy('ISU_NAMA')->get();
            foreach ($data as $data) {
                $view         = $view."<option value='".$data->ISU_ID."' style='color:black'>".$data->ISU_NAMA."</option>";
            }            
        }
        elseif($tipe == 'kamus'){
            $data = Kamus::where('ISU_ID',$id)->orderBy('KAMUS_NAMA')->get();
            foreach ($data as $data) {
                $view         = $view."<option value='".$data->KAMUS_ID."' style='color:black'>".$data->KAMUS_NAMA."</option>";
            }            
        }
        return $view;        
    }

    public function detailusulan($tahun,$id){
        $rts = null;
        $daftar_rt = null;

        $cekData_1      = Usulan::where('USULAN_ID',$id)->where('USULAN_POSISI','1')->first();
        if (empty($cekData_1)){
            $rw     = Usulan::where('USULAN_ID',$id)->where('USULAN_POSISI','2')->first();
        }       
        else{
            $rw     = $cekData_1;
            $rts = RT::whereIn('RT_ID',$rw->RT_ID)->first(); 
            $daftar_rt = RT::whereIn('RT_ID',$rw->RT_ID)->orderBy('RT_NAMA')->get(); 
        }

        $cekData_2      = Usulan_Rekap::where('USULAN_ID',$id)->where('USULAN_POSISI','2')->first();
        if (empty($cekData_2)){
            $kel     = Usulan::where('USULAN_ID',$id)->where('USULAN_POSISI','2')->first();
        }else{
            $kel     = $cekData_2;
            echo "<br>rw : ".$rw;
        }

        $cekData_3      = Usulan_Rekap::where('USULAN_ID',$id)->where('USULAN_POSISI','3')->first();
        if (empty($cekData_3)){
            $kec     = Usulan::where('USULAN_ID',$id)->where('USULAN_POSISI','3')->first();
        }else{
            $kec     = $cekData_3;
        }

        $cekData_4      = Usulan_Rekap::where('USULAN_ID',$id)->where('USULAN_POSISI','4')->first();
        if (empty($cekData_4)){
            $kota     = Usulan::where('USULAN_ID',$id)->where('USULAN_POSISI','4')->first();
        }else{
            $kota     = $cekData_4;
        }

        $timeline       = Usulan::where('USULAN_ID',$id)->first();

        $data = array(
                    'tahun'     => $tahun,
                    'rw'        => $rw,
                    'kel'       => $kel,
                    'kec'       => $kec,
                    'kota'      => $kota,
                    'timeline'  => $timeline,
                    'rts'       => $rts,
                    'pageURL'   => Request::url(),
                    'daftar_rt' => $daftar_rt,       
                );

        if (!$rw) dd($data,"Object RW tidak boleh kosong");
        return View::make('musrenbang.public.detailusulan',$data);    
    }

     public function berita($tahun,$id){
        $berita    = BeritaAcara::where('id',$id)->get();
        $BeritaAcaraFoto    = BeritaAcaraFoto::where('id',$id)->get();
        $user = User::where('id',$id)->first();

        $data = array(
                    'berita'    =>$berita,
                    'beritaFoto'    =>$BeritaAcaraFoto,
                    'user'     =>$user,
                    'tahun'    =>$tahun); 

       // 
        //dd($data);                   
        
        return View::make('musrenbang.public.berita',$data);
    }

    public function ubahkamusskpd($tahun, $id_kamus, $kode_skpd){
        $skpd = SKPD::where('SKPD_KODE',$kode_skpd)->where('SKPD_TAHUN',$tahun)->first();
        if ($skpd==null) exit("SKPD Tidak Ditemukan");
        else {
            $kamus = Kamus::find($id_kamus);
            if ($kamus==null) exit("Kamus tidak ditemukan");
            else {
                $kamus->KAMUS_SKPD = $skpd->SKPD_ID;
                $kamus->save();
                dd("Sukses memindahkan kamus ".$kamus->KAMUS_NAMA." ke ".$skpd->SKPD_NAMA."!");
            }
        }   
    }


    public function pippklkkanomaliview($tahun){
        return View('musrenbang.public.pippklkkanomaliview',compact('tahun'));
    }

    public function pippklkkanomali($tahun, $threshold){
        $ret = [];
        $user = User::orWhereIn('level',[1,2,3,4]);
        $no = 1;
        $out = null;

        $cachekey = "pippklkkanomali-".md5(serialize($user->toSql()).serialize($user->getBindings()).$tahun.$threshold);
        $cacheexpire = env('REDIS_TIMEOUT',60);

        if (Redis::exists($cachekey)){ //kalau ada di cache
            $out = unserialize(Redis::get($cachekey));
            Log::debug('Redis at key '.$cachekey.' HIT!. Will expire in '.Redis::ttl($cachekey).'s');
        } else {
            //kalau tidak ada di cache
            $user = $user->get();
            foreach ($user as $u){
                $usulans = $u->usulan()->where('USULAN_TAHUN',$tahun)->where('USULAN_TUJUAN',2)->get(); //ambil usulan per user, pippk == 2
                $nominal = 0;


                if ($usulans != null){
                        foreach ($usulans as $s){
                        $k = $s->kamus()->first(); //ambil kamus
                        if ($k != null) //validation
                            $nominal = $nominal + $k->KAMUS_HARGA;
                    }   
                }

                if ($nominal >= $threshold) {
                    $obj = [];
                    $obj['no']      = $no;
                    $obj['nama']    = "Kecamatan ".$u->kecamatan()->first()->KEC_NAMA.", Kelurahan ".$u->kelurahan()->first()->KEL_NAMA.", RW ".$u->rw()->first()->RW_NAMA;
                    $obj['nominal'] = "Rp ".number_format($nominal,0);

                    array_push($ret, $obj);
                    $no++;

                }

            }

            $out = array("aaData"=>$ret);

            Redis::set($cachekey, serialize($out)); //cache $out
            Redis::expire($cachekey, $cacheexpire);
            Log::debug('Redis at key '.$cachekey.' MISS!. Set expire in: '.$cacheexpire.'s');
        }

        return Response::JSON($out);
    }

    public function renjaanomali($tahun){
        return View('musrenbang.public.renjaanomali',compact('tahun'));
    }


    public function renjaanomalinominal($tahun, $threshold){
        $ret = [];
        $user = User::orWhereIn('level',[1,5]);
        $no = 1;
        $out = null;

        $cachekey = "renjaanomalinominal-".md5(serialize($user->toSql()).serialize($user->getBindings()).$tahun.$threshold);
        $cacheexpire = env('REDIS_TIMEOUT',60);

        if (Redis::exists($cachekey)){ //kalau ada di cache
            $out = unserialize(Redis::get($cachekey));
            Log::debug('Redis at key '.$cachekey.' HIT!. Will expire in '.Redis::ttl($cachekey).'s');
        } else {
            //kalau tidak ada di cache
            $user = $user->get();
            foreach ($user as $u){
                $usulans = $u->usulan()->where('USULAN_TAHUN',$tahun)->where('USULAN_TUJUAN',1)->get(); //ambil usulan per user, pippk == 2
                $nominal = 0;

                if ($usulans != null){
                        foreach ($usulans as $s){
                        $k = $s->kamus()->first(); //ambil kamus
                        if ($k != null) //validation
                            $nominal = $nominal + $k->KAMUS_HARGA;
                    }   
                }

                if ($nominal >= $threshold) {
                    $obj = [];
                    $obj['no']      = $no;
                    $obj['nama']    = "Kecamatan ".$u->kecamatan()->first()->KEC_NAMA.", Kelurahan ".$u->kelurahan()->first()->KEL_NAMA.", RW ".$u->rw()->first()->RW_NAMA;
                    $obj['nominal'] = "Rp ".number_format($nominal,0);

                    array_push($ret, $obj);
                    $no++;

                }

            }

            $out = array("aaData"=>$ret);

            Redis::set($cachekey, serialize($out)); //cache $out
            Redis::expire($cachekey, $cacheexpire);
            Log::debug('Redis at key '.$cachekey.' MISS!. Set expire in: '.$cacheexpire.'s');
        }

        return Response::JSON($out);
    }


    public function renjaanomalijumlahview($tahun){
        return View('musrenbang.public.renjaanomalijumlah',compact('tahun'));
    }

    public function renjaanomalijumlah($tahun, $threshold){
        $ret = [];
        $user = User::orWhereIn('level',[1,5]);
        $no = 1;
        $out = null;

        $cachekey = "renjaanomalijumlah-".md5(serialize($user->toSql()).serialize($user->getBindings()).$tahun.$threshold);
        $cacheexpire = env('REDIS_TIMEOUT',60);

        if (Redis::exists($cachekey)){ //kalau ada di cache
            $out = unserialize(Redis::get($cachekey));
            Log::debug('Redis at key '.$cachekey.' HIT!. Will expire in '.Redis::ttl($cachekey).'s');
        } else {
            //kalau tidak ada di cache
            $user = $user->get();
            foreach ($user as $u){
                $usulans = $u->usulan()->where('USULAN_TAHUN',$tahun)->where('USULAN_TUJUAN',1)->get(); //ambil usulan per user, pippk == 2
                $usulan = 0;
                
                if ($usulans != null){
                        foreach ($usulans as $s){
                        $k = $s->kamus()->first(); //ambil kamus
                        if ($k != null) //validation
                            $usulan = $usulan + 1; //jumlah dari si renjanya
                    }   
                }

                if ($usulan > $threshold) {
                    $obj = [];
                    $obj['no']      = $no;
                    $obj['nama']    = "Kecamatan ".$u->kecamatan()->first()->KEC_NAMA.", Kelurahan ".$u->kelurahan()->first()->KEL_NAMA.", RW ".$u->rw()->first()->RW_NAMA;
                    $obj['usulan'] = $usulan;

                    array_push($ret, $obj);
                    $no++;

                }

            }

            $out = array("aaData"=>$ret);

            Redis::set($cachekey, serialize($out)); //cache $out
            Redis::expire($cachekey, $cacheexpire);
            Log::debug('Redis at key '.$cachekey.' MISS!. Set expire in: '.$cacheexpire.'s');
        }

        return Response::JSON($out);
    }

    public function likeUsulan($usulanid,$react){
        $facebook_user = Session::get("facebook_user");

        if ($facebook_user == null){
            echo json_encode(['code'=>500,'message'=>'Silahkan login terlebih dahulu']);
        } else {
            if ($react=="like"){
                Likes::updateOrCreate(
                    ['oauth_uid' => $facebook_user['id'], 'USULAN_ID' => $usulanid],
                    ['oauth_uid' => $facebook_user['id'], 'USULAN_ID' => $usulanid, 'is_like'   => true]
                );
                echo json_encode(['code'=>200,'message'=>'Terima kasih atas responnya']);
            } else if ($react="dislike"){
                Likes::updateOrCreate(
                    ['oauth_uid' => $facebook_user['id'], 'USULAN_ID' => $usulanid],
                    ['oauth_uid' => $facebook_user['id'], 'USULAN_ID' => $usulanid, 'is_like'   => false]
                );
                echo json_encode(['code'=>200,'message'=>'Terima kasih atas responnya']);
            } else echo "Invalid Request";
        }
    }

     public function getApiEbudgeting(){
        $json       = file_get_contents('http://182.23.92.130/main/2018/murni/usulan/api');
        $obj        = json_decode($json);

       /*$a = count($obj);
       dd($a);*/

        foreach ($obj as $usulan) {

                //$arrayUsul = array('usulan_id' =>$usulan->USULAN_ID );
              
              if($usulan->USULAN_TUJUAN == 1){
                Usulan::where('USULAN_ID', $usulan->USULAN_ID)
                        ->update(['USULAN_STATUS' => '2', 'USULAN_POSISI' => '4']);
                }
                else if($usulan->USULAN_TUJUAN == 2){
                    Usulan::where('USULAN_ID', $usulan->USULAN_ID)
                        ->update(['USULAN_STATUS' => '2', 'USULAN_POSISI' => '3']);
                }

                    /*$data       = Usulan::find($usulan->USULAN_ID);

                    $rekap      = new Usulan_Rekap_Apbd;
                    
                    $rekap->USULAN_ID       = $data->USULAN_ID;
                    $rekap->USULAN_TAHUN    = $data->USULAN_TAHUN;
                    $rekap->KAMUS_ID        = $data->KAMUS_ID;
                    $rekap->USULAN_URGENSI  = $data->USULAN_URGENSI;
                    $rekap->USULAN_VOLUME   = $data->USULAN_VOLUME;
                    $rekap->USULAN_GAMBAR   = $data->USULAN_GAMBAR != null ? $data->USULAN_GAMBAR[0] : null;
                    $rekap->RT_ID           = $data->RT_ID != null ? $data->RT_ID[0] : null;
                    $rekap->USULAN_LAT      = $data->USULAN_LAT;
                    $rekap->USULAN_LONG     = $data->USULAN_LONG;
                    $rekap->USULAN_PRIORITAS= $data->USULAN_PRIORITAS;
                    $rekap->USULAN_STATUS   = 2;
                    $rekap->USULAN_POSISI   = 4;
                    $rekap->USER_CREATED    = $data->USER_CREATED;
                    $rekap->TIME_CREATED    = $data->TIME_CREATED;
                    $rekap->IP_CREATED      = $data->IP_CREATED;
                    $rekap->USER_UPDATED    = $data->USER_UPDATED;
                    $rekap->TIME_UPDATED    = $data->TIME_UPDATED;
                    $rekap->IP_UPDATED      = $data->IP_UPDATED;
                    $rekap->USULAN_TUJUAN    = $data->USULAN_TUJUAN;
                  
                    $rekap->save();*/

                Kamus::where('KAMUS_ID', $usulan->KAMUS_ID)
                ->update([
                    'KAMUS_HARGA'          => $usulan->KAMUS_HARGA
                    ]);
                
                
        }
        


    }


    public function trackingUsulan($tahun){
        $kec    = Kecamatan::whereBetween('KEC_ID', array(1, 30))->get();
        $kel    = Kelurahan::whereBetween('KEL_ID', array(1, 151))->get();
        $rw     = RW::whereBetween('RW_ID', array(1, 1589))->get();
        $usulan_tahun = Usulan::distinct()->select('USULAN_TAHUN')->get();;
        //dd($usulan);
        $diproses =  USULAN::where('USULAN_STATUS',1)->count();
        $diterima =  USULAN::where('USULAN_STATUS',2)->count();
        $ditolak  =  USULAN::where('USULAN_STATUS',0)->count();

         $data   = array('tahun'    => $tahun,
                        'usulan_tahun' => $usulan_tahun,
                        'kecamatan' => $kec,
                        'kelurahan' => $kel,
                        'rw'        => $rw,
                        'diproses'  => $diproses,
                        'diterima'  => $diterima,
                        'ditolak'   => $ditolak,
                         );

         return View::make('musrenbang.public.tracking-usulan',$data); 
    }

    public function getKelurahanTracking($tahun,$id){
        $data       = Kelurahan::where('KEC_ID',$id)->get();
        $view   = "";
        foreach ($data as $data) {
            $view         = $view."<option value='".$data->KEL_ID."' style='color:black'>".$data->KEL_NAMA."</option>";
        }
        $out    = array('opt'=>$view);
        return $out;
    }

    public function getRwTracking($tahun,$id){
        $data       = RW::where('KEL_ID',$id)->orderBy('RW_NAMA')->get();
        $view   = "";
        foreach ($data as $data) {
            $view         = $view."<option value='".$data->RW_ID."' style='color:black'>RW.".$data->RW_NAMA."-".$data->kelurahan->KEL_NAMA."</option>";
        }
        $out    = array('opt'=>$view);
        return $out;
    }

    public function trackingGetdata($tahun, $status, $kecamatan = "x", $kelurahan="x", $rw="x", $tipe="x")
    {
      
        $usulans = Usulan::where("USULAN_TAHUN",$tahun)->where('USULAN_STATUS',$status);
                    
        $diproses = '';
        $diterima = '';           
        $ditolak = '';
        $h_kec = '';
        $h_kel = '';
        $h_rw = '';
        $h_tipe = '';

        if($kecamatan != "x")
        {
            $usulans = $usulans->wherehas("user",function($user) use($kecamatan){
                $user->where('KEC_ID',$kecamatan);
            });
            $diproses =  USULAN::wherehas("user",function($user) use($kecamatan){
                            $user->where('KEC_ID',$kecamatan);
                         })->where('USULAN_STATUS',1)->where('USULAN_TUJUAN',$tipe)->count();
            $diterima =  USULAN::wherehas("user",function($user) use($kecamatan){
                            $user->where('KEC_ID',$kecamatan);
                         })->where('USULAN_STATUS',2)->where('USULAN_TUJUAN',$tipe)->count();
            $ditolak  =  USULAN::wherehas("user",function($user) use($kecamatan){
                            $user->where('KEC_ID',$kecamatan);
                         })->where('USULAN_STATUS',0)->where('USULAN_TUJUAN',$tipe)->count();

            $h_kec = Kecamatan::where('KEC_ID',$kecamatan)->first();
           // dd($kec->KEC_NAMA);
        }

        if ($kelurahan != "x")
        {
             $usulans = $usulans->wherehas("user",function($user) use($kelurahan){
                $user->where('KEL_ID',$kelurahan);
            });
             $diproses =  USULAN::wherehas("user",function($user) use($kelurahan){
                            $user->where('KEL_ID',$kelurahan);
                         })->where('USULAN_STATUS',1)->where('USULAN_TUJUAN',$tipe)->count();
            $diterima =  USULAN::wherehas("user",function($user) use($kelurahan){
                            $user->where('KEL_ID',$kelurahan);
                         })->where('USULAN_STATUS',2)->where('USULAN_TUJUAN',$tipe)->count();
            $ditolak  =  USULAN::wherehas("user",function($user) use($kelurahan){
                            $user->where('KEL_ID',$kelurahan);
                         })->where('USULAN_STATUS',0)->where('USULAN_TUJUAN',$tipe)->count();
            $h_kel = Kelurahan::where('KEL_ID',$kelurahan)->first();
        }
        if ($rw != "x")
        {
             $usulans = $usulans->wherehas("user",function($user) use($rw){
                $user->where('RW_ID',$rw);
            });
            $diproses =  USULAN::wherehas("user",function($user) use($rw){
                            $user->where('RW_ID',$rw);
                         })->where('USULAN_STATUS',1)->where('USULAN_TUJUAN',$tipe)->count();
            $diterima =  USULAN::wherehas("user",function($user) use($rw){
                            $user->where('RW_ID',$rw);
                         })->where('USULAN_STATUS',2)->where('USULAN_TUJUAN',$tipe)->count();
            $ditolak  =  USULAN::wherehas("user",function($user) use($rw){
                            $user->where('RW_ID',$rw);
                         })->where('USULAN_STATUS',0)->where('USULAN_TUJUAN',$tipe)->count();
            $h_rw = rw::where('RW_ID',$rw)->first();
        }
        if ($tipe != "x")
        {
            $usulans = $usulans->where('USULAN_TUJUAN',$tipe);
            if ($tipe == 1) $h_tipe = 'Renja SKPD'; 
            else $h_tipe =  'PIPPK';   
        }
        
        $usulans = $usulans->get();
       // dd($usulans);
        
        $no             = 1;
        $aksi           = '';
        $view           = array();
        $rt             = '';
        $rts            = '';
        $jum = 0;
        foreach ($usulans as $data) {

            $aksi    = '<div class="action visible pull-right">';

            if($data->USULAN_STATUS == 1) $status = '<i class="fa fa-refresh text-info"></i>';
            elseif($data->USULAN_STATUS == 2) $status = '<i class="fa fa-check text-success"></i>';
            else $status = '<i class="fa fa-close text-danger"></i>';

            if($data->USULAN_POSISI == 1) $posisi = ' Kelurahan';
            elseif($data->USULAN_POSISI == 2) $posisi = ' Kecamatan';
            elseif($data->USULAN_POSISI == 3) $posisi = ' Kota';
            else $posisi = ' APBD';

            if($data->kamus->KAMUS_SKPD != 0){
                $skpd = $data->kamus->skpd->SKPD_NAMA;
            }
            else $skpd = '-';

            $pengusul   = 'Kecamatan '.$data->rw->kelurahan->kecamatan->KEC_NAMA.'<br>'.
                          'Kelurahan '.$data->rw->kelurahan->KEL_NAMA.'<br>'.
                          'RW '.$data->rw->RW_NAMA;

            $pagu       = $data->USULAN_VOLUME * $data->kamus->KAMUS_HARGA;

            
            
            $aksi   .= '<a href="'.url('/musrenbang/'.$tahun.'/public/tracking/detail/'.$data->USULAN_ID).'" class="action-preveiw" target="_blank"><i class="mi-eye"></i></a>';
            $aksi   .= '</div>';

            array_push($view, array( 'USULAN_ID'        => $data->USULAN_ID,
                                     'NO'               => $no,
                                     'PENGUSUL'           => $pengusul,
                                     'ISUKAMUS'         => $data->kamus->isu->ISU_NAMA."<br><p class='text-orange'>".$data->kamus->KAMUS_NAMA."</p>",
                                     'STATUS'           => "<p>".$status.$posisi."</p>",
                                     'VOLUME'           => $data->USULAN_VOLUME." ".$data->kamus->KAMUS_SATUAN,
                                     'HARGA'            => "Rp. ".$data->kamus->KAMUS_HARGA,
                                     'SKPD'             => $skpd,
                                     'TOTAL'            => "Rp. ".number_format($pagu,0,'.',','),
                                     'KET'              => $data->USULAN_CATATANKEL,
                                     'AKSI'             => $aksi,
                                     ));
            $no++;
            $jum += $pagu;
        }
            $out = array("aaData"=>$view,"jumlah"=>number_format($jum,0,'.',','),"diproses"=>$diproses.' Di Proses',"diterima"=>$diterima.' Di Terima',"ditolak"=>$ditolak.' Di Tolak', "kec"=>$h_kec->KEC_NAMA, "kel"=>$h_kel->KEL_NAMA, "rw"=>$h_rw->RW_NAMA, "tipe"=>$h_tipe, "tahun"=>$tahun);
            return Response::JSON($out);

    }

    public function trackingUsulanDetail($tahun,$id){
        $usulan = Usulan::where('USULAN_ID',$id)->where('USULAN_TAHUN',$tahun)->first();
        //dd($usulan);

        if($usulan != null){
                $daftar_rt = RT::whereIn('RT_ID',$usulan->RT_ID)->orderBy('RT_NAMA')->get(); 
        }else
            $daftar_rt = "-";
        
        $data = array(
                    'tahun'      => $tahun,
                    'usulan'     => $usulan,
                    'daftar_rt'  => $daftar_rt);

        return View::make('musrenbang.public.detail-tracking',$data);
    }

    public function apiMusrenbang($tahun){
        $usulans = Usulan::where("USULAN_TAHUN",$tahun)->get();
        $view           = array();
        foreach ($usulans as $data) {

            if($data->USULAN_STATUS == 1) $status = '<i class="fa fa-refresh text-info"></i>';
            elseif($data->USULAN_STATUS == 2) $status = '<i class="fa fa-check text-success"></i>';
            else $status = '<i class="fa fa-close text-danger"></i>';


            if($data->USULAN_POSISI == 1) $posisi = ' Kelurahan';
            elseif($data->USULAN_POSISI == 2) $posisi = ' Kecamatan';
            elseif($data->USULAN_POSISI == 3) $posisi = ' Kota';
            else $posisi = ' APBD';

            if($data->kamus->KAMUS_SKPD != 0){
                $skpd = $data->kamus->skpd->SKPD_NAMA;
            }
            else $skpd = '-';

            $pengusul   = 'Kecamatan '.$data->rw->kelurahan->kecamatan->KEC_NAMA.'<br>'.
                          'Kelurahan '.$data->rw->kelurahan->KEL_NAMA.'<br>'.
                          'RW '.$data->rw->RW_NAMA;

            $pagu       = $data->USULAN_VOLUME * $data->kamus->KAMUS_HARGA;

            array_push($view, array( 'USULAN_ID'        => $data->USULAN_ID,
                                     'PENGUSUL'           => $pengusul,
                                     'ISUKAMUS'         => $data->kamus->isu->ISU_NAMA."<br><p class='text-orange'>".$data->kamus->KAMUS_NAMA."</p>",
                                     'STATUS'           => "<p>".$status.$posisi."</p>",
                                     'VOLUME'           => $data->USULAN_VOLUME." ".$data->kamus->KAMUS_SATUAN,
                                     'HARGA'            => "Rp. ".$data->kamus->KAMUS_HARGA,
                                     'SKPD'             => $skpd,
                                     'TOTAL'            => "Rp. ".number_format($pagu,0,'.',','),
                                     'KET'              => $data->USULAN_CATATANKEL,
                                     ));
        }
            $out = array("aaData"=>$view);
            return Response::JSON($out);
    }

    public function album($tahun){
        
        $berita             = BeritaAcara::All();
        $BeritaAcaraFoto    = BeritaAcaraFoto::All();
        $user               = User::where('id',892)->first();
        $kecamatan          = Kecamatan::where('KEC_ID','<=',30)->get();
       // dd($kecamatan);
        $data = array(
                    'berita'        =>$berita,
                    'beritaFoto'    =>$BeritaAcaraFoto,
                    'user'          =>$user,
                    'tahun'         =>$tahun,
                    'kecamatan'     =>$kecamatan
                    ); 

        return View::make('musrenbang.public.album',$data);
    }

    public function albumGetData($tahun,$id){
        
        $berita             = BeritaAcara::where('id',$id)->get();
        $BeritaAcaraFoto    = BeritaAcaraFoto::where('id',$id)->get();
        $user               = User::where('id',$id)->first();
       // dd($kecamatan);
       $view           = array();
        foreach ($BeritaAcaraFoto as $data) {

            array_push($view, array( 'ID_BERITA_FOTO' => $data->ID_BERITA_FOTO,
                                     'tgl_update'     => $data->tgl_update,
                                     'image'          => $data->image,
                                     'id'             => $data->id,
                                     ));
        }
            $out = array("aaData"=>$view);
            return Response::JSON($out);
    }

 
  

}

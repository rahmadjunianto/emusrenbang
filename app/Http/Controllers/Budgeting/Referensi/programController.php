<?php

namespace App\Http\Controllers\Budgeting\Referensi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Program;
use App\Model\Urusan;
use App\Model\SKPD;
use View;
use Carbon;
use Response;
use DB;
use Illuminate\Support\Facades\Input;
class programController extends Controller
{
	public function index($tahun,$status){
		$urusan 	= Urusan::where('URUSAN_TAHUN',$tahun)->get();
		$skpd 		= SKPD::where('SKPD_TAHUN',$tahun)->get();
    	return View('budgeting.referensi.program',['tahun'=>$tahun,'status'=>$status,'urusan'=>$urusan,'skpd'=>$skpd]);
    }

    public function getData($tahun){
    	$data 			= Program::where('PROGRAM_TAHUN',$tahun)
    							->groupBy('URUSAN_ID','SKPD_ID')
    							->orderBy('URUSAN_ID')
    							->select('URUSAN_ID','SKPD_ID',DB::raw('COUNT("PROGRAM_ID")'))
    							->get();
    	$no 			= 1;
    	$aksi 			= '';
    	$view 			= array();
    	foreach ($data as $data) {
    		// $aksi 		= '<div class="action visible pull-right"><a onclick="return ubah(\''.$data->PROGRAM_ID.'\')" class="action-edit"><i class="mi-edit"></i></a><a onclick="return hapus(\''.$data->PROGRAM_ID.'\')" class="action-delete"><i class="mi-trash"></i></a></div>';
    		array_push($view, array( 'id_urusan'		=>$data->URUSAN_ID,
                                     'id_skpd'  		=>$data->SKPD_ID,
                                     'no'				=>$no,
                                     'URUSAN'  			=>$data->urusan->URUSAN_NAMA,
                                     'SKPD'				=>$data->skpd->SKPD_NAMA,
                                     'TOTAL'			=>$data->count));
    		$no++;
    	}
		$out = array("aaData"=>$view);    	
    	return Response::JSON($out);
    }

	public function getDataDetail($tahun,$status,$urusan,$skpd){
    	$data 			= Program::where('PROGRAM_TAHUN',$tahun)
    							->where('URUSAN_ID',$urusan)
    							->where('SKPD_ID',$skpd)
    							->get();
    	$no 			= 1;
    	$aksi 			= '';
    	$view 			= array();
    	foreach ($data as $data) {
    		$aksi 		= '<div class="action visible pull-right"><a onclick="return ubah(\''.$data->PROGRAM_ID.'\')" class="action-edit"><i class="mi-edit"></i></a><a onclick="return hapus(\''.$data->PROGRAM_ID.'\')" class="action-delete"><i class="mi-trash"></i></a></div>';
    		array_push($view, array( 'PROGRAM_ID' 		=>$data->PROGRAM_ID,
    								 'PROGRAM_KODE'  	=>$data->urusan->URUSAN_KODE.".".$data->skpd->SKPD_KODE.".".$data->PROGRAM_KODE,
                                     'PROGRAM_NAMA'		=>$data->PROGRAM_NAMA,
                                     'AKSI'				=>$aksi));
    		$no++;
    	}
		$out = array("aaData"=>$view);    	
    	return Response::JSON($out);
    }

    public function getDetail($tahun,$status,$id){
    	$data 			= Program::where('PROGRAM_ID',$id)->get();
    	return $data;
    }

    public function submitAdd(){
    	$program 	= new program;
    	$urusan 	= Urusan::where('URUSAN_TAHUN',Input::get('tahun'))
    						->where('URUSAN_ID',Input::get('urusan'))
    						->value('URUSAN_KODE');
    	$skpd 		= SKPD::where('SKPD_TAHUN',Input::get('tahun'))
    						->where('SKPD_ID',Input::get('skpd'))
    						->value('SKPD_KODE');
    	$no 		= Program::where('PROGRAM_TAHUN',Input::get('tahun'))
    						->where('URUSAN_ID',Input::get('urusan')) 
    						->where('SKPD_ID',Input::get('skpd'))
    						->orderBy('PROGRAM_KODE','DESC')
    						->value('PROGRAM_KODE');
    	$kode 		= "";
    	if(empty($no)){
    		$kode 	= '01';
    	}else{
    		$no = $no+1;
    		if($no < 10){
    			$kode 	= '0'.$no;
    		}else{
    			$kode 	= $no;
    		}
    	}
    	// print_r($kode);exit();

    	$cekKode 	= Program::where('PROGRAM_NAMA',Input::get('nama_program'))
                            ->where('PROGRAM_TAHUN',Input::get('tahun'))
                            ->where('URUSAN_ID',Input::get('urusan'))
    						->where('SKPD_ID',Input::get('skpd'))
    						->value('PROGRAM_NAMA');
    	if(empty($cekKode)){
            $program->PROGRAM_TAHUN       = Input::get('tahun');
            $program->URUSAN_ID           = Input::get('urusan');
            $program->SKPD_ID             = Input::get('skpd');
            $program->PROGRAM_KODE        = $kode;
	    	$program->PROGRAM_NAMA        = Input::get('nama_program');
            $program->save();
	    	return '1';
    	}else{
	    	return '0';
    	}
    }

    public function submitEdit(){
    	$program   = new program;
        $cek        = Program::where('PROGRAM_ID',Input::get('id_program'))->get();
        $no         = Program::where('PROGRAM_TAHUN',Input::get('tahun'))
                            ->where('URUSAN_ID',Input::get('urusan')) 
                            ->where('SKPD_ID',Input::get('skpd'))
                            ->orderBy('PROGRAM_KODE','DESC')
                            ->value('PROGRAM_KODE');
        $kode       = "";
        if($cek[0]['URUSAN_ID'] == Input::get('urusan') && $cek[0]['SKPD_ID'] == Input::get('skpd')){
            $kode   = $no;
        }elseif(empty($no)){
            $kode   = '01';
        }else{
            $no = $no+1;
            if($no < 10){
                $kode   = '0'.$no;
            }else{
                $kode   = $no;
            }
        }

        $cekKode 	= Program::where('PROGRAM_NAMA',Input::get('nama_program'))
                            ->where('PROGRAM_TAHUN',Input::get('tahun'))
                            ->where('URUSAN_ID',Input::get('urusan'))
                            ->where('SKPD_ID',Input::get('skpd'))
    						->value('PROGRAM_NAMA');
    	if(empty($cekKode) || $cekKode != Input::get('nama_program')){    						
    	Program::where('PROGRAM_ID',Input::get('id_program'))
    			->update(['PROGRAM_TAHUN'		=>Input::get('tahun'),
    					  'PROGRAM_KODE'  		=>$kode,
    					  'URUSAN_ID' 	        =>Input::get('urusan'),
    					  'SKPD_ID' 		    =>Input::get('skpd'),
    					  'PROGRAM_NAMA'		=>Input::get('nama_program')]);
    		return '1';
    	}else{
	    	return '0';
    	}
    }

    public function delete(){
    	Program::where('PROGRAM_ID',Input::get('id_program'))->delete();
    	return 'Berhasil dihapus!';
    }
}

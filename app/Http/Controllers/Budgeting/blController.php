<?php

namespace App\Http\Controllers\Budgeting;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use View;
class blController extends Controller
{
	//SHOW
    public function index($tahun,$status){
        if($status == 'murni') return $this->showMurni($tahun,$status);
        else return $this->showPerubahan();
    }

    public function showMurni($tahun,$status){
        return View('budgeting.belanja-langsung.index',['tahun'=>$tahun,'status'=>$status]);
    }

    public function showPerubahan(){

    }

    public function showDetail($tahun,$status,$id){
        return View('budgeting.belanja-langsung.detail',['tahun'=>$tahun,'status'=>$status]);
    }

    public function showRincian($tahun,$status,$id){

    }

    //ADD
    public function add($tahun,$status){
        return View('budgeting.belanja-langsung.add',['tahun'=>$tahun,'status'=>$status]);

    }

    public function submitDetail(){

    }

    public function submitRincian(){

    }

    //EDIT
    public function edit(){

    }

    public function submitDetailEdit(){

    }

    public function submitRincianEdit(){

    }

    //DELETE
    public function deleteBL(){

    }

    public function deleteRincian(){

    }

    //VALIDASI
    public function validasi(){

    }
}

import requests
import time
import thread

def accessURL(url):
	global timeout
	while True:
		print host+url+" -> "+str(requests.get(host+url).elapsed.total_seconds())+" second"
		time.sleep(timeout)

host = "http://bappeda.bandung.go.id/"
path = ["/musrenbang/2017/public/usulan/getUsulan/x/x/x/x/x/x",
		"/musrenbang/2017/public/kelurahan/getrw/All",
		"/musrenbang/2017/public/kelurahan/getapproval",
		"/musrenbang/2017/public/kecamatan/getapproval",
		"/musrenbang/2017/public/skpd/getapproval",
		"/musrenbang/2017/public/lkk/data",
		"/musrenbang/2017/public/kamus/getKamus",
		"/musrenbang/2017/public/kecamatan/getKecamatan",
		"/musrenbang/2017/public/kecamatan/getKelurahan/all",
		"/musrenbang/2017/public/",
		"/musrenbang/2017/public/anomali/pippklkk/100000000/api",
		"/musrenbang/2017/public/anomali/renja/500000000/api",
		"/musrenbang/2017/public/anomali/renjajumlah/4/api"
		]

timeout = 180 #samakan dengan timeout cache dari web

print "Keep-alive caching is running"
print "Using threads..."

try:
	for p in path:
		thread.start_new_thread( accessURL, (p, ))
except:
	print "Error: unable to start thread"

while 1:
	pass